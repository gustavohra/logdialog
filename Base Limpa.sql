CREATE DATABASE  IF NOT EXISTS `logdialo_base` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `logdialo_base`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: logdialog.com.br    Database: logdialo_base
-- ------------------------------------------------------
-- Server version	5.5.55-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) CHARACTER SET utf8 NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8,
  `payload` text CHARACTER SET utf8 NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_admin_servico_padrao`
--

DROP TABLE IF EXISTS `tb_admin_servico_padrao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_admin_servico_padrao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL COMMENT 'nome do serviço.\n\nesta tabela armazena as definições dos serviços padrões para os profissionais, definido pelo admin master.\n\nos valores ativos aqui, são aplicados de forma automática para os novos profissionais',
  `valor_sugerido` decimal(10,2) NOT NULL DEFAULT '0.00',
  `criado_por_usuario_id` bigint(20) unsigned NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ativo\n1 = inativo',
  PRIMARY KEY (`id`),
  KEY `fk_tb_admin_servico_padrao_tb_usuario1_idx` (`criado_por_usuario_id`),
  CONSTRAINT `fk_tb_admin_servico_padrao_tb_usuario1` FOREIGN KEY (`criado_por_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_admin_servico_padrao`
--

LOCK TABLES `tb_admin_servico_padrao` WRITE;
/*!40000 ALTER TABLE `tb_admin_servico_padrao` DISABLE KEYS */;
INSERT INTO `tb_admin_servico_padrao` VALUES (1,'consulta individual',50.00,15,'2016-12-21 17:06:23',0);
/*!40000 ALTER TABLE `tb_admin_servico_padrao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_agenda`
--

DROP TABLE IF EXISTS `tb_agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_agenda` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `paciente_id` bigint(20) unsigned NOT NULL,
  `profissional_id` bigint(20) unsigned NOT NULL,
  `nome_compromisso` varchar(255) NOT NULL COMMENT 'esse nome aparece no resumo do calendário',
  `data_compromisso` timestamp NULL DEFAULT NULL COMMENT 'data e hora do compromisso, em formato UNIX',
  `confirmado_usuario_convidado` tinyint(1) DEFAULT NULL COMMENT '0 = o segundo não usuário confirmou o agendamento\n1 = o segundo usuário confirmou o agendamento',
  `data_confirmacao` timestamp NULL DEFAULT NULL COMMENT 'data de confirmação pelo segundo usuário',
  `data_solicitacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `cancelada_por_usuario_id` bigint(20) unsigned DEFAULT NULL,
  `data_encerramento` timestamp NULL DEFAULT NULL COMMENT 'data em que a consulta foi encerrada.\nnormalmente a mesma data de encerramento da chamada de vídeo.',
  `encerrada_por_usuario_id` bigint(20) unsigned DEFAULT NULL,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ativo\n1 = inativo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_tb_agenda_tb_paciente1_idx` (`paciente_id`),
  KEY `fk_tb_agenda_tb_profissional1_idx` (`profissional_id`),
  KEY `fk_tb_agenda_tb_usuario1_idx` (`cancelada_por_usuario_id`),
  KEY `fk_tb_agenda_tb_usuario2_idx` (`encerrada_por_usuario_id`),
  CONSTRAINT `fk_tb_agenda_tb_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `tb_paciente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_agenda_tb_profissional1` FOREIGN KEY (`profissional_id`) REFERENCES `tb_profissional` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_agenda_tb_usuario1` FOREIGN KEY (`cancelada_por_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_agenda_tb_usuario2` FOREIGN KEY (`encerrada_por_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_agenda`
--

LOCK TABLES `tb_agenda` WRITE;
/*!40000 ALTER TABLE `tb_agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_agenda` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_agenda_AFTER_INSERT` AFTER INSERT ON `tb_agenda`
 FOR EACH ROW BEGIN
	# replicando para o log
    INSERT INTO tb_log_agenda_alteracao
		( agenda_id, paciente_id, profissional_id, nome_compromisso, data_compromisso, 
		  confirmado_usuario_convidado, data_confirmacao, data_solicitacao, data_cancelamento, 
          data_encerramento, inativo, cancelada_por_usuario_id, encerrada_por_usuario_id )
	VALUES
		( NEW.id, NEW.paciente_id, NEW.profissional_id, NEW.nome_compromisso, NEW.data_compromisso,
          NEW.confirmado_usuario_convidado, NEW.data_confirmacao, NEW.data_solicitacao, NEW.data_cancelamento, 
          NEW.data_encerramento, NEW.inativo, NEW.cancelada_por_usuario_id, NEW.encerrada_por_usuario_id );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_agenda_AFTER_UPDATE` AFTER UPDATE ON `tb_agenda`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.paciente_id != OLD.paciente_id OR 
        NEW.profissional_id != OLD.profissional_id OR
        NEW.nome_compromisso != OLD.nome_compromisso OR 
        NEW.data_compromisso != OLD.data_compromisso OR
        NEW.confirmado_usuario_convidado != OLD.confirmado_usuario_convidado OR
        NEW.data_confirmacao != OLD.data_confirmacao OR
        NEW.confirmado_usuario_convidado != OLD.confirmado_usuario_convidado OR 
        NEW.data_confirmacao != OLD.data_confirmacao OR
        NEW.data_solicitacao != OLD.data_solicitacao OR
        NEW.data_cancelamento != OLD.data_cancelamento OR
        NEW.data_encerramento != OLD.data_encerramento OR
        NEW.inativo != OLD.inativo OR
        NEW.cancelada_por_usuario_id != OLD.cancelada_por_usuario_id OR 
        NEW.encerrada_por_usuario_id != OLD.encerrada_por_usuario_id)
	
    THEN
		
        INSERT INTO tb_log_agenda_alteracao
			( agenda_id, paciente_id, profissional_id, nome_compromisso, data_compromisso, 
              confirmado_usuario_convidado, data_confirmacao, data_solicitacao, data_cancelamento, 
              data_encerramento, inativo, cancelada_por_usuario_id, encerrada_por_usuario_id)
		VALUES
			( NEW.id, OLD.paciente_id, OLD.profissional_id, OLD.nome_compromisso, OLD.data_compromisso,
              OLD.confirmado_usuario_convidado, OLD.data_confirmacao, OLD.data_solicitacao, OLD.data_cancelamento, 
              OLD.data_encerramento, OLD.inativo, OLD.cancelada_por_usuario_id, OLD.encerrada_por_usuario_id);
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_agenda_AFTER_DELETE` AFTER DELETE ON `tb_agenda`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_agenda_alteracao
		SET original_deletado = 1
	WHERE agenda_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_anotacao`
--

DROP TABLE IF EXISTS `tb_anotacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_anotacao` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `relacionado_agenda_id` bigint(20) unsigned DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `conteudo` longtext,
  `data_anotacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_tb_anotacao_tb_usuario1_idx` (`usuario_id`),
  KEY `fk_tb_anotacao_tb_agenda1_idx` (`relacionado_agenda_id`),
  CONSTRAINT `fk_tb_anotacao_tb_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_anotacao`
--

LOCK TABLES `tb_anotacao` WRITE;
/*!40000 ALTER TABLE `tb_anotacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_anotacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_anotacao_AFTER_INSERT` AFTER INSERT ON `tb_anotacao`
 FOR EACH ROW BEGIN
	# replicando para o log
    INSERT INTO tb_log_anotacao
		( anotacao_id, relacionado_agenda_id, usuario_id, titulo, conteudo )
	VALUES
		( NEW.id, NEW.usuario_id, NEW.relacionado_agenda_id, NEW.titulo, NEW.conteudo );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_anotacao_AFTER_DELETE` AFTER DELETE ON `tb_anotacao`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_anotacao
		SET original_deletado = 1
	WHERE anotacao_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_contato`
--

DROP TABLE IF EXISTS `tb_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_contato` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `mensagem` text NOT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_contato`
--

LOCK TABLES `tb_contato` WRITE;
/*!40000 ALTER TABLE `tb_contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_especialidade`
--

DROP TABLE IF EXISTS `tb_especialidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_especialidade` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profissional_id` bigint(20) unsigned NOT NULL,
  `especialidade` varchar(255) NOT NULL,
  `documento` text NOT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_aprovado` timestamp NULL DEFAULT NULL,
  `aprovado_usuario_id` bigint(20) unsigned DEFAULT NULL COMMENT 'usuário que aprovou o documento',
  `data_reprovado` timestamp NULL DEFAULT NULL,
  `reprovado_usuario_id` bigint(20) unsigned DEFAULT NULL,
  `visibilidade` enum('PUBLICO','PRIVADO') DEFAULT 'PUBLICO',
  PRIMARY KEY (`id`),
  KEY `fk_tb_especialidade_tb_profissional1_idx` (`profissional_id`),
  KEY `fk_tb_especialidade_tb_usuario1_idx` (`aprovado_usuario_id`),
  KEY `fk_tb_especialidade_tb_usuario2_idx` (`reprovado_usuario_id`),
  CONSTRAINT `fk_tb_especialidade_tb_profissional1` FOREIGN KEY (`profissional_id`) REFERENCES `tb_profissional` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_especialidade_tb_usuario2` FOREIGN KEY (`reprovado_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_especialidade_tb_usuario1` FOREIGN KEY (`aprovado_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_especialidade`
--

LOCK TABLES `tb_especialidade` WRITE;
/*!40000 ALTER TABLE `tb_especialidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_especialidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_grupo_sistema`
--

DROP TABLE IF EXISTS `tb_grupo_sistema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_grupo_sistema` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `grupo` varchar(255) NOT NULL DEFAULT 'paciente' COMMENT '- admin\n- paciente\n- profissional\n- etc…',
  `inativo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_grupo_sistema`
--

LOCK TABLES `tb_grupo_sistema` WRITE;
/*!40000 ALTER TABLE `tb_grupo_sistema` DISABLE KEYS */;
INSERT INTO `tb_grupo_sistema` VALUES (1,'admin',0),(2,'profissional',0),(3,'paciente',0);
/*!40000 ALTER TABLE `tb_grupo_sistema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_historico_sentimento`
--

DROP TABLE IF EXISTS `tb_historico_sentimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_historico_sentimento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `paciente_id` bigint(20) unsigned NOT NULL,
  `valor_olhos` varchar(255) DEFAULT NULL COMMENT 'valor armazenado com base nas opções do modal, com os dados dos olhos',
  `valor_boca` varchar(255) DEFAULT NULL COMMENT 'valor armazenado com base nas opções do modal, com os dados da boca',
  `frase` text COMMENT 'frase descritiva, resultante da combinação dos olhos e boca, que são exibidos para o usuário',
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = registro válido\n1 = registro inválido',
  PRIMARY KEY (`id`),
  KEY `fk_tb_historico_sentimento_tb_paciente1_idx` (`paciente_id`),
  CONSTRAINT `fk_tb_historico_sentimento_tb_paciente1` FOREIGN KEY (`paciente_id`) REFERENCES `tb_paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_historico_sentimento`
--

LOCK TABLES `tb_historico_sentimento` WRITE;
/*!40000 ALTER TABLE `tb_historico_sentimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_historico_sentimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_inbox`
--

DROP TABLE IF EXISTS `tb_inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_inbox` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `de_usuario_id` bigint(20) unsigned NOT NULL,
  `para_usuario_id` bigint(20) unsigned NOT NULL,
  `mensagem` longtext NOT NULL,
  `data_mensagem` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_tb_inbox_tb_usuario1_idx` (`de_usuario_id`),
  KEY `fk_tb_inbox_tb_usuario2_idx` (`para_usuario_id`),
  CONSTRAINT `fk_tb_inbox_tb_usuario1` FOREIGN KEY (`de_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_inbox_tb_usuario2` FOREIGN KEY (`para_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_inbox`
--

LOCK TABLES `tb_inbox` WRITE;
/*!40000 ALTER TABLE `tb_inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_inbox` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_inbox_AFTER_INSERT` AFTER INSERT ON `tb_inbox`
 FOR EACH ROW BEGIN
	# replicando para o log
    INSERT INTO tb_log_inbox
		( inbox_id, de_usuario_id, para_usuario_id, mensagem )
	VALUES
		( NEW.id, NEW.de_usuario_id, NEW.para_usuario_id, NEW.mensagem );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_inbox_AFTER_DELETE` AFTER DELETE ON `tb_inbox`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_inbox
		SET original_deletado = 1
	WHERE inbox_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_log_agenda_alteracao`
--

DROP TABLE IF EXISTS `tb_log_agenda_alteracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_agenda_alteracao` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `agenda_id` bigint(20) unsigned NOT NULL,
  `paciente_id` bigint(20) unsigned DEFAULT NULL,
  `profissional_id` bigint(20) unsigned DEFAULT NULL,
  `nome_compromisso` varchar(255) DEFAULT NULL,
  `data_compromisso` timestamp NULL DEFAULT NULL,
  `confirmado_usuario_convidado` tinyint(1) DEFAULT NULL,
  `data_confirmacao` timestamp NULL DEFAULT NULL,
  `data_solicitacao` timestamp NULL DEFAULT NULL,
  `data_cancelamento` timestamp NULL DEFAULT NULL,
  `cancelada_por_usuario_id` bigint(20) DEFAULT NULL,
  `data_encerramento` timestamp NULL DEFAULT NULL,
  `encerrada_por_usuario_id` bigint(20) DEFAULT NULL,
  `inativo` tinyint(1) DEFAULT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data de registro do log',
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = registro na tabela original ainda existe\n1 = registro na tabela original foi removido',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id_UNIQUE` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_agenda_alteracao`
--

LOCK TABLES `tb_log_agenda_alteracao` WRITE;
/*!40000 ALTER TABLE `tb_log_agenda_alteracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_agenda_alteracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_anotacao`
--

DROP TABLE IF EXISTS `tb_log_anotacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_anotacao` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `anotacao_id` bigint(20) unsigned NOT NULL,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `relacionado_agenda_id` bigint(20) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `conteudo` longtext,
  `data_anotacao` timestamp NULL DEFAULT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = registro na tabela original ainda existe\n1 = registro na tabela original foi removido',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id_UNIQUE` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_anotacao`
--

LOCK TABLES `tb_log_anotacao` WRITE;
/*!40000 ALTER TABLE `tb_log_anotacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_anotacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_inbox`
--

DROP TABLE IF EXISTS `tb_log_inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_inbox` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `inbox_id` bigint(20) unsigned NOT NULL,
  `de_usuario_id` bigint(20) unsigned DEFAULT NULL,
  `para_usuario_id` bigint(20) unsigned DEFAULT NULL,
  `mensagem` longtext,
  `data_mensagem` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = registro na tabela original ainda existe\n1 = registro na tabela original foi removido',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id_UNIQUE` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_inbox`
--

LOCK TABLES `tb_log_inbox` WRITE;
/*!40000 ALTER TABLE `tb_log_inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_login`
--

DROP TABLE IF EXISTS `tb_log_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_login` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(255) NOT NULL COMMENT 'pode receber IPv6',
  `navegador` text COMMENT 'dados do navegador user_agent, para analise futura',
  `referer` text COMMENT 'informação de referer, para análise de origem de tráfego',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id_UNIQUE` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_login`
--

LOCK TABLES `tb_log_login` WRITE;
/*!40000 ALTER TABLE `tb_log_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_notificacao`
--

DROP TABLE IF EXISTS `tb_log_notificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_notificacao` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `notificacao_id` bigint(20) NOT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_registro` timestamp NULL DEFAULT NULL,
  `para_usuario_id` bigint(20) DEFAULT NULL,
  `relacionado_usuario_id` bigint(20) DEFAULT NULL,
  `relacionado_agenda_id` bigint(20) DEFAULT NULL,
  `tipo_notificacao_id` bigint(20) DEFAULT NULL,
  `texto` text,
  `data_visualizada` timestamp NULL DEFAULT NULL,
  `exibir` tinyint(1) DEFAULT NULL,
  `alerta` tinyint(1) DEFAULT NULL,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = original existe na tabela\n1 = original deletado da tabela',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_notificacao`
--

LOCK TABLES `tb_log_notificacao` WRITE;
/*!40000 ALTER TABLE `tb_log_notificacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_notificacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_paciente_alteracao`
--

DROP TABLE IF EXISTS `tb_log_paciente_alteracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_paciente_alteracao` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `paciente_id` bigint(20) unsigned NOT NULL,
  `usuario_id` bigint(20) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `sobrenome` varchar(255) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `endereco` text,
  `end_numero` varchar(100) DEFAULT NULL,
  `end_complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(45) DEFAULT NULL,
  `cidade` text,
  `estado` text,
  `cep` varchar(255) DEFAULT NULL,
  `avatar` text,
  `documento_autorizacao_menor` varchar(255) DEFAULT NULL,
  `data_aceito_autorizacao_menor` timestamp NULL DEFAULT NULL,
  `documento_menor_aceito_por_usuario_id` bigint(20) DEFAULT NULL,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = registro na tabela original ainda existe\n1 = registro na tabela original foi removido',
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data de registro do log',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id_UNIQUE` (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_paciente_alteracao`
--

LOCK TABLES `tb_log_paciente_alteracao` WRITE;
/*!40000 ALTER TABLE `tb_log_paciente_alteracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_paciente_alteracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_pagseguro`
--

DROP TABLE IF EXISTS `tb_log_pagseguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_pagseguro` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pagseguro_id` bigint(20) NOT NULL,
  `de_usuario_id` bigint(20) unsigned NOT NULL,
  `agenda_id` bigint(20) unsigned DEFAULT NULL,
  `data_registro` timestamp NULL DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `descricao` text,
  `api_checkout_code` varchar(255) DEFAULT NULL,
  `api_checkout_date` varchar(255) DEFAULT NULL,
  `api_error_code` varchar(255) DEFAULT NULL,
  `api_error_message` text,
  `api_code` varchar(255) DEFAULT NULL,
  `api_notification_code` varchar(255) DEFAULT NULL,
  `api_transaction_code` varchar(255) DEFAULT NULL,
  `api_transaction_date` varchar(255) DEFAULT NULL,
  `api_transaction_type` int(11) DEFAULT NULL,
  `api_transaction_status` int(11) DEFAULT NULL,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = original ainda existe\n1 = original removido',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_pagseguro`
--

LOCK TABLES `tb_log_pagseguro` WRITE;
/*!40000 ALTER TABLE `tb_log_pagseguro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_pagseguro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_profissional_alteracao`
--

DROP TABLE IF EXISTS `tb_log_profissional_alteracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_profissional_alteracao` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `profissional_id` bigint(20) NOT NULL,
  `usuario_id` bigint(20) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `sobrenome` varchar(255) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `numero_crp` varchar(255) DEFAULT NULL,
  `documento_crp` text,
  `documento_identidade` text,
  `documento_diploma` text,
  `avatar` text,
  `experiencia_profissional` longtext,
  `afiliacoes` longtext,
  `formacao_academica` longtext,
  `cpf` varchar(45) DEFAULT NULL,
  `rua` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `cep` varchar(45) DEFAULT NULL,
  `primeiro_contato_gratis` tinyint(1) DEFAULT NULL,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = original existe\n1 = original deletado',
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_profissional_alteracao`
--

LOCK TABLES `tb_log_profissional_alteracao` WRITE;
/*!40000 ALTER TABLE `tb_log_profissional_alteracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_profissional_alteracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_profissional_dia_atendimento`
--

DROP TABLE IF EXISTS `tb_log_profissional_dia_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_profissional_dia_atendimento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profissional_dia_atendimento_id` bigint(20) DEFAULT NULL,
  `profissional_id` bigint(20) DEFAULT NULL,
  `dia_semana` tinyint(1) DEFAULT NULL,
  `horario_inicio` time DEFAULT NULL,
  `horario_fim` time DEFAULT NULL,
  `sem_atendimento` tinyint(1) DEFAULT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_profissional_dia_atendimento`
--

LOCK TABLES `tb_log_profissional_dia_atendimento` WRITE;
/*!40000 ALTER TABLE `tb_log_profissional_dia_atendimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_profissional_dia_atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_profissional_horario_bloqueado`
--

DROP TABLE IF EXISTS `tb_log_profissional_horario_bloqueado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_profissional_horario_bloqueado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profissional_horario_bloqueado_id` bigint(20) DEFAULT NULL,
  `profissional_id` bigint(20) DEFAULT NULL,
  `dia_horario` timestamp NULL DEFAULT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_profissional_horario_bloqueado`
--

LOCK TABLES `tb_log_profissional_horario_bloqueado` WRITE;
/*!40000 ALTER TABLE `tb_log_profissional_horario_bloqueado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_profissional_horario_bloqueado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_profissional_servico_alteracao`
--

DROP TABLE IF EXISTS `tb_log_profissional_servico_alteracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_profissional_servico_alteracao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `servico_id` bigint(20) NOT NULL,
  `profissional_id` bigint(20) NOT NULL,
  `servico` varchar(250) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `data_inserido` timestamp NULL DEFAULT NULL,
  `data_modificacao` timestamp NULL DEFAULT NULL,
  `inativo` tinyint(1) DEFAULT NULL,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0',
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_profissional_servico_alteracao`
--

LOCK TABLES `tb_log_profissional_servico_alteracao` WRITE;
/*!40000 ALTER TABLE `tb_log_profissional_servico_alteracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_profissional_servico_alteracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_log_usuario_alteracao`
--

DROP TABLE IF EXISTS `tb_log_usuario_alteracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_log_usuario_alteracao` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `data_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data de registro do log, não do usuário',
  `username` text,
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `inativo` tinyint(1) DEFAULT NULL,
  `grupo_sistema_id` bigint(20) DEFAULT NULL,
  `original_deletado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = registro na tabela original ainda existe\n1 = registro na tabela original foi removido',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `log_id_UNIQUE` (`log_id`),
  KEY `idx` (`username`(100),`email`(100))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_log_usuario_alteracao`
--

LOCK TABLES `tb_log_usuario_alteracao` WRITE;
/*!40000 ALTER TABLE `tb_log_usuario_alteracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_log_usuario_alteracao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_newsletter_email`
--

DROP TABLE IF EXISTS `tb_newsletter_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_newsletter_email` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ativo\n1 = inativo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_newsletter_email`
--

LOCK TABLES `tb_newsletter_email` WRITE;
/*!40000 ALTER TABLE `tb_newsletter_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_newsletter_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_notificacao`
--

DROP TABLE IF EXISTS `tb_notificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_notificacao` (
  `id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data de registro da notificação no sistema',
  `para_usuario_id` bigint(20) unsigned NOT NULL COMMENT 'usuário destinatário da notificação',
  `tipo_notificacao_id` bigint(20) NOT NULL,
  `relacionado_usuario_id` bigint(20) unsigned DEFAULT NULL COMMENT 'id do outro usuário, quando a notificação envolver ou fazer referência a outra pessoa.',
  `relacionado_agenda_id` bigint(20) unsigned DEFAULT NULL COMMENT 'quando é relacionado com um agendamento expecífico',
  `texto` text NOT NULL COMMENT 'frase / conteúdo da mensagem',
  `data_visualizada` timestamp NULL DEFAULT NULL COMMENT 'data em que o usuário visualizou a notificação, pela primeira vez',
  `exibir` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = exibir para o usuário\n0 = não exibir para o usuário',
  `alerta` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = exibir na contagem do alerta, notificação quente\n0 = não é notificação quente',
  PRIMARY KEY (`id`),
  KEY `fk_tb_notificacao_tb_usuario1_idx` (`para_usuario_id`),
  KEY `fk_tb_notificacao_tb_usuario2_idx` (`relacionado_usuario_id`),
  KEY `fk_tb_notificacao_tb_agenda1_idx` (`relacionado_agenda_id`),
  KEY `fk_tb_notificacao_tb_tipo_notificacao1_idx` (`tipo_notificacao_id`),
  CONSTRAINT `fk_tb_notificacao_tb_agenda1` FOREIGN KEY (`relacionado_agenda_id`) REFERENCES `tb_agenda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_notificacao_tb_tipo_notificacao1` FOREIGN KEY (`tipo_notificacao_id`) REFERENCES `tb_tipo_notificacao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_notificacao_tb_usuario1` FOREIGN KEY (`para_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_notificacao_tb_usuario2` FOREIGN KEY (`relacionado_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_notificacao`
--

LOCK TABLES `tb_notificacao` WRITE;
/*!40000 ALTER TABLE `tb_notificacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_notificacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_notificacao_AFTER_INSERT` AFTER INSERT ON `tb_notificacao`
 FOR EACH ROW BEGIN
	# replicando para o log
    INSERT INTO tb_log_notificacao
		( notificacao_id, data_registro, para_usuario_id, relacionado_usuario_id, relacionado_agenda_id, tipo_notificacao_id, texto, data_visualizada, exibir, alerta )
	VALUES
		( NEW.id, NEW.data_registro, NEW.para_usuario_id, NEW.relacionado_usuario_id, NEW.relacionado_agenda_id, NEW.tipo_notificacao_id, NEW.texto, NEW.data_visualizada, NEW.exibir, NEW.alerta );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_notificacao_AFTER_UPDATE` AFTER UPDATE ON `tb_notificacao`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.data_registro != OLD.data_registro OR
		NEW.para_usuario_id != OLD.para_usuario_id OR
        NEW.relacionado_usuario_id != OLD.relacionado_usuario_id OR
        NEW.relacionado_agenda_id != OLD.relacionado_agenda_id OR
		NEW.tipo_notificacao_id != OLD.tipo_notificacao_id OR
		NEW.texto != OLD.texto OR
		NEW.data_visualizada != OLD.data_visualizada OR
		NEW.exibir != OLD.exibir OR
		NEW.alerta != OLD.alerta )
        
    THEN
		
        INSERT INTO tb_log_notificacao
			( notificacao_id, data_registro, para_usuario_id, relacionado_usuario_id, relacionado_agenda_id, tipo_notificacao_id, texto, data_visualizada, exibir, alerta )
		VALUES
			( NEW.id, OLD.data_registro, OLD.para_usuario_id, OLD.relacionado_usuario_id, OLD.relacionado_agenda_id,
              OLD.tipo_notificacao_id, OLD.texto, OLD.data_visualizada, OLD.exibir, OLD.alerta );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_notificacao_AFTER_DELETE` AFTER DELETE ON `tb_notificacao`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_notificacao
		SET original_deletado = 1
	WHERE notificacao_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_orientacao_pergunta`
--

DROP TABLE IF EXISTS `tb_orientacao_pergunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_orientacao_pergunta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `pergunta` text NOT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Ativo\n1 = inativo',
  `resposta_sim` bigint(20) unsigned NOT NULL COMMENT 'resposta caso o usuário dia sim',
  PRIMARY KEY (`id`),
  KEY `fk_tb_orientacao_pergunta_tb_orientacao_resposta1_idx` (`resposta_sim`),
  CONSTRAINT `fk_tb_orientacao_pergunta_tb_orientacao_resposta1` FOREIGN KEY (`resposta_sim`) REFERENCES `tb_orientacao_resposta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_orientacao_pergunta`
--

LOCK TABLES `tb_orientacao_pergunta` WRITE;
/*!40000 ALTER TABLE `tb_orientacao_pergunta` DISABLE KEYS */;
INSERT INTO `tb_orientacao_pergunta` VALUES (1,'Oscilação de humor','Seu humor oscila?','2017-01-14 15:33:47',0,1),(2,'Dores inexplicáveis','Você sente uma dor inexplicável?','2017-01-14 15:33:47',0,2),(3,'Dificuldades para se relacionar','Você está com dificuldades para se relacionar?','2017-01-14 15:33:47',0,3),(4,'Problemas para dormir ou alteração no apetite','Você tem problemas para dormir ou alteração no apetite?','2017-01-14 15:33:47',0,4),(5,'Esquecimento, falta de atenção ou sem concentração','Você anda muito esquecido, desatento ou sem concentração?','2017-01-14 15:33:47',0,5),(6,'Vontade de se prejudicar ou se ferir','Você quer se prejudicar ou se ferir?','2017-01-14 15:33:47',0,6);
/*!40000 ALTER TABLE `tb_orientacao_pergunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_orientacao_resposta`
--

DROP TABLE IF EXISTS `tb_orientacao_resposta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_orientacao_resposta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resposta` text NOT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ativo\n1 = inativo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_orientacao_resposta`
--

LOCK TABLES `tb_orientacao_resposta` WRITE;
/*!40000 ALTER TABLE `tb_orientacao_resposta` DISABLE KEYS */;
INSERT INTO `tb_orientacao_resposta` VALUES (1,'Se você se irrita ou se frustra com facilidade e seus níveis de tolerância estão muito baixos você provavelmente se beneficiaria em falar com um psicólogo.','2017-01-14 15:33:47',0),(2,'Sintomas físicos ou somáticos inexplicáveis, tais como dores no corpo, dores de cabeça frequentes ou sintomas gastrointestinais podem ser sinais de sofrimento que merecem cuidado psicológico.','2017-01-14 15:33:47',0),(3,'Se você sentir que está mais isolado ou com dificuldades de se relacionar com pessoas importantes (amigos, pais, parceiro) ou até mesmo com dificuldades para se  socializar, provavelmente é hora de procurar ajuda.','2017-01-14 15:33:47',0),(4,'Ambos são sintomas muitas vezes ignorados entretanto muitos conflitos ou sofrimentos psicológicos como ansiedade, tristeza, preocupações excessivas, uso abusivo de álcool e outras substâncias estão associados a distúrbios do sono ou alteração no apetite.','2017-01-14 15:33:47',0),(5,'A maioria dos problemas psicológicos também provocam dificuldades no funcionamento cognitivo, incluindo falta de atenção e concentração, esquecimento e dificuldade na tomada de decisões. Se estes sintomas se tornarem frequentes, eles merecem ser investigados.','2017-01-14 15:33:47',0),(6,'Este é um sinal de alarme que não deve ser ignorado. Você deve agir imediatamente, procurando ajuda. Se às vezes você sente o desejo de se ferir fisicamente, com certeza é motivo de preocupação e neste caso, o seu lugar para procurar ajuda não é neste serviço de atendimento psicológico online. Você deve procurar uma ajuda psiquiátrica o quanto antes.','2017-01-14 15:33:47',0);
/*!40000 ALTER TABLE `tb_orientacao_resposta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_paciente`
--

DROP TABLE IF EXISTS `tb_paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_paciente` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `nome` varchar(255) NOT NULL,
  `sobrenome` varchar(255) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` enum('feminino','masculino') DEFAULT NULL,
  `endereco` text,
  `end_numero` varchar(100) DEFAULT NULL,
  `end_complemento` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` text,
  `estado` text,
  `cep` varchar(255) DEFAULT NULL,
  `avatar` text,
  `documento_autorizacao_menor` varchar(255) DEFAULT NULL COMMENT 'caminho do documento de autorização para menor de idade',
  `data_aceito_autorizacao_menor` timestamp NULL DEFAULT NULL,
  `documento_menor_aceito_por_usuario_id` bigint(20) unsigned DEFAULT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_tb_paciente_tb_usuario_idx` (`usuario_id`),
  KEY `fk_tb_paciente_tb_usuario1_idx` (`documento_menor_aceito_por_usuario_id`),
  CONSTRAINT `fk_tb_paciente_tb_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_paciente_tb_usuario1` FOREIGN KEY (`documento_menor_aceito_por_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20000 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_paciente`
--

LOCK TABLES `tb_paciente` WRITE;
/*!40000 ALTER TABLE `tb_paciente` DISABLE KEYS */;
INSERT INTO `tb_paciente` VALUES (6,15,'Admin','LogDialog',NULL,'1990-01-01','masculino',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-04-14 02:54:41');
/*!40000 ALTER TABLE `tb_paciente` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_paciente_AFTER_INSERT` AFTER INSERT ON `tb_paciente`
 FOR EACH ROW BEGIN
	# replicando para o log
	INSERT INTO tb_log_paciente_alteracao
		( paciente_id, usuario_id, nome, sobrenome, cpf, nascimento, endereco, end_numero, end_complemento, bairro, cidade, estado, cep, avatar, sexo,
          documento_autorizacao_menor, data_aceito_autorizacao_menor, documento_menor_aceito_por_usuario_id )
	VALUES
		( NEW.id, NEW.usuario_id, NEW.nome, NEW.sobrenome, NEW.cpf, NEW.nascimento, NEW.endereco, NEW.end_numero, NEW.end_complemento, 
          NEW.bairro, NEW.cidade, NEW.estado, NEW.cep, NEW.avatar, NEW.sexo, NEW.documento_autorizacao_menor, NEW.data_aceito_autorizacao_menor,
          NEW.documento_menor_aceito_por_usuario_id );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_paciente_AFTER_UPDATE` AFTER UPDATE ON `tb_paciente`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.usuario_id != OLD.usuario_id OR 
        NEW.nome != OLD.nome OR
        NEW.sobrenome != OLD.sobrenome OR 
        NEW.cpf != OLD.cpf OR 
        NEW.nascimento != OLD.nascimento OR 
        NEW.endereco != OLD.endereco OR 
        NEW.end_numero != OLD.end_numero OR 
        NEW.end_complemento != OLD.end_complemento OR
        NEW.bairro != OLD.bairro OR 
        NEW.cidade != OLD.cidade OR 
        NEW.estado != OLD.estado OR 
        NEW.cep != OLD.cep OR 
        NEW.avatar != OLD.avatar OR
        NEW.sexo != OLD.sexo OR
        NEW.documento_autorizacao_menor != OLD.documento_autorizacao_menor OR
        NEW.data_aceito_autorizacao_menor != OLD.data_aceito_autorizacao_menor OR
        NEW.documento_menor_aceito_por_usuario_id != OLD.documento_menor_aceito_por_usuario_id )
	
    THEN
		
        INSERT INTO tb_log_paciente_alteracao
			( paciente_id, usuario_id, nome, sobrenome, cpf, nascimento, endereco, end_numero, end_complemento, bairro, cidade, estado, cep, avatar,
              sexo, documento_autorizacao_menor, data_aceito_autorizacao_menor, documento_menor_aceito_por_usuario_id )
		VALUES
			( NEW.id, OLD.usuario_id, OLD.nome, OLD.sobrenome, OLD.cpf, OLD.nascimento, OLD.endereco, OLD.end_numero, OLD.end_complemento,
              OLD.bairro, OLD.cidade, OLD.estado, OLD.cep, OLD.avatar, OLD.sexo,
              OLD.documento_autorizacao_menor, OLD.data_aceito_autorizacao_menor, OLD.documento_menor_aceito_por_usuario_id );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_paciente_AFTER_DELETE` AFTER DELETE ON `tb_paciente`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_paciente_alteracao
		SET original_deletado = 1
	WHERE paciente_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_pagina`
--

DROP TABLE IF EXISTS `tb_pagina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pagina` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `conteudo` longtext NOT NULL,
  `url` varchar(455) NOT NULL,
  `imagem` varchar(455) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `criado_por_usuario_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_vantagem_tb_usuario1_idx` (`criado_por_usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pagina`
--

LOCK TABLES `tb_pagina` WRITE;
/*!40000 ALTER TABLE `tb_pagina` DISABLE KEYS */;
INSERT INTO `tb_pagina` VALUES (2,'Sobre a Empresa','<p><span style=\"font-weight: 400;\">O Logdialog surgiu da tend&ecirc;ncia do uso da tecnologia para realizar Orienta&ccedil;&atilde;o psicol&oacute;gica Online, o que atualmente &eacute; regulamentado pelo Conselho Federal de Psicologia. Nesse cen&aacute;rio, o Logdialog oferece uma solu&ccedil;&atilde;o atrav&eacute;s da qual psic&oacute;logos e pacientes podem oferecer e receber servi&ccedil;os de sa&uacute;de mental de qualidade.</span></p>\n<h3>Sendo assim nossa miss&atilde;o &eacute;:</h3>\n<ul>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Voc&ecirc; melhor a cada dia.</span></li>\n</ul>\n<h3>Nossa vis&atilde;o &eacute;:</h3>\n<ul>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Transformar a sa&uacute;de mental de qualidade em um servi&ccedil;o dispon&iacute;vel para todos e, atrav&eacute;s do uso de tecnologia, gerar resultados significantes para o bem-estar de toda a popula&ccedil;&atilde;o.</span></li>\n</ul>\n<h3>Na Logdialog trabalhamos seguindo alguns valores importantes:</h3>\n<ul>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Trabalhar com rigor &eacute;tico e transpar&ecirc;ncia. </span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Assegurar a confidencialidade e privacidade de nossos clientes.</span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Disponibilizar atendimento profissional de alta qualidade. </span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Oferecer praticidade e comodidade para nossos clientes.</span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Resolver problemas com agilidade e efici&ecirc;ncia.</span></li>\n</ul>','sobre','','2017-04-14 00:32:20',9),(3,'Profissionais','<p>O Logdialog &eacute; uma empresa de software que fornece uma plataforma digital e pode ser acessada atrav&eacute;s de dispositivos m&oacute;veis, ou outros meios digitais, por psic&oacute;logos e pacientes.</p>\n<p>Se voc&ecirc; &eacute; psic&oacute;logo e est&aacute; inscrito no seu Conselho Regional de Psicologia - CRP, ser&aacute; simples realizar atendimento online (atrav&eacute;s do Logdialog). Basta fazer seu cadastro que (e) entraremos em contato.</p>\n<p>No Logdialog voc&ecirc;:</p>\n<ul>\n<li>Define o valor da sua consulta.</li>\n<li>Disponibiliza os hor&aacute;rio que deseja atender.</li>\n<li>Realiza atendimento por videoconfer&ecirc;ncia.</li>\n<li>Registra o atendimento em prontu&aacute;rio, e n&oacute;s o guardamos com seguran&ccedil;a para voc&ecirc;.</li>\n<li>Recebe o pagamento na sua conta sem se preocupar em fazer cobran&ccedil;a ou inadimpl&ecirc;ncia.</li>\n</ul>','termos-profissionais','','2017-04-14 00:32:40',9),(4,'Termos de Uso','<p><span style=\"font-weight: 400;\">Este Contrato define e cont&eacute;m todos os termos e condi&ccedil;&otilde;es entre voc&ecirc;, paciente ou psic&oacute;logo, doravante denominado &nbsp;\"USU&Aacute;RIO\" e www.Logdialog.com.br (Logdialog). </span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO declara que, ao acessar o Logdialog ou utilizar quaisquer dos servi&ccedil;os disponibilizados, concorda de forma total e sem ressalvas com as condi&ccedil;&otilde;es previstas neste contrato.</span></p>\n<p><span style=\"font-weight: 400;\">Logdialog reserva-se ao direito de alterar os Termos de Uso a qualquer momento, conforme julgar necess&aacute;rio. Tais altera&ccedil;&otilde;es e sua data de atualiza&ccedil;&atilde;o ser&atilde;o publicadas em www.logdialog.com.br para que o USU&Aacute;RIO possa ler e aceitar sem restri&ccedil;&atilde;o as condi&ccedil;&otilde;es do Termo de Uso atuais neste site.</span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog &eacute; uma empresa de software que fornece uma plataforma digital, que pode ser acessada atrav&eacute;s de dispositivos m&oacute;veis e outros meios digitais, por psic&oacute;logos e pacientes. </span></p>\n<p><span style=\"font-weight: 400;\">Os psic&oacute;logos devidamente inscritos no Conselho Regional de Psicologia - CRP podem se cadastrar para realizar atendimento online, e o USU&Aacute;RIO paciente pode escolher dentre os psic&oacute;logos cadastrados, atrav&eacute;s do curr&iacute;culo, habilidades e experi&ecirc;ncia dispon&iacute;veis e descritos no site, aquele que &nbsp;julgar mais apropriado para realizar seu atendimento. </span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO pode consultar o Logdialog para obter informa&ccedil;&otilde;es. No entanto, n&atilde;o &eacute; permitido copiar, distribuir, modificar, transmitir ou revisar o conte&uacute;do deste site sem permiss&atilde;o por escrito do Logdialog. Os Termos de Uso est&atilde;o dispon&iacute;veis e podem ser acessados atrav&eacute;s de v&aacute;rias plataformas, de dispositivos m&oacute;veis e do site www.logdialog.com.br. </span></p>\n<p><span style=\"font-weight: 400;\">O uso ou acesso do Logdialog n&atilde;o transfere para terceiros o t&iacute;tulo ou os direitos de propriedade intelectual. Todos os direitos, t&iacute;tulos e interesses em e para todos os aspectos deste site continuam a ser propriedade exclusiva da Logdialog.</span></p>\n<p></p>\n<h3>Ren&uacute;ncia de responsabilidade</h3>\n<p><span style=\"font-weight: 400;\">O Logdialog, n&atilde;o cadastra m&eacute;dicos, assim como n&atilde;o autoriza a realiza&ccedil;&atilde;o de diagn&oacute;stico m&eacute;dico, orienta&ccedil;&atilde;o m&eacute;dica, nem prescri&ccedil;&atilde;o de medicamentos ou qualquer outro ato m&eacute;dico. Todos os psic&oacute;logos cadastrados em nossa plataforma, concordam </span><span style=\"font-weight: 400;\">em utilizar apenas princ&iacute;pios, conhecimentos e t&eacute;cnicas reconhecidamente fundamentados na ci&ecirc;ncia psicol&oacute;gica, na &eacute;tica e na legisla&ccedil;&atilde;o profissional ao realizar Orienta&ccedil;&atilde;o Psicol&oacute;gica Online.</span></p>\n<p><span style=\"font-weight: 400;\">Logdialog n&atilde;o emprega os psic&oacute;logos cadastrados atrav&eacute;s deste site. Os psic&oacute;logos atuam no Logdialog como prestadores de servi&ccedil;o. Os Psic&oacute;logos candidatos a presta&ccedil;&atilde;o de servi&ccedil;o no do Logdialog s&atilde;o cuidadosamente avaliados por psic&oacute;logos registrados no Conselho Regional de Psicologia - CRP. </span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog n&atilde;o assume qualquer responsabilidade por qualquer ato, omiss&atilde;o ou transgress&atilde;o de qualquer psic&oacute;logo, uma vez que tais profissionais n&atilde;o s&atilde;o empregados, nem representantes do Logdialog. </span></p>\n<p><span style=\"font-weight: 400;\">Apesar do Logdialog ajudar o USU&Aacute;RIO a encontrar um psic&oacute;logo, ou, caso n&atilde;o haja empatia, encontrar um novo psic&oacute;logo, n&atilde;o garantimos que voc&ecirc; encontrar&aacute; o psic&oacute;logo adequado, ou que satisfa&ccedil;a suas necessidades.</span></p>\n<p><span style=\"font-weight: 400;\">Os agendamentos ser&atilde;o realizados tendo como refer&ecirc;ncia o fuso hor&aacute;rio correspondente ao local de registro do Psic&oacute;logo. Portanto, os hor&aacute;rios dispon&iacute;veis na agenda do psic&oacute;logo tem como refer&ecirc;ncia o fuso hor&aacute;rio correspondente ao local de registro do profissional. Sendo assim, o USU&Aacute;RIO cadastrado no Logdialog est&aacute; de acordo que qualquer atendimento ser&aacute; agendado de acordo com o local de registro profissional do psic&oacute;logo.</span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO est&aacute; ciente de todos os riscos e benef&iacute;cios relativos ao uso de dispositivos m&oacute;veis para atendimento psicol&oacute;gico online. </span></p>\n<p></p>\n<h3>Verifica&ccedil;&atilde;o de documentos</h3>\n<p><span style=\"font-weight: 400;\">As credenciais, como forma&ccedil;&atilde;o acad&ecirc;mica em psicologia e inscri&ccedil;&atilde;o no Conselho regional de Psicologia e especializa&ccedil;&otilde;es, precisam ser comprovadas e s&atilde;o checadas pelo Logdialog. O Logdialog verifica se o profissional est&aacute; inscrito e ativo no Conselho de Psicologia</span><a href=\"http://cadastro.cfp.org.br/cfp/\"> <span style=\"font-weight: 400;\">http://cadastro.cfp.org.br/cfp/</span></a><span style=\"font-weight: 400;\">, assim como todos os documentos enviados pelos psic&oacute;logos (diploma de forma&ccedil;&atilde;o acad&ecirc;mica, registro profissional, certificados de especializa&ccedil;&atilde;o, mestrado, doutorado e outras experi&ecirc;ncias de atua&ccedil;&atilde;o) antes de realizar o cadastramento e autorizar a utiliza&ccedil;&atilde;o do nosso servi&ccedil;o. </span></p>\n<p><span style=\"font-weight: 400;\">Assuntos relacionados ao conte&uacute;do da sess&atilde;o de orienta&ccedil;&atilde;o, abordagem te&oacute;rica e t&eacute;cnicas utilizadas pelo psic&oacute;logo devem ser tratados somente com o psic&oacute;logo. O Logdialog n&atilde;o tem livre acesso a todos os documentos, portanto n&atilde;o pode controlar os assuntos abordados durante a sess&atilde;o de atendimento.</span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog se empenha e toma todas as medidas razo&aacute;veis para seguran&ccedil;a de seus USU&Aacute;RIOS, mas ainda assim, recomendamos que voc&ecirc; tenha prud&ecirc;ncia para usar nosso servi&ccedil;o. </span></p>\n<p><span style=\"font-weight: 400;\">Nossos servi&ccedil;os n&atilde;o substituem </span><span style=\"font-weight: 400;\">uma sess&atilde;o presencial com um psic&oacute;logo, </span><span style=\"font-weight: 400;\">nem</span><span style=\"font-weight: 400;\"> com outros profissionais. Recomendamos ent&atilde;o, que voc&ecirc; se sinta confort&aacute;vel para buscar orienta&ccedil;&atilde;o presencial de outro psic&oacute;logo, assim como de </span><span style=\"font-weight: 400;\">outros profissionais antes</span><span style=\"font-weight: 400;\"> de tomar qualquer decis&atilde;o que seja importante, ou que possa comprometer sua sa&uacute;de e bem-estar.</span></p>\n<p><span style=\"font-weight: 400;\"></span></p>\n<h3></h3>\n<h3>ATEN&Ccedil;&Atilde;O!</h3>\n<p><span style=\"font-weight: 400;\">SE VOC&Ecirc; EST&Aacute; PENSANDO SOBRE SUIC&Iacute;DIO,</span></p>\n<p><span style=\"font-weight: 400;\">SE VOC&Ecirc; CONSIDERA TER ALGUMA ATITUDE QUE PODE TE PREJUDICAR, OU PREJUDICAR OUTRAS PESSOAS,</span></p>\n<p><span style=\"font-weight: 400;\">SE VOC&Ecirc; SE SENTE EM PERIGO, OU ACHA QUE PODE COLOCAR OUTRA PESSOA EM PERIGO,</span></p>\n<p><span style=\"font-weight: 400;\">SE VOC&Ecirc; EST&Aacute; EM ALGUMA SITUA&Ccedil;&Atilde;O DE EMERG&Ecirc;NCIA M&Eacute;DICA, </span></p>\n<p><span style=\"font-weight: 400;\">VOC&Ecirc; DEVE IMEDIATAMENTE LIGAR PARA UM DOS N&Uacute;MEROS DOS SERVI&Ccedil;OS DE EMERG&Ecirc;NCIA ABAIXO E AVISAR AS AUTORIDADES COMPETENTES. </span></p>\n<p><span style=\"font-weight: 400;\">190 - POL&Iacute;CIA MILITAR</span></p>\n<p><span style=\"font-weight: 400;\">192 - SERVI&Ccedil;O P&Uacute;BLICO DE REMO&Ccedil;&Atilde;O DE DOENTES (AMBUL&Acirc;NCIA)</span></p>\n<p><span style=\"font-weight: 400;\">193 - CORPO DE BOMBEIROS</span></p>\n<p><span style=\"font-weight: 400;\">Ainda que o Logdialog proteja sua Privacidade, os psic&oacute;logos cadastrados, que prestam servi&ccedil;os atrav&eacute;s do Logdialog, podem decidir pela quebra de sigilo, conforme regulamentado pelo C&oacute;digo de &Eacute;tica do Psic&oacute;logo. O Cliente reconhece que o fato de ser atendido por um profissional cadastrado no Logdialog n&atilde;o diminui nosso compromisso &eacute;tico e possibilidade de quebra de sigilo profissional atrav&eacute;s do fornecimento de informa&ccedil;&otilde;es necess&aacute;rias (localiza&ccedil;&atilde;o do USU&Aacute;RIO, endere&ccedil;o IP ou identifica&ccedil;&atilde;o pessoal) para as autoridades competentes.</span></p>\n<p></p>\n<h3>Pagamento</h3>\n<p><span style=\"font-weight: 400;\">O pagamento pelo uso do Logdialog &eacute; usado para remunerar o Logdialog por seu desenvolvimento de software, despesas gerais, servi&ccedil;os administrativos, custos corporativos, impostos e taxas, incluindo taxas de transa&ccedil;&atilde;o para uso de cart&atilde;o de cr&eacute;dito. Os psic&oacute;logos cadastrados que atendem atrav&eacute;s do Logdialog tamb&eacute;m pagam pelo uso da tecnologia Logdialog e por taxas administrativas. </span></p>\n<p><span style=\"font-weight: 400;\">Parte do seu pagamento &eacute; contabilizado separadamente e transferido ao psic&oacute;logo para pagamento dos servi&ccedil;os cl&iacute;nicos fornecidos a voc&ecirc;. Logdialog n&atilde;o participa, divide ou assume uma percentagem desta parte do pagamento. Independentemente deste pagamento, Logdialog n&atilde;o &eacute; considerado um prestador de servi&ccedil;o como &eacute; o psic&oacute;logo cadastrado. </span></p>\n<p><span style=\"font-weight: 400;\">Tanto o psic&oacute;logo como o paciente concorda em indenizar o Logdialog por quaisquer reivindica&ccedil;&otilde;es que um psic&oacute;logo ou paciente fizer contra o outro. O paciente e o psic&oacute;logo cadastrado concordam em indenizar e isentar a Logdialog, suas entidades relacionadas, afiliadas e seus diretores, executivos, gerentes, funcion&aacute;rios, doadores, agentes e licenciadores, de e contra todas as perdas, despesas, danos e custos resultantes de qualquer viola&ccedil;&atilde;o destes Termos de Uso. </span></p>\n<p></p>\n<h3>Uso Adequado do Site</h3>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO declara e garante possuir plena capacidade jur&iacute;dica para a celebra&ccedil;&atilde;o do presente contrato, ser civilmente capaz, conforme a legisla&ccedil;&atilde;o brasileira, sendo que, caso seja constatado qualquer dano cometido por agentes total ou relativamente incapazes, com ou sem permiss&atilde;o de seus pais, tutores ou representantes legais, estes ser&atilde;o solidariamente respons&aacute;veis por todos os atos praticados pelos menores.</span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO est&aacute; ciente que &eacute; obrigado, aceita e se compromete a fornecer informa&ccedil;&otilde;es necess&aacute;rias, verdadeiras e atualizadas para se cadastrar no Logdialog, realizar pagamento de servi&ccedil;os contratados, ou de produtos dispon&iacute;veis atrav&eacute;s do Logdialog.</span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO concorda em respeitar toda e qualquer legisla&ccedil;&atilde;o brasileira vigente e aplic&aacute;vel &agrave; utiliza&ccedil;&atilde;o do Logdialog e aos atos do USU&Aacute;RIO dentro dele, bem como qualquer norma ou lei aplic&aacute;vel no pa&iacute;s de onde se origina o acesso do USU&Aacute;RIO.</span></p>\n<p></p>\n<h3>Proibi&ccedil;&otilde;es para o Uso do Logdialog</h3>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido</span><span style=\"font-weight: 400;\"> fazer publicidade, vender produtos servi&ccedil;o, coletar dados ou utiliza&ccedil;&atilde;o do servi&ccedil;o para qualquer fim diferente do que &eacute; oferecido originalmente pelo Logdialog,</span><span style=\"font-weight: 400;\"> sem nossa permiss&atilde;o expressa.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido</span><span style=\"font-weight: 400;\"> o uso do Logdialog para qualquer finalidade que possa violar qualquer lei municipal, estadual, federal ou internacional</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido publicar qualquer material que seja difamat&oacute;rio, abusivo ou obsceno, incluindo, mas sem limita&ccedil;&atilde;o, aqueles que </span><span style=\"font-weight: 400;\">contenham conte&uacute;do discriminat&oacute;rio em raz&atilde;o de sexo, ra&ccedil;a, religi&atilde;o, condi&ccedil;&atilde;o social, idade, cren&ccedil;a.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido utilizar senhas de car&aacute;ter ofensivo ou ilegal, que induzam terceiros a erro, ou que j&aacute; estejam em uso por outro USU&Aacute;RIO.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido o uso de qualquer material que encoraje conduta que constitua ofensa criminal, d&ecirc; origem a responsabilidade civil ou de outra forma violar qualquer lei nacional ou internacional aplic&aacute;vel.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido</span><span style=\"font-weight: 400;\"> ao USU&Aacute;RIO fingir ser outra pessoa.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido realizar ou divulgar qualquer tipo de grava&ccedil;&atilde;o, de atendimento ou sess&atilde;o, independente da tecnologia utilizada, em que apare&ccedil;am imagens, sons, textos, ou outra linguagem criptografada ou n&atilde;o, sob pena de incorrer em responsabilidade civil por danos morais e at&eacute; mesmo crime.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido agredir, caluniar, injuriar ou difamar os psic&oacute;logos, antes, durante ou ap&oacute;s as sess&otilde;es.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido</span><span style=\"font-weight: 400;\"> infringir quaisquer direitos de propriedade industrial e autoral de terceiros na utiliza&ccedil;&atilde;o do Logdialog.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido constituir solicita&ccedil;&otilde;es de caridade, cartas em cadeia ou esquemas de pir&acirc;mide.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido o uso de \"frame\" para </span><span style=\"font-weight: 400;\">incluir qualquer p&aacute;gina ou fragmento de p&aacute;gina ou conte&uacute;do do Logdialog dentro de outras p&aacute;ginas ou janela do browser. </span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido utilizar o Logdialog para obter, atrav&eacute;s de qualquer procedimento il&iacute;cito, acesso n&atilde;o autorizado a outro computador, servidor, ou rede; interromper servi&ccedil;o, servidores, ou rede de computadores; burlar qualquer sistema de autentica&ccedil;&atilde;o ou de seguran&ccedil;a, disseminar material que </span><span style=\"font-weight: 400;\">contenha v&iacute;rus, worm, cavalo de Tr&oacute;ia, bomba de tempo ou qualquer outro programa ou componente prejudicial</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido obter acesso n&atilde;o autorizado a &aacute;reas restritas do site, outras contas, sistemas de computador ou redes conectadas ao site, por meio de minera&ccedil;&atilde;o por senha ou por qualquer outro meio.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; proibido invadir a privacidade ou </span><span style=\"font-weight: 400;\">vigiar secretamente; acessar informa&ccedil;&otilde;es confidenciais, informa&ccedil;&otilde;es financeiras, n&uacute;meros de cart&otilde;es de cr&eacute;dito, contas banc&aacute;ria ou que possam causar preju&iacute;zos a qualquer pessoa.</span></p>\n<p></p>\n<h3>Funcionamento e falhas no funcionamento</h3>\n<p><span style=\"font-weight: 400;\">A conex&atilde;o do USU&Aacute;RIO com a internet n&atilde;o &eacute; responsabilidade do Logdialog. O acesso ao Logdialog depende de equipamentos tecnol&oacute;gicos, navegador e acesso &agrave; Internet, que &eacute; responsabilidade do USU&Aacute;RIO.</span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog n&atilde;o pode garantir que n&atilde;o ocorram falhas ou interrup&ccedil;&otilde;es, uma vez que o funcionamento dependente de diversos fatores e de terceiros.</span></p>\n<p><span style=\"font-weight: 400;\">O uso de plataformas de tecnologia desatualizadas n&atilde;o &eacute; seguro, portanto o Logdialog recomenda o uso de plataformas de acesso e tecnologia atualizadas para evitar danos de qualquer natureza.</span></p>\n<p><span style=\"font-weight: 400;\">O uso inadequado do site exclui o Logdialog de poss&iacute;veis perdas e danos.</span></p>\n<p><span style=\"font-weight: 400;\">Logdialog poder&aacute;, por motivos t&eacute;cnicos, ou para melhorar a experi&ecirc;ncia do USU&Aacute;RIO, atualizar seu sistema, mesmo que poss&iacute;veis altera&ccedil;&otilde;es ocorram.</span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog n&atilde;o tem controle sobre servi&ccedil;os e sites de terceiros, portanto n&atilde;o garante aus&ecirc;ncia de qualquer elemento nocivo nos documentos eletr&ocirc;nicos e arquivos que venham a ser instalados em seu sistema inform&aacute;tico a partir de sites de terceiros. </span></p>\n<p></p>\n<h3>Cookies</h3>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO autoriza o uso de &ldquo;cookies&rdquo; (</span><span style=\"font-weight: 400;\">pacote de dados enviados de um website para o navegador, para notificar atividades pr&eacute;vias e lembrar informa&ccedil;&otilde;es da atividade do USU&Aacute;RIO)</span><span style=\"font-weight: 400;\">, ou arquivos de natureza an&aacute;loga, para acessar informa&ccedil;&otilde;es sobre a conta do USU&Aacute;RIO a fim de oferecer um servi&ccedil;o melhor e mais personalizado. O uso de &ldquo;cookies&rdquo; n&atilde;o &eacute; obrigat&oacute;rio e n&atilde;o prejudica o servi&ccedil;o oferecido caso seja desabilitado pelo USU&Aacute;RIO . No entanto, uma vez que os cookies s&atilde;o desligados, o USU&Aacute;RIO dever&aacute; fazer o acesso com login e senha cada vez que acessar &aacute;reas do Logdialog em que esses dados s&atilde;o requeridos.</span></p>\n<p></p>\n<h3>Responsabilidade do USU&Aacute;RIO por Posts.</h3>\n<p><span style=\"font-weight: 400;\">O Logdialog n&atilde;o tem a obriga&ccedil;&atilde;o de responder mensagens abertas postadas no site, em redes sociais abertas ou qualquer outra m&iacute;dia agora conhecida ou desenvolvida no futuro. Se o USU&Aacute;RIO enviar mensagem para Logdialog, postar ou publicar qualquer informa&ccedil;&atilde;o (texto, v&iacute;deo, fotografia, &aacute;udio) nas redes sociais vinculadas ao Logdialog, ele &eacute; respons&aacute;vel por tais informa&ccedil;&otilde;es, materiais e pelas as conseq&uuml;&ecirc;ncias de sua postagem. </span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO concorda que qualquer publica&ccedil;&atilde;o que optar por fazer &eacute; de livre e espont&acirc;nea vontade e est&aacute; em conformidade com a lei. Logdialog reserva-se ao direito de editar ou excluir qualquer material postado no Logdialog. O Logdialog n&atilde;o endossa quaisquer opini&otilde;es expressas por terceiros. O USU&Aacute;RIO reconhece, se responsabiliza e assume o risco pelo uso que qualquer conte&uacute;do postado por outras pessoas no Logdialog.</span></p>\n<p></p>\n<h3>Modifica&ccedil;&otilde;es, Interrup&ccedil;&otilde;es e Suspens&otilde;es e Descontinua&ccedil;&atilde;o &nbsp;do Logdialog</h3>\n<p><span style=\"font-weight: 400;\">O Logdialog fica dispon&iacute;vel 24 (vinte e quatro) horas por dia, 7 (sete) dias por semana. </span><span style=\"font-weight: 400;\">No entanto, a tecnologia do Logdialog &eacute; tecnicamente complicada e depende de muitos fatores como engenharia, hospedagem, software, hardware, servi&ccedil;os de telecomunica&ccedil;&otilde;es, fornecedores de servi&ccedil;os, diversas ferramentas tecnol&oacute;gicas e necessidade de manuten&ccedil;&atilde;o. </span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO do Logdialog est&aacute; ciente e aceita que Logdialog pode modificar, suspender, interromper ou descontinuar Logdialog ou qualquer parte do site Logdialog, seja para todos os USU&Aacute;RIOs ou para algum USU&Aacute;RIO especificamente, a qualquer momento caso seja necess&aacute;rio.</span></p>\n<p><span style=\"font-weight: 400;\">Diante dos motivos citados, o Logdialog n&atilde;o garante que seu funcionamento </span><span style=\"font-weight: 400;\">&eacute; ininterrupto, que ser&aacute; 100% seguro, consistente ou sem erros</span><span style=\"font-weight: 400;\">, uma vez que est&aacute; sujeito a falhas em servi&ccedil;os e outras dificuldades t&eacute;cnicas que podem a qualquer momento comprometer ou interromper o funcionamento do servi&ccedil;o do Logdialog.</span></p>\n<p></p>\n<h3>Vig&ecirc;ncia e rescis&atilde;o de contrato</h3>\n<p><span style=\"font-weight: 400;\">O contrato de uso do Logdialog tem prazo de validade indeterminado, a contar da data de cadastramento do USU&Aacute;RIO. </span></p>\n<p><span style=\"font-weight: 400;\">Em caso de uso inadequado ou descumprimento do Termo de Uso, o USU&Aacute;RIO poder&aacute; ter seu acesso cancelado imediatamente e sem aviso pr&eacute;vio. </span></p>\n<p><span style=\"font-weight: 400;\">O uso continuado do Logdialog pelo USU&Aacute;RIO significar&aacute; sua aceita&ccedil;&atilde;o das mudan&ccedil;as feitas no website ou nos Termos de Uso. Se o Cliente n&atilde;o aceitar as altera&ccedil;&otilde;es, o &uacute;nico e exclusivo recurso do Cliente &eacute; descontinuar o uso do Logdialog.</span></p>\n<p><span style=\"font-weight: 400;\">O cancelamento da conta no Logdialog s&oacute; ser&aacute; realizado e efetivado ap&oacute;s o descadastramento, que deve ser realizado pelo USU&Aacute;RIO no Logdialog. </span></p>\n<p></p>\n<h3>Propriedade intelectual do Logdialog</h3>\n<p><span style=\"font-weight: 400;\">Todos os conte&uacute;dos e materiais como textos, desenhos, gr&aacute;ficos, </span><span style=\"font-weight: 400;\">&iacute;cones, algoritmos, </span><span style=\"font-weight: 400;\">nomes de dom&iacute;nio, c&oacute;digo, imagens, mensagens, logotipos e o termo \"Logdialog\" que est&atilde;o dispon&iacute;veis no Logdialog, s&atilde;o propriedade intelectual da Logdialog e est&atilde;o protegidos pelas leis de direitos autorais e propriedade industrial aplic&aacute;veis. Todos os direitos reservados.</span></p>\n<p><span style=\"font-weight: 400;\">Os conte&uacute;dos das telas do Logdialog, assim como os programas, bancos e bases de dados, redes, arquivos, e quaisquer outras cria&ccedil;&otilde;es autorais e intelectuais dos prepostos do Logdialog, s&atilde;o de propriedade exclusiva do Logdialog e est&atilde;o protegidos pelas leis e tratados internacionais, sendo vedada sua c&oacute;pia, reprodu&ccedil;&atilde;o, ou qualquer outro tipo de utiliza&ccedil;&atilde;o, ficando os infratores sujeitos &agrave;s san&ccedil;&otilde;es civis e criminais correspondentes.</span></p>\n<p><span style=\"font-weight: 400;\">O uso da marca registrada ou de qualquer outro conte&uacute;do do Logdialog &eacute; rigorosamente proibido, exceto com autoriza&ccedil;&atilde;o expressa e por escrito do Logdialog, conforme disposto nestes Termos de Uso.</span></p>\n<p><span style=\"font-weight: 400;\">O uso ou acesso ao Logdialog n&atilde;o d&aacute; ao USU&Aacute;RIO o direito de utilizar marcas, nomes comerciais e logotipos dispon&iacute;veis ou publicados no no Logdialog, uma vez que o Logdialog &eacute; o propriet&aacute;rio dos itens citados.</span></p>\n<p><span style=\"font-weight: 400;\">Logdialog n&atilde;o oferece servi&ccedil;o, nem produto, assim como n&atilde;o faz uso de nome, log&oacute;tipo ou marca em territ&oacute;rio onde n&atilde;o tenha direito de faz&ecirc;-lo. </span></p>\n<p><span style=\"font-weight: 400;\">Qualquer uso inadequado, incluindo mas n&atilde;o se resumindo &agrave; reprodu&ccedil;&atilde;o, distribui&ccedil;&atilde;o, exibi&ccedil;&atilde;o ou transmiss&atilde;o de qualquer conte&uacute;do deste site &eacute; estritamente proibido.</span></p>\n<p></p>\n<h3>Requisitos de idade, senha, e-mail e acesso ao Logdialog</h3>\n<p><span style=\"font-weight: 400;\">Ao usar o Logdialog voc&ecirc; afirma que tem 18 anos de idade ou mais.</span><span style=\"font-weight: 400;\"> Logdialog n&atilde;o tem motivos para acreditar que o USU&Aacute;RIO n&atilde;o seja julgado um adulto competente de acordo com a lei. &nbsp;</span></p>\n<p><span style=\"font-weight: 400;\">No caso de USU&Aacute;RIOS menores de 18 anos, o respons&aacute;vel deve preencher o \"Termo de Autoriza&ccedil;&atilde;o para Atendimento de Menor de 18 anos\", dispon&iacute;vel no cadastro, para autorizar que seu dependente, menor, seja atendido pelo Logdialog.</span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO do Logdialog ou o respons&aacute;vel por menor de 18 anos USU&Aacute;RIO do Logdialog est&aacute; ciente e concorda com os termos e condi&ccedil;&otilde;es deste Contrato, assim como est&aacute; de acordo e aceita os termos da Pol&iacute;tica de Privacidade do Logdialog. O USU&Aacute;RIO ou o respons&aacute;vel por USU&Aacute;RIO menor de 18 anos, &eacute; respons&aacute;vel por manter a confidencialidade de sua Senha de USU&Aacute;RIO, e-mail de cadastro e quaisquer outras informa&ccedil;&otilde;es de seguran&ccedil;a relacionadas &agrave; conta de USU&Aacute;RIO. Logdialog n&atilde;o ser&aacute; respons&aacute;vel por qualquer perda que o USU&Aacute;RIO ou USU&Aacute;RIO menor de 18 anos tiver ou acarretar a terceiro, caso outra pessoa, mesmo sem seu consentimento, utilize sua conta de USU&Aacute;RIO ou senha.</span></p>\n<p><span style=\"font-weight: 400;\">As senhas de acesso ao Logdialog s&atilde;o pessoais, intransfer&iacute;veis e de responsabilidade do pr&oacute;prio USU&Aacute;RIO, sendo que a transfer&ecirc;ncia e utiliza&ccedil;&atilde;o de senha por terceiros &eacute; proibida.</span></p>\n<p><span style=\"font-weight: 400;\">O acesso a &aacute;reas restritas &eacute; expressamente proibido para pessoas n&atilde;o autorizadas. Para ter acesso a &aacute;reas restritas &eacute; necess&aacute;rio cadastro, login, senha e ser devidamente autorizado. O USU&Aacute;RIO est&aacute; ciente de que pode sofrer san&ccedil;&otilde;es civis e criminais decorrentes de sua conduta.</span></p>\n<p></p>\n<h3>Seguran&ccedil;a</h3>\n<p><span style=\"font-weight: 400;\">Para proteger suas informa&ccedil;&otilde;es pessoais, tomamos precau&ccedil;&otilde;es razo&aacute;veis e seguimos as melhores pr&aacute;ticas da ind&uacute;stria para nos certificar que elas n&atilde;o ser&atilde;o perdidas inadequadamente, usurpadas, acessadas, divulgadas, alteradas ou destru&iacute;das.</span></p>\n<p>Embora nenhum m&eacute;todo de transmiss&atilde;o pela Internet ou armazenamento eletr&ocirc;nico seja 100% seguro, n&oacute;s seguimos todos os requisitos e implementamos padr&otilde;es adicionais geralmente aceitos pela ind&uacute;stria.</p>\n<p>O USU&Aacute;RIO est&aacute; ciente que por quest&otilde;es de seguran&ccedil;a o Logdialog aconselha que n&atilde;o sejam utilizados como senha n&uacute;meros sequenciais, n&uacute;meros de documentos, datas de anivers&aacute;rio, nomes pr&oacute;prios, de familiares ou de pessoas pr&oacute;ximas. O Logdialog tamb&eacute;m recomenda que as senhas sejam de pelo menos seis d&iacute;gitos e contenham ao menos um n&uacute;mero, uma letra mai&uacute;scula, uma letra min&uacute;scula e um caractere especial (ex. ! @ * % _) e que sejam alteradas periodicamente. O Logdialog poder&aacute; enviar mensagens sugerindo que a senha seja alterada periodicamente.</p>\n<p></p>\n<h3>Confidencialidade das informa&ccedil;&otilde;es na Orienta&ccedil;&atilde;o Psicol&oacute;gica</h3>\n<p><span style=\"font-weight: 400;\">Logdialog tem um compromisso com a confidencialidade. As informa&ccedil;&otilde;es fornecidas e armazenadas s&atilde;o protegidas por um sistema avan&ccedil;ado de criptografia que codifica as informa&ccedil;&otilde;es de forma que s&oacute; quem envia e quem recebe as informa&ccedil;&otilde;es consigam decifr&aacute;-las.</span></p>\n<p><span style=\"font-weight: 400;\">As &uacute;nicas informa&ccedil;&otilde;es pessoais que ser&atilde;o enviadas pelo Logdialog ao psic&oacute;logo para identifica&ccedil;&atilde;o correta do paciente s&atilde;o, o nome completo do paciente e a data de nascimento. No caso de orienta&ccedil;&atilde;o para menores, tamb&eacute;m ser&aacute; disponibilizado para o psic&oacute;logo o nome completo do respons&aacute;vel e o documento de autoriza&ccedil;&atilde;o j&aacute; preenchido pelo respons&aacute;vel.</span></p>\n<p>Informa&ccedil;&otilde;es complementares que o psic&oacute;logo julgar necess&aacute;rias poder&atilde;o ser solicitadas pelo pr&oacute;prio psic&oacute;logo durante a sess&atilde;o, onde as informa&ccedil;&otilde;es s&atilde;o criptografadas e o Logdialog n&atilde;o tem acesso.</p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO fica ciente e concorda que qualquer informa&ccedil;&atilde;o, dado, texto, v&iacute;deo, mensagem ou qualquer outro material transmitido atrav&eacute;s do atendimento com o psic&oacute;logo ser&aacute; de total responsabilidade do USU&Aacute;RIO, pois n&atilde;o h&aacute; possibilidade do Logdialog controlar ou ter acesso sobre essas informa&ccedil;&otilde;es. </span></p>\n<p><span style=\"font-weight: 400;\">Todas as informa&ccedil;&otilde;es sobre o paciente que o psic&oacute;logo armazenar em prontu&aacute;rio tamb&eacute;m est&atilde;o protegidas por criptografia.</span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; importante ressaltar que os psic&oacute;logos geralmente s&atilde;o obrigados a notificar as suspeitas de crimes, incluindo suspeita de maus-tratos e viol&ecirc;ncia sexual contra crian&ccedil;as ou adultos, absoluta ou relativamente incapazes. </span></p>\n<p><span style=\"font-weight: 400;\">Al&eacute;m disso, o psic&oacute;logo poder&aacute; quebrar sigilo quando o paciente estiver em condi&ccedil;&otilde;es mentais ou emocionais que representem perigo para si pr&oacute;prio ou aos outros. </span></p>\n<p><span style=\"font-weight: 400;\">De forma geral, o psic&oacute;logo dever&aacute; respeitar a legisla&ccedil;&atilde;o atual e seguir as orienta&ccedil;&otilde;es contidas no C&oacute;digo de &Eacute;tica do Conselho Federal de Psicologia vigente no Brasil.</span></p>\n<p></p>\n<p></p>\n<h3>Termos legais gerais</h3>\n<p><span style=\"font-weight: 400;\">O presente Contrato ser&aacute; vinculativo para ambas as partes. </span></p>\n<p><span style=\"font-weight: 400;\">Nenhuma emenda a este Contrato ser&aacute; efetiva a menos que seja feita por escrito. Os cabe&ccedil;alhos dos par&aacute;grafos s&atilde;o exclusivamente por raz&otilde;es de conveni&ecirc;ncia e n&atilde;o ser&atilde;o aplicados na interpreta&ccedil;&atilde;o deste documento.</span></p>\n<p><span style=\"font-weight: 400;\">Se qualquer disposi&ccedil;&atilde;o deste Contrato for considerada por um tribunal de jurisdi&ccedil;&atilde;o competente como ilegal, inv&aacute;lida, inexeq&uuml;&iacute;vel ou de outra forma contr&aacute;ria &agrave; lei, as restantes disposi&ccedil;&otilde;es deste Contrato permanecer&atilde;o em pleno vigor e efeito.</span></p>\n<p><span style=\"font-weight: 400;\">A toler&acirc;ncia de uma parte para com a outra, relativamente a descumprimento de qualquer das obriga&ccedil;&otilde;es ora assumidas, n&atilde;o ser&aacute; considerada nova&ccedil;&atilde;o ou ren&uacute;ncia a qualquer direito, constituindo mera liberalidade, que n&atilde;o impedir&aacute; a parte tolerante de exigir da outra o fiel cumprimento deste contrato, a qualquer tempo.</span></p>\n<p><span style=\"font-weight: 400;\">As atividades desenvolvidas pelo Logdialog, atrav&eacute;s do PORTAL, n&atilde;o s&atilde;o consideradas de risco, sendo inaplic&aacute;vel a responsabilidade objetiva disposta no artigo 927, par&aacute;grafo &uacute;nico, do C&oacute;digo Civil, o que o USU&Aacute;RIO declara concordar, neste ato.</span></p>\n<p><span style=\"font-weight: 400;\">O USU&Aacute;RIO concorda com a obriga&ccedil;&atilde;o de indenizar, em a&ccedil;&atilde;o regressiva, qualquer preju&iacute;zo causado ao Logdialog em decorr&ecirc;ncia de a&ccedil;&otilde;es que envolvam seus atos inclusive por meio da denuncia&ccedil;&atilde;o da lide, prevista no artigo 125, II do C&oacute;digo de Processo Civil. </span></p>\n<p><span style=\"font-weight: 400;\">Este Acordo ser&aacute; interpretado apenas de acordo com as leis brasileiras.</span></p>\n<p><span style=\"font-weight: 400;\">Fica eleito o Foro da Comarca de S&atilde;o Paulo, com ren&uacute;ncia expressa de qualquer outro, ainda que mais ben&eacute;fico a qualquer das partes, para decidir controv&eacute;rsia decorrente do presente contrato, que ser&aacute; regido pelas leis vigentes na Rep&uacute;blica Federativa do Brasil.</span></p>','termos-de-uso','','2017-04-14 00:33:00',9),(5,'Política de Pagamento e Reembolso','<h3>01.CANCELAMENTO E DEVOLU&Ccedil;&Atilde;O DO VALOR PAGO</h3>\n<p>O pagamento da orienta&ccedil;&atilde;o psicol&oacute;gica &eacute; feito antecipadamente ao in&iacute;cio da sess&atilde;o, onde cada cr&eacute;dito equivale a uma sess&atilde;o. Caso o USU&Aacute;RIO desista, ele poder&aacute; solicitar a devolu&ccedil;&atilde;o dos valores at&eacute; 24 horas antes do atendimento, caso contr&aacute;rio, o valor n&atilde;o ser&aacute; devolvido. Ser&aacute; descontado do valor a ser devolvido, todas as taxas que foram cobradas na transa&ccedil;&atilde;o financeira do PayPal. Caso queira solicitar a devolu&ccedil;&atilde;o dos valores pagos, por favor, envie um e-mail com a solicita&ccedil;&atilde;o para <a href=\"mailto:logdialog@logdialog.com.br\">logdialog@logdialog.com.br.</a></p>\n<p class=\"notes\">OBSERVA&Ccedil;&Atilde;O: No caso de alguma ocorr&ecirc;ncia de problemas t&eacute;cnicos do provedor Logdialog.com.br que prejudiquem a qualidade ou at&eacute;a mesmo a impossibilidade da realiza&ccedil;&atilde;o do atendimento, a sess&atilde;o ser&aacute; reagendada sem nenhum &ocirc;nus para o USU&Aacute;RIO.</p>\n<h3>02.N&Atilde;O COMPARECIMENTO DO CLIENTE</h3>\n<p>Caso o USU&Aacute;RIO fa&ccedil;a o agendamento com o psic&oacute;logo e n&atilde;o compare&ccedil;a no hor&aacute;rio marcado, o cr&eacute;dito ser&aacute; debitado da conta do USU&Aacute;RIO e o valor da sess&atilde;o ser&aacute; repassado ao psic&oacute;logo que disponibilizou a hora para o agendamento e ficou aguardando os 50 minutos no sistema. Lembramos que o USU&Aacute;RIO poder&aacute; cancelar o agendamento sem nenhum &ocirc;nus 24 horas antes do atendimento.</p>\n<h3>03.N&Atilde;O COMPARECIMENTO DO PSIC&Oacute;LOGO</h3>\n<p>No caso do n&atilde;o comparecimento do Psic&oacute;logo na sess&atilde;o, o cr&eacute;dito ser&aacute; devolvido e poder&aacute; ser usado novamente para o agendamento de uma nova sess&atilde;o. Neste caso pedimos que nos envie um e-mail e indique o ocorrido para entrarmos em contato com o psic&oacute;logo e verificar o motivo do n&atilde;o comparecimento.</p>\n<h3>04.MUDAN&Ccedil;A NO HOR&Aacute;RIO DA SESS&Atilde;O</h3>\n<p>O USU&Aacute;RIO poder&aacute; mudar o hor&aacute;rio da sess&atilde;o sem nenhum custo at&eacute; 24 horas antes do in&iacute;cio da sess&atilde;o marcada. Caso precise remarcar, &eacute; necess&aacute;rio cancelar o hor&aacute;rio at&eacute; 24 horas antes do in&iacute;cio da sess&atilde;o e em logo em seguida remarcar a sess&atilde;o no novo hor&aacute;rio escolhido.</p>\n<h3>05.AGENDAMENTO DA SESS&Atilde;O</h3>\n<p>Imediatamente, ap&oacute;s o USU&Aacute;RIO solicitar uma sess&atilde;o com o profissional escolhido, o psic&oacute;logo ir&aacute; receber uma mensagem atrav&eacute;s do sistema onde dever&aacute; confirmar ou n&atilde;o este atendimento.</p>\n<h3>2. OS PSIC&Oacute;LOGOS CADASTRADOS</h3>\n<p>O site Logdialog.com.br efetiva apenas o cadastro dos psic&oacute;logos devidamente registrados e autorizados pelo Conselho Federal de Psicologia a realizar atendimento psicol&oacute;gico e que estejam devidamente em dia com suas obriga&ccedil;&otilde;es &eacute;ticas e legais. Os dados fornecidos pelo psic&oacute;logo s&atilde;o verificados e a cada 30 dias ser&aacute; averiguado se houve alguma ocorr&ecirc;ncia, problema disciplinar ou at&eacute; mesmo a perda do registro junto ao Conselho Federal de Psicologia. N&atilde;o ser&atilde;o aceitos em hip&oacute;tese alguma profissionais que n&atilde;o possuem a gradua&ccedil;&atilde;o conclu&iacute;da no curso de Psicologia.</p>\n<h3>3. RESPONSABILIDADE DO LOGDIALOG SOBRE O ATENDIMENTO REALIZADO PELO PSIC&Oacute;LOGO</h3>\n<p><span class=\"contrato-topico\">3.1</span> O site Logdialog.com.br n&atilde;o se responsabiliza pelo conte&uacute;do da troca de informa&ccedil;&otilde;es realizada no atendimento entre o psic&oacute;logo e o USU&Aacute;RIO. Tanto o direcionamento como a abordagem que &eacute; utilizada na orienta&ccedil;&atilde;o psicol&oacute;gica &eacute; de total responsabilidade do psic&oacute;logo que deve, obrigatoriamente, seguir o C&oacute;digo de &Eacute;tica definido pelo Conselho Federal de Psicologia. Na ocorr&ecirc;ncia de m&aacute; conduta ou falta grave pelo psic&oacute;logo cadastrado no Logdialog.com.br junto ao USU&Aacute;RIO, solicitamos que seja nos comunicado imediatamente para que as provid&ecirc;ncias cab&iacute;veis sejam tomadas.</p>\n<p><span class=\"contrato-topico\">3.2</span> O site Logdialog.com.br n&atilde;o endossa ou recomenda qualquer profissional cadastrado no site, sendo que a escolha &eacute; feita de forma livre pelo USU&Aacute;RIO que ir&aacute; utilizar o servi&ccedil;o.</p>\n<p><span class=\"contrato-topico\">3.3</span> &Eacute; importante ressaltar que o Logdialog.com.br apenas disponibiliza e oferecer a utiliza&ccedil;&atilde;o do espa&ccedil;o e das ferramentas para todos os USU&Aacute;RIOs que buscam uma orienta&ccedil;&atilde;o psicol&oacute;gica online realizando a intermedia&ccedil;&atilde;o entre o profissional psic&oacute;logo e seu USU&Aacute;RIO.</p>\n<p><span class=\"contrato-topico\">3.4</span> Como as sess&otilde;es n&atilde;o s&atilde;o monitoradas, n&atilde;o temos a obriga&ccedil;&atilde;o e nem o direito de dirigir os trabalhos do psic&oacute;logo, cabendo ao USU&Aacute;RIO n&atilde;o continuar com a sess&atilde;o caso n&atilde;o fique satisfeito ou se julgue prejudicado, podendo optar por outro psic&oacute;logo nas pr&oacute;ximas sess&otilde;es.</p>\n<p><span class=\"contrato-topico\">3.5</span> No caso do USU&Aacute;RIO e o psic&oacute;logo optarem pelo atendimento presencial em consult&oacute;rio, o site Logdialog.com.br ficar&aacute; isento de qualquer responsabilidade pela negocia&ccedil;&atilde;o bem como todo o acerto do contrato deste atendimento.</p>\n<h3>4. VALOR DOS SERVI&Ccedil;OS</h3>\n<p><span class=\"contrato-topico\">4.1</span> CHAT OU VIDEOCONFER&Ecirc;NCIA: O valor minimo de atendimento no chat ou videoconfer&ecirc;ncia disponibilizado em nosso site &eacute; de R$ 90,00 e a dura&ccedil;&atilde;o &eacute; de 50 minutos.</p>\n<p><span class=\"contrato-topico\">4.2</span> O site Logdialog.com.br &eacute; respons&aacute;vel em efetuar o pagamento dos valores pagos pelo USU&Aacute;RIO ao psic&oacute;logo.</p>\n<p><span class=\"contrato-topico\">4.3</span> O cr&eacute;dito s&oacute; ser&aacute; liberado mediante confirma&ccedil;&atilde;o de pagamento pelo sistema do PayPal.</p>\n<h3>5. CANCELAMENTO E DEVOLU&Ccedil;&Atilde;O DO VALOR PAGO</h3>\n<p><span class=\"contrato-topico\">5.1</span> O pagamento da orienta&ccedil;&atilde;o psicol&oacute;gica &eacute; feito antecipadamente ao in&iacute;cio da sess&atilde;o, onde cada cr&eacute;dito equivale a uma sess&atilde;o. Caso o USU&Aacute;RIO desista, ele poder&aacute; solicitar a devolu&ccedil;&atilde;o dos valores at&eacute; 24 horas antes do atendimento, caso contr&aacute;rio, o valor n&atilde;o ser&aacute; devolvido. Ser&aacute; descontado do valor a ser devolvido, todas as taxas que foram cobradas na transa&ccedil;&atilde;o financeira do PayPal. Caso queira solicitar a devolu&ccedil;&atilde;o dos valores pagos, por favor, envie um e-mail com a solicita&ccedil;&atilde;o para logdialog@logdialog.com.br.</p>\n<p><span class=\"contrato-topico\">5.2</span> N&Atilde;O COMPARECIMENTO DO CLIENTE: Caso o USU&Aacute;RIO fa&ccedil;a o agendamento com o psic&oacute;logo e n&atilde;o compare&ccedil;a no hor&aacute;rio marcado, o cr&eacute;dito ser&aacute; debitado da conta do USU&Aacute;RIO e o valor da sess&atilde;o ser&aacute; repassado ao psic&oacute;logo que disponibilizou a hora para o agendamento e ficou aguardando os 50 minutos no sistema. Lembramos que o USU&Aacute;RIO poder&aacute; cancelar o agendamento sem nenhum &ocirc;nus 3 horas antes do atendimento.</p>\n<p><span class=\"contrato-topico\">5.3</span> N&Atilde;O COMPARECIMENTO DO PSIC&Oacute;LOGO: No caso do n&atilde;o comparecimento do Psic&oacute;logo na sess&atilde;o, o cr&eacute;dito ser&aacute; devolvido e poder&aacute; ser usado novamente para o agendamento de uma nova sess&atilde;o. Neste caso pedimos que nos envie um e-mail e indique o ocorrido para entrarmos em contato com o psic&oacute;logo e verificar o motivo do n&atilde;o comparecimento.</p>\n<h3>6.MUDAN&Ccedil;A NO HOR&Aacute;RIO DA SESS&Atilde;O</h3>\n<p>O USU&Aacute;RIO poder&aacute; mudar o hor&aacute;rio da sess&atilde;o sem nenhum custo at&eacute; 3 horas antes do in&iacute;cio da sess&atilde;o marcada. Caso precise remarcar, &eacute; necess&aacute;rio cancelar o hor&aacute;rio at&eacute; 3 horas antes do in&iacute;cio da sess&atilde;o e em logo em seguida remarcar a sess&atilde;o no novo hor&aacute;rio escolhido.</p>\n<h3>7. FORMA DE PAGAMENTO</h3>\n<p>As nossas opera&ccedil;&otilde;es financeiras s&atilde;o realizadas pela empresa PayPal. Para mais informa&ccedil;&otilde;es, por favor, clique no site do PayPal</p>\n<h3>8. AGENDAMENTO DA SESS&Atilde;O</h3>\n<p>Imediatamente, ap&oacute;s o USU&Aacute;RIO solicitar uma sess&atilde;o com o profissional escolhido, o psic&oacute;logo ir&aacute; receber uma mensagem atrav&eacute;s do sistema onde dever&aacute; confirmar ou n&atilde;o este atendimento. A sess&atilde;o ir&aacute; ocorrer independentemente do confirma&ccedil;&atilde;o ou n&atilde;o do psic&oacute;logo. Caso ocorra o n&atilde;o comparecimento do psic&oacute;logo o cr&eacute;dito ser&aacute; estornado ao USU&Aacute;RIO.</p>\n<h3>10. RESTRI&Ccedil;&Otilde;ES</h3>\n<p><span class=\"contrato-topico\">10.1</span> O Logdialog.com.br s&oacute; aceitar&aacute; o cadastramento de USU&Aacute;RIOs maiores de 18 anos, impreterivelmente.</p>\n<p><span class=\"contrato-topico\">10.2</span> Antes da compra do cr&eacute;dito para agendar a sess&atilde;o, o USU&Aacute;RIO dever&aacute; confirmar que est&aacute; ciente que o atendimento n&atilde;o poder&aacute; acontecer caso esteja em alguma das seguintes condi&ccedil;&otilde;es:</p>\n<ul>\n<li>o USU&Aacute;RIO tiver vontade de se matar ou ter pensamentos suicida</li>\n<li>o USU&Aacute;RIO tiver algum dist&uacute;rbio psicol&oacute;gico grave, e estar sob forte medica&ccedil;&atilde;o</li>\n<li>o USU&Aacute;RIO ter utilizado, antes do atendimento, alguma droga il&iacute;cita ou estar alcoolizado, prejudicando assim a sua aten&ccedil;&atilde;o, percep&ccedil;&atilde;o e o racioc&iacute;nio</li>\n<li>o USU&Aacute;RIO n&atilde;o falar fluentemente o idioma portugu&ecirc;s</li>\n<li>o USU&Aacute;RIO estiver em crise ou descontrole emocional</li>\n</ul>\n<h3>13. AVISO</h3>\n<p>Os Termos do Servi&ccedil;o constituem o acordo &uacute;nico e integral entre o USU&Aacute;RIO e o Logdialog.com.br, sendo que o relacionamento entre ambas as partes ser&aacute; regido pelas leis da Rep&uacute;blica Federativa do Brasil. O USU&Aacute;RIO e o Logdialog.com.br concordam em submeter-se &agrave; compet&ecirc;ncia &uacute;nica e exclusiva dos tribunais localizados no Brasil.</p>\n<h3>14. ACEITE</h3>\n<p>Ao clicar em \"Eu Aceito\" e utilizar qualquer servi&ccedil;o do Logdialog.com.br, o USU&Aacute;RIO indica que leu e concordou, mesmo que tacitamente, com a vers&atilde;o mais recente dos Termos do Servi&ccedil;o, vinculando-se automaticamente &agrave;s regras aqui contidas.</p>','politica-de-pagamento-e-reembolso','','2017-04-14 00:33:13',9),(6,'É psicólogo e deseja se cadastrar?','<p><span style=\"font-weight: 400;\">O Logdialog &eacute; uma empresa de software que fornece uma plataforma digital e pode ser acessada atrav&eacute;s de dispositivos m&oacute;veis, ou outros meios digitais, por psic&oacute;logos e pacientes. </span></p>\n<p><span style=\"font-weight: 400;\">Se voc&ecirc; &eacute; psic&oacute;logo e est&aacute; inscrito no seu Conselho Regional de Psicologia - CRP, ser&aacute; simples realizar atendimento online (atrav&eacute;s do Logdialog).</span></p>\n<p><span style=\"font-weight: 400;\">Basta fazer seu cadastro que (e) entraremos em contato. </span></p>\n<p><span style=\"font-weight: 400;\">No Logdialog voc&ecirc;:</span></p>\n<ol>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Define o valor da sua consulta.</span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Disponibiliza os hor&aacute;rio que deseja atender.</span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Realiza atendimento por videoconfer&ecirc;ncia.</span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Registra o atendimento em prontu&aacute;rio, e n&oacute;s o guardamos com seguran&ccedil;a para voc&ecirc;. </span></li>\n<li style=\"font-weight: 400;\"><span style=\"font-weight: 400;\">Recebe o pagamento na sua conta sem se preocupar em fazer cobran&ccedil;a ou inadimpl&ecirc;ncia.</span></li>\n</ol>','contrato-psicologo','','2017-04-14 00:33:26',9),(7,'Perguntas Frequentes','<h3 class=\"title-titlecase\">Logdialog substitui a terapia?</h3>\n<p>Logdialog surgiu como mais uma op&ccedil;&atilde;o para que as pessoas possam se cuidar e encontrar profissionais que realmente atendem a sua necessidade. Nosso objetivo &eacute; ampliar e facilitar acesso aos psic&oacute;logos e n&atilde;o substituir a psicoterapia presencial.</p>\n<hr />\n<h3 class=\"title-titlecase\">O que &eacute; orienta&ccedil;&atilde;o psicol&oacute;gica online?</h3>\n<p>Orienta&ccedil;&atilde;o psicol&oacute;gica online &eacute; um servi&ccedil;o psicol&oacute;gico, autorizado e regulamentado pelo Conselho Federal de Psicologia - CFP, realizado em at&eacute; 20 sess&otilde;es, por meios tecnol&oacute;gicos de comunica&ccedil;&atilde;o a dist&acirc;ncia desde que pontuais, informativos e focados no tema proposto.</p>\n<hr />\n<h3 class=\"title-titlecase\">Qual a dura&ccedil;&atilde;o de cada consulta?</h3>\n<p><span style=\"font-weight: 400;\">As consultas s&atilde;o de 50 minutos.</span></p>\n<hr />\n<h3 class=\"title-titlecase\">Qual a frequ&ecirc;ncia das consultas?</h3>\n<p>Voc&ecirc; tamb&eacute;m decide quando quer se consultar. No entanto, para atender a um planejamento adequado, &eacute; importante que isso seja conversado com o profissional.</p>\n<hr />\n<h3 class=\"title-titlecase\">E a privacidade?</h3>\n<p>A primeira preocupa&ccedil;&atilde;o do Logdialog &eacute; garantir o sigilo e a seguran&ccedil;a dos nossos clientes. A sua tranquilidade e conforto &eacute; muito importante para n&oacute;s e por esse motivo os profissionais cadastrados no site seguem o C&oacute;digo de &Eacute;tica Profissional vigente no Brasil e a nossa tecnologia atende aos mais rigorosos padr&otilde;es de seguran&ccedil;a da informa&ccedil;&atilde;o.</p>\n<hr />\n<h3 class=\"title-titlecase\">Os atendimentos s&atilde;o gravados?</h3>\n<p>N&atilde;o. O &uacute;nico registro existente &eacute; o prontu&aacute;rio, que s&oacute; pode ser acessado pelo psic&oacute;logo, e &eacute; protegido por tecnologia de seguran&ccedil;a e codifica&ccedil;&atilde;o avan&ccedil;ada.</p>\n<hr />\n<h3 class=\"title-titlecase\">Qualquer um pode utilizar o servi&ccedil;o do Logdialog?</h3>\n<p>Sim. Qualquer pessoa com mais de 18 anos pode se cadastrar. Menores de 18 anos tamb&eacute;m podem, mas precisam de autoriza&ccedil;&atilde;o dos pais.</p>\n<hr />\n<h3 class=\"title-titlecase\">Os psic&oacute;logos s&atilde;o registrados?</h3>\n<p>Sim, todos eles. Nossa miss&atilde;o &eacute; oferecer orienta&ccedil;&atilde;o psicol&oacute;gica de qualidade. Para isso al&eacute;m do registro no conselho de psicologia, cada profissional passa por um rigoroso processo de sele&ccedil;&atilde;o, checagem de documentos e treinamento.</p>\n<hr />\n<h3 class=\"title-titlecase\">Quem s&atilde;o os psic&oacute;logos?</h3>\n<p>N&oacute;s temos profissionais especializados em diversas &aacute;reas da psicologia e treinados para atender em nossa plataforma. Temos tamb&eacute;m profissionais que monitoram todos os detalhes do nosso servi&ccedil;o.</p>\n<hr />\n<h3 class=\"title-titlecase\">Como agendar um hor&aacute;rio?</h3>\n<p>Para agendar &eacute; f&aacute;cil. Basta entrar na agenda do profissional que voc&ecirc; escolheu e clicar no hor&aacute;rio dispon&iacute;vel que desejar. Ser&aacute; enviado uma solicita&ccedil;&atilde;o de hor&aacute;rio ao profissional. O profissional poder&aacute; aceitar, ou enviar uma nova sugest&atilde;o. Assim que a solicita&ccedil;&atilde;o for aceita, voc&ecirc; ser&aacute; notificado via e-mail e nos avisos da nossa plataforma.</p>\n<hr />\n<h3 class=\"title-titlecase\">Como fa&ccedil;o para mudar meu hor&aacute;rio agendado?</h3>\n<p>Voc&ecirc; pode alterar o hor&aacute;rio da sua consulta com at&eacute; 12 horas de anteced&ecirc;ncia sem precisar pagar a consulta nem taxa administrativa.</p>\n<hr />\n<h3 class=\"title-titlecase\">E se eu precisar alterar meu hor&aacute;rio com menos de 12 horas para o in&iacute;cio da consulta?</h3>\n<p>Neste caso uma taxa administrativa de 50% do valor da consulta ser&aacute; cobrada.</p>\n<hr />\n<h3 class=\"title-titlecase\">Se por acaso eu me atrasar?</h3>\n<p>N&atilde;o se preocupe. Seu psic&oacute;logo estar&aacute; te esperando at&eacute; o final do seu hor&aacute;rio.</p>\n<hr />\n<h3 class=\"title-titlecase\">E se eu faltar, a consulta ser&aacute; cobrada?</h3>\n<p><span style=\"font-weight: 400;\">Sim. Ao agendar uma consulta, seu psic&oacute;logo estar&aacute; o aguardando at&eacute; o final do seu hor&aacute;rio. Al&eacute;m disso o hor&aacute;rio do psic&oacute;logo fica bloqueado para atendimento de outro paciente. &nbsp;</span></p>\n<hr />\n<h3 class=\"title-titlecase\">Meu psic&oacute;logo pode alterar o hor&aacute;rio ?</h3>\n<p><span style=\"font-weight: 400;\">O psic&oacute;logo tamb&eacute;m pode alterar o hor&aacute;rio da consulta. Assim como voc&ecirc;, ele deve avisar com 12 horas de anteced&ecirc;ncia para n&atilde;o ser cobrado.</span></p>\n<hr />\n<h3 class=\"title-titlecase\"><b>O que devo fazer se meu psic&oacute;logo se atrasar?</b></h3>\n<p><span style=\"font-weight: 400;\">&Eacute; s&oacute; aguardar e ele ir&aacute; atend&ecirc;-lo assim que poss&iacute;vel. Tamb&eacute;m n&atilde;o se preocupe com o tempo, pois ele poder&aacute; ser compensado ao final da consulta, ou ser&aacute; agendado um novo hor&aacute;rio. </span></p>\n<hr />\n<h3 class=\"title-titlecase\">Pode acontecer do psic&oacute;logo faltar. Como fica o valor que j&aacute; paguei?</h3>\n<p>Voc&ecirc; poder&aacute; agendar uma nova consulta sem custo.</p>\n<hr />\n<h3 class=\"title-titlecase\">E se eu n&atilde;o gostar do meu psic&oacute;logo?</h3>\n<p>Para ajudar na escolha, o Logdialog oferece a primeira consulta para apresenta&ccedil;&atilde;o sem custo. O paciente e o profissional agendam um hor&aacute;rio para conversarem e se conhecerem. Caso n&atilde;o se identifique com o profissional, o paciente pode agendar com outro profissional.</p>\n<p>Se preferir, tamb&eacute;m poder&aacute; entrar em contato com o Logdialog e tentaremos lhe ajudar a encontrar o profissional mais adequado ao seu perfil. Temos muito interesse em receber qualquer coment&aacute;rio que nos ajude a entender por qual motivo voc&ecirc; n&atilde;o se sentiu confort&aacute;vel.</p>\n<hr />\n<h3 class=\"title-titlecase\">O perfil do psic&oacute;logo &eacute; p&uacute;blico?</h3>\n<p>Depende. O profissional pode optar por manter o seu perfil oculto, p&uacute;blico ou divulgar apenas para quem desejar.</p>\n<hr />\n<h3 class=\"title-titlecase\">Como funciona o pagamento?</h3>\n<p>O processo &eacute; simples e seguro. Todas as transa&ccedil;&otilde;es financeiras s&atilde;o realizadas dentro do nosso site, que est&aacute; integrado ao PagSeguro, para que voc&ecirc; possa efetuar o pagamento com tranquilidade. Al&eacute;m disso, todas as informa&ccedil;&otilde;es do cart&atilde;o de cr&eacute;dito s&atilde;o mantidas em seguran&ccedil;a e n&oacute;s n&atilde;o divulgamos nenhuma informa&ccedil;&atilde;o pessoal.</p>\n<hr />\n<h3 class=\"title-titlecase\">Eu n&atilde;o tenho conta no PagSeguro. Como fazer?</h3>\n<p>N&atilde;o tem problema. N&atilde;o &eacute; necess&aacute;rio ter conta no PagSeguro para efetuar o pagamento.</p>\n<hr />\n<h3 class=\"title-titlecase\">Quais s&atilde;o os requisitos b&aacute;sicos para uma orienta&ccedil;&atilde;o psicol&oacute;gica online?</h3>\n<p>&Eacute; necess&aacute;rio um computador ou dispositivo com acesso &agrave; Internet 4G ou Wi-Fi e estar cadastrado no site www.logdialog.com.br.</p>\n<hr />\n<h3 class=\"title-titlecase\">Eu preciso fazer download ou instalar alguma coisa no meu computador?</h3>\n<p>N&atilde;o. Basta estar logado, pois todo processo de atendimento acontece dentro do site.</p>','perguntas-frequentes','','2017-04-14 00:33:40',9),(8,'Política de Privacidade','<p><span style=\"font-weight: 400;\">Sua privacidade &eacute; muito importante para n&oacute;s. Criamos nossa Pol&iacute;tica de Privacidade para divulgar informa&ccedil;&otilde;es importantes sobre o uso de informa&ccedil;&otilde;es e como voc&ecirc; pode usar o Logdialog.</span><br /><br /></p>\n<h3>Sobre as informa&ccedil;&otilde;es coletadas</h3>\n<p>Quando voc&ecirc; utiliza nosso servi&ccedil;o, como parte do processo de contrata&ccedil;&atilde;o, coletamos &nbsp;suas informa&ccedil;&otilde;es pessoais, por voc&ecirc; fornecidas, tais como seu nome, endere&ccedil;o e endere&ccedil;o de e-mail.</p>\n<p>Quando voc&ecirc; navega pelo nosso site, recebemos tamb&eacute;m automaticamente o protocolo de internet do seu computador, denominado endere&ccedil;o de IP, a fim de obtermos informa&ccedil;&otilde;es sobre seu navegador e sistema operacional.</p>\n<p>Com sua permiss&atilde;o, podemos lhe enviar emails sobre nosso servi&ccedil;o, novos produtos e outras atualiza&ccedil;&otilde;es.</p>\n<p>O Logdialog n&atilde;o ir&aacute; comercializar quaisquer de suas informa&ccedil;&otilde;es pessoais com terceiros, no entanto, o uso de terceiros &eacute; necess&aacute;rio em determinados momentos, como por exemplo, no processamento das informa&ccedil;&otilde;es para realiza&ccedil;&atilde;o de pagamento. De qualquer maneira, exigimos que estes terceiros cumpram estritamente &agrave;s suas instru&ccedil;&otilde;es e que n&atilde;o utilizem as suas informa&ccedil;&otilde;es pessoais para os seus pr&oacute;prios fins comerciais.</p>\n<p><span style=\"font-weight: 400;\">O Logdialog utiliza apenas informa&ccedil;&otilde;es n&atilde;o-identific&aacute;veis para aprimorar a tecnologia utilizada, atualiza&ccedil;&atilde;o t&eacute;cnicas das ferramentas utilizadas, para divulga&ccedil;&atilde;o dos servi&ccedil;os prestados, para melhorar a experi&ecirc;ncia de uso e navega&ccedil;&atilde;o cujo conte&uacute;do n&atilde;o permite a identifica&ccedil;&atilde;o dos usu&aacute;rios.​​</span></p>\n<p>As avalia&ccedil;&otilde;es por parte de usu&aacute;rios e profissionais cadastrados e pesquisas de satisfa&ccedil;&atilde;o s&atilde;o realizadas apenas informa&ccedil;&otilde;es n&atilde;o-identific&aacute;veis com o intuito de .</p>\n<p><span style=\"font-weight: 400;\">Nenhuma informa&ccedil;&atilde;o de identifica&ccedil;&atilde;o pessoal ser&aacute; vinculada aos resultados e o Logdialog n&atilde;o compartilhar&aacute; nada que possa ser usado para identificar sua conta ou suas informa&ccedil;&otilde;es privadas.</span></p>\n<p><span style=\"font-weight: 400;\">Se nossas pr&aacute;ticas de informa&ccedil;&atilde;o mudarem em algum momento no futuro, entraremos em contato com voc&ecirc; antes de usar seus dados para esses novos fins para notific&aacute;-lo sobre a altera&ccedil;&atilde;o de pol&iacute;tica e para fornecer a voc&ecirc; a capacidade de optar por esses novos usos.</span></p>\n<p><span style=\"font-weight: 400;\">N&oacute;s armazenamos dados somente enquanto houver necessidade de fornecer produtos e servi&ccedil;os para voc&ecirc; e outros, incluindo aqueles descritos acima e para prote&ccedil;&otilde;es legais ou conforme exigido pelas leis e regulamentos aplic&aacute;veis.</span></p>\n<p><span style=\"font-weight: 400;\">Podemos permitir o acesso a informa&ccedil;&otilde;es p&uacute;blicas que foram fornecidas por voc&ecirc; e compartilhadas por meio de nossos servi&ccedil;os, como por exemplo o perfil do profissional.</span></p>\n<p><span style=\"font-weight: 400;\">Certas informa&ccedil;&otilde;es s&atilde;o necess&aacute;rias para fornecer servi&ccedil;os, portanto, podemos permitir que os provedores de servi&ccedil;os acessem determinadas informa&ccedil;&otilde;es para viabilizar o fornecimento de servi&ccedil;os. </span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog.com.br permite que voc&ecirc; altere as suas informa&ccedil;&otilde;es cadastrais a qualquer instante. </span></p>\n<p><span style=\"font-weight: 400;\">Voc&ecirc; pode solicitar a exclus&atilde;o da sua conta. Por favor, envie um e-mail para </span><span style=\"font-weight: 400;\">contato@Logdialog.com.br ou nos envie uma correspond&ecirc;ncia em: Logdialog - Rua do R&oacute;cio, 423 - Conj. 1411, S&atilde;o Paulo, SP, 04705-010, Brazil.</span></p>\n<p><span style=\"font-weight: 400;\">O Logdialog s&oacute; far&aacute; a exclus&atilde;o sem autoriza&ccedil;&atilde;o do cliente mediante o recebimento de c&oacute;pia da certid&atilde;o de &oacute;bito enviada para o Logdialog no seguinte endere&ccedil;o: </span><span style=\"font-weight: 400;\">Rua do R&oacute;cio, 423 - Conj. 1411, S&atilde;o Paulo, SP, 04705-010, Brazil. A exclus&atilde;o da conta a acareeta no encerramento de todo conte&uacute;do. </span><br /><br /></p>\n<h3>Protegendo a privacidade de seus usu&aacute;rios do Logdialog</h3>\n<p><span style=\"font-weight: 400;\">Durante o uso dos servi&ccedil;os de Logdialog.com.br, voc&ecirc;:</span></p>\n<p><span style=\"font-weight: 400;\">N&atilde;o enviar&aacute; ou n&atilde;o publicar&aacute; comunica&ccedil;&otilde;es comerciais n&atilde;o autorizadas (como spam) no Logdialog.com.br.</span></p>\n<p><span style=\"font-weight: 400;\">N&atilde;o ir&aacute; coletar o conte&uacute;do dos usu&aacute;rios ou informa&ccedil;&otilde;es, ou acessar Logdialog.com.br, usando meios automatizados n&atilde;o permitidos (bots de coleta, rob&ocirc;s ou spiders) sem nossa permiss&atilde;o.</span></p>\n<p><span style=\"font-weight: 400;\">N&atilde;o enviar&aacute; v&iacute;rus ou outro c&oacute;digo malicioso.</span></p>\n<p><span style=\"font-weight: 400;\">N&atilde;o solicitar&aacute; informa&ccedil;&otilde;es de login ou acessar uma conta pertencente a outra pessoa.</span></p>\n<p><span style=\"font-weight: 400;\">N&atilde;o vai intimidar ou assediar qualquer outro usu&aacute;rio.</span></p>\n<p>N&atilde;o postar&aacute; conte&uacute;do odioso, amea&ccedil;ador ou pornogr&aacute;fico; que incita &agrave; viol&ecirc;ncia; ou que contenha nudez ou viol&ecirc;ncia gr&aacute;fica ou gratuita.</p>\n<p><span style=\"font-weight: 400;\">N&atilde;o fornecer&aacute; nenhuma informa&ccedil;&atilde;o pessoal falsa no Logdialog.com.br, ou criar&aacute; uma conta para qualquer pessoa que n&atilde;o seja voc&ecirc; mesmo sem permiss&atilde;o. </span></p>\n<p>N&atilde;o criar&aacute; mais de um perfil pessoal.<br /><br /></p>\n<h3>Direitos Autorais</h3>\n<p><span style=\"font-weight: 400;\">Esperamos que respeite os direitos autorais, portanto voc&ecirc; n&atilde;o postar&aacute; conte&uacute;do ou far&aacute; qualquer a&ccedil;&atilde;o no Logdialog que infrinja ou viole os direitos de outra pessoa ou que de alguma maneira viole a lei. </span></p>\n<p><span style=\"font-weight: 400;\">Podemos remover qualquer conte&uacute;do ou informa&ccedil;&atilde;o que voc&ecirc; postar no Logdialog se acreditarmos que viola esta Pol&iacute;tica de Privacidade ou os Termos de Uso deste Site. </span></p>\n<p><span style=\"font-weight: 400;\">Voc&ecirc; n&atilde;o usar&aacute; nossa marca, ou nossos direitos autorais sem nossa permiss&atilde;o por escrito. </span></p>\n<p><span style=\"font-weight: 400;\">Se voc&ecirc; compartilhar dados pessoais com terceiros atrav&eacute;s do Logdialog, voc&ecirc; assume total responsabilidade e deixa claro, que voc&ecirc;, e n&atilde;o Logdialog, &eacute; aquele que compartilhou as informa&ccedil;&otilde;es e: </span></p>\n<p>Voc&ecirc; declara possuir o direito de comunicar ou divulgar dados pessoais a terceiros e de ter recebido informa&ccedil;&otilde;es pr&eacute;vias, dispensando o Logdialog de toda a responsabilidade pelo seu uso impr&oacute;prio ap&oacute;s a divulga&ccedil;&atilde;o ou divulga&ccedil;&atilde;o.</p>\n<p>Voc&ecirc; n&atilde;o postar&aacute; documentos de identifica&ccedil;&atilde;o de ningu&eacute;m ou informa&ccedil;&otilde;es financeiras confidenciais no Logdialog.com.br.</p>\n<p><span style=\"font-weight: 400;\">Voc&ecirc; n&atilde;o marcar&aacute; os usu&aacute;rios nem enviar&aacute; convites por e-mail a n&atilde;o-usu&aacute;rios sem consentimento.</span><br /><br /></p>\n<h3>Dispositivos Eletr&ocirc;nicos Port&aacute;teis</h3>\n<p><span style=\"font-weight: 400;\">A Logdialog dispon&iacute;vel em uma infinidade de dispositivos eletr&ocirc;nicos port&aacute;teis. </span></p>\n<p>N&oacute;s fornecemos nossa conex&atilde;o a servi&ccedil;os m&oacute;veis, mas lembre-se de que as taxas das operadoras de telefonia e internet ainda podem ser aplicadas.</p>\n<p><span style=\"font-weight: 400;\">Voc&ecirc; fornece todos os direitos necess&aacute;rios para permitir que os usu&aacute;rios sincronizem (inclusive por meio de um aplicativo) suas listas de contatos com quaisquer informa&ccedil;&otilde;es b&aacute;sicas e informa&ccedil;&otilde;es de contato que sejam vis&iacute;veis no Logdialog, bem como seu nome e imagem de perfil.</span><br /><br /></p>\n<h3>Disputas</h3>\n<p>Voc&ecirc; resolver&aacute; qualquer reclama&ccedil;&atilde;o, causa de a&ccedil;&atilde;o ou disputa (reclama&ccedil;&atilde;o) que tenha conosco decorrentes ou relacionados a esta declara&ccedil;&atilde;o ou Logdialog exclusivamente em um tribunal estadual ou federal localizado em S&atilde;o Paulo.</p>\n<p><span style=\"font-weight: 400;\">As leis do Brasileiras reger&atilde;o esta declara&ccedil;&atilde;o, bem como qualquer reclama&ccedil;&atilde;o que possa surgir entre voc&ecirc; e o Logdialog.</span></p>\n<p><span style=\"font-weight: 400;\">Se algu&eacute;m apresentar uma reclama&ccedil;&atilde;o contra n&oacute;s relacionada &agrave;s suas a&ccedil;&otilde;es ou conte&uacute;do, voc&ecirc; o indenizar&aacute; e nos mante</span><span style=\"font-weight: 400;\">r&aacute; isento co</span><span style=\"font-weight: 400;\">ntra todos os danos, perdas e despesas de qualquer tipo (incluindo honor&aacute;rios e custos legais razo&aacute;veis) vinculados a tal reivindica&ccedil;&atilde;o.</span><br /><br /></p>\n<h3>Redes Sociais e Logdialog</h3>\n<p><span style=\"font-weight: 400;\">O uso das redes sociais &eacute; pro&iacute;bido para realiza&ccedil;&atilde;o de Orienta&ccedil;&atilde;o Psicol&oacute;gica Online, assim como qualquer tipo de atendimento psicol&oacute;gico.</span></p>\n<p>Estes servi&ccedil;os n&atilde;o s&atilde;o activados automaticamente, requerem autoriza&ccedil;&atilde;o expressa do utilizador, permitem que o Logdialog acesse os dados do seu perfil nas redes sociais e interaja por meio de sua postagem.</p>\n<p><span style=\"font-weight: 400;\">Os usu&aacute;rios do Logdialog podem compartilhar informa&ccedil;&otilde;es e dados fornecidos, com as redes sociais em que est&aacute; registrado, mas para isso deve aceitar a pol&iacute;tica de privacidade das redes sociais correspondentes. </span></p>\n<p>Qualquer postagem postagem realizada pelo psic&oacute;logo em redes sociais &eacute; de responsabilidade do pr&oacute;prio psic&oacute;logo e n&atilde;o do Logdialog. Qualquer postagem que possa identificar o paciente &eacute; expressamente proibida.</p>\n<p><span style=\"font-weight: 400;\">Qualquer postagem postagem realizada pelo paciente em redes sociais &eacute; de responsabilidade do pr&oacute;prio paciente e n&atilde;o do Logdialog. Postagens realizadas por pacientes nos perfis das redes sociais do Logdialog n&atilde;o ser&atilde;o consideradas informa&ccedil;&otilde;es confidenciais e &eacute; de responsabilidade do pr&oacute;prio psic&oacute;logo e n&atilde;o do Logdialog. Qualquer postagem que possa identificar outros pacientes &eacute; expressamente proibida.</span><br /><br /></p>\n<h3>Pagamento</h3>\n<p><span style=\"font-weight: 400;\">Quando voc&ecirc; nos fornece as suas informa&ccedil;&otilde;es pessoais para completar uma transa&ccedil;&atilde;o ou verificar seu cart&atilde;o de cr&eacute;dito, entendemos que voc&ecirc; concorda com a nossa coleta e que o uso desses dados ser&aacute; apenas para aquele motivo espec&iacute;fico.</span><br /><br /></p>\n<h3>Servi&ccedil;os de terceiros</h3>\n<p>No geral, os servi&ccedil;os terceirizados usados por n&oacute;s ir&atilde;o apenas coletar, usar e divulgar suas informa&ccedil;&otilde;es na medida do necess&aacute;rio para permitir que eles realizem os servi&ccedil;os que eles nos fornecem.</p>\n<p><span style=\"font-weight: 400;\">Entretanto, certos fornecedores de servi&ccedil;os terceirizados, tais como gateways de pagamento e outros processadores de transa&ccedil;&atilde;o de pagamento, t&ecirc;m suas pr&oacute;prias pol&iacute;ticas de privacidade, portanto recomendamos que voc&ecirc; leia suas pol&iacute;ticas de privacidade para que possa entender de que maneira suas informa&ccedil;&otilde;es pessoais ser&atilde;o usadas por esses fornecedores.</span><br /><br /></p>\n<h3>Seguran&ccedil;a</h3>\n<p><span style=\"font-weight: 400;\">Para proteger suas informa&ccedil;&otilde;es pessoais, tomamos precau&ccedil;&otilde;es razo&aacute;veis e seguimos as melhores pr&aacute;ticas da ind&uacute;stria para nos certificar que elas n&atilde;o ser&atilde;o perdidas inadequadamente, usurpadas, acessadas, divulgadas, alteradas ou destru&iacute;das.</span></p>\n<p><span style=\"font-weight: 400;\">Embora nenhum m&eacute;todo de transmiss&atilde;o pela Internet ou armazenamento eletr&ocirc;nico seja 100% seguro, n&oacute;s seguimos todos os requisitos e implementamos padr&otilde;es adicionais geralmente aceitos pela ind&uacute;stria.</span><br /><br /></p>\n<h3>Confidencialidade das informa&ccedil;&otilde;es na Orienta&ccedil;&atilde;o Psicol&oacute;gica</h3>\n<p><span style=\"font-weight: 400;\">Logdialog.com.br tem um compromisso com a confidencialidade. As informa&ccedil;&otilde;es fornecidas e armazenadas s&atilde;o protegidas por sistema de criptografia que </span><span style=\"font-weight: 400;\">codifica as informa&ccedil;&otilde;es de forma que s&oacute; o emissor e o receptor consigam decifr&aacute;-las</span></p>\n<p>As &uacute;nicas informa&ccedil;&otilde;es pessoais que ser&atilde;o enviadas para o psic&oacute;logo s&atilde;o o nome completo do paciente e a data de nascimento, para a correta identifica&ccedil;&atilde;o.</p>\n<p><span style=\"font-weight: 400;\">No caso de orienta&ccedil;&atilde;o para menores, tamb&eacute;m ser&aacute; disponibilizado para o profissional, o nome completo, do respons&aacute;vel e o documento de autoriza&ccedil;&atilde;o prenchi, disponivel no logdialog.com.br e preenchido pelo responsavel.</span></p>\n<p><span style=\"font-weight: 400;\">Informa&ccedil;&otilde;es complementares que o psic&oacute;logo julgar necess&aacute;rias, poder&atilde;o ser solicitadas pelo profissional durante a sess&atilde;o. </span></p>\n<p>O cliente fica ciente e concorda que qualquer informa&ccedil;&atilde;o, dados, texto, v&iacute;deo,</p>\n<p><span style=\"font-weight: 400;\">mensagem ou qualquer outro material transmitido atrav&eacute;s do atendimento com o psic&oacute;logo ser&aacute; de total responsabilidade do cliente, pois o Logdialog n&atilde;o ir&aacute; controlar e ter acesso em hip&oacute;tese alguma sobre essas informa&ccedil;&otilde;es. </span></p>\n<p><span style=\"font-weight: 400;\">Todas as informa&ccedil;&otilde;es sobre o paciente que o psic&oacute;logo armazenar em prontu&aacute;rio, tamb&eacute;m ent&atilde;o protegidas por criptografia.</span></p>\n<p>&Eacute; importante ressaltar que os psic&oacute;logos geralmente s&atilde;o obrigados a notificar suspeitas de crimes de qualquer inst&acirc;ncia, incluindo suspeita de maus-tratos e viol&ecirc;ncia sexual contra crian&ccedil;as ou adultos dependentes ao Conselho Regional de Psicologia - CRP .</p>\n<p><span style=\"font-weight: 400;\">Al&eacute;m disso, o psic&oacute;logo poder&aacute; quebrar sigilo quando julgar que o usu&aacute;rio est&aacute; em condi&ccedil;&otilde;es mentais ou emocionais, que o tornam um perigo para si pr&oacute;prio ou aos outros. </span></p>\n<p>De forma geral, o psic&oacute;logo dever&aacute; respeitar a legisla&ccedil;&atilde;o atual e seguir as orienta&ccedil;&otilde;es contidas no c&oacute;digo de &eacute;tica do Conselho Federal de Psicologia.<br /><br /></p>\n<h3>Registros documentais e Prontu&aacute;rio Eletr&ocirc;nico</h3>\n<p><span style=\"font-weight: 400;\">Pensando nas melhores pr&aacute;ticas de atendimento, o Logdialog desenvolveu um prontu&aacute;rio eletr&ocirc;nico que atende &agrave;s exig&ecirc;ncias do Conselho Federal de Psicologia CRP. </span></p>\n<p>Todas as informa&ccedil;&otilde;es trocadas entre psic&oacute;logos e pacientes s&atilde;o confidenciais e n&atilde;o s&atilde;o divulgadas. Informa&ccedil;&otilde;es armazenadas em prontu&aacute;rio s&atilde;o criptografadas e mantidas em \"arquivo\" por 5 anos para consulta em caso de disputa legal por ordem judicial de acordo com a resolu&ccedil;&atilde;o Resolu&ccedil;&atilde;o CFP N&ordm; 001/2009.</p>\n<p><span style=\"font-weight: 400;\">A reten&ccedil;&atilde;o de arquivos protege a seguran&ccedil;a do usu&aacute;rio e dos terapeutas. Os dados ser&atilde;o protegidos em altos n&iacute;veis de seguran&ccedil;a e privacidade.</span></p>\n<p><span style=\"font-weight: 400;\">​​Devido &agrave; natureza da Internet e ambientes digitais, dados perdidos / expostos / usados devido a a&ccedil;&otilde;es ilegais de hackers e criminosos, falhas t&eacute;cnicas de servidores e banco de dados etc, n&atilde;o ser&atilde;o de responsabilidade do Logdialog.</span></p>\n<p><span style=\"font-weight: 400;\">Para garantir confidencialidade confiabilidade e seguran&ccedil;a para o psic&oacute;logo e para o paciente, uma vez salva, as informa&ccedil;&otilde;es armazenadas em prontu&aacute;rio n&atilde;o poder&atilde;o ser alteradas.</span></p>\n<p><span style=\"font-weight: 400;\">Estas s&atilde;o as condi&ccedil;&otilde;es aceitas pelo usu&aacute;rio do Logdialog e usu&aacute;rios da Internet em geral e voc&ecirc; est&aacute; concordando com estas condi&ccedil;&otilde;es ao usar nosso site.</span><br /><br /></p>\n<h3>Perfil do profissional e do paciente</h3>\n<p><span style=\"font-weight: 400;\">O cliente do Logdialog dever&aacute; criar uma senha individual, confidencial e intransfer&iacute;vel para acesso ao site, bem como para efetuar altera&ccedil;&otilde;es nos dados cadastrais. </span></p>\n<p><span style=\"font-weight: 400;\">&Eacute; responsabilidade do cliente n&atilde;o permitir que sua conta seja acessada por outras pessoas.</span></p>\n<p><span style=\"font-weight: 400;\">Em caso de suspeita de fraude ou uso n&atilde;o autorizado, o cliente deve comunicar imediatamente o Logdialog atrav&eacute;s do e-mail atendimento@logdialog.com.br.</span></p>\n<p><span style=\"font-weight: 400;\">O logdialog n&atilde;o se responsabiliza pelas a&ccedil;&otilde;es, conte&uacute;dos, informa&ccedil;&otilde;es ou dados de terceiros. De igual modo nossos diretores, funcion&aacute;rios e agentes tamb&eacute;m est&atilde;o eximidos de quaisquer reclama&ccedil;&otilde;es e danos, conhecidos e desconhecidos, decorrentes de qualquer reclama&ccedil;&atilde;o contra terceiros.</span><br /><br /></p>\n<h3>O uso de cookies</h3>\n<p><span style=\"font-weight: 400;\">Um cookie &eacute; uma pequena quantidade de dados, que muitas vezes inclui um identificador exclusivo que &eacute; enviado e armazenado no disco r&iacute;gido do dispositivo para registram informa&ccedil;&otilde;es sobre suas prefer&ecirc;ncias e que nos permitem adaptar Logdialog aos seus interesses. </span></p>\n<p><span style=\"font-weight: 400;\">Os cookies podem ser desativados a qualquer momento pelo usu&aacute;rio. No entanto, as informa&ccedil;&otilde;es fornecidas pelos cookies podem ajudar-nos a analisar o perfil dos nossos visitantes para que possamos proporcionar-lhe uma melhor experi&ecirc;ncia ao utilizar nosso site.</span><br /><br /></p>\n<h3>Idade e consentimento</h3>\n<p><span style=\"font-weight: 400;\">Ao usar este site, voc&ecirc; afirma que tem 18 anos ou mais, ou que nos deu seu consentimento para permitir que seu dependentes menor de idade use esse site.</span><br /><br /></p>\n<h3>Altera&ccedil;&otilde;es da pol&iacute;tica de privacidade</h3>\n<p><span style=\"font-weight: 400;\">Reservamos o direito de modificar essa pol&iacute;tica de privacidade a qualquer momento. Se fizermos altera&ccedil;&otilde;es em nossa pol&iacute;tica, iremos notific&aacute;-lo sobre tais altera&ccedil;&otilde;es, para que voc&ecirc; sempre tenha ci&ecirc;ncia sobre quais informa&ccedil;&otilde;es coletamos, como e sob que circunst&acirc;ncias poder&atilde;o ser usadas, &nbsp;ou divulgadas.</span></p>\n<p><span style=\"font-weight: 400;\">Altera&ccedil;&otilde;es e esclarecimentos v&atilde;o surtir efeito imediatamente ap&oacute;s sua publica&ccedil;&atilde;o no site. </span></p>','politica-de-privacidade','','2017-04-14 00:34:00',9);
/*!40000 ALTER TABLE `tb_pagina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pagseguro`
--

DROP TABLE IF EXISTS `tb_pagseguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pagseguro` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `de_usuario_id` bigint(20) unsigned NOT NULL COMMENT 'usuário pagante da transação',
  `agenda_id` bigint(20) unsigned DEFAULT NULL COMMENT 'caso seja referente à um agendamento, relaciona ele na tabela',
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'data de inserção do registro nesta tabela, que é a mesma data em que a transação foi submetida ao pagseguro',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'valor da transação',
  `descricao` text COMMENT 'caso haja alguma informação para descrever',
  `api_checkout_code` varchar(255) DEFAULT NULL COMMENT 'quando a api retorna uma chamada, sempre tem um número de checkout, usado para direcionar o comprador',
  `api_checkout_date` varchar(255) DEFAULT NULL COMMENT 'valor com informação da data de criação do checkout, retornado pela API. No formato: YYYY-MM-DDThh:mm:ss.sTZD',
  `api_error_code` varchar(255) DEFAULT NULL COMMENT 'código de erro da api',
  `api_error_message` text COMMENT 'mensagem de erro da api',
  `api_code` varchar(255) DEFAULT NULL COMMENT 'após o checkout com sucesso, e passar pelas etapas de validação, a api retorna um código, para que o comprador seja direcionado.\n\nEx: https://pagseguro.uol.com.br/v2/checkout/payment.html?code=8CF4BE7DCECEF0F004A6DFA0A8243412',
  `api_notification_code` varchar(255) DEFAULT NULL COMMENT 'código de notificação',
  `api_transaction_code` varchar(255) DEFAULT NULL COMMENT 'código da transação',
  `api_transaction_date` varchar(255) DEFAULT NULL COMMENT 'data de transação, retornado pela API. No formato: YYYY-MM-DDThh:mm:ss.sTZD',
  `api_transaction_type` int(11) DEFAULT NULL COMMENT 'código inteiro com o tipo de transação',
  `api_transaction_status` int(11) DEFAULT NULL COMMENT 'código inteiro com o status da transação. A lista completa de códigos está neste link: https://pagseguro.uol.com.br/v3/guia-de-integracao/api-de-notificacoes.html#v3-item-api-de-notificacoes-status-da-transacao',
  PRIMARY KEY (`id`),
  KEY `fk_tb_pagseguro_tb_usuario1_idx` (`de_usuario_id`),
  KEY `fk_tb_pagseguro_tb_agenda1_idx` (`agenda_id`),
  CONSTRAINT `fk_tb_pagseguro_tb_agenda1` FOREIGN KEY (`agenda_id`) REFERENCES `tb_agenda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tb_pagseguro_tb_usuario1` FOREIGN KEY (`de_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pagseguro`
--

LOCK TABLES `tb_pagseguro` WRITE;
/*!40000 ALTER TABLE `tb_pagseguro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_pagseguro` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_pagseguro_AFTER_INSERT` AFTER INSERT ON `tb_pagseguro`
 FOR EACH ROW BEGIN
	# replicando para o log
    INSERT INTO tb_log_pagseguro
		( pagseguro_id, de_usuario_id, agenda_id, data_registro, valor,
          descricao, api_checkout_code, api_checkout_date, api_error_code,
          api_error_message, api_code, api_notification_code, api_transaction_code,
          api_transaction_date, api_transaction_type, api_transaction_status )
	VALUES
		( NEW.id, NEW.de_usuario_id, NEW.agenda_id, NEW.data_registro, NEW.valor,
          NEW.descricao, NEW.api_checkout_code, NEW.api_checkout_date, NEW.api_error_code,
          NEW.api_error_message, NEW.api_code, NEW.api_notification_code, NEW.api_transaction_code,
          NEW.api_transaction_date, NEW.api_transaction_type, NEW.api_transaction_status  );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_pagseguro_AFTER_UPDATE` AFTER UPDATE ON `tb_pagseguro`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.de_usuario_id != OLD.de_usuario_id OR
        NEW.agenda_id != OLD.agenda_id OR
        NEW.data_registro != OLD.data_registro OR
        NEW.valor != OLD.valor OR
		NEW.descricao != OLD.descricao OR
        NEW.api_checkout_code != OLD.api_checkout_code OR
        NEW.api_checkout_date != OLD.api_checkout_date OR
        NEW.api_error_code != OLD.api_error_code OR
        NEW.api_error_message != OLD.api_error_message OR
        NEW.api_code != OLD.api_code OR
        NEW.api_notification_code != OLD.api_notification_code OR
        NEW.api_transaction_code != OLD.api_transaction_code OR
        NEW.api_transaction_date != OLD.api_transaction_date OR
        NEW.api_transaction_type != OLD.api_transaction_type OR
        NEW.api_transaction_status != OLD.api_transaction_status
	   )
	
    THEN
		
        INSERT INTO tb_log_pagseguro
		( pagseguro_id, de_usuario_id, agenda_id, data_registro, valor,
          descricao, api_checkout_code, api_checkout_date, api_error_code,
          api_error_message, api_code, api_notification_code, api_transaction_code,
          api_transaction_date, api_transaction_type, api_transaction_status )
	VALUES
		( NEW.id, OLD.de_usuario_id, OLD.agenda_id, OLD.data_registro, OLD.valor,
          OLD.descricao, OLD.api_checkout_code, OLD.api_checkout_date, OLD.api_error_code,
          OLD.api_error_message, OLD.api_code, OLD.api_notification_code, OLD.api_transaction_code,
          OLD.api_transaction_date, OLD.api_transaction_type, OLD.api_transaction_status  );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_pagseguro_AFTER_DELETE` AFTER DELETE ON `tb_pagseguro`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_pagseguro
		SET original_deletado = 1
	WHERE pagseguro_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_profissional`
--

DROP TABLE IF EXISTS `tb_profissional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_profissional` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(20) unsigned NOT NULL,
  `nome` varchar(255) NOT NULL,
  `sobrenome` varchar(255) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `sexo` enum('feminino','masculino') DEFAULT NULL,
  `numero_crp` varchar(255) DEFAULT NULL COMMENT 'Número de registro no conselho',
  `documento_crp` text,
  `documento_identidade` text,
  `documento_diploma` text,
  `avatar` text,
  `experiencia_profissional` longtext,
  `afiliacoes` longtext,
  `formacao_academica` longtext,
  `cpf` varchar(45) DEFAULT NULL,
  `rua` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `cep` varchar(45) DEFAULT NULL,
  `primeiro_contato_gratis` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = primeiro contato pago\n1 = primeiro contato gratis',
  `data_aprovada_documentacao` timestamp NULL DEFAULT NULL,
  `original_deletado` tinyint(1) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_profissional_tb_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_tb_profissional_tb_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20000 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_profissional`
--

LOCK TABLES `tb_profissional` WRITE;
/*!40000 ALTER TABLE `tb_profissional` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_profissional` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_AFTER_INSERT` AFTER INSERT ON `tb_profissional`
 FOR EACH ROW BEGIN
	# replicando para o log
	INSERT INTO tb_log_profissional_alteracao
		( profissional_id, usuario_id, nome, sobrenome, nascimento, sexo, numero_crp, documento_crp,
          documento_identidade, documento_diploma, avatar, experiencia_profissional,
          afiliacoes, formacao_academica, cpf, rua, bairro, cidade, estado, cep,
          primeiro_contato_gratis)
	VALUES
		( NEW.id, NEW.usuario_id, NEW.nome, NEW.sobrenome, NEW.nascimento, NEW.sexo, NEW.numero_crp, NEW.documento_crp,
          NEW.documento_identidade, NEW.documento_diploma, NEW.avatar, NEW.experiencia_profissional,
          NEW.afiliacoes, NEW.formacao_academica, NEW.cpf, NEW.rua, NEW.bairro, NEW.cidade, NEW.estado, NEW.cep,
          NEW.primeiro_contato_gratis);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_AFTER_UPDATE` AFTER UPDATE ON `tb_profissional`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.usuario_id != OLD.usuario_id OR 
        NEW.nome != OLD.nome OR
        NEW.sobrenome != OLD.sobrenome OR 
        NEW.nascimento != OLD.nascimento OR 
        NEW.sexo != OLD.sexo OR
        NEW.numero_crp != OLD.numero_crp OR
        NEW.documento_crp != OLD.documento_crp OR
        NEW.documento_identidade != OLD.documento_identidade OR
        NEW.documento_diploma != OLD.documento_diploma OR
        NEW.avatar != OLD.avatar OR
        NEW.experiencia_profissional != OLD.experiencia_profissional OR
        NEW.afiliacoes != OLD.afiliacoes OR
        NEW.formacao_academica != OLD.formacao_academica OR
        NEW.cpf != OLD.cpf OR
        NEW.rua != OLD.rua OR
        NEW.bairro != OLD.bairro OR
        NEW.cidade != OLD.cidade OR
        NEW.estado != OLD.estado OR
        NEW.cep != OLD.cep OR
        NEW.primeiro_contato_gratis != OLD.primeiro_contato_gratis)
	
    THEN
		
        INSERT INTO tb_log_profissional_alteracao
			( profissional_id, usuario_id, nome, sobrenome, nascimento, sexo, numero_crp, documento_crp,
			  documento_identidade, documento_diploma, avatar, experiencia_profissional, 
              afiliacoes, formacao_academica, cpf, rua, bairro, cidade, estado, cep,
              primeiro_contato_gratis)
		VALUES
			( NEW.id, OLD.usuario_id, OLD.nome, OLD.sobrenome, OLD.nascimento, OLD.sexo, OLD.numero_crp, OLD.documento_crp,
			  OLD.documento_identidade, OLD.documento_diploma, OLD.avatar, OLD.experiencia_profissional,
              OLD.afiliacoes, OLD.formacao_academica, OLD.cpf, OLD.rua, OLD.bairro, OLD.cidade, OLD.estado, OLD.cep,
              OLD.primeiro_contato_gratis);
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_AFTER_DELETE` AFTER DELETE ON `tb_profissional`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_profissional_alteracao
		SET original_deletado = 1
	WHERE profissional_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_profissional_dia_atendimento`
--

DROP TABLE IF EXISTS `tb_profissional_dia_atendimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_profissional_dia_atendimento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profissional_id` bigint(20) unsigned NOT NULL,
  `dia_semana` tinyint(1) unsigned NOT NULL COMMENT 'os dias da semana começam no domingo com zero, e vão até sábado com 6',
  `horario_inicio` time DEFAULT NULL,
  `horario_fim` time DEFAULT NULL,
  `sem_atendimento` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = com atendimento\n1 = sem atendimento',
  PRIMARY KEY (`id`),
  KEY `fk_tb_profissional_dia_atendimento_tb_profissional1_idx` (`profissional_id`),
  CONSTRAINT `fk_tb_profissional_dia_atendimento_tb_profissional1` FOREIGN KEY (`profissional_id`) REFERENCES `tb_profissional` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_profissional_dia_atendimento`
--

LOCK TABLES `tb_profissional_dia_atendimento` WRITE;
/*!40000 ALTER TABLE `tb_profissional_dia_atendimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_profissional_dia_atendimento` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_dia_atendimento_AFTER_INSERT` AFTER INSERT ON `tb_profissional_dia_atendimento`
 FOR EACH ROW BEGIN
	# replicando para o log
	INSERT INTO tb_log_profissional_dia_atendimento
		( profissional_dia_atendimento_id, profissional_id, dia_semana, horario_inicio, horario_fim, sem_atendimento )
	VALUES
		( NEW.id, NEW.profissional_id, NEW.dia_semana, NEW.horario_inicio, NEW.horario_fim, NEW.sem_atendimento );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_dia_atendimento_AFTER_UPDATE` AFTER UPDATE ON `tb_profissional_dia_atendimento`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.profissional_id != OLD.profissional_id OR
        NEW.dia_semana != OLD.dia_semana OR
        NEW.horario_inicio != OLD.horario_inicio OR
        NEW.horario_fim != OLD.horario_fim OR
        NEW.sem_atendimento != OLD.sem_atendimento )
	
    THEN
		  
		INSERT INTO tb_log_profissional_dia_atendimento
			( profissional_dia_atendimento_id, profissional_id, dia_semana, horario_inicio, horario_fim, sem_atendimento )
		VALUES
			( NEW.id, OLD.profissional_id, OLD.dia_semana, OLD.horario_inicio, OLD.horario_fim, OLD.sem_atendimento );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_dia_atendimento_AFTER_DELETE` AFTER DELETE ON `tb_profissional_dia_atendimento`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_profissional_dia_atendimento
		SET original_deletado = 1
	WHERE profissional_dia_atendimento_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_profissional_horario_bloqueado`
--

DROP TABLE IF EXISTS `tb_profissional_horario_bloqueado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_profissional_horario_bloqueado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profissional_id` bigint(20) unsigned NOT NULL,
  `dia_horario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tb_profissional_horario_bloqueado_tb_profissional1_idx` (`profissional_id`),
  CONSTRAINT `fk_tb_profissional_horario_bloqueado_tb_profissional1` FOREIGN KEY (`profissional_id`) REFERENCES `tb_profissional` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_profissional_horario_bloqueado`
--

LOCK TABLES `tb_profissional_horario_bloqueado` WRITE;
/*!40000 ALTER TABLE `tb_profissional_horario_bloqueado` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_profissional_horario_bloqueado` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_horario_bloqueado_AFTER_INSERT` AFTER INSERT ON `tb_profissional_horario_bloqueado`
 FOR EACH ROW BEGIN
	# replicando para o log
	INSERT INTO tb_log_profissional_horario_bloqueado
		( profissional_horario_bloqueado_id, profissional_id, dia_horario )
	VALUES
		( NEW.id, NEW.profissional_id, NEW.dia_horario );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_horario_bloqueado_AFTER_UPDATE` AFTER UPDATE ON `tb_profissional_horario_bloqueado`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.profissional_id != OLD.profissional_id OR
        NEW.dia_horario != OLD.dia_horario )
	
    THEN
		  
		INSERT INTO tb_log_profissional_horario_bloqueado
			( profissional_horario_bloqueado_id, profissional_id, dia_horario )
		VALUES
			( NEW.id, OLD.profissional_id, OLD.dia_horario );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_horario_bloqueado_AFTER_DELETE` AFTER DELETE ON `tb_profissional_horario_bloqueado`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_profissional_horario_bloqueado
		SET original_deletado = 1
	WHERE profissional_horario_bloqueado_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_profissional_servico`
--

DROP TABLE IF EXISTS `tb_profissional_servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_profissional_servico` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `profissional_id` bigint(20) unsigned NOT NULL,
  `servico` varchar(250) NOT NULL COMMENT 'nome do serviço oferecido',
  `servico_padrao_id` bigint(20) DEFAULT NULL COMMENT 'setado apenas se este for um serviço padrão do sistema',
  `valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `data_inserido` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_modificacao` timestamp NULL DEFAULT NULL COMMENT 'data em que o profissional viu este serviço, se tiver esta informação, ele viu e salvou o valor',
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ativo\n1 = inativo\n\nusado quando o profissional não deseja mais prestar esse serviço.\n\nMas não pode ser removido, por causa do histórico de pagamentos relacionados',
  `visibilidade` enum('PUBLICO','PRIVADO') DEFAULT 'PUBLICO',
  PRIMARY KEY (`id`),
  KEY `fk_tb_profissional_servico_tb_profissional1_idx` (`profissional_id`),
  KEY `fk_tb_profissional_servico_tb_admin_servico_padrao1_idx` (`servico_padrao_id`),
  CONSTRAINT `fk_tb_profissional_servico_tb_admin_servico_padrao1` FOREIGN KEY (`servico_padrao_id`) REFERENCES `tb_admin_servico_padrao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_profissional_servico_tb_profissional1` FOREIGN KEY (`profissional_id`) REFERENCES `tb_profissional` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_profissional_servico`
--

LOCK TABLES `tb_profissional_servico` WRITE;
/*!40000 ALTER TABLE `tb_profissional_servico` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_profissional_servico` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_servico_AFTER_INSERT` AFTER INSERT ON `tb_profissional_servico`
 FOR EACH ROW BEGIN
	# replicando para o log
	INSERT INTO tb_log_profissional_servico_alteracao
		( servico_id, profissional_id, servico, valor, data_inserido, inativo, data_modificacao )
	VALUES
		( NEW.id, NEW.profissional_id, NEW.servico, NEW.valor, NEW.data_inserido, NEW.inativo, NEW.data_modificacao );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_servico_AFTER_UPDATE` AFTER UPDATE ON `tb_profissional_servico`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.servico != OLD.servico OR
        NEW.valor != OLD.valor OR
        NEW.data_inserido != OLD.data_inserido OR
        NEW.inativo != OLD.inativo OR
        NEW.data_modificacao != OLD.data_modificacao )
	
    THEN
		  
		INSERT INTO tb_log_profissional_servico_alteracao
			( servico_id,    profissional_id,     servico,     valor,     data_inserido,     inativo,     data_modificacao )
		VALUES
			( NEW.id,    OLD.profissional_id, OLD.servico, OLD.valor, OLD.data_inserido, OLD.inativo, OLD.data_modificacao );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_profissional_servico_AFTER_DELETE` AFTER DELETE ON `tb_profissional_servico`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_profissional_servico_alteracao
		SET original_deletado = 1
	WHERE servico_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_slider`
--

DROP TABLE IF EXISTS `tb_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_slider` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `botao_texto` varchar(100) NOT NULL,
  `url` varchar(455) NOT NULL,
  `imagem` varchar(455) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `criado_por_usuario_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_vantagem_tb_usuario1_idx` (`criado_por_usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_slider`
--

LOCK TABLES `tb_slider` WRITE;
/*!40000 ALTER TABLE `tb_slider` DISABLE KEYS */;
INSERT INTO `tb_slider` VALUES (1,'Logdialog, você melhor a cada dia.','Cadastre-se','https://logdialog.com.br/cadastro','https://logdialog.com.br/storage/app/public/site/sliders//8eb050353a9ed9f8aeaef240ebe7513f_.png','2017-04-13 21:02:59',14),(2,'Orientação Psicológica mesmo quando estiver viajando.','Cadastre-se','https://logdialog.com.br/cadastro','https://logdialog.com.br/storage/app/public/site/sliders//df9caa5df6350113267349d456374ebe_.png','2017-04-14 00:14:28',9),(3,'Seja atendido onde e quando você preferir.','Cadastre-se','https://logdialog.com.br/cadastro','https://logdialog.com.br/storage/app/public/site/sliders//a649b6facef83a100c57a8d1113e8143_.png','2017-04-14 00:14:59',9);
/*!40000 ALTER TABLE `tb_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_notificacao`
--

DROP TABLE IF EXISTS `tb_tipo_notificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_notificacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_notificacao`
--

LOCK TABLES `tb_tipo_notificacao` WRITE;
/*!40000 ALTER TABLE `tb_tipo_notificacao` DISABLE KEYS */;
INSERT INTO `tb_tipo_notificacao` VALUES (1,'novo_inbox'),(2,'solicitacao_agenda'),(3,'agendamento_confirmado'),(4,'agendamento_cancelado'),(5,'agendamento_lembrete'),(6,'pagamento_aguardando'),(7,'pagamento_confirmado'),(8,'pagamento_erro'),(9,'sistema_aviso');
/*!40000 ALTER TABLE `tb_tipo_notificacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_token_reset_senha`
--

DROP TABLE IF EXISTS `tb_token_reset_senha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_token_reset_senha` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `de_usuario_id` bigint(20) unsigned NOT NULL,
  `token` varchar(40) NOT NULL,
  `data_gerado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_expiracao` timestamp NULL DEFAULT NULL,
  `usado` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = nao usado\n1 = usado',
  PRIMARY KEY (`id`),
  KEY `fk_tb_log_token_reset_senha_tb_usuario1_idx` (`de_usuario_id`),
  CONSTRAINT `fk_tb_log_token_reset_senha_tb_usuario1` FOREIGN KEY (`de_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_token_reset_senha`
--

LOCK TABLES `tb_token_reset_senha` WRITE;
/*!40000 ALTER TABLE `tb_token_reset_senha` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_token_reset_senha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` text COMMENT 'esta coluna armazena o token/id no caso de autenticação pelas redes sociais, ou simplesmente o nickname local',
  `email` varchar(255) DEFAULT NULL,
  `senha` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL COMMENT 'campo para lembrar sessão do usuário',
  `grupo_sistema_id` bigint(20) unsigned NOT NULL,
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_ativado` timestamp NULL DEFAULT NULL,
  `via_facebook` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = não\n1 = sim',
  `via_linkedin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = não\n1 = sim',
  `ultimo_login` timestamp NULL DEFAULT NULL,
  `sessao_token_registro` varchar(255) DEFAULT NULL,
  `inativo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = ativo\n1 = inativo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`(100)),
  KEY `idx` (`username`(100),`email`(100)),
  KEY `fk_tb_usuario_tb_grupo_sistema1_idx` (`grupo_sistema_id`),
  CONSTRAINT `fk_tb_usuario_tb_grupo_sistema1` FOREIGN KEY (`grupo_sistema_id`) REFERENCES `tb_grupo_sistema` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20000 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario`
--

LOCK TABLES `tb_usuario` WRITE;
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` VALUES (15,'admin@logdialog.com.br','admin@logdialog.com.br','$2y$10$eQymcF4YWhOEeBrSAEKNnegF37S6abSG0W3a6s3XKE7t9ylN1ysqO','BjBjxZu0LRvSH4ApHO8vMT2N7BKwRACGH6zBvFMn2mnbzJhjsSmfS22KXgS5',3,'2017-04-14 02:54:41','2017-04-13 03:00:00',0,0,NULL,NULL,0);
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_usuario_AFTER_INSERT` AFTER INSERT ON `tb_usuario`
 FOR EACH ROW BEGIN
	# replicando para o log
    INSERT INTO tb_log_usuario_alteracao
		( usuario_id, username, email, senha, inativo, grupo_sistema_id )
	VALUES
		( NEW.id, NEW.username, NEW.email, NEW.senha, NEW.inativo, NEW.grupo_sistema_id );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_usuario_AFTER_UPDATE` AFTER UPDATE ON `tb_usuario`
 FOR EACH ROW BEGIN
	# atualizar a tabela de log/hist�rico, sempre que houver algum update
    # apenas dados importantes
    IF( NEW.username != OLD.username OR 
        NEW.email != OLD.email OR
        NEW.senha != OLD.senha OR 
        NEW.inativo != OLD.inativo OR
        NEW.grupo_sistema_id != OLD.grupo_sistema_id)
	
    THEN
		
        INSERT INTO tb_log_usuario_alteracao
			( usuario_id, username, email, senha, inativo, grupo_sistema_id )
		VALUES
			( NEW.id, OLD.username, OLD.email, OLD.senha, OLD.inativo, OLD.grupo_sistema_id );
	
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`logdialog`@`localhost`*/ /*!50003 TRIGGER `tb_usuario_AFTER_DELETE` AFTER DELETE ON `tb_usuario`
 FOR EACH ROW BEGIN
	# adicionando flag de removido
    UPDATE tb_log_usuario_alteracao
		SET original_deletado = 1
	WHERE usuario_id = OLD.id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_usuario_has_administrador`
--

DROP TABLE IF EXISTS `tb_usuario_has_administrador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_has_administrador` (
  `tb_usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`tb_usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario_has_administrador`
--

LOCK TABLES `tb_usuario_has_administrador` WRITE;
/*!40000 ALTER TABLE `tb_usuario_has_administrador` DISABLE KEYS */;
INSERT INTO `tb_usuario_has_administrador` VALUES (15);
/*!40000 ALTER TABLE `tb_usuario_has_administrador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario_orientacao`
--

DROP TABLE IF EXISTS `tb_usuario_orientacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_orientacao` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resposta_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '0 = resposta não,\nint = resposta relacionada',
  `pergunta_id` bigint(20) unsigned NOT NULL,
  `respondido_usuario_id` bigint(20) unsigned DEFAULT NULL COMMENT 'se ficar vazio, é que o usuário que respondeu não estava logado',
  `data_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sessao_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_usuario_orientacao_tb_orientacao_pergunta1_idx` (`pergunta_id`),
  KEY `fk_tb_usuario_orientacao_tb_usuario1_idx` (`respondido_usuario_id`),
  CONSTRAINT `fk_tb_usuario_orientacao_tb_orientacao_pergunta1` FOREIGN KEY (`pergunta_id`) REFERENCES `tb_orientacao_pergunta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_usuario_orientacao_tb_usuario1` FOREIGN KEY (`respondido_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario_orientacao`
--

LOCK TABLES `tb_usuario_orientacao` WRITE;
/*!40000 ALTER TABLE `tb_usuario_orientacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_usuario_orientacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_vantagem`
--

DROP TABLE IF EXISTS `tb_vantagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_vantagem` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `conteudo` longtext NOT NULL,
  `imagem` varchar(455) NOT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `criado_por_usuario_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_vantagem_tb_usuario1_idx` (`criado_por_usuario_id`),
  CONSTRAINT `fk_tb_vantagem_tb_usuario1` FOREIGN KEY (`criado_por_usuario_id`) REFERENCES `tb_usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_vantagem`
--

LOCK TABLES `tb_vantagem` WRITE;
/*!40000 ALTER TABLE `tb_vantagem` DISABLE KEYS */;
INSERT INTO `tb_vantagem` VALUES (2,'Inovação','<p><span style=\"font-weight: 400;\">Seguindo alt&iacute;ssimos padr&otilde;es de seguran&ccedil;a e praticidade, a tecnologia Logdialog foi desenhada por profissionais de diversas &aacute;reas dedicados em atender, tanto as necessidades de pacientes, como de psic&oacute;logos, assim como viabilizar e difundir com excel&ecirc;ncia a sa&uacute;de mental online.</span><br /><i></i></p>\n<p><span style=\"font-weight: 400;\">Desenvolvemos uma ferramenta especializada em </span><span style=\"font-weight: 400;\">Orienta&ccedil;&atilde;o Psicol&oacute;gica Online</span><span style=\"font-weight: 400;\">, que integra as solu&ccedil;&otilde;es tecnol&oacute;gicas mais avan&ccedil;adas e seguras em comunica&ccedil;&atilde;o por videoconfer&ecirc;ncia de alta performance, agendamento e pagamento. &nbsp;</span></p>\n<p><span style=\"font-weight: 400;\">Primeiro site de </span><span style=\"font-weight: 400;\">Orienta&ccedil;&atilde;o Psicol&oacute;gica Online</span><span style=\"font-weight: 400;\"> a oferecer prontu&aacute;rio eletr&ocirc;nico protegido e possibilidade de envio de documentos em ambiente pr&oacute;prio e restrito.</span></p>','https://logdialog.com.br/storage/app/public/site//6cf697feccbbb6a87621e5964e68427d_.png','2017-04-13 18:09:04',15),(3,'Segurança & Privacidade','<p><span style=\"font-weight: 400;\">Consideramos o sigilo como um princ&iacute;pio fundamental que permite estabelecer um bom v&iacute;nculo entre paciente e profissional. Pensando nisso, </span><span style=\"font-weight: 400;\">seguimos as melhores pr&aacute;ticas da ind&uacute;stria para nos certificar que suas informa&ccedil;&otilde;es est&atilde;o protegidas.</span></p>\n<p><span style=\"font-weight: 400;\">Embora nenhum m&eacute;todo de transmiss&atilde;o pela Internet ou armazenamento eletr&ocirc;nico seja 100% seguro, n&oacute;s seguimos todos os requisitos e implementamos padr&otilde;es de seguran&ccedil;a adicionais aceitos pela ind&uacute;stria, de modo que, os dados do nosso servi&ccedil;o s&atilde;o criptografados </span><span style=\"font-weight: 400;\">(t&eacute;cnicas que transformam as informa&ccedil;&otilde;es em c&oacute;digos)</span><span style=\"font-weight: 400;\">.</span></p>\n<p><span style=\"font-weight: 400;\">O sistema de videoconfer&ecirc;ncia desenvolvido para Orienta&ccedil;&atilde;o Psicol&oacute;gica Online e o prontu&aacute;rio do profissional seguem o mesmo padr&atilde;o de seguran&ccedil;a.</span></p>\n<p><i><span style=\"font-weight: 400;\">Apesar de todo empenho e cuidado com a seguran&ccedil;a, o ambiente online n&atilde;o &eacute; livre de riscos. Recomendamos que os usu&aacute;rios do Logdialog tomem medidas preventivas como, manter o navegador atualizado, n&atilde;o fornecer sua senha para outra pessoa e alterar periodicamente a sua senha. Para mais informa&ccedil;&otilde;es, o Centro de Estudos, Respostas e Tratamento de Incidentes de Seguran&ccedil;a no Brasil disponibiliza em seu site a Cartilha de Seguran&ccedil;a para a Internet.</span></i></p>','https://logdialog.com.br/storage/app/public/site//6bee78f88f11b2b0d3fb4a1669297058_.png','2017-04-13 18:09:04',15),(4,'Fácil de usar','<p><span style=\"font-weight: 400;\">Simples de usar, o Logdialog &eacute; intuitivo e dispensa a necessidade conhecimento especializado. </span></p>\n<p><span style=\"font-weight: 400;\">Todo o processo, desde agendamento, at&eacute; o atendimento &eacute; realizado dentro do ambiente do site </span><a href=\"http://www.logdialog.com.br\"><span style=\"font-weight: 400;\">www.logdialog.com.br</span></a></p>\n<p><span style=\"font-weight: 400;\">O Logdialog pode ser acessado atrav&eacute;s de computador, tablet e celular. Basta utilizar o navegador de sua prefer&ecirc;ncia (Safari, GoogleCrome, Explorer) e digitar o endere&ccedil;o </span><a href=\"http://www.logdialog.com.br\"><span style=\"font-weight: 400;\">www.logdialog.com.br</span></a></p>\n<p><span style=\"font-weight: 400;\">Se voc&ecirc; j&aacute; utiliza algum destes navegadores &eacute; s&oacute; clicar no endere&ccedil;o </span><a href=\"http://www.logdialog.com.br\"><span style=\"font-weight: 400;\">www.logdialog.com.br</span></a></p>','https://logdialog.com.br/storage/app/public/site//8020e23dc6e9d219668cb10b351d8ac1_.png','2017-04-13 18:09:04',15),(5,'Agendamento','<p><span style=\"font-weight: 400;\">O sistema de agendamento &eacute; pr&aacute;tico e proativo. O paciente e psic&oacute;logo economizam tempo e trabalho para encontrar hor&aacute;rios e fazer o agendamento.</span></p>\n<p><span style=\"font-weight: 400;\">Para facilitar ainda mais, a agenda do Logdialog &eacute; compat&iacute;vel com o Google Agenda e com o iCall.</span></p>','https://logdialog.com.br/storage/app/public/site//c2da9c70b828a1f22005a18d4abae2a6_.png','2017-04-13 18:09:04',15),(6,'Comodidade e/ou praticidade e flexibilidade','<p><span style=\"font-weight: 400;\">As </span><span style=\"font-weight: 400;\">sess&otilde;es de orienta&ccedil;&atilde;o</span><span style=\"font-weight: 400;\"> podem ser realizadas em hor&aacute;rios e locais que sejam mais convenientes para paciente psic&oacute;logo. </span></p>\n<p><span style=\"font-weight: 400;\">Sem precisar se deslocar, &eacute; poss&iacute;vel encontrar e ser atendido pelo profissional que mais atende a sua necessidade. </span></p>\n<p><span style=\"font-weight: 400;\">O pagamento &eacute; realizado online e a cobran&ccedil;a ocorre somente depois que paciente e profissional &nbsp;derem \"OK\" confirmando o hor&aacute;rio agendado.</span></p>','https://logdialog.com.br/storage/app/public/site//448183c4d0014ce6489066a9152ade88_.png','2017-04-13 18:09:04',15),(12,'Profissionais qualificados & Controle de qualidade','<p><span style=\"font-weight: 400;\">Nosso processo de sele&ccedil;&atilde;o para cadastramento de psic&oacute;logos foi desenvolvido por profissionais qualificados e obedece crit&eacute;rios cient&iacute;ficos/ acad&ecirc;micos. &nbsp;</span></p>\n<p><span style=\"font-weight: 400;\">Tamb&eacute;m desenvolvemos um &aacute;gil e refinado processo treinamento para o cadastramento de nossos psic&oacute;logos.</span></p>\n<p><span style=\"font-weight: 400;\">Al&eacute;m da forma&ccedil;&atilde;o, as especialidades de atendimento e outras qualifica&ccedil;&otilde;es informadas, s&atilde;o comprovadas pelos profissionais antes de serem oferecidas pelo LogDialog.</span></p>\n<p><span style=\"font-weight: 400;\">As pesquisas de qualidade realizadas nos ajudam melhorar sua experi&ecirc;ncia de uso. </span></p>','https://logdialog.com.br/storage/app/public/site//08444e0376f37e3a9375bf7b7c46abf3_.png','2017-04-20 15:31:27',15);
/*!40000 ALTER TABLE `tb_vantagem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_videochamada`
--

DROP TABLE IF EXISTS `tb_videochamada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_videochamada` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agenda_id` bigint(20) unsigned NOT NULL,
  `data_inicio_chamada` timestamp NULL DEFAULT NULL,
  `data_fim_chamada` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tb_videochamada_tb_agenda1_idx` (`agenda_id`),
  CONSTRAINT `fk_tb_videochamada_tb_agenda1` FOREIGN KEY (`agenda_id`) REFERENCES `tb_agenda` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_videochamada`
--

LOCK TABLES `tb_videochamada` WRITE;
/*!40000 ALTER TABLE `tb_videochamada` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_videochamada` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-13 11:36:52
