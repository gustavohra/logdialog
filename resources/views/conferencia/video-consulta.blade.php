{{-- Este documento, não é de testes, é para o chat real das consultas --}}
{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

<?php $sucesso = false; ?>

@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

	{{-- menu principal --}}
    @yield('nav-menu-principal')

    <style>
    	#remote-video img,
    	#remote-video video,
    	#remote-video audio,

    	#local-video img,
    	#local-video video,
    	#local-video audio{
    		width: 100%;
    		height: 100%;
    	}

        .video-container{
            display: table;
            margin: 0 auto;
            text-align: center;
            border: 1px solid #d8d8d8;
            position: relative;
        }

        #bloco-notas{
            position: relative;
        }

        #bloco-notas h3{
            font-size: 20px;
            margin-top: 0;
            margin-bottom: 20px;
        }

            #bloco-notas span{
                font-size: 15px;
                display: block;
                color: #d8d8d8;
                text-align: right;
                margin-top: 20px;
                height: 20px;
            }

            #bloco-notas textarea{
                resize: vertical;
                width: 100%;
                height: 330px;
                border: 1px solid #d8d8d8;
            }

            #bloco-notas button{
                position: absolute;
                right: 0;
                top: 0;
                margin: 0;
                padding: 10px 15px;
                color: #fff;
                border-radius: 5px;
                background: #232973;
                border: none;
            }

            #bloco-notas .btn-inativo{
                opacity: 0.2;
            }

        #remote-video{
            display: table-cell;
            vertical-align: middle;
            width: 400px;
            height: 400px;
        }

        #local-video{
            position: absolute;
            right: 0;
            bottom: 0;
            z-index: 1;
            width: 100px;
            height: 100px;
        }
    </style>

    <div class="admincontent-wrap">
    	<section class="home-section" style="text-align: left;">

    		<div style="width: 80%; margin: 0 auto 80px;">
    			@if( Auth::user()->grupoSistema() == 'profissional' )
					<h5>Consulta com: <span id="nomeParceiro">{{ $agenda['nome_paciente'] }} {{ $agenda['sobrenome_paciente'] }}</span></h5>
				@else
					<h5>Consulta com: <span id="nomeParceiro">{{ $agenda['tratamento_profissional'] }} {{ $agenda['nome_profissional'] }} {{ $agenda['sobrenome_profissional'] }}</span></h5>
				@endif

				<p>Agendada para: {{ Carbon\Carbon::parse($agenda['data_compromisso'])->formatLocalized("%A, %d de %B, %Y às %H:%M") }}</p>
    		</div>

			{{-- verificando se está no horário da consulta
			ela é iniciada no horário marcado
			ou com até 5 minutos de atraso, depois disso é necessário remarcar a consulta --}}

			{{-- caso esteja cedo --}}
			@if(
				Carbon\Carbon::now()->timestamp <= Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp
			)

				<p style="text-align: center; margin-bottom: 300px;">
					<strong>Ainda não está na hora da sua consulta, por favor, mantenha esta página aberta até o horário marcado.</strong>

					<br>
					<br>
					Tempo restante: <span id="contagemRegressiva" style="font-size: 20px; white-space: nowrap;"><i>calculando...</i></span>
				</p>

			{{-- caso tenha passado cinco minutos do horário --}}
			@elseif( Carbon\Carbon::now()->diffInMinutes( Carbon\Carbon::parse($agenda['data_compromisso']) ) > 5 )

				<p style="text-align: center; margin-bottom: 300px;">
					<strong>Desculpe, mas você está atrasado para a consulta. O tempo limite excedeu os 5 (cinco) minutos de tolerância.

					<br>
					<br>

					Esta consulta deverá ser remarcada.</strong>
				</p>

			@else

				<?php $sucesso = true; ?>

				<div style="width: 80%; margin: 0 auto 80px;">

                    {{-- bloco que comporta os vídeos e o bloco de notas da consulta --}}
                    <div class="bloco-video">
                        <div class="row">
                            <div class="col-md-6">
                                {{-- Aqui aparece o vídeo do usuário remoto --}}
                                <div class="video-container">
                                    <div id="remote-video">

                                        @if( Auth::user()->grupoSistema() == 'profissional' )
                                            <p>Aguardando, {{ $agenda['nome_paciente'] }} {{ $agenda['sobrenome_paciente'] }} se conectar...</p>
                                        @else
                                            <p>Aguardando, {{ $agenda['tratamento_profissional'] }} {{ $agenda['nome_profissional'] }} {{ $agenda['sobrenome_profissional'] }} se conectar...</p>
                                        @endif

                                    </div>

                                    <div id="local-video"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- aqui fica o bloco de notas
                                que salva automaticamente a cada 20 segundos
                                primeiro é feita uma verificação se algo foi modificado
                                caso seja, é submetido ao servidor
                                de qualquer forma, o usuário terá o botão para salvar as notas manualmente --}}
                                <div id="bloco-notas">
                                    <h3>Anotações da consulta:</h3>
                                    <span class="status"></span>

                                    <button onclick="javascript:_salvarNotas();">Salvar nota!</button>

                                    <textarea cols="30" rows="10"></textarea>

                                    <script>
                                        $(document).ready(function(){
                                            $("#bloco-notas textarea").bind('change', function(){
                                                var tmpConteudo = $.trim( $(this).val() );

                                                // caso seja diferente do atual
                                                // e o time seja suprior a 20 segundos
                                                // iniciar envio da nota e inativar o botão temporariamente
                                                if( moment().diff( moment.unix( ultimoStamp ), 'seconds', true ) > 20 &&
                                                    salvando == false )
                                                {
                                                    _salvarNotas();
                                                }
                                            });
                                        });

                                        var ultimaNota = '';
                                        var ultimoStamp = moment().unix();
                                        var salvando = false;

                                        /**
                                         * Função para enviar ao servidor o conteúdo das notas para salvar
                                         */
                                        function _salvarNotas()
                                        {
                                            // apenas se ainda não estiver salvando
                                            if( !salvando )
                                            {
                                                // setando como em progresso
                                                salvando = true;

                                                // setando o botão como inativo
                                                $("#bloco-notas button").addClass('btn-inativo');

                                                // aviso
                                                $("#bloco-notas span").html('salvando...');

                                                if( $.trim( $("#bloco-notas textarea").val() ).length > 0 )
                                                {
                                                    // enviando ao servidor
                                                    $.ajax({
                                                        url : '/anotacao/durante-consulta',
                                                        type: 'post',
                                                        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                                                        data: {
                                                            _token: $('meta[name=_token]').attr('content'),
                                                            'conteudo' : $.trim( $("#bloco-notas textarea").val() ),
                                                            'token' : '{{ Hash::make($agenda['agenda_id'] . '_' . Auth::user()->toArray()['id']) }}',
                                                            'agendaId' : '{{ $agenda['agenda_id'] }}'
                                                        },
                                                        success: function()
                                                        {
                                                            // setando status
                                                            salvando = false;

                                                            // ativar o botão
                                                            $("#bloco-notas button").removeClass('btn-inativo');

                                                            // aviso
                                                            $("#bloco-notas span").html('Anotação salva em: ' + moment().format('DD/MM/YYYY HH:mm:ss'));

                                                            // atualizando timestamp
                                                            ultimoStamp = moment().unix();
                                                        },
                                                        error: function()
                                                        {
                                                            // setando status
                                                            salvando = false;

                                                            // ativar o botão
                                                            $("#bloco-notas button").removeClass('btn-inativo');

                                                            // aviso
                                                            $("#bloco-notas span").html('Anotação salva em: ' + moment().format('DD/MM/YYYY HH:mm:ss'));

                                                            // atualizando timestamp
                                                            ultimoStamp = moment().unix();
                                                        }
                                                    });
                                                }
                                                else
                                                {
                                                    // setando status
                                                    salvando = false;

                                                    // ativar o botão
                                                    $("#bloco-notas button").removeClass('btn-inativo');

                                                    // aviso
                                                    $("#bloco-notas span").html('Não há conteúdo!');
                                                }
                                            }
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- aqui seriam os botões para encerrar a consulta
                    definir como será esta etapa de encerramento
                    se será por tempo, daí teremos um contador ou um botão --}}
					<div style="text-align: center; display: none;">
						<div style="display: inline-table; margin: 30px;">
							Encerrar consulta
						</div>

						<div style="display: inline-table; margin: 30px;">
							Opa
						</div>
					</div>

				</div>

			@endif
		</section>
    </div>

    <div style="display: none;">
        <br><br><br>
        <h6>Log:</h6>
        <div id="log-content"></div>
    </div>

    <script src="{{ asset('js/countdown.min.js') }}"></script>

    {{-- caso esteja ok com a consulta --}}
	@if( $sucesso )
	    <script src="https://media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
	  	<script src="https://media.twiliocdn.com/sdk/js/conversations/v0.13/twilio-conversations.min.js"></script>

	  	<script>
    		var conversationsClient;
    		var activeConversation;
    		var previewMedia;

    		// check for WebRTC
    		if(!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia){
      			alert('Não é possível iniciar a conferência, seu navegador não suporta o recurso de WebRTC, recomendados a utilização da versão mais recente do Google Chrome.');
    		}

    		// generate an AccessToken in the Twilio Account Portal - https://www.twilio.com/user/account/video/testing-tools
    		var accessToken = "{{ $tokenTwilio }}";

    		// use our AccessToken to generate an AccessManager object
    		var accessManager = new Twilio.AccessManager(accessToken);

    		// create a Conversations Client and connect to Twilio
    		conversationsClient = new Twilio.Conversations.Client(accessManager);

    		conversationsClient.listen().then(
      			clientConnected,
      			function(error)
      			{
        			console.log(error);
        			log('Could not connect to Twilio: ' + error.message);
      			}
    		);

    		{{-- o convite só pode ser feito pelo profissional --}}
    		@if( Auth::user()->grupoSistema() == 'profissional' )
    			/**
	    		 * Função para enviar o convite para o outro usuário
	    		 */
	    		function convidarUsuario( identity )
	    		{
	    			// convidando um novo usuário
	    			// que é a outra parte desta consulta
	    			var inviteTo = identity;

	    			if (activeConversation) {
	      				// add a participant
	      				activeConversation.invite(inviteTo);
	    			} else {
	      				// create a conversation
	      				var options = {};

	      				if(previewMedia){
	        				options.localMedia = previewMedia;
	      				}

	  					conversationsClient.inviteToConversation(inviteTo, options).then(
	        				conversationStarted,
	        				function (error) {
	          					// aguardar dois segundos e convidar novamente
	          					setTimeout(function(){
	          						activeConversation = null;
	          						convidarUsuario( identity );
	          					}, 2000);
	        				}
	      				);
	    			}
	    		}
    		@endif

    		// successfully connected!
    		function clientConnected()
    		{
      			// meu identificador
      			// console.log( conversationsClient.identity );

      			conversationsClient.on('invite', function (invite) {
        			log('Incoming invite from: ' + invite.from);
        			invite.accept().then(conversationStarted);
      			});

      			{{-- o convite só pode ser feito pelo profissional --}}
    			@if( Auth::user()->grupoSistema() == 'profissional' )
    				activeConversation = null;
      				// convidando o outro usuário
      				convidarUsuario( '{{ $convidado }}' );
      			@endif
    		};

    		// conversation is live
    		function conversationStarted(conversation) {
      			log('In an active Conversation');
      			activeConversation = conversation;

				// draw local video, if not already previewing
      			if (!previewMedia) {
        			conversation.localMedia.attach('#local-video');
      			}

				// when a participant joins, draw their video on screen
      			conversation.on('participantConnected', function (participant) {
    				log("Participant '" + participant.identity + "' connected");

    				// removendo o texto de espera
    				$("#remote-video p").remove();

        			participant.media.attach('#remote-video');
      			});

				// when a participant disconnects, note in log
      			conversation.on('participantDisconnected', function (participant) {
        			log("Participant '" + participant.identity + "' disconnected");

        			// adicionando aviso de desconectado
        			$("#remote-video").html('');

        			$("#remote-video").html( $('p').html("Conexão perdida com " + $("#nomeParceiro").html() +", aguardando reconexão...") );

        			$("#remote-video p").each(function(idx){
        				if( idx > 0 && $("#remote-video p").size() > 1 )
        					$(this).remove();
        			});

        			{{-- apenas profisisonais podem convidar --}}
        			@if( Auth::user()->grupoSistema() == 'profissional' )
	        			// aguardar dois segundos e convidar novamente
	  					setTimeout(function(){
	  						activeConversation = null;
	  						convidarUsuario( '{{ $convidado }}' );
	  					}, 2000);
	  				@endif
      			});

				// when the conversation ends, stop capturing local video
      			conversation.on('ended', function (conversation) {
        			log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
        			conversation.localMedia.stop();
        			conversation.disconnect();
        			activeConversation = null;
      			});
    		};

    		// função para exibir a minha câmera
    		function mostrarMinhaCamera(){
      			if (!previewMedia) {
        			previewMedia = new Twilio.Conversations.LocalMedia();
        			Twilio.Conversations.getUserMedia().then(
          				function (mediaStream) {
            				previewMedia.addStream(mediaStream);
            				previewMedia.attach('#local-video');
          				},

          				function (error) {
            				console.error('Não é possível acessar a câmera local', error);
            				log('Não é possível acessar a câmera e microfone.');
          				}
        			);
      			};
    		};

    		// carregando a imagem da câmera atual
    		$(document).ready(function(){
    			mostrarMinhaCamera();
    		});

    		// activity log
    		function log(message) {
      			document.getElementById('log-content').innerHTML = message;
    		};
  		</script>
	@endif

    <script>

    	{{-- contagem regressiva --}}
    	@if(
			Carbon\Carbon::now()->timestamp <= Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp
    	)

    		countdown.setLabels(
				' milissegundo| segundo| minuto| hora| dia| semana| mês| ano| década| século| milênio',
				' milissegundos| segundos| minutos| horas| dias| semanas| meses| anos| décadas| séculos| milênios',
				' e ',
				', ',
				'agora');

	    	var timer = countdown(
	    		function(ts){
	    			atualizarContador( ts );
	    		},
	    		new Date({{ Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp }} * 1000)
	    	);

            var terminadoCronometro = false;

	    	/**
	    	 * Função para gerar a string da contagem regressiva
	    	 */
	    	function atualizarContador( ts )
	    	{
                if( ts.toString() == 'agora' )
                {
                    terminadoCronometro = true;

                    setTimeout(function(){
                        window.top.location.reload();
                    }, 10000);
                }
                else if( !terminadoCronometro )
                {
                    $("#contagemRegressiva").html( ts.toString() );
                }
                else
                {
                    $("#contagemRegressiva").html( "Iniciando consulta..." );
                }
	    	}

    	@endif;

    </script>

@stop