{{-- Este documento é a sala de espera, quando ainda não está na hora da consulta --}}
{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
	<link href="{{ asset('css/conference.css') }}" rel="stylesheet">
@stop

@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    <style>
    	.circulo-wave{
    		width: 50px;
    		height: 50px;
    		border-radius: 100%;
    		overflow: hidden;
    		text-align: center;
    		display: inline-block;
    		position: relative;
    	}

    	.circulo-wave:before{
    		position: absolute;
    		left: 0;
    		top: 0;
    		display: block;
    		content: "";
    		background: url("{{ asset('images/sound-circle.svg') }}") no-repeat center;
    		background-size: 100%;
    		width: 50px;
    		height: 50px;

    		-webkit-animation-name: rotate;
		  	-webkit-animation-duration:10s;
		  	-webkit-animation-iteration-count:infinite;
		  	-webkit-animation-timing-function:linear;

		  	-moz-animation-name: rotate;
		  	-moz-animation-duration:10s;
		  	-moz-animation-iteration-count:infinite;
		  	-moz-animation-timing-function:linear;
    	}

    	@-webkit-keyframes rotate {
		  from {-webkit-transform:rotate(0deg);}
		  to {  -webkit-transform:rotate(360deg);}
		}

		@-moz-keyframes rotate {
		  from {-moz-transform:rotate(0deg);}
		  to {  -moz-transform:rotate(360deg);}
		}

    	.circulo-wave canvas{
    		width: 32px;
    		margin-top: 6px;
    	}
    </style>

	<div id="conference-wrap">
	    <div class="container">
	        <div class="conference-content">
	            <div class="row">
	                <div class="col-lg-4 col-lg-offset-4">
	                    <div id="waiting-message">
	                        <h3>Aproveite a música</h3>

	                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>

	                        {{-- imagem padrão, quando o navegador não suporta o sound wave --}}
	                        <div style="display: none;">
	                        	<img src="{{ asset("images/sound-icon.png") }}" alt="" id="imagemPadrao">
	                        </div>

	                        {{-- canvas, para suporte ao audio wave --}}
	                        <div class="circulo-wave">
	                        	<canvas id="audioWave" width="50" height="50"></canvas>
	                        	<div id="myaudio" style="display: none;"></div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div id="status-message">
	                    <p><span id="contagemRegressiva" style="white-space: nowrap;"><i>calculando...</i>.</p>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	{{-- campo oculto com o caminho do áudio no sistema --}}
	<input type="hidden" id="audioPath" value="{{ asset("mp3/we-are-alive.mp3") }}">

	{{-- define o volume do áudio, valores entre 0 e 1, ex: 0.2 equivale a 20% do volume --}}
	{{-- atualmente é necessário informar o volume pela url, se não for informado o som não toca --}}
	{{-- caso deseje deixar fixo um valor e tocar assim que a página for carregada, altere a propriedade value para ficar com o valor desejado, ex: value="0.9" --}}
	<input type="hidden" id="audioVolume" value="{{ isset($_GET['volume']) ? $_GET['volume'] : '0' }}">

	<script src="{{ asset('js/logDialog/wead-RealAudioWave.js') }}"></script>

	<script src="{{ asset('js/countdown.min.js') }}"></script>

	<script>
		setInterval(function(){
            $.get('/');
        }, 60000);


    	{{-- contagem regressiva --}}
    	@if(
			Carbon\Carbon::now()->timestamp <= Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp
    	)

    		countdown.setLabels(
				' milissegundo| segundo| minuto| hora| dia| semana| mês| ano| década| século| milênio',
				' milissegundos| segundos| minutos| horas| dias| semanas| meses| anos| décadas| séculos| milênios',
				' e ',
				', ',
				'agora');

	    	var timer = countdown(
	    		function(ts){
	    			atualizarContador( ts );
	    		},
	    		new Date({{ Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp }} * 1000)
	    	);

            var terminadoCronometro = false;

	    	/**
	    	 * Função para gerar a string da contagem regressiva
	    	 */
	    	function atualizarContador( ts )
	    	{
                if( ts.toString() == 'agora' )
                {
                    terminadoCronometro = true;

                    setTimeout(function(){
                        window.top.location.reload();
                    }, 10000);
                }
                else if( !terminadoCronometro )
                {
                    $("#contagemRegressiva").html( "Sua consulta começa em " + ts.toString() );
                }
                else
                {
                    $("#contagemRegressiva").html( "Iniciando consulta..." );
                }
	    	}

    	@endif;

    </script>
@stop