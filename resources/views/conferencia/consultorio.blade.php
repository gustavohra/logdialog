{{-- Este documento, não é de testes, é para o chat real das consultas --}}
{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/conference.css') }}" rel="stylesheet">
@stop

<style>
    #remote-video video,
    #remote-video audio,

    #local-video video,
    #local-video audio{
        width: 100%;
    }

    #remote-video img,
    #local-video img{
        display: none;
    }

    #remote-video p{
        display: block;
        padding-top: 200px;
        height: 773px;
        text-align: center;
        color: #fff;
    }

    #conference-controllers{
        z-index: 2;
    }

    #imgconference.active{
        padding-top: 0 !important;
    }

    #conference-player{
        background: #000;
    }

    #messagefield-wrap input[type="text"]{
        height: 90px !important;
    }

    .mensagens-chat .bold{
        width: 55%;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        display: inline-block;
    }

    i.mce-i-fa:before {
        content: "\f00c";
        font-family: "FontAwesome";
    }

    /*.cf-salvar button,
    .cf-salvar i{
        color: #009646 !important;
        text-transform: uppercase !important;
    }*/

    #textoNotasEditor{
        width: 100%;
        height: 100%;
    }

    .nota-atual{
        color: #2a9fe2
    }

    #bloco-notas span{
        padding: 5px 15px;
        font-size: 13px;
        font-weight: normal;
        display: block;
        text-align: center;
    }
</style>

@section('conteudo')

    <div id="conference-wrap">
        <div class="container">
            <div id="conference-player">
                <div id="conference-controllers">
                    <div class="cc-logo" onclick="encerrarChamada(0)">
                        <img src="{{ asset('images/icon-logo.png') }}" alt="">
                    </div>
                    <div class="cc-functions hidden-sm hidden-xs">
                        <div id="btn-chat" class="ccfunctions-btn"></div>
                        <div id="btn-notes" class="ccfunctions-btn"></div>
                    </div>
                    <div class="cc-actions">
                        <div class="cc-mic cc-icons" onclick="alternarMute()">
                            <span class="fa-stack fa-2x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i id="icone-mic" class="fa fa-microphone fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>
                        <div class="cc-call cc-icons" onclick="encerrarChamada(0)">
                            <span class="fa-stack fa-2x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i id="icone-fone" class="fa fa-phone fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>
                        <!-- <a href=""><i class="fa fa-microphone" aria-hidden="true"></i></a> -->
                    </div>
                    <div class="cc-fullscreen" onclick="fullScreenConference()">
                        <span class="fa-stack fa-2x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-arrows-alt fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div id="local-video" class="cc-miniscreen">
                        <img src="{{ asset('images/conference-cliente2.jpg') }}" alt="" class="imgfull">
                    </div>
                </div>

                <!-- -------- CHAT -------- -->
                <div id="conference-chat">
                    <div class="cfheader">
                        <div class="cftitle">CHAT</div>
                        <div class="cfclosebtn"><i class="fa fa-times" aria-hidden="true"></i></div>
                        <div class="clear"></div>
                    </div>
                    <div class="cfcontent cfchat">
                        <div class="chat-wrap">
                            <ul class="mensagens-chat" id="listaChat">
                                {{-- listando as ultimas mensagens trocadas por estes usuários --}}

                                @foreach( $mensagens as $k => $item )
                                    <li class="chat-{{ $item['minha_mensagem'] == 1 ? 'right' : 'left' }}" data-timestamp="{{ \Carbon\Carbon::parse( $item['data_mensagem'] )->timestamp }}">
                                        <div class="row">
                                            <div class="col-lg-12 no-padding-right">
                                                <span class="bold" title="{{ $item['de_nome'] }}">{{ $item['de_nome'] }}</span>
                                                <span class="mensagem-time">
                                                    {{ \Carbon\Carbon::parse( $item['data_mensagem'] )->format("H:i") }}
                                                        -
                                                    {{ \Carbon\Carbon::parse( $item['data_mensagem'] )->format("d/m") }}
                                                </span>
                                                <div class="mensagem-text-{{ $item['minha_mensagem'] == 1 ? 'right' : 'left' }} mensagem-tip">
                                                    <div class="mensagem-text-inside">
                                                        {{ $item['mensagem'] }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                            <div id="messagefield-wrap">
                                <form id="formChat" method="post" class="message-input">
                                    <input type="text">
                                    <button type="submit" class="btn btn-sendmessage">Enviar</button>
                                </form>
                            </div>
                        </div><!--  /.chat-wrap -->
                    </div>
                </div>

                <!-- -------- NOTES LIST -------- -->
                <div id="conference-notes">
                    <div class="cfheader">
                        <div class="cfcancelbtn">Notas</div>
                    </div>
                    <div class="cfcontent">
                        <ul id="notes-list">

                            @foreach( $notas as $k => $nota )
                                <li class="{{ $nota['relacionado_agenda_id'] == $agenda['agenda_id'] ? 'nota-atual' : '' }}">
                                    <div id="note_{{ $nota['id'] }}" class="notes-item" onclick="editarNota({{ $nota['relacionado_agenda_id'] == $agenda['agenda_id'] ? 1 : 0 }}, {{ $nota['id'] }})">
                                        <p>
                                            @if( strlen( $nota['titulo'] ) > 0 )
                                                {{ $nota['titulo'] }}
                                            @else
                                                Consulta de {{ \Carbon\Carbon::parse( $nota['data_anotacao'] )->format("d/m/Y") }}
                                            @endif
                                        </p>
                                        <p class="notes-date">{{ \Carbon\Carbon::parse( $nota['data_anotacao'] )->format("d/m/Y") }}</p>

                                        {{-- conteúdo da nota, oculto --}}
                                        <div class="conteudoNota" style="display: none;">{{ $nota['conteudo'] }}</div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

                <!-- -------- NOTES NEW -------- -->
                <div id="conference-notes-open">
                    <div class="cfheader">
                        <div class="cfcancelbtn"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Cancelar</div>
                    </div>
                    <div class="cfcontent">
                        <div class="cfeditor">

                            <div id="bloco-notas">
                                <span>Anotação ainda não salva</span>
                            </div>

                            <form id="formNotas">
                                <textarea id="textoNotasEditor"></textarea>
                            </form>

                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

                <div id="imgconference">
                    <div id="remote-video">
                        <img src="{{ asset('images/conference-cliente.jpg') }}" alt="" class="imgfull">

                        @if( Auth::user()->grupoSistema() == 'profissional' )
                            <p>Aguardando, {{ $agenda['nome_paciente'] }} {{ $agenda['sobrenome_paciente'] }} se conectar...</p>
                        @else
                            <p>Aguardando, {{ $agenda['tratamento_profissional'] }} {{ $agenda['nome_profissional'] }} {{ $agenda['sobrenome_profissional'] }} se conectar...</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/criaturo-conference.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="{{ asset('js/classie.js') }}"></script>
    <script src="{{ asset('js/cbpAnimatedHeader.js') }}"></script>

    {{-- TinyMCE --}}
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>

    {{-- editor WYSIWYG --}}
    <script>
    var tinyEditor = null;

        $(document).ready(function(){
            tinymce.init({
                selector: '#textoNotasEditor',
                toolbar: 'undo redo bold italic underline custom_save',
                menubar: false,
                statusbar: false,
                height: 600,
                language_url: '{{ asset('js/tinymce/langs/pt_BR.js') }}',
                setup: function (editor) {
                    editor.addButton('custom_save',{
                        text: 'Salvar',
                        icon: 'fa fa-check',
                        onclick: function(){
                            salvarNota( editor.getContent() );
                        },
                        onpostrender: function(){
                            $("#" + this._id ).addClass('cf-salvar');
                        }
                    });
                },
                init_instance_callback: function (editor) {
                    editor.on('Change', function (e) {
                        // sempre que for detectada mudança no conteúdo
                        // pegar o conteúdo do editor
                        // e salvar no html, para evitar perda quando trocar de nota
                        $(".nota-atual").eq(0).find('.conteudoNota').text( editor.getContent() );
                    });
                }
            });

            // avisar o usuário, ao tentar sair da página no meio da chamada
            $(window).bind('beforeunload', function() {
                // provavelmente esta mensagem será ignorada
                // pois os navegadores não permitem que o site impeça os usuário de sairem
                // nestes casos, é exibido um aviso padrão do navegador
                return "Se você sair desta página, a consulta será encerrada. Deseja prosseguir?";
            });
        });

        /**
         * Esta função exibe editor da nota
         */
        function editarNota( corrente, notaId )
        {
            $("#conference-notes").css('display', 'none');
            $("#conference-notes-open").css('display', 'block');
            $("#conference-controllers").css('width', '75%');

            // se for a nota da consulta atual
            if( corrente == 1 )
            {
                $("#bloco-notas span").html('Anotação ainda não salva');

                // permitir edição
                tinyMCE.get('textoNotasEditor').setMode('design');

                // carregando o conteúdo da nota
                tinyMCE.get('textoNotasEditor').setContent( $("#note_" + notaId).find('.conteudoNota').eq(0).text() );
            }
            else
            {
                $("#bloco-notas span").html('');

                // apenas mostrar o conteúdo da nota
                // sem permitir edição
                tinyMCE.get('textoNotasEditor').setMode('readonly');

                // carregando o conteúdo da nota
                tinyMCE.get('textoNotasEditor').setContent( $("#note_" + notaId).find('.conteudoNota').eq(0).text() );
            }
        }

        /**
         * Esta função envia a atualização da nota ao servidor
         */
        function salvarNota( conteudo )
        {
            conteudo = $.trim( conteudo );

            if( conteudo.length > 0 )
            {
                // sempre que for detectada mudança no conteúdo
                // pegar o conteúdo do editor
                // e salvar no html, para evitar perda quando trocar de nota
                $(".nota-atual").eq(0).find('.conteudoNota').eq(0).text( conteudo );

                $("#bloco-notas span").html('Salvando...');

                // enviando ao servidor
                $.ajax({
                    url : '{{ action("AgendaController@notaConsulta") }}',
                    type: 'post',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data: {
                        _token: $('meta[name=_token]').attr('content'),
                        'conteudo' : conteudo,
                        'token' : '{{ Hash::make($agenda['agenda_id'] . '_' . Auth::user()->toArray()['id']) }}',
                        'agendaId' : '{{ $agenda['agenda_id'] }}'
                    },
                    success: function()
                    {
                        // aviso
                        $("#bloco-notas span").html('Salva em: ' + moment().format('DD/MM/YYYY HH:mm:ss'));
                    },
                    error: function()
                    {
                        // aviso
                        $("#bloco-notas span").html('Salva em: ' + moment().format('DD/MM/YYYY HH:mm:ss'));
                    }
                });
            }
        }
    </script>


    {{-- controles da tela --}}
    <script>
        // ajuste do tamanho dos elementos
        // no caso de alternar entre normal e fullscreeen
        $(document).ready(function(){
            $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange', function(){
                if(
                    document.fullscreenElement ||
                    document.webkitFullscreenElement ||
                    document.mozFullScreenElement ||
                    document.msFullscreenElement
                ){
                    $("#conference-player").css({
                        "width" : "100%",
                        "height" : "100%"
                    });
                }
                else
                {
                    $("#conference-player").css({
                        "width" : "auto",
                        "height" : "auto"
                    });
                }
            });

            // ao pressionar ESC, sair do full screen
            $(document).bind('keyup', function(evt){
                if( evt.keyCode == 27 )
                {
                    if(document.exitFullscreen)
                    {
                        document.exitFullscreen();
                    }
                    else if (document.mozCancelFullScreen)
                    {
                        document.mozCancelFullScreen();
                    }
                    else if (document.webkitExitFullscreen)
                    {
                        document.webkitExitFullscreen();
                    }
                    else if (document.msExitFullscreen)
                    {
                        document.msExitFullscreen();
                    }
                }
            });

            // ao submeter o formulário do chat
            $("#formChat").bind('submit', function(){
                return enviarMensagemChat();
            });

            // ao inicializar
            // iniciar carregamento de mensagens do chat
            carregarNovasMensagens();

            // scroll down ao iniciar
            setTimeout(function(){
                $("ul.mensagens-chat").stop().animate({
                    scrollTop : document.getElementById("listaChat").scrollHeight
                }, 500);
            }, 3000);
        });

        /**
         * Esta função verifia e envia a mensagem digitada no chat
         */
        function enviarMensagemChat()
        {
            var mensagem = $.trim( $("#formChat input[type=text]").eq(0).val() );

            if( mensagem.length > 0 )
            {
                // enviar ao servidor
                $.ajax({
                    url : '{{ action("MensagemController@APINovaMensagem") }}',
                    type : 'post',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data : {
                        _token: $('meta[name=_token]').attr('content'),
                        'mensagem' : mensagem,
                        'paraUsuarioId' : {{ $com_usuario_id }}
                    },
                    success: function()
                    {
                        carregarNovasMensagens();
                    },
                    error: function()
                    {
                        carregarNovasMensagens();
                    }
                });
            }

            // limpando campo
            $("#formChat input[type=text]").eq(0).val('');

            // não propagar evento disparado
            return false;
        }

        /**
         * Esta função, verifica de tempos em tempos
         * As novas mensagens no chat
         */
        var carregandoMensagens = false;

        function carregarNovasMensagens()
        {
            // caso já não esteja processando
            if( !carregandoMensagens )
            {
                carregandoMensagens = true;

                var timestamp = $("[data-timestamp]:last").attr("data-timestamp");

                if( !timestamp )
                    timestamp = 1483063200; // timestamp inicial (Amy)

                $.ajax({
                    url : '{{ action("MensagemController@APICarregarMensagens") }}',
                    type : 'post',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data : {
                        _token: $('meta[name=_token]').attr('content'),
                        'timestamp' : timestamp,
                        'com_usuario_id' : {{ $com_usuario_id }}
                    },
                    dataType: 'json',
                    success: function( data )
                    {
                        if( data.mensagem )
                        {
                            data = data.mensagem;

                            for( var i = 0; i < data.length; i++ )
                            {
                                // adicionando na tela
                                var html = '<li class="chat-right" data-timestamp="' + moment( data[i].data_mensagem ).unix() + '">';
                                        html += '<div class="row">';
                                            html += '<div class="col-lg-12 no-padding-right">';
                                                html += '<span class="bold" title="' + data[i].de_nome + '">' + data[i].de_nome + '</span>';
                                                html += '<span class="mensagem-time">' + '\n\t' +  moment( data[i].data_mensagem ).format("H:mm") + '\n';
                                                        html += '-' + '\n';
                                                        html += moment( data[i].data_mensagem ).format("D/MM") + '\n\t' + '</span>';
                                                html += '<div class="mensagem-text-right mensagem-tip">'
                                                    html += '<div class="mensagem-text-inside">';
                                                            html += data[i].mensagem;
                                                    html += '</div>';
                                                    html += '</div>';
                                                html+= '</div>';
                                            html += '</div>';
                                        html += '</li>';

                                $("ul.mensagens-chat").append( html );

                                // scroll down
                                $("ul.mensagens-chat").stop().animate({
                                    scrollTop : document.getElementById("listaChat").scrollHeight
                                }, 500);
                            }
                        }

                        // chamar novamente
                        setTimeout(function(){
                            carregarNovasMensagens();
                        }, 2000);

                        carregandoMensagens = false;
                    },
                    error: function()
                    {
                        // chamar novamente
                        setTimeout(function(){
                            carregarNovasMensagens();
                        }, 2000);

                        carregandoMensagens = false;
                    }
                });
            }
        }

        /**
         * Esta função coloca o bloco de conferência em full screen
         */
        function fullScreenConference()
        {
            // se já estiver em fullscreen, sai
            // se não, coloca em fullscreen
            if(
                document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement
            ){
                if(document.exitFullscreen)
                {
                    document.exitFullscreen();
                }
                else if (document.mozCancelFullScreen)
                {
                    document.mozCancelFullScreen();
                }
                else if (document.webkitExitFullscreen)
                {
                    document.webkitExitFullscreen();
                }
                else if (document.msExitFullscreen)
                {
                    document.msExitFullscreen();
                }
            }
            else
            {
                element = $('#conference-player').get(0);

                if(element.requestFullscreen)
                {
                    element.requestFullscreen();
                }
                else if (element.mozRequestFullScreen)
                {
                    element.mozRequestFullScreen();
                }
                else if (element.webkitRequestFullscreen)
                {
                    element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
                else if (element.msRequestFullscreen)
                {
                    element.msRequestFullscreen();
                }
            }
        }

        /**
         * Esta função coloca / retira a conexão do usuário em mute
         * O outro usuário vê a imagem mas não ouve nada
         */
        function alternarMute()
        {
            // verifica se o usuário atual já está em mute
            if( activeConversation.localMedia.isMuted )
            {
                // retirando o mute
                activeConversation.localMedia.mute(false);

                // trocando o icone
                $("#icone-mic").addClass("fa-microphone");
                $("#icone-mic").removeClass("fa-microphone-slash");
            }
            else
            {
                // colocando em mute
                activeConversation.localMedia.mute(true);

                // trocando o icone
                $("#icone-mic").removeClass("fa-microphone");
                $("#icone-mic").addClass("fa-microphone-slash");
            }
        }

        /**
         * Esta função confere a ação com o usuário
         * e em caso positivo, a conexão é encerrada e comunicada ao outro usuário
         */
        function encerrarChamada(status)
        {
            // ao clicar para encerrar chamada
            // se estiver em fullscreen, sair
            if(document.exitFullscreen)
            {
                document.exitFullscreen();
            }
            else if (document.mozCancelFullScreen)
            {
                document.mozCancelFullScreen();
            }
            else if (document.webkitExitFullscreen)
            {
                document.webkitExitFullscreen();
            }
            else if (document.msExitFullscreen)
            {
                document.msExitFullscreen();
            }

            // caso seja para apenas mostrar o aviso
            if( status == 0 )
            {
                bootbox.confirm({
                    message : "Deseja realmente encerrar esta consulta? Se prosseguir, esta conexão não poderá ser reestabelecida, sendo necessário marcar uma nova consulta.",
                    buttons : {
                        confirm: {
                            label: 'Sim',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Não',
                            className: 'btn-danger'
                        }
                    },
                    callback : function(result){
                        if( result )
                        {
                            encerrarChamada(1);
                        }
                    }
                });
            }
            // prosseguir com o encerramento
            else
            {
                // enviando ao sistema o aviso de encerramento
                $.ajax({
                    url : '{{ action("ConferenciaController@encerrar", ["agendaId" => $agenda['agenda_id']]) }}',
                    type : 'post',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data:{
                        _token: $('meta[name=_token]').attr('content')
                    },
                    success: function(){
                        // encerrando no twilio
                        if( activeConversation )
                            activeConversation.disconnect();

                        // direcionando o usuário atual para a tela de perfil
                        window.top.location = '{{ action("PerfilController@meu") }}';
                    },
                    error: function(){
                        // encerrando no twilio
                        if( activeConversation )
                            activeConversation.disconnect();

                        // direcionando o usuário atual para a tela de perfil
                        window.top.location = '{{ action("PerfilController@meu") }}';
                    }
                });
            }

            return false;
        }
    </script>



    {{-- processamento da conferência --}}
    <script src="https://media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
    <script src="https://media.twiliocdn.com/sdk/js/conversations/v0.13/twilio-conversations.min.js"></script>

    <script>
        var conversationsClient;
        var activeConversation;
        var previewMedia = null;

        @if( Auth::user()->grupoSistema() == 'profissional' )
            var nomeParceiro = '{{ $agenda['nome_paciente'] }} {{ $agenda['sobrenome_paciente'] }}';
        @else
            var nomeParceiro = '{{ $agenda['tratamento_profissional'] }} {{ $agenda['nome_profissional'] }} {{ $agenda['sobrenome_profissional'] }}'
        @endif

        // check for WebRTC
        if(!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia){
            alert('Não é possível iniciar a conferência, seu navegador não suporta o recurso de WebRTC, recomendados a utilização da versão mais recente do Google Chrome.');
        }

        // generate an AccessToken in the Twilio Account Portal - https://www.twilio.com/user/account/video/testing-tools
        var accessToken = "{{ $tokenTwilio }}";

        // use our AccessToken to generate an AccessManager object
        var accessManager = new Twilio.AccessManager(accessToken);

        // create a Conversations Client and connect to Twilio
        conversationsClient = new Twilio.Conversations.Client(accessManager);

        conversationsClient.listen().then(
            clientConnected,
            function(error)
            {
                console.log(error);
                log('Could not connect to Twilio: ' + error.message);
            }
        );

        {{-- o convite só pode ser feito pelo profissional --}}
        @if( Auth::user()->grupoSistema() == 'profissional' )
            /**
             * Função para enviar o convite para o outro usuário
             */
            function convidarUsuario( identity )
            {
                // convidando um novo usuário
                // que é a outra parte desta consulta
                var inviteTo = identity;

                if (activeConversation) {
                    // add a participant
                    activeConversation.invite(inviteTo);
                } else {
                    // create a conversation
                    var options = {};

                    if(previewMedia){
                        options.localMedia = previewMedia;
                    }

                    conversationsClient.inviteToConversation(inviteTo, options).then(
                        conversationStarted,
                        function (error) {
                            // aguardar dois segundos e convidar novamente
                            setTimeout(function(){
                                activeConversation = null;
                                convidarUsuario( identity );
                            }, 2000);
                        }
                    );
                }
            }
        @endif

        // successfully connected!
        function clientConnected()
        {
            // meu identificador
            // console.log( conversationsClient.identity );

            conversationsClient.on('invite', function (invite) {
                log('Incoming invite from: ' + invite.from);
                invite.accept().then(conversationStarted);
            });

            {{-- o convite só pode ser feito pelo profissional --}}
            @if( Auth::user()->grupoSistema() == 'profissional' )
                activeConversation = null;
                // convidando o outro usuário
                convidarUsuario( '{{ $convidado }}' );
            @endif
        };

        // conversation is live
        function conversationStarted(conversation) {
            log('In an active Conversation');
            activeConversation = conversation;

            // draw local video, if not already previewing
            if (!previewMedia) {
                conversation.localMedia.attach('#local-video');
            }

            // when a participant joins, draw their video on screen
            conversation.on('participantConnected', function (participant) {
                log("Participant '" + participant.identity + "' connected");

                // removendo o texto de espera
                $("#remote-video p").remove();

                participant.media.attach('#remote-video');
            });

            // when a participant disconnects, note in log
            conversation.on('participantDisconnected', function (participant) {
                log("Participant '" + participant.identity + "' disconnected");

                // adicionando aviso de desconectado
                $("#remote-video").html('');

                $("#remote-video p").each(function(idx){
                    if( idx > 0 && $("#remote-video p").size() > 1 )
                        $(this).remove();
                });

                var p = $('<p>');
                    p.html("Conexão perdida com " + nomeParceiro +", aguardando reconexão...");
                    p.css("width", $("#remote-video").width());

                $("#remote-video").html( p );

                {{-- apenas profisisonais podem convidar --}}
                @if( Auth::user()->grupoSistema() == 'profissional' )
                    // aguardar dois segundos e convidar novamente
                    setTimeout(function(){
                        activeConversation = null;
                        convidarUsuario( '{{ $convidado }}' );
                    }, 2000);
                @endif
            });

            // when the conversation ends, stop capturing local video
            conversation.on('ended', function (conversation) {
                log("Connected to Twilio. Listening for incoming Invites as '" + conversationsClient.identity + "'");
                conversation.localMedia.stop();
                conversation.disconnect();
                activeConversation = null;
            });
        };

        // função para exibir a minha câmera
        function mostrarMinhaCamera(){
            if (!previewMedia) {
                previewMedia = new Twilio.Conversations.LocalMedia();
                Twilio.Conversations.getUserMedia().then(
                    function (mediaStream) {
                        previewMedia.addStream(mediaStream);
                        previewMedia.attach('#local-video');
                    },

                    function (error) {
                        console.error('Não é possível acessar a câmera local', error);
                        log('Não é possível acessar a câmera e microfone.');
                    }
                );
            };
        };

        // carregando a imagem da câmera atual
        $(document).ready(function(){
            mostrarMinhaCamera();

            setInterval(function(){
                $.get('/');
            }, 60000);
        });

        // activity log
        function log(message) {
            //document.getElementById('log-content').innerHTML = message;
            console.log(message);
        };
    </script>

@stop