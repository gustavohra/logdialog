{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
    {{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

	@yield('nav-menu-principal')

	<div class="pages-wrap">
        <div class="container">
           <div class="pages-main-content">
               <ul class="nav nav-tabs">
                    <li class="active"><a class="registermenu registermenu01" href="#register-user">Cadastro Paciente</a></li>
                    <li><a class="registermenu registermenu02" href="#register-psicologo">Cadastro Profissional</a></li>
                </ul>

                <div class="tab-content">
                    <div id="register-user" class="tab-pane fade in active">
                        <div class="tabcontent-inside">
                            <h3>Crie sua conta</h3>
                            <form action="{{ action('CadastroController@paciente') }}" method="post">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="name">Nome</label>
                                        <input type="text" name="name" id="name" placeholder="Nome" value="{{ old('name') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('name') ? 'display:block' : 'display:none' }}" >{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="lastname">Sobrenome</label>
                                        <input type="text" name="lastname" id="lastname" placeholder="Sobrenome" value="{{ old('lastname') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('lastname') ? 'display:block' : 'display:none' }}" >{{ $errors->first('lastname') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" id="email" placeholder="E-mail" value="{{ old('email') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('email') ? 'display:block' : 'display:none' }}" >{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="birth">Data de Nascimento</label>
                                        <input type="text" name="birth" id="birth" placeholder="Data de nascimento" value="{{ old('birth') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('birth') ? 'display:block' : 'display:none' }}" >{{ $errors->first('birth') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="gender">Sexo</label>
                                        <div class="radioitem">
                                            <input type="radio" name="gender" value="male" {{ ( old('gender') == 'male' || old('gender') == '' ) ? 'checked="checked"' : '' }}>
                                            Masculino
                                        </div>
                                        <div class="radioitem">
                                            <input type="radio" name="gender" value="female" {{ old('gender') == 'female' ? 'checked="checked"' : '' }}>
                                            Feminino
                                        </div>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('gender') ? 'display:block' : 'display:none' }}" >{{ $errors->first('gender') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-lg-6">
                                        <label for="password">Senha</label>
                                        <input type="password" name="password" id="password" placeholder="Senha"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('password') ? 'display:block' : 'display:none' }}" >{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="passwordconfirmation">Confirmação de senha</label>
                                        <input type="password" name="passwordconfirmation" id="passwordconfirmation" placeholder="Confirmação de senha"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('passwordconfirmation') ? 'display:block' : 'display:none' }}" >{{ $errors->first('passwordconfirmation') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="register-button">
                                        <button type="submit" class="btn btn-register">Criar conta</button>
                                          <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                              <input type="checkbox" class="custom-control-input" id="check-termos-de-uso" style="height:auto" required>
                                              <label class="custom-control-label" for="check-termos-de-uso" style="    display: inline-block;text-align: left;    font-size: 15px;    color: #666;">Ao criar uma conta, você concoda com os <a href="{{ asset('institucional/termos-de-uso') }}">Termos de Condições</a></label>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </form>
                        </div> {{-- /.tabcontent-inside --}}
                    </div> {{-- /#meuperfil --}}

                    <div id="register-psicologo" class="tab-pane fade">
                        <div class="tabcontent-inside">
                            <h3>Crie sua conta</h3>
                            <form class="{{ ( count($errors->all()) > 0 ) ? 'form_prof_error' : '' }}" action="{{ action('CadastroController@profissional') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="name">Nome</label>
                                        <input type="text" name="prof_name" id="prof_name" placeholder="Nome" value="{{ old('prof_name') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('prof_name') ? 'display:block' : 'display:none' }}" >{{ $errors->first('prof_name') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="lastname">Sobrenome</label>
                                        <input type="text" name="prof_lastname" id="prof_lastname" placeholder="Sobrenome" value="{{ old('prof_lastname') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('prof_lastname') ? 'display:block' : 'display:none' }}" >{{ $errors->first('prof_lastname') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="email">Email</label>
                                        <input type="email" name="prof_email" id="prof_email" placeholder="E-mail" value="{{ old('prof_email') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('prof_email') ? 'display:block' : 'display:none' }}" >{{ $errors->first('prof_email') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="birth">Data de Nascimento</label>
                                        <input type="text" name="prof_birth" id="prof_birth" placeholder="Data de nascimento" value="{{ old('prof_birth') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('prof_birth') ? 'display:block' : 'display:none' }}" >{{ $errors->first('prof_birth') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="gender">Sexo</label>
                                        <div class="radioitem">
                                            <input type="radio" name="prof_gender" value="male" {{ ( old('prof_gender') == 'male' || old('prof_gender') == '' ) ? 'checked="checked"' : '' }}>
                                            Masculino
                                        </div>
                                        <div class="radioitem">
                                            <input type="radio" name="prof_gender" value="female" {{ old('prof_gender') == 'female' ? 'checked="checked"' : '' }}>
                                            Feminino
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                     <div class="col-lg-6">
                                        <label for="password">Senha</label>
                                        <input type="password" name="prof_password" id="prof_password" placeholder="Senha"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('prof_password') ? 'display:block' : 'display:none' }}" >{{ $errors->first('prof_password') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="passwordconfirmation">Confirmação de senha</label>
                                        <input type="password" name="prof_passwordconfirmation" id="prof_passwordconfirmation" placeholder="Confirmação de senha"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('prof_passwordconfirmation') ? 'display:block' : 'display:none' }}" >{{ $errors->first('prof_passwordconfirmation') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="breakline"></div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <label for="prof_crp">CRP<span class="sm-registerlabel">(necessário o upload da foto/imagem escaneada do documento para validação)</span></label>
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" name="nr_crp" id="prof_crp" placeholder="Nº da carteira do conselho" value="{{ old('nr_crp') }}"><br>
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('nr_crp') ? 'display:block' : 'display:none' }}" >{{ $errors->first('nr_crp') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <input type="text" readonly="readyonly" class="input-upload" placeholder="Selecione o arquivo...">
                                            <input type="file" name="crp" class="registro_upload">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-upload" type="button"><i class="fa fa-file-image-o" aria-hidden="true"></i> Procurar</button>
                                            </span>
                                        </div>{{-- /input-group --}}
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('crp') ? 'display:block' : 'display:none' }}" >{{ $errors->first('crp') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="password">Documento de identidade<span class="sm-registerlabel">(CNH ou RG)</span> </label>
                                        <div class="input-group">
                                            <input type="text" readonly="readyonly" class="input-upload" placeholder="Selecione o arquivo...">
                                            <input type="file" name="rg_cnh" class="registro_upload">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-upload" type="button"><i class="fa fa-file-image-o" aria-hidden="true"></i> Procurar</button>
                                            </span>
                                        </div>{{-- /input-group --}}
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('rg_cnh') ? 'display:block' : 'display:none' }}" >{{ $errors->first('rg_cnh') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="password">Diploma</label>
                                        <div class="input-group">
                                            <input type="text" readonly="readyonly" class="input-upload" placeholder="Selecione o arquivo...">
                                            <input type="file" name="diploma" class="registro_upload">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-upload" type="button"><i class="fa fa-file-image-o" aria-hidden="true"></i> Procurar</button>
                                            </span>
                                        </div>{{-- /input-group --}}
                                        @if( count($errors->all()) > 0 )
                                            <span class="form_error" style="{{ $errors->has('diploma') ? 'display:block' : 'display:none' }}" >{{ $errors->first('diploma') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="register-button">
                                        <button type="submit" class="btn btn-register">Criar conta</button>
                                    </div>
                                </div>
                            </form>
                        </div> {{-- /.tabcontent-inside --}}
                    </div> {{-- /#minhaagenda --}}
                </div>
           </div>
        </div>
    </div>

    <script>
		$(document).ready(function(){
		    $(".nav-tabs a").click(function(){
		        $(this).tab('show');
		    });
		    $('.nav-tabs a').on('shown.bs.tab', function(event){
		        var x = $(event.target).text();         // active tab
		        var y = $(event.relatedTarget).text();  // previous tab
		        $(".act span").text(x);
		        $(".prev span").text(y);
		    });

            if( window.location.hash.length > 0 )
            {
                $('a[href="' + window.location.hash + '"]').trigger('click');
            }

            @if( isset($tab) )
                $('a[href="#{{ $tab }}"]').trigger('click');
            @endif

            // máscara dos campos
            $("#birth").mask('99/99/9999');
            $("#prof_birth").mask('99/99/9999');

            // listener ao selecionar um arquivo
            $("[type='file']").bind('change', function(){
                var valor = $(this).val().split('\\');
                valor = valor.pop();

                $(this).parent().find('.input-upload').eq(0).val( valor );
            });
		});
	</script>
@stop