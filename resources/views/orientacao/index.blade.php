{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

	{{-- Pergunta --}}
	<section id="question-banner" class="intro-section">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-12">
	                <div class="question-title">
	                    <h1>Você precisa de orientação psicológica?</h1>
	                    <p>Lorem ipsum dolo sit amet consectetur.</p>
	                </div>
	                <div class="question-content">
	                    <div class="row">
	                        <div class="col-lg-9 col-nopadding">
	                            <div class="question-text">
	                                <h2>{{ $pergunta['pergunta'] }}</h2>
	                            </div>
	                        </div> <!-- /.col -->
	                        <div class="col-lg-3 col-nopadding">
	                            <div class="answer-content">
	                                <div class="answer-item">
	                                    <a href="{{ action("OrientacaoController@pergunta", ["id" => $pergunta['id'], "res" => 0]) }}">
	                                        <p>Não <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></p>
	                                    </a>
	                                </div>
	                                <div class="answer-item">
	                                    <a href="{{ action("OrientacaoController@pergunta", ["id" => $pergunta['id'], "res" => $pergunta["resposta_sim"]]) }}">
	                                        <p>Sim <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></p>
	                                    </a>    
	                                </div>
	                            </div>
	                        </div> <!-- /.col -->
	                    </div> <!-- /.row -->
	                </div>
	                
	                
	            </div>
	        </div>
	    </div>
	</section>
@stop