{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

	{{-- resposta --}}
	<div class="pages-wrap">
        <div class="container">
            <div class="results-wrap">
                <div class="results-title">
                    <h2>Resultado</h2>
                    <p>Veja aqui os pontos que merecem atenção, de acordo com suas respostas</p>
                </div>

				@foreach( $resultado as $k => $v )
					<div class="results-item">
	                    <h4>{{ $v['titulo'] }}</h4>
	                    <p>{{ $v['resposta'] }}</p>
	                </div>
				@endforeach

                <div class="results-btn">
                    <button id="btn-results" class="btn btn-gradient">
                        <a href="{{ action('ProfissionaisController@index') }}">AGENDAR UMA CONSULTA</a>  
                    </button>
                </div>
                <div class="results-text">
                    <p>Esta não é uma ferramenta diagnóstica, apenas <br>sintomas de alerta para a procura de ajuda psicológica</p>
                </div>
            </div>
        </div>
    </div>

@stop