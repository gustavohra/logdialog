{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <p>Minhas próximas consultas</p>

	<div class="admincontent-wrap">
        <div class="container pagina-notificacoes">
            <div class="admin-content-main">
                <div class="admin-content-title">
                    <h1>Todas minhas consultas</h1>
                </div>
                <div class="admin-content-body">
                    <div class="notifications-box">
	                    @if( sizeof($consultas) > 0 )
	                    	
	                    	@foreach( $consultas as $item )
		                    	<?php
		                            // pegando dados mais específicos da notificação
		                            $item = \LogDialog\Model\Agenda::detalharItem( $item->agenda_id ); 
		                        ?>
								
								{{-- apenas para consultas solicitadas --}}
								@if( isset($item->data_compromisso) )

		                            {{-- Notification Item --}}
			                        <div class="notifications-item">
			                            <div class="row">
			                                <div class="col-lg-1">
			                                    <div class="notification-image">
			                                        <img src="{{ asset($item->avatar_profissional) }}" alt="{{ $item->nome_profissional }}" class="img-responsive">
			                                    </div>
			                                </div>
			                                <div class="col-lg-7">
			                                    <div class="notification-content">
			                                        <p><span class="bold">{{$item->tratamento_profissional}} {{ $item->nome_profissional }} {{ $item->sobrenome_profissional }}</span> {{ $item->nome_compromisso }}</p>
			                                        <p><span class="bold">Dia: </span> {{ $item->data_compromisso->formatLocalized("%d de %B de %Y") }}</p>
			                                        <p><span class="bold">Horário: </span>{{ $item->data_compromisso->format("H\hi") }}</p>
			                                    </div>
			                                
			                                
			                                </div>
			                                <div class="col-lg-4">
			                                    <div class="notification-actions"> 
                                                @if($item->data_pagamento != null && $item->token_pagamento != null)
				                                    @if($item->status_pagamento == 'CONFIRMADO' && time() - strtotime($item->data_compromisso) < 0)
	                                                    	<a class="btn btn-actions btn-irconsulta" href="{{ action("ConferenciaController@consulta", ["agendaId" => $item->agenda_id]) }}"><i class="fa fa-play-circle-o" aria-hidden="true"></i> Ir para consulta</a>
	                                                  @endif
	                                             @elseif($item->status_pagamento == 'PENDENTE' && time() - strtotime($item->data_compromisso) < 0)
                                                            <a class="btn btn-success btn-irconsulta" href="{{ action("FinanceiroController@realizarPagamento", ["agendaId" => $item->agenda_id]) }}"><i class="fa fa-credit-card" aria-hidden="true"></i> Realizar pagamento</a>
	                                             @endif
			                                    </div>
			                                </div>
			                            </div>
			                        </div> {{-- /.notifications-item --}} 
			                        @endif
	                        @endforeach
	                    @else
	                    	{{-- Notification Item --}}
	                        <div class="notifications-item">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="notification-content">
	                                        <p>
	                                        	Não há notificações
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div> {{-- /.notifications-item --}}
	                        {{-- Notification Item --}}
	                    @endif
                    </div>
                </div> <!-- /.box -->
                @if(Count($consultas) > 1)
                <div class="pagination-wrap">
                    {{ $consultas->links() }}
                </div>
                @endif
            </div>     
        </div>
    </div> <!-- /.admincontent-wrap -->
@stop
{{-- fim do conteúdo do yield atual --}}