{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">

    <style type="text/css">
    	.pay-icon {
    		display: inline-block;
    		height: 40px;
		    top: -2px;
		    position: relative;
    		transform: translateZ(0);
    	}
    	.panel-heading .accordion-toggle:after {
    		display: none;
    	}
    	.btn-success {
    		border-color:transparent;
    	}
		.panel-open >.panel-heading {
		    background:#dadada !important;
		}
    </style>
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <p>Minhas próximas consultas</p>

	<div class="admincontent-wrap" ng-app="logdialogApp">
        <div class="container pagina-notificacoes">
            <div class="admin-content-main">
                <div class="admin-content-title">
                    <h1>Pagamento de consulta</h1>
                </div>
                <div class="admin-content-body">
                    <div class="notifications-box" ng-controller="PagamentoCtrl" ng-cloak>
	                    @if( $consulta != null )
	                    	  
								 
								@if( isset($consulta->data_compromisso) )

		                            {{-- Notification Item --}}
			                        <div class="notifications-item">
			                            <div class="row">
			                                <div class="col-lg-1">
			                                    <div class="notification-image">
			                                        <img src="{{ asset($consulta->avatar_profissional) }}" alt="{{ $consulta->nome_profissional }}" class="img-responsive">
			                                    </div>
			                                </div>
			                                <div class="col-lg-7">
			                                    <div class="notification-content">
			                                        <p><span class="bold">{{$consulta->tratamento_profissional}} {{ $consulta->nome_profissional }} {{ $consulta->sobrenome_profissional }}</span> {{ $consulta->nome_compromisso }}</p>
			                                        <p><span class="bold">Dia: </span> {{ $consulta->data_compromisso->formatLocalized("%d de %B de %Y") }}</p>
			                                        <p><span class="bold">Horário: </span>{{ $consulta->data_compromisso->format("H\hi") }}</p>
			                                    </div>
			                                
			                                
			                                </div>
			                                <div class="col-lg-4"> 
                                               		<h4>Valor total</h4>
                                               		<span>R${{$consulta->valor}}</span> 
			                                </div>
			                            </div>
			                        </div> {{-- /.notifications-item --}} 

			                        <div class="row" style="padding:10px 30px">
				                        		<div class="col-lg-12 col-md-12 col-xs-12">
							                        <div class="row">
							                        		<div class="col-lg-12 col-md-12 col-xs-12">
							                        			<h4>Selecione uma forma de pagamento abaixo:</h4>
							                        		</div>
							                        </div>
							                        <br />
							                        <div class="row">
							                        		<div class="col-lg-12 col-md-12 col-xs-12">
															  <uib-accordion close-others="true">
															    <div uib-accordion-group class="panel-default">
																      <uib-accordion-heading>
																       Boleto <img ng-src="{{ asset('images/icones-pagamento/boleto.png')}}" class="img-responsive pay-icon" />
																      </uib-accordion-heading> 
																      Ao prosseguir, um boleto será enviado para o e-mail <b>{{ $usuario['email'] }}</b>. <br />Após o pagamento, pode levar até <b>48 horas</b> para que o pagamento seja aprovado e a consulta liberada.
																	    <div class="row">
																	    		<div class="col-lg-12 col-md-12 col-xs-12">
																	    			<a class="btn btn-success pull-right"  href="{{ action("FinanceiroController@gerarBoleto", ["agendaId" => $consulta->agenda_id]) }}">Gerar boleto</a>
																	    		</div>
																	    </div>
															    </div> 
															    <div uib-accordion-group class="panel-default">
																      <uib-accordion-heading>
																       Cartão de crédito <img ng-src="{{ asset('images/icones-pagamento/credit-card.png')}}" class="img-responsive pay-icon" />
																      </uib-accordion-heading> 
																	      <form name="ccForm" ng-submit="enviar(this.ccForm)">
																	        <div class="row">
																	        	<div class="col-lg-12 col-md-12 col-xs-12">
																			        <div class="form-group">
																			          <label for="nome">Nome completo</label>
																			          	<div class="row">
																			          		<div class="col-lg-12 col-md-12 col-xs-12">
																			          			<input type="text" class="form-control" id="nome"  name="ccNumber" ng-model="card.nome" required>
																			          		</div>
																			      		</div>
																			        </div> 
																			      </div>
																			 </div>
																	        <div class="row">
																	        	<div class="col-lg-12 col-md-12 col-xs-12">
																			        <div class="form-group">
																			          <label for="card-number">Número do cartão</label>
																			          	<div class="row">
																			          		<div class="col-lg-12 col-md-12 col-xs-12">
																			          			<input type="text" class="form-control" id="cardNumber" cc-number cc-eager-type name="ccNumber" ng-model="card.number">
																			          		</div> 
																			      		</div>
																			        </div> 
																			      </div>
																			 </div>
																	        <div class="row">
																	        	<div class="col-lg-3 col-md-3 col-xs-12">
																			        <div class="form-group">
																			          <label for="cvc">CVC</label>
																			          <input type="text" class="form-control" id="cvc" cc-cvc cc-type="ccForm.ccNumber.$ccType" name="ccCvc" ng-model="card.cvc">
																			        </div>
																			    </div> 
																	        	<div class="col-lg-9 col-md-9 col-xs-12">
																			        <div class="form-group">
																			          <label for="card-number">Validade</label>
																			          <div class="row" cc-exp>
																			            <div class="col-xs-3">
																			              <input placeholder="MM" type="text" class="form-control" cc-exp-month name="ccExpMonth" ng-model="card.expiration.month">
																			            </div>
																			            <div class="col-xs-3">
																			              <input placeholder="AA" type="text" class="form-control" cc-exp-year name="ccExpYear" ng-model="card.expiration.year">
																			            </div>
																			          </div>
																			        </div>
																			     </div>
																	    </div>
																	    <div class="row">
																	    		<div class="col-lg-12 col-md-12 col-xs-12">
																	    			<button class="btn btn-success pull-right" type="submit">Confirmar pagamento</button>
																	    		</div>
																	    </div>
																	      </form>
															    </div> 
															  </uib-accordion>
							                        		</div>
							                        </div>
				                    		</div>
			                    	</div>
			                        @endif 
	                    @else
	                    	{{-- Notification Item --}}
	                        <div class="notifications-item">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="notification-content">
	                                        <p>
	                                        	Não há nenhum pagamento pendente para esta consulta.
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div> {{-- /.notifications-item --}}
	                        {{-- Notification Item --}}
	                    @endif
                    </div>
                </div> <!-- /.box --> 
            </div>     
        </div>
    </div> <!-- /.admincontent-wrap -->


<script type="text/javascript">
   const BASE_URL  = '{{ asset("")  }}'; 
   const AUTH_KEY   = '{{ csrf_token()  }}';
</script>
<script type="text/javascript" src="{{ asset('lib/angular/angular.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angularUtils-pagination/dirPagination.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-animate/angular-animate.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-loading-bar/build/loading-bar.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/ngSanitize/index.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/sweetalert/dist/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/ngSweetAlert/SweetAlert.min.js')}}"></script> 
<script type="text/javascript" src="{{ asset('lib/angular-bootstrap/ui-bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/ng-file-upload/ng-file-upload-all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/wow/dist/wow.min.js')}}"></script>  
<script type="text/javascript" src="{{ asset('lib/ng-dialog/js/ngDialog.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/api-check/dist/api-check.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-formly/dist/formly.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-ui-router-anim-in-out/anim-in-out.js')}}"></script> 
<script type="text/javascript" src="{{ asset('lib/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-ui-tinymce/dist/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-credit-cards/angular-credit-cards.js')}}"></script>
<script type="text/javascript" src="{{ asset('painel-admin/js/app.module.js')}}"></script>
<script type="text/javascript" src="{{ asset('painel-admin/js/app.routes.js')}}"></script>

<script type="text/javascript">
	var app = angular.module('logdialogApp', ['ui.bootstrap', 'credit-cards'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

		app.controller('PagamentoCtrl', function ($scope) {

			  $scope.status = {
			    isCustomHeaderOpen: false,
			    isFirstOpen: false,
			    isFirstDisabled: false
			  };
		});

</script>
@stop
{{-- fim do conteúdo do yield atual --}}