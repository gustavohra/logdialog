{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">

    <style type="text/css">
    	.pay-icon {
    		display: inline-block;
    		height: 40px;
		    top: -2px;
		    position: relative;
    		transform: translateZ(0);
    	}
    	.panel-heading .accordion-toggle:after {
    		display: none;
    	}
    	.btn-success {
    		border-color:transparent;
    	}
		.panel-open >.panel-heading {
		    background:#dadada !important;
		}
    </style>
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <p>Minhas próximas consultas</p>

	<div class="admincontent-wrap" ng-app="logdialogApp">
        <div class="container pagina-notificacoes">
            <div class="admin-content-main">
                <div class="admin-content-title">
                    <h1>Pagamento de consulta</h1>
                </div>
                <div class="admin-content-body">
                    <div class="notifications-box" ng-controller="PagamentoCtrl" ng-cloak>
	                    @if( $consulta != null )
	                    	  
								 
								@if( isset($consulta->data_compromisso) )

		                            {{-- Notification Item --}}
			                        <div class="notifications-item">
			                            <div class="row">
			                                <div class="col-lg-1">
			                                    <div class="notification-image">
			                                        <img src="{{ asset($consulta->avatar_profissional) }}" alt="{{ $consulta->nome_profissional }}" class="img-responsive">
			                                    </div>
			                                </div>
			                                <div class="col-lg-7">
			                                    <div class="notification-content">
			                                        <p><span class="bold">{{$consulta->tratamento_profissional}} {{ $consulta->nome_profissional }} {{ $consulta->sobrenome_profissional }}</span> {{ $consulta->nome_compromisso }}</p>
			                                        <p><span class="bold">Dia: </span> {{ $consulta->data_compromisso->formatLocalized("%d de %B de %Y") }}</p>
			                                        <p><span class="bold">Horário: </span>{{ $consulta->data_compromisso->format("H\hi") }}</p>
			                                    </div>
			                                
			                                
			                                </div>
			                                <div class="col-lg-4"> 
                                               		<h4>Valor total</h4>
                                               		<span>R${{$consulta->valor}}</span> 
			                                </div>
			                            </div>
			                        </div> {{-- /.notifications-item --}} 

			                        <div class="row" style="padding:10px 30px">
				                        		<div class="col-lg-12 col-md-12 col-xs-12">
				                        			@if($consulta->status_pagamento != 'WAITING' && $consulta->status_pagamento != 'IN_ANALYSIS')
							                        <div class="row">
							                        		<div class="col-lg-12 col-md-12 col-xs-12">
							                        			<h4>Selecione uma forma de pagamento abaixo:</h4>
							                        		</div>
							                        </div>
							                        <br />
							                        <div class="row">
							                        		<div class="col-lg-12 col-md-12 col-xs-12">
															  <uib-accordion close-others="true">
															    <div uib-accordion-group class="panel-default">
																      <uib-accordion-heading>
																       Boleto <img ng-src="{{ asset('images/icones-pagamento/boleto.png')}}" class="img-responsive pay-icon" />
																      </uib-accordion-heading> 
																      Ao prosseguir, um boleto será enviado para o e-mail <b>{{ $usuario['email'] }}</b>. <br />Após o pagamento, pode levar até <b>48 horas</b> para que o pagamento seja aprovado e a consulta liberada.
																	    <div class="row">
																	    		<div class="col-lg-12 col-md-12 col-xs-12">
																	    			<form name="frmBoleto" id="frmBoleto">
																	    				<button type="submit" class="btn btn-success pull-right">Gerar boleto</button>														    				
																	    			</form>
																	    		</div>
																	    </div>
																	    <script type="text/javascript">
																	    	$(function() {
																	    		$("#frmBoleto").submit(function(event) {
																	    			event.preventDefault();

																	    			$.ajax({
																	    				url: '{{ action("FinanceiroController@gerarBoleto", ["agendaId" => $consulta->agenda_id]) }}',
																	    				type: 'get',
																	    				data: $(this).serialize(),
																	    			})
																	    			.done(function() {
																	    				alert("Boleto enviado para o e-mail {{$usuario['email']}}!");
																	    			})
																	    			.fail(function() {
																	    				alert("Ocorreu um probema. Por favor, tente novamente ou entre em contato com o suporte técnico.");
																	    			})
																	    			.always(function() {
																	    				
																	    			});
																	    			
																	    		});
																	    	});
																	    </script>

															    </div> 
															    <div uib-accordion-group class="panel-default">
																      <uib-accordion-heading>
																       Cartão de crédito <img ng-src="{{ asset('images/icones-pagamento/credit-card.png')}}" class="img-responsive pay-icon" />
																      </uib-accordion-heading> 
																      	<div class="pagamento-retorno" ng-if="resposta.status > 0">
																      		<h2><% resposta.titulo %></h2>
																      		<p><% resposta.texto %></p>
																      	</div>
																	      <form name="ccForm" ng-submit="enviar(this.ccForm,card)" ng-if="resposta.status == 0">
																	        <div class="row">
																	        	<div class="col-lg-12 col-md-12 col-xs-12">
																			        <div class="form-group">
																			          <label for="nome">Nome completo</label>
																			          	<div class="row">
																			          		<div class="col-lg-12 col-md-12 col-xs-12">
																			          			<input type="text" class="form-control" id="nome"  name="ccNumber" ng-model="card.nome" required>
																			          		</div>
																			      		</div>
																			        </div> 
																			      </div>
																			 </div>
																	        <div class="row">
																	        	<div class="col-lg-12 col-md-12 col-xs-12">
																			        <div class="form-group">
																			          <label for="card-number">Número do cartão</label>
																			          	<div class="row">
																			          		<div class="col-lg-12 col-md-12 col-xs-12">
																			          			<input type="text" class="form-control" id="cardNumber" cc-number   name="ccNumber" ng-model="card.number">
																			          		</div> 
																			      		</div>
																			        </div> 
																			      </div>
																			 </div>
																	        <div class="row">
																	        	<div class="col-lg-3 col-md-3 col-xs-12">
																			        <div class="form-group">
																			          <label for="cvc">CVC</label>
																			          <input type="text" class="form-control" id="cvc" cc-cvc cc-type="ccForm.ccNumber.$ccType" name="ccCvc" ng-model="card.cvc">
																			        </div>
																			    </div> 
																	        	<div class="col-lg-9 col-md-9 col-xs-12">
																			        <div class="form-group">
																			          <label for="card-number">Validade</label>
																			          <div class="row" cc-exp>
																			            <div class="col-xs-3">
																			              <input placeholder="MM" type="text" class="form-control" cc-exp-month name="ccExpMonth" ng-model="card.expiration.month">
																			            </div>
																			            <div class="col-xs-3">
																			              <input placeholder="AA" type="text" class="form-control" cc-exp-year name="ccExpYear" ng-model="card.expiration.year">
																			            </div>
																			          </div>
																			        </div>
																			     </div>
																	    </div>
																	    <div class="row">
																	    		<div class="col-lg-12 col-md-12 col-xs-12">
																	    			<button class="btn btn-success pull-right" type="submit" ng-disabled="carregando">Confirmar pagamento</button>
																	    		</div>
																	    </div>
																	      </form>
															    </div> 
															  </uib-accordion>
							                        		</div>
							                        </div>
							                        @elseif($consulta->status_pagamento == 'WAITING')
												      	<div class="pagamento-retorno" >
												      		<h2>Aguardando pagamento</h2>
												      		<p>Caso já tenha realizado o pagamento, por favor, entre em contato com nosso suporte técnico.</p>
												      	</div>
							                        @else
												      	<div class="pagamento-retorno">
												      		<h2>Pagamento em análise</h2>
												      		<p>Você será informado assim que o pagamento for confirmado pela operadora de crédito.</p>
												      	</div>
							                        @endif
				                    		</div>
			                    	</div>
			                        @endif 
	                    @else
	                    	{{-- Notification Item --}}
	                        <div class="notifications-item">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="notification-content">
	                                        <p>
	                                        	Não há nenhum pagamento pendente para esta consulta.
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div> {{-- /.notifications-item --}}
	                        {{-- Notification Item --}}
	                    @endif
                    </div>
                </div> <!-- /.box --> 
            </div>     
        </div>
    </div> <!-- /.admincontent-wrap -->


<script type="text/javascript">
   const BASE_URL  = '{{ asset("")  }}'; 
   const AUTH_KEY   = '{{ csrf_token()  }}';
</script>
<script type="text/javascript" src="{{ asset('lib/angular/angular.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angularUtils-pagination/dirPagination.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-animate/angular-animate.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-loading-bar/build/loading-bar.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/ngSanitize/index.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/sweetalert/dist/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/ngSweetAlert/SweetAlert.min.js')}}"></script> 
<script type="text/javascript" src="{{ asset('lib/angular-bootstrap/ui-bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/ng-file-upload/ng-file-upload-all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/wow/dist/wow.min.js')}}"></script>  
<script type="text/javascript" src="{{ asset('lib/ng-dialog/js/ngDialog.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/api-check/dist/api-check.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-formly/dist/formly.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-ui-router-anim-in-out/anim-in-out.js')}}"></script> 
<script type="text/javascript" src="{{ asset('lib/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-ui-tinymce/dist/tinymce.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('lib/angular-credit-cards/angular-credit-cards.js')}}"></script>
<script type="text/javascript" src="{{ asset('painel-admin/js/app.module.js')}}"></script>
<script type="text/javascript" src="{{ asset('painel-admin/js/app.routes.js')}}"></script>

<script type="text/javascript">
	var app = angular.module('logdialogApp', ['ui.bootstrap', 'credit-cards'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

		app.controller('PagamentoCtrl', function ($scope, $http) {
				$scope.carregando = false;
				$scope.card = {};
				$scope.resposta = {status: 0};
			  $scope.status = {
			    isCustomHeaderOpen: false,
			    isFirstOpen: false,
			    isFirstDisabled: false
			  };

			  $scope.enviar = function(frm, card) { 
			  		$scope.carregando = true;
			  		if( (card.number != undefined && card.number.length == 16)){
					  	$http.post('{{ action("FinanceiroController@pagamentoCartao", ["agendaId" => $consulta->agenda_id]) }}', card).then(function(response) {
					  		var retorno = response.data;

					  		if(!retorno.erro) {
					  			$scope.resposta.status = 1;
					  			switch(retorno.dados.status) {
					  				case 'IN_ANALYSIS':
					  						$scope.resposta.titulo = "Pagamento em análise!";
					  						$scope.resposta.texto = "Você será informado assim que o pagamento for confirmado pela operadora de crédito.";
					  					break;
					  				case 'AUTHORIZED':
					  						$scope.resposta.titulo = "Pagamento autorizado!";
					  						$scope.resposta.texto = "Agradecemos pelo pagamento! A consulta estará disponível para a realização no horário agendado junto ao profissional.";					  					
					  					break;
					  				case 'CANCELLED':
					  						$scope.resposta.titulo = "Pagamento não autorizado!";
					  						$scope.resposta.texto = "O pagamento não foi realizado. Operação cancelada pela operadora.";
					  					break;
					  				case 'WAITING':
					  						$scope.resposta.titulo = "Aguardando pagamento";
					  						$scope.resposta.texto = "Caso já tenha realizado o pagamento, por favor, entre em contato com nosso suporte técnico.";
					  					break;
					  			} 
					  		}
					  		else {
					  			alert("Ocorreu um erro. "+retorno.msg);
					  		}
					  	})
						.catch(function(response) { 
							alert("Ocorreu um erro. Por favor, tente novamente.");
						})
						.finally(function() { 
				  			$scope.carregando = false;
				  		});   
					}
					else {
				  			$scope.carregando = false;
						alert("O número do cartão deve possuir 16 dígitos.");
			  		 
			  }
					}
		});

</script>
@stop
{{-- fim do conteúdo do yield atual --}}