
        /**
         * Funções relacionadas à troca de mensagens
         */
        var mensagens = {
            comUsuarioId: 0,
            currentPage: 1,
            dados: [],
            verificarIdMensagem:   function (indice) { 
                    var msgs = mensagens.dados;   
                    var retorno = false;
                    for (var i = 0; i < msgs.length; i++) {    
                    console.log(msgs[i].id == indice);
                        if(msgs[i].id == indice)
                            retorno = true;
                    } 
                    return retorno;
                },
            /**
             * Carregar mensagem
             */
            carregarMensagens: function( fazerScrollDown )
            {
                $.ajax({
                    url: '{{ action('MensagemController@APICarregarMensagens') }}/?page=' + mensagens.currentPage,
                    type: 'GET',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data: {
                        _token: $('meta[name=_token]').attr('content'),
                        'com_usuario_id' : mensagens.comUsuarioId,
                        'paginar' : 15
                    },
                    dataType: 'json',
                    success: function( data )
                    {
                        // incrementando paginacao
                        mensagens.currentPage++;

                        // remover botão de carregar anterior
                        $("#btnCarregarMensagemAnterior").remove();

                        // não armazenar cache
                        EJS.config({cache: false});

                        // pegando o nome do usuário contato na lista de contatos
                        var nome = $("#usuarioMensagemChat .selecionado").find('.bold').eq(0).html();

                        $("h4.nomeContatoMensagem").html("MENSAGENS COM " + nome);

                        if(mensagens.dados.length == 0)
                            mensagens.dados = data.mensagem;
 
                            for( var i = 0; i < data.mensagem.length; i++ )
                            {
                                // caso seja uma mensagem do usuário atual
                                if( data.mensagem[i].minha_mensagem == 1 )
                                {
                                    // carregando html
                                    var html = new EJS({
                                        url : '/ejs/template/inbox/eu.ejs'
                                    }).render({mensagem : data.mensagem[i]});
                                }
                                // de outro usuário
                                else
                                {
                                    // carregando html
                                    var html = new EJS({
                                        url : '/ejs/template/inbox/ela.ejs'
                                    }).render({mensagem : data.mensagem[i]});
                                }
     
                                    // adicionando na lista
                                    $("#usuarioMensagemChat .mensagens-chat").prepend( html ); 
                            } 

                        // adicionando botão para carregar mensagens anteriores
                        // apenas se tiver mensagens carregadas atualmente
                        if( data.mensagem.length > 0 )
                        {
                            $("#usuarioMensagemChat .mensagens-chat").prepend(
                                $('<li>').attr("id","btnCarregarMensagemAnterior")
                                         .html('<span>Carregar mensagens anteriores</span>')
                            );
                        }
                        else
                        {
                            $("#usuarioMensagemChat .mensagens-chat").prepend(
                                $('<li>').attr("id","btnCarregarMensagemAnterior")
                                         .addClass("semMensagens")
                                         .html('<span>Não há mensagens anteriores</span>')
                            );
                        }

                        // ao clicar para carregar mais mensagens
                        $("#btnCarregarMensagemAnterior").on('click', function(){
                            // carregando mais mensagens
                            // caso tenha mais
                            if( !$(this).hasClass('semMensagens') )
                                mensagens.carregarMensagens( false );
                        });

                        if( fazerScrollDown )
                        {
                            $("#usuarioMensagemChat .mensagens-chat").stop().animate({
                                scrollTop: Math.abs($("#usuarioMensagemChat .mensagens-chat").prop('scrollHeight') + $("#usuarioMensagemChat .mensagens-chat").scrollTop())
                            }, 2000);
                        }
                    }
                });
            },

            /**
             * Carregar os contatos do usuário logado
             */
            carregarContatos: function( usuarioId )
            {
                $.ajax({
                    url: '{{ action('MensagemController@APICarregarContatos') }}',
                    type: 'post',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data: {
                        _token: $('meta[name=_token]').attr('content'),
                        usuario_id : usuarioId
                    },
                    success: function( data )
                    {
                        // carregando as mensagens do primeiro contato
                        if( data.contatos.length > 0 )
                        {
                            mensagens.comUsuarioId = data.contatos[0].usuario_id;
                            mensagens.currentPage = 1;

                            // se for uma nova mensagem, não carregar mensagens
                            if( !GetQueryStringParams('novaMensagem') )
                                mensagens.carregarMensagens( true );
                        }

                        // não armazenar cache
                        EJS.config({cache: false});

                        // carregando html
                        var html = new EJS({
                            url : '/ejs/template/inbox/contato.ejs'
                        }).render({contatos : data.contatos});

                        // adicionando nos contatos
                        $("#usuarioMensagemChat .contatos-list").html( html );

                        // adicionado bind ao selecionar um contato
                        $("#usuarioMensagemChat .contatos-list li").bind('click', function(){
                            // se o atual não já estiver selecionado
                            if( !$(this).hasClass('selecionado') && parseInt($(this).attr('data-usuario-id')) > 0 )
                            {
                                // setando id de busca
                                mensagens.comUsuarioId = parseInt($(this).attr('data-usuario-id'));
                                mensagens.currentPage = 1;

                                // setando html
                                $("h4.nomeContatoMensagem").html("Carregando mensagens...");
                                $("#usuarioMensagemChat .mensagens-chat").html('');

                                // carregando mensagens
                                mensagens.carregarMensagens( true );

                                // removendo a classe dos demais elementos
                                $("#usuarioMensagemChat .contatos-list li").removeClass('selecionado');

                                // setando o clicado como selecionado
                                $(this).addClass('selecionado');
                            }
                        });
                    }
                });
            },

            /**
             * Função para enviar uma nova mensagem
             */
            enviarMensagem: function($obj)
            {
                // pegando a mensagem
                var mensagemText = $.trim($($obj).parent().find('.textoNovaMensagem').eq(0).val());

                if( mensagemText.length > 0 )
                {
                    objMensagem = {
                        mensagem: mensagemText,
                        data_mensagem: moment().format("YYYY-MM-DD HH:mm:ss"),
                        de_nome: $($obj).attr('data-nome'),
                        de_avatar: $($obj).attr('data-avatar')
                    }

                    // carregando html
                    var html = new EJS({
                        url : '/ejs/template/inbox/eu.ejs'
                    }).render({
                        mensagem : objMensagem
                    });

                    // adicionando na lista
                    $("#usuarioMensagemChat .mensagens-chat").append( html );

                    $("#usuarioMensagemChat .mensagens-chat").stop().animate({
                        scrollTop: Math.abs($("#usuarioMensagemChat .mensagens-chat").prop('scrollHeight') + $("#usuarioMensagemChat .mensagens-chat").scrollTop())
                    }, 500);

                    // enviando ao servidor
                    $.ajax({
                        url: '{{ action('MensagemController@APINovaMensagem') }}',
                        type: 'post',
                        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                        data: {
                            _token: $('meta[name=_token]').attr('content'),
                            paraUsuarioId: mensagens.comUsuarioId,
                            mensagem: objMensagem.mensagem
                        }
                    });

                    // limando campo
                    $($obj).parent().find('.textoNovaMensagem').eq(0).val('');
                }
            }
        }
        function verificarChat() {

            mensagens.carregarContatos( {{ Auth::user()->toArray()['id'] }} );

            // se for uma nova mensagem
            // carregar informações do contato
            if( parseInt( GetQueryStringParams('novaMensagem') ) > 0 )
            {
                mensagens.comUsuarioId = GetQueryStringParams('novaMensagem');

                // carregando informações
                $.ajax({
                    url: '{{ action('MensagemController@APIDadosUsuario') }}',
                    type: 'post',
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    data: {
                        _token: $('meta[name=_token]').attr('content'),
                        usuarioId : GetQueryStringParams('novaMensagem')
                    },
                    success: function( data )
                    {
                        $("h4.nomeContatoMensagem").html("MENSAGENS COM " + data.dados.nome);

                        $("#carregandoContatos").remove();

                        // carregando html
                        var html = new EJS({
                            url : '/ejs/template/inbox/contato.ejs'
                        }).render({contatos : [
                            {
                                avatar: data.dados.avatar,
                                usuario_id: GetQueryStringParams('novaMensagem'),
                                nome: data.dados.nome,
                                ultima_mensagem: ''
                            }
                        ]});

                        // adicionando nos contatos
                        $("#usuarioMensagemChat .contatos-list").prepend( html );
                    }
                });
            }
        }
        /**
         * Carregar todos os contatos do usuário
         */
        $(document).ready(function(){

            verificarChat();

        });
