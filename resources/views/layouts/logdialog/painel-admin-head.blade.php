<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <base href="<% url('/') %>/meu/perfil/" />
        <title>LogDialog</title>


        <link href="<% asset('painel-admin/css/bootstrap.min.css') %>" rel="stylesheet">
        <link href="<% asset('painel-admin/css/bootstrap.flat.min.css') %>" rel="stylesheet">
        <link href="<% asset('painel-admin/css/criaturo-admin-master.css') %>" rel="stylesheet">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<% asset('painel-admin/css/custom.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/angular-loading-bar/build/loading-bar.min.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/sweetalert/dist/sweetalert.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/animate.css/animate.min.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/izitoast/dist/css/iziToast.min.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/ng-dialog/css/ngDialog.min.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/ng-dialog/css/ngDialog-theme-default.min.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/Ionicons/css/ionicons.min.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/angular-ui-router-anim-in-out/css/anim-in-out.css') %>">
        <link rel="stylesheet" type="text/css" href="<% asset('lib/tinymce/skins/lightgray/skin.min.css') %>">
        <%-- Plugins AngularJS --%>
        <script type="text/javascript">
                const BASE_URL  = '<% asset("")   %>';
                const URL_API   = '<% asset("")   %>';
                const AUTH_KEY   = '<% csrf_token()   %>';
        </script>
        <script type="text/javascript" src="<% asset('lib/angular/angular.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angularUtils-pagination/dirPagination.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-animate/angular-animate.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-loading-bar/build/loading-bar.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/ngSanitize/index.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/sweetalert/dist/sweetalert.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/ngSweetAlert/SweetAlert.min.js') %>"></script> 
        <script type="text/javascript" src="<% asset('lib/angular-bootstrap/ui-bootstrap.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-bootstrap/ui-bootstrap-tpls.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/ng-file-upload/ng-file-upload-all.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/wow/dist/wow.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/izitoast/dist/js/iziToast.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/cub3/cub3Toast.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/cub3/cub3Filtros.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/ng-dialog/js/ngDialog.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/api-check/dist/api-check.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-formly/dist/formly.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-ui-router/release/angular-ui-router.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-ui-router-anim-in-out/anim-in-out.js') %>"></script> 
        <script type="text/javascript" src="<% asset('lib/tinymce/tinymce.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('lib/angular-ui-tinymce/dist/tinymce.min.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.module.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.routes.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.denuncias.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.especializacoes.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.pacientes.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.paginas.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.profissionais.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.servicos.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.servicos-solicitados.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.sliders.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.vantagens.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.factory.js') %>"></script>
        <script type="text/javascript" src="<% asset('painel-admin/js/app.services.js') %>"></script>

</head>