{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	    <div class="modal-dialog modal-login">
	         {{-- Modal content --}}
	        <div class="modal-content">
	            <div class="modal-header-login">
	                <img src="{{ asset('images/logdialog-footer.png') }}">
	            </div>
	            <div class="modal-body-login">
	                <p>Bem vindo ao LogDialog</p>
	                <form id="loginform" action="{{ action('AdminController@Login') }}" method="post">
						{{ csrf_field() }}

	                    <div class="form-group">
	                        <input type="text" class="form-control" name="login" id="usrname" placeholder="Usuário" value="{{ old('login') }}">

	                        @if( count($errors->all()) > 0 )
                                <span class="form_error" style="{{ $errors->has('login') ? 'display:block' : 'display:none' }}" >{{ $errors->first('login') }}</span>
                            @endif
	                    </div>
	                    <div class="form-group">
	                        <input type="password" class="form-control" name="senha" id="psw" placeholder="Senha">

	                        @if( count($errors->all()) > 0 )
                                <span class="form_error" style="{{ $errors->has('senha') ? 'display:block' : 'display:none' }}" >{{ $errors->first('senha') }}</span>
                            @endif
	                    </div>
	                    <div class="checkbox">
	                        <button type="submit" class="btn">Entrar</button>
	                    </div>
	                </form>
	            </div> 
	        </div>
	        <div class="modal-login-bottom">
	            <p onclick="javascript:home.modalEsqueciSenha();">Esqueci minha senha</p>
	        </div>
	    </div>

@stop
{{-- fim do conteúdo do yield atual --}}