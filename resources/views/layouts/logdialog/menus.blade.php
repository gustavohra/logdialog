{{-- colocando o conteúdo do yield 'nav-menu-principal' --}}
@section('nav-menu-principal')
    {{-- Main Menu --}}
    {{-- quando o usuário não está logado --}}
    @if(!Auth::check()  )
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                {{-- Brand and toggle get grouped for better mobile display --}}
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ action('IndexController@index') }}"><img class="header-logo" src="{{ asset('images/logdialog-logo.png') }}" alt=""></a>
                </div>

                {{-- Collect the nav links, forms, and other content for toggling --}}
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="{{ $view_name == 'index-home' ? 'menu-destaque' : '' }}" href="{{ action('IndexController@index') }}">Home</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#comofunciona" data-scroll="#comofunciona">Como funciona</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#profissionais" data-scroll="#profissionais">Profissionais</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#sobre" data-scroll="#sobre">Sobre</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#contato" data-scroll="#contato">Contato</a></li>
                        <li><a class="" data-toggle="modal" data-target="#modal-log">Login</a></li>
                    </ul>
                </div>
                {{-- /.navbar-collapse --}}
            </div>
            {{-- /.container-fluid --}}
        </nav>
    @else
        {{-- caso seja área do admin --}}
        @if( $view_name == 'profissionais-admin' || $view_name == 'paciente-admin' ||
             ( Auth::check() && $view_name == 'profissionais-agenda-completa' ) )
            {{-- menu quando o usuário está logado --}}
            <nav class="navbar navbar-default nav-bar-logado">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ action('IndexController@index') }}"><img class="header-logo" src="{{ asset('images/logdialog-admin.png') }}" alt=""></a>
                    </div>

                    <div class="search-top">
                         <form class="search" method="post" action="#" id="busca" >
                             <input type="text" id="buscaIn" name="q" placeholder="Pesquisar profissional..." />
                         </form>

                         <script type="text/javascript">
                             jQuery(function() {
                                 jQuery("#busca").submit(function(event) {
                                     event.preventDefault();
                                     var query = $("#buscaIn").val();
                                     window.location.href = "https://logdialog.com.br/profissionais?nome="+query;
                                 });
                             });
                         </script>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ action("PerfilController@meu") }}"><i class="fa fa-home"></i> Meu perfil</a></li>
                            
                            {{-- não exibir para os profissionais --}}
                            @if( Auth::user()->grupoSistema() != 'profissional' )
                                <li><a href="{{ action('ProfissionaisController@index') }}"><i class="fa fa-user-md" aria-hidden="true"></i> Profissionais</a></li>
                            @endif
                            
                            <li><a href="#" data-jq-dropdown="#notifications-dropdown"><i class="fa fa-commenting"></i> Notificações</a></li>
                            <li><a href="{{ action('LoginController@logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
                        </ul>
                    </div>
                    <div id="notifications-dropdown" class="jq-dropdown jq-dropdown-tip">
                        {{-- carregando notificações do usuário logado --}}
                        <?php
                            // pegando notificações pendentes
                            // apenas algumas para exibir no dropdown
                            $notificacoes = LogDialog\Model\Notificacao::listarNotificacoes( Auth::user()->toArray()['id'], 4 );
                        ?>

                        <ul class="jq-dropdown-menu notificationspreview">
                            <li class="title"><h3>Notificações</h3></li>
                            @foreach( $notificacoes as $item )
                                <?php
                                    // pegando dados mais específicos da notificação
                                    $item = LogDialog\Model\Notificacao::detalharItem( $item->id, $item->tipo );

                                ?>
                                <li class="notificationspreview-item notificationspreview-notready">
                                    <a href="{{ action('NotificacoesController@visualizar', ['idNotificacao' => $item['id'], 'idUsuario' => $item['para_usuario_id'], 'tipo' => $item['tipo']]) }}" class="row">
                                <div class="col-xs-9">
                                        <div class="notificationspreview-thumb">
                                            <img src="{{ asset($item['avatar_notificacao']) }}" alt="{{ $item['nome_usuario'] }}">
                                        </div>
                                        <div class="notificationspreview-content">
                                           <!--  <p><strong>{{ $item['nome_usuario'] }}</strong>, --> {{ $item['texto'] }}</p>
                                        </div>
                                </div>
                                <div class="col-xs-3">
                                    <small class="horario">
                                    @if(date('d/m/Y') == $item['data_registro']->format('d/m/Y'))
                                        {{$item['data_registro']->format('h:i')}}
                                    @else
                                        {{$item['data_registro']->format('d \d\e M')}}
                                    @endif
                                    </small>
                                </div>
                                    </a>
                                </li>
                            @endforeach
                            
                            @if( $notificacoes->count() == 0 )
                                <li class="txt-center">
                                    Não há nenhuma notificação pendente
                                </li>
                            @endif
                                <li class="vertodas">
                                    <a href="{{ action('NotificacoesController@index') }}">Ver todas as notificações</a>
                                </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        @else
            {{-- caso seja a home, e estiver logado, exibir com algumas alterações --}}
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    {{-- Brand and toggle get grouped for better mobile display --}}
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ action('IndexController@index') }}"><img class="header-logo" src="{{ asset('images/logdialog-logo.png') }}" alt=""></a>
                    </div>

                    {{-- Collect the nav links, forms, and other content for toggling --}}
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="{{ $view_name == 'index-home' ? 'menu-destaque' : '' }}" href="{{ action('IndexController@index') }}">Home</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#comofunciona" data-scroll="#comofunciona">Como funciona</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#profissionais" data-scroll="#profissionais">Profissionais</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#sobre" data-scroll="#sobre">Sobre</a></li>
                        <li><a class="" href="{{ action('IndexController@index') }}/#contato" data-scroll="#contato">Contato</a></li>
                            <li><a class="menu-destaque" href="{{ action('PerfilController@meu') }}">Meu Perfil</a></li>
                            <li><a class="menu-destaque" href="{{ action('LoginController@logout') }}">Logout</a></li>
                        </ul>
                    </div>
                    {{-- /.navbar-collapse --}}
                </div>
                {{-- /.container-fluid --}}
            </nav>
        @endif
    @endif

    <script>
        $(document).ready(function(){
            $(".navbar-nav li a").bind("click", function(evt){
                $(".navbar-nav li a").removeClass("menu-destaque");
                $(this).addClass("menu-destaque");
            });
        });
    </script>
@stop()