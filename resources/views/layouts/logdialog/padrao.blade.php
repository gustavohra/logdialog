<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_token" content="{!! csrf_token() !!}"/>

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.ico') }}"/>

        <title>LogDialog</title>

        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

            <script type="text/javascript">
                var site_url = '{{ asset('/') }}';
            </script>
        {{-- caso não esteja logado --}}
        @if( !Auth::check() )
            <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
        @else
            <script type="text/javascript">
                var usuarioId = {{Auth::user()->toArray()['id']}};
            </script>
            {{-- caso esteja logado --}}
            <link type="text/css" rel="stylesheet" href="{{ asset('dropdown/jquery.dropdown.css') }}" />

            {{-- jQuery Calendar --}}
            <link href="{{ asset('calendar/fullcalendar.css') }}" rel='stylesheet' />
            <link href="{{ asset('calendar/fullcalendar.print.css') }}" rel='stylesheet' media='print' />

            {{-- chartlist --}}
            <link rel="stylesheet" href="{{ asset("chartist/chartist.css") }}">

            {{-- carregando stylesheet --}}
            @yield('stylesheet')

            {{-- Cropper --}}
            <link href="{{ asset('cropper/cropper.min.css') }}" rel="stylesheet">

            {{-- bootstrap file input --}}
            <link href="{{ asset('bootstrap-fileinput/css/fileinput.min.css') }}" rel="stylesheet">
        @endif

        {{-- meus ajustes --}}
        <link href="{{ asset('css/wead.css') }}" rel="stylesheet">

        {{-- Owl Carousel Assets --}}
        <link href="{{ asset('owl-carousel/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('owl-carousel/owl.theme.css') }}" rel="stylesheet">

        {{-- token javascript --}}
        <script>
            var javascriptToken = "{{ csrf_token() }}";
        </script>

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <link href="{{ asset('filter-slider/nouislider.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
    </head>

    {{-- Este documento, é um modelo de template padrão, que será carregado por diversas views, e deve possuir um header e footer padrão. --}}

    {{-- O conteúdo (miolo), será colocado de acordo com cada yield setado aqui e em cada view --}}

    <body>

        {{-- div para teste de login com o facebook --}}
        {{-- <a href="javascript:appFB.doLogin();">Teste login Facebook</a> --}}
        {{-- <div id="status"></div> --}}

        {{-- Plugin JavaScript --}}
        {{-- Scripts --}}
        {{-- jQuery --}}
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>

        <script src="//use.fontawesome.com/c7448c9c8e.js"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

        {{-- Bootstrap Core JavaScript --}}
        <script src="{{ asset('js/bootstrap-validator.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/bootbox.min.js') }}"></script>

        <script src="{{ asset('js/classie.js') }}"></script>
        <script src="{{ asset('js/cbpAnimatedHeader.js') }}"></script>

        {{-- Custom Theme JavaScript --}}
        <script src="{{ asset('js/agency.js') }}"></script>

        <script src="{{ asset('owl-carousel/owl.carousel.js') }}"></script>

        {{-- carregando os scripts essenciais --}}
        <script src="{{ asset('js/facebook.js') }}"></script>

        {{-- jquery base64 //http://www.jqueryscript.net/text/Base64-Decode-Encode-Plugin-base64-js.html --}}
        <script src="{{ asset('js/jquery.base64.js') }}"></script>

        {{-- moment js - para manipulação de tempo --}}
        <script src="{{ asset('js/logDialog/moment.js') }}"></script>
        <script src="{{ asset('js/moment-pt.js') }}"></script>
        {{-- Datepicker --}}
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ asset('js/logDialog/locale-date.js') }}"></script>

        {{-- o script abaixo deve estar em todas as páginas que precisam de integraçao com o Facebook. --}}

        <script>
            function GetQueryStringParams(sParam)
            {
                var sPageURL = window.location.search.substring(1);

                var sURLVariables = sPageURL.split('&');

                for(var i = 0; i < sURLVariables.length; i++)
                {
                    var sParameterName = sURLVariables[i].split('=');

                    if(sParameterName[0] == sParam)
                    {
                        return sParameterName[1];
                    }
                }
            }

            // window.fbAsyncInit = function() {
            //     FB.init({
            //         appId      : '509579302576132',
            //         xfbml      : true,
            //         version    : 'v2.6'
            //     });
            // };

            // (function(d, s, id){
            //     var js, fjs = d.getElementsByTagName(s)[0];
            //     if (d.getElementById(id)) {return;}
            //     js = d.createElement(s); js.id = id;
            //     js.src = "https://connect.facebook.net/{{ str_replace('-', '_', Config::get('app.locale')) }}/sdk.js";
            //     fjs.parentNode.insertBefore(js, fjs);
            // }(document, 'script', 'facebook-jssdk'));

            // $(document).ready(function(){
            //     cbpAnimatedHeader();
            // });
        </script>

        {{-- cada yield, é uma sessão de conteúdo, que será definida na view --}}
        @yield('conteudo')

        {{-- aqui abaixo, vai o rodapé padrão --}}
        {{-- não carregar na tela de consulta --}}
        @if( $view_name != 'conferencia-consultorio' )
            <div id="footer-wrap">
                {{-- caso não esteja logado --}}
                {{-- e não seja a área admin dos perfis --}}
                @if( !Auth::check() || ( $view_name != 'profissionais-admin' && $view_name != 'paciente-admin' ) &&
                     ( Auth::check() && $view_name != 'profissionais-agenda-completa' ) &&
                     ( Auth::check() && $view_name != 'conferencia-espera' ) )
                    <div id="footer-main">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div style="text-align: center;">
                                        <img src="{{ asset('images/logdialog-footer.png') }}" alt="" class="img-responsive" style="padding-top: 20px; width: 120px; height: auto; text-align: center; display: inline-block;">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <h4>LogDialog</h4>
                                    <ul>
                                        <li><a href="{{ asset('institucional/sobre') }}">Sobre a Empresa</a></li>
                                        <li><a href="{{ asset('institucional/termos-profissionais') }}">Profissionais</a></li>

                                        @if( Auth::check() )
                                            @if( Auth::user()->grupoSistema() == 'profissional' )
                                                <li><a href="{{ asset('institucional/termos-profissionais') }}">Termos de Uso</a></li>
                                            @else
                                                <li><a href="{{ asset('institucional/termos-de-uso') }}">Termos de Uso</a></li>
                                            @endif
                                        @else
                                            <li><a href="{{ asset('institucional/termos-de-uso') }}">Termos de Uso</a></li>
                                        @endif
 
                                        <li><a href="{{ asset('institucional/perguntas-frequentes') }}">Perguntas Frequentes</a></li>
                                        <li><a href="{{ asset('institucional/politica-de-privacidade') }}">Política de Privacidade</a></li>
                                        {{-- <li><a href="">Segurança</a></li>
                                        <li><a href="">Trabalhe Conosco</a></li>
                                        <li><a href="">Vendas Corporativas</a></li> --}}
                                    </ul>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <h4>Links</h4>
                                    <ul>
                                        <li><a target="_blank" href="http://site.cfp.org.br/">Conselho Federal Psicologia</a></li>
                                        <li><a target="_blank" href="http://www.crpsp.org/site/">Conselho Regional Psicologia</a></li>
                                        <li><a target="_blank" href="http://site.cfp.org.br/wp-content/uploads/2012/07/Resoluxo_CFP_nx_011-12.pdf">Resolução CPF nº 11/2012</a></li>
                                        <li><a target="_blank" href="http://site.cfp.org.br/wp-content/uploads/2012/07/codigo_etica.pdf">Código de Ética do Psicólogo</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <h4>Suporte</h4>
                                    <ul>
                                        <li><a href="mailto:contato@logdialog.com.br">contato@logdialog.com.br</a></li>
                                        <li>Maximiliano Gigena Siqueira</li>
                                        <li>CRP 06/74122</li>
                                    </ul>
                                    <p>Rua do Rócio, 423 conj. 1411, 14º andar<br>Vila Olímpia - São Paulo - SP<br>CEP 04552-000<br>Tel.:(11) 3044-0319</p>
                                </div>
                                {{-- <div class="col-md-3 col-sm-12 col-xs-12">
                                    <h4>Newsletter</h4> 
                                    <form action="" method="post" id="cadastroNewsletter">
                                        <input type="text" id="emailNewsletter" placeholder="Insira seu email..."/>
                                        <div type="button" class="btn btn-gradient" onclick="newsletter.cadastrar('emailNewsletter'); return false;">Cadastrar</div>
                                    </form>
                                </div> --}}
                            </div>
                        </div>
                    </div> <!-- /#footer-main -->
                    <div id="footer-bottom">
                        <div class="container">
                            <div class="col-lg-6"><p>LogDialog {{ date('Y') }} - Todos os Direitos Reservados</p></div>
                            <div class="col-lg-6">
                                {{-- <ul class="social-btn">
                                    <li><a target="_blank" href=""><img src="{{ asset('images/social-fb.png') }}" alt=""></a></li>
                                    <li><a target="_blank" href=""><img src="{{ asset('images/social-ig.png') }}" alt=""></a></li>
                                    <li><a target="_blank" href=""><img src="{{ asset('images/social-tw.png') }}" alt=""></a></li>
                                </ul> --}}
                            </div>
                        </div>
                    </div>
                    <div class="footer-payment">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12"><img src="{{ asset('images/pagseguro.png') }}" alt="" class="img-responsive"></div>
                            </div>
                        </div>
                    </div>
                @else
                    {{-- não carregar em algumas telas --}}
                    @if( $view_name != 'conferencia-espera' )
                        {{-- caso esteja logado --}}
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="align-right">
                                        <ul class="menuadmin-footer">
                                            @if( Auth::check() )
                                                @if( Auth::user()->grupoSistema() == 'profissional' )
                                                    <li><a href="{{ asset('institucional/termos-profissionais') }}">Termos de Uso</a></li>
                                                @else
                                                    <li><a href="{{ asset('institucional/termos-de-uso') }}">Termos de Uso</a></li>
                                                @endif
                                            @else
                                                <li><a href="{{ asset('institucional/termos-de-uso') }}">Termos de Uso</a></li>
                                            @endif

                                            <li><a href="{{ asset('institucional/perguntas-frequentes') }}">Perguntas Frequentes</a></li>
                                            <li><a href="#">Suporte</a></li>
                                            <li><a href="#">Denunciar</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        @endif

        {{-- EJS Template --}}
        <script src="{{ asset('ejs/ejs_production.js') }}"></script>
        <script src="{{ asset('ejs/view.js') }}"></script>

        {{-- máscara --}}
        <script src="{{ asset('js/jquery.mask.min.js') }}"></script>

        {{-- carregando scripts utilitários --}}
        <script src="{{ asset('js/newsletter.js') }}"></script>
        <script src="{{ asset('js/logDialog_Util.js') }}"></script>

        <script src="{{ asset('dropdown/jquery.dropdown.js') }}"></script>

        {{-- script para controle de funções na home --}}
        <script src="{{ asset('js/logDialog/home.js') }}"></script>

        @if( Auth::check() )
            {{-- Cropper --}}
            <script src="{{ asset('cropper/cropper.min.js') }}"></script>
            <script src="{{ asset('js/cep-autocomplete.min.js') }}"></script>
            
            <script type="text/javascript">
                $("#cep_profissional").autocompleteAddress();
                $("#cep_paciente").autocompleteAddress();
                
            </script>
            {{-- bootstrap file input --}}
            <script src="{{ asset('bootstrap-fileinput/js/fileinput.min.js') }}"></script>
            <script src="{{ asset('bootstrap-fileinput/js/locales/pt-BR.js') }}"></script>

            @if( Auth::user()->grupoSistema() == 'paciente' )
                {{-- script para controle do formulário de edição de perfil do paciente --}}
                <script src="{{ asset('js/logDialog/perfilPaciente.js?v=2017') }}"></script>
            @elseif( Auth::user()->grupoSistema() == 'profissional' )
                {{-- script para controle do formulário de edição de perfil do profissional --}}
                <script src="{{ asset('js/logDialog/perfilProfissional.js?v=2017') }}"></script>
            @endif

            {{-- script para controle de exibição de calendário / agenda --}}
            <script src="{{ asset('js/logDialog/calendario.js?v=2017') }}"></script>

            {{-- script para controle de anotações --}}
            <script src="{{ asset('js/logDialog/nota.js') }}"></script>

            <script>
                // verificar a cada cinco minutos de página aberta
                // se nos próximos dez minutos o usuário tem alguma conferência para iniciar
                // e avisar caso tenha

                // exceto se a página atual já for de conferência
                @if( $view_name != 'conferencia-video-consulta' && $view_name != 'conferencia-consultorio' )
                    function verificarConferenciaProxima()
                    {
                        $.ajax({
                            url : '/conferencia/verificar/proxima',
                            type : 'post',
                            cache: false,               
                            headers: { 'X-CSRF-Token' : '{{ csrf_token()}}' },
                            dataType: 'json',
                            data: {
                                _token: $('meta[name=_token]').attr('content')
                            },
                            success: function( data )
                            {
                                // caso tenha retorno
                                if( data.id )
                                {
                                    _logDialog_Util.modalAlert(
                                        "Está na hora de uma cosulta!",
                                        "Já está na hora da sua consulta com: " + data.nome + ", não se atrase! <br /><br /> Clique <a href='" + data.link + "'>aqui</a> para entrar no consultório.",
                                        "Cancelar"
                                    );
                                }
                                else
                                {
                                    setTimeout(function(){
                                        verificarConferenciaProxima();
                                    }, 15000);
                                }
                            }
                        });
                    }

                    // ao carregar a página
                    // fazer uma verificação
                    verificarConferenciaProxima();

                    setTimeout(function(){
                        verificarConferenciaProxima();
                    }, 15000);
                @endif
            </script>

            {{-- como o usuário está logado --}}
            {{-- exibir a contagem de notificações não lidas, caso tenha --}}
            {{-- usando o css, pois a Criaturo desenvolveu o layout com o contador desta forma --}}

            {{-- buscando a contagem de notificações não lidas --}}
            <?php
                $naoLidas = \LogDialog\Model\Notificacao::whereParaUsuarioId( Auth::user()->toArray()['id'] )
                                ->where("data_visualizada", "=", null)
                                ->whereExibir(1)
                                ->count();
            ?>

            @if( $naoLidas > 0 )
                <style>
                    .fa-commenting:after{
                        content: "{{ $naoLidas > 99 ? 99 : $naoLidas }}" !important;
                    }
                </style>
            @else
                <style>
                    .fa-commenting:after{
                        display: none;
                    }
                </style>
            @endif
        @endif

        @if( isset($abrirModal) )
            <script>
                $(document).ready(function(){
                    $('#{{ $abrirModal }}').modal('show');
                });
            </script>
        @endif

        {{-- Datepicker --}}
        <script type="text/javascript"> 
                jQuery(document).ready(function(){
                   jQuery(".datepicker").datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true,
                        startDate: '0d',
                        language: "pt-BR",
                        locale: 'pt-br'
                    });

                $('[data-toggle="tooltip"]').tooltip(); 
//                   jQuery(".timepicker").timepicker();
                });
        </script>
 
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97545046-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>