@if(Auth::Check())
	<script type="text/javascript">
		var id_fix = {{Auth::user()->toArray()['id']}};
		var token_fix = '{{ csrf_token() }}';
	</script>
@endif
	@if(isset($vantagens))
<span class="hide">{{$i=0}}</span>


     @foreach($vantagens as $vantagem)
	<div id="modal-v0{{$i++}}" class="modal fade" role="dialog">
	    <div class="modal-dialog">
	         {{-- Modal content --}}
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title">{{$vantagem->titulo}}</h4>
	            </div>

	            <div class="img-v" style="background-image: url({{ $vantagem->imagem }})"> 
	            </div>

	            <div class="modal-body-v">
	               {!! $vantagem->conteudo !!}
	            </div>
	        </div>
	    </div>
	</div>
	@endforeach;
	@endif;
 
 	{{-- Modal de vídeo para a home --}}
 	<link rel="stylesheet" type="text/css" href="{{asset('lib/video.js/src/css/video-js.css')}}">
 	<script type="text/javascript" src="{{asset('lib/video.js/src/js/video-js.js')}}"></script>
 	@if (Route::getCurrentRoute()->uri() == '/')
	<div id="modal-video-home" class="modal fade" role="dialog" aria-labelledby="modal-video-home">
	    <div class="modal-video" > 
		        	<div class="modal-content">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
					<!-- -->
					  <video id="video-institucional" class="video-js" controls preload="auto" width="710" height="400"
						   data-setup="{'autoplay': false}">
						    <source src="{{asset('videos/Logdialog.mp4')}}" type='video/mp4'> 
						    <p class="vjs-no-js">  
						    	<iframe width="640" height="360" src="https://www.youtube.com/embed/mywcYeCy8io?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> 
						    </p>
						  </video>

	        </div>
	    </div>
	</div>
	<script type="text/javascript">
	$(function() {
		var videoInstitucional = videojs("video-institucional");
		    videoInstitucional.pause();
		  $('#video-home').click(function(event) {
		  	videoInstitucional.play();
		  });

			$('#modal-video-home').on('hidden.bs.modal', function () {
			    videoInstitucional.pause();
			});
	});
		
	</script>
	@endif
 	{{-- Modal orientação --}}
	<div id="modal-orientacao" class="modal fade" role="dialog">
	    <div class="modal-dialog">
	         {{-- Modal content --}}
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title">Entende-se por orientação:</h4>
	            </div>
 
	            <div class="modal-body-v"> 
					Atendimento realizado em até 20 encontros ou contatos virtuais, síncronos ou assíncronos; <br /><br />
					Processos prévios de Seleção de Pessoal; <br /><br />
					Aplicação de Testes devidamente regulamentados por resolução pertinente; <br /><br />
					Supervisão do trabalho de psicólogos, realizada de forma eventual ou complementar ao processo de sua formação profissional presencial; <br /><br />
					Atendimento Eventual de clientes em trânsito e/ou de clientes que momentaneamente se encontrem impossibilitados de comparecer ao atendimento presencial.  <br /><br />
					O uso de testes psicológicos informatizados são permitidos, desde que tenham avaliação favorável de acordo com Resolução CFP N° 002/03.<br /><br />
					<b>
					Casos graves, que representam algum risco para vida ou segurança do paciente, requerem e serão encaminhados para atendimento presencial. O Logdialog é um serviço de Orientação Psicológica Online, portanto se você está em risco ou pode colocar alguém em risco, orientamos para que não use o nosso serviço, mas  ficaremos satisfeitos em auxiliar no encaminhamento mais indicado para sua saúde e bem estar.</b> <br /><br />

					Para ajuda no encaminhamento entre em contato através do <a href="mailto:encaminhamento@logdialog.com.br">encaminhamento@logdialog.com.br</a> ou pelo tel. +55 11 4561-6695.
	            </div>
	        </div>
	    </div>
	</div>

	{{-- MODAL com o perfil dos profissionais --}}
	{{-- processa apenas se a variável com a listagem de profisisonais existir --}}
	@if( isset($profissionais) )
		@foreach( $profissionais as $k => $pessoa )
			<div class="modal fade" id="profissional_{{ $pessoa->usuario_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		      	<div class="modal-dialog" role="document">
		        	<div class="modal-content">
		          		<div class="modal-header">
		                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                	<h4 class="modal-title" id="myModalLabel">PERFIL DO PROFISSIONAL</h4>
		          		</div>
		          		<div class="modal-body">
		                	<div class="pt-top">
		                    	<div class="img-top">
		                       		<img src="{{ Avatar::urlImagem( $pessoa->usuario_id ) }}">
		                    	</div>
		                    	<div class="txt-top">
		                        	<h4>{{ $pessoa->tratamento }} {{ $pessoa->nome }} {{ $pessoa->sobrenome }}</h4>
		                        	<p>CRP {{ $pessoa->numero_crp }}<br>
		                        	   {{ $pessoa->cidade }} - {{ $pessoa->estado }}<br>Valor da consulta: Consulta de 1h - R${{ number_format( \LogDialog\Model\Profissional::valorPorHora( $pessoa->usuario_id, 'menor' ), 2, ',', '.' ) }}</p>
		                    	</div>
		                	</div>
			                <div class="pt-one">
			                    <h4>Especialidades/áreas de atuação</h4>

								{{-- pegando as especialidades --}}
								<?php
									$especialidades =  \LogDialog\Model\Especialidade::whereProfissionalId($pessoa->id)
														 ->where('data_aprovado', '!=', null)
														 ->where('visibilidade', '!=', 'PRIVADO')
								                         ->pluck('especialidade')
								                         ->toArray();
								?>

			                    <ul>
			                    	@foreach( $especialidades as $k => $item )
			                    		<li>{{ $item }}.</li>
			                    	@endforeach
			                    </ul>
			                </div>
			                <div class="pt-two">
			                    <h4>Experiência Profissional</h4>
			                    <p>{!! nl2br($pessoa->experiencia_profissional) !!}</p>
			                </div>
			                <div class="pt-three">
			                    <h4>Formação Acadêmica</h4>
			                    <p>{!! nl2br($pessoa->formacao_academica) !!}</p>
			                </div>
			                <div class="pt-four">
			                    <h4>Afiliações</h4>
			                    <p>{!! nl2br($pessoa->afiliacoes) !!}</p>
			                </div>

							{{-- não exibir quando for um profisisonal logado --}}
							{{-- ou se for a tela de agenda completa --}}
							@if( $view_name != 'profissionais-agenda-completa' )
								@if( Auth::check() )
									@if( Auth::user()->grupoSistema() != 'profissional' )
										<div class="btns-modal">
						                    <button type="button" class="btn btn-gradient" onclick="javascript:window.top.location='{{ action('ProfissionaisController@agendaCompleta', ["usuarioId" => $pessoa->usuario_id]) }}';">Agendar Consulta</button>
						                    <button id="bunt-space"type="button" class="btn btn-gradient" onclick="javascript:home.enviarMensagem('profissional', {{ $pessoa->usuario_id }}, {{ Auth::check() ? 1 : 0 }});">Enviar Mensagem</button>
						                </div>
									@endif
				                @else
				                	<div class="btns-modal">
					                    <button onclick="javascript:window.top.location='{{ action('ProfissionaisController@agendaCompleta', ["usuarioId" => $pessoa->usuario_id]) }}';" type="button" class="btn btn-gradient">Agendar Consulta</button>
					                    <button id="bunt-space"type="button" class="btn btn-gradient" onclick="javascript:home.enviarMensagem('profissional', {{ $pessoa->usuario_id }}, {{ Auth::check() ? 1 : 0 }});">Enviar Mensagem</button>
					                </div>
				                @endif
				            @endif
		          		</div>
		    		</div>
		      	</div>
		    </div>

			{{-- modal para agendar com este profissional --}}
			{{-- o conteúdo é carregado de forma dinâmica, para verificações na agenda --}}
			{{-- apenas se o usuário logado não for menor de idade --}}
			@if( Auth::check() )
				@if( AutorizacaoMenorIdade::agendamentoPermitido( Auth::user()->toArray()['id'] ) )
					<div class="modal fade" id="agendarProfissional_{{ $pessoa->usuario_id }}" tabindex="-1" role="dialog" aria-labelledby="agendarProfissional">
						<div class="modal-dialog" role="document">
						    <div class="modal-content">
						        <div class="modal-header">
						            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						            <h4 class="modal-title" id="agendarProfissional">AGENDE SUA CONSULTA</h4>
						        </div>
						        <div class="modal-body">
						            <div class="profile-topcontent">
						                <div class="row">
						                    <div class="col-lg-3 col-sm-12">
						                        <div class="profile-image">
						                            <img src="{{ Avatar::urlImagem( $pessoa->usuario_id ) }}" class="img-responsive">
						                        </div>
						                    </div>
						                    <div class="col-lg-9 col-sm-12">
						                        <div class="profile-text">
						                            <h4>{{ $pessoa->tratamento }} {{ $pessoa->nome }} {{ $pessoa->sobrenome }}</h4>
						                            <p>CRP {{ $pessoa->numero_crp }}<br>
						                               {{ $pessoa->cidade }} - {{ $pessoa->estado }}<br>Valor da consulta: Consulta de 1h - R${{ number_format(\LogDialog\Model\Profissional::valorPorHora( $pessoa->usuario_id, 'menor' ), 2, ',', '.' ) }}</p>
						                        </div>
						                    </div>
						                </div>
						            </div><!--.profile-topcontent -->

									{{-- aqui vai a parte dinâmica dos dias e horários --}}
									<div class="opcoesDeAgendamento">
										<form action="{{ action("ProfissionaisController@agendarConsultaModal", ["profissionalUsuarioId" => $pessoa->usuario_id]) }}" method="post" id="formAgendaModal_{{ $pessoa->id }}">
											<input type="hidden" name="horarioSelecionado" value="">

											<h4>ESCOLHA O HORÁRIO DESEJADO</h4>
						                        <div class="">
						                            <select name="servico" class="form-control">
						                            <?php
						                            	$servicos = \LogDialog\Model\ProfissionalServico::whereProfissionalId($pessoa->id)->where('visibilidade', 'PUBLICO')->get()->toArray();
						                            ?>
						                            	@foreach( $servicos as $keyServico => $itemServico )
						                            		<option value="{{ $itemServico['id'] }}">{{ $itemServico['servico'] }} - R$ {{ number_format($itemServico['valor'], 2, ',', '.') }}</option>
						                            	@endforeach
						                            </select>

									        <script type="text/javascript"> 
									                jQuery(document).ready(function(){
						                            <?php
						                            	// variável com os próximos 60 dias
											            // para dar as opções de dia ao usuário
											            $opcoesDias = [];
											            $opcoesDiasFormatado = '';
											            $tmp = Calendario::intervaloDias( \Carbon\Carbon::today(), \Carbon\Carbon::today()->addDays(60) );

											            $total = count($tmp);
											            $aux 	= 0;
											            $retorno = [];
											            foreach( $tmp as $kDia => $dia ){
											                if( !HorarioAtendimento::diaSemAtendimento( $pessoa->usuario_id, $dia->dayOfWeek ) ){
											                    $opcoesDias[] = $dia;
											                    $retorno[] = $dia->format('d/m/Y');

											                    
											                }
											            }
 
											            $retorno = json_encode($retorno);

						                            ?>
						                            var datasDisponiveis = $.parseJSON('{!!$retorno!!}'); 
									                   jQuery(".datepicker-diaOpcao2").datepicker({
									                        format: 'dd/mm/yyyy',
									                        startDate: '0d',
        													autoclose: true,
									                        language: "pt-BR",
									                        locale: 'pt-br',
											                beforeShowDay: function(date){
											                    var formattedDate = $.fn.datepicker.DPGlobal.formatDate(date, 'dd/mm/yyyy', 'pt-BR');
											                    if ($.inArray(formattedDate.toString(), datasDisponiveis) == -1){
											                        return {
											                            enabled : false
											                        };
											                    }
											                    return;
											                }
									                    });

									//                   jQuery(".timepicker").timepicker();
									                });
									        </script>
			                               <div class="input-group date datepicker-diaOpcao2" data-provide="datepicker">
										    <input type="text" class="form-control" name="data" placeholder="Clique para selecionar uma data">
											    <div class="input-group-addon">
											        <span class="glyphicon glyphicon-th"></span>
											    </div>
											</div>

 
						                        </div> <!-- .container (horário desejado) -->
						                    <div class="lista-horarios">
						                        <h4>Agendar horário</h4>
						                        <ul id="opcoesHorariosAgendaModal_{{ $pessoa->id }}">
						                        	@if( isset($opcoesDias[0]) )

							                        	<?php
							                        		$tmp = HorarioAtendimento::lista('todos');
							                        	?>
							                        	@foreach( $tmp as $kHorario => $item )
							                        		<?php if(
																	// verificando atendimento nesta data e hora
														            HorarioAtendimento::horarioAtendido( $pessoa->usuario_id, $opcoesDias[0]->dayOfWeek, $item ) &&

														            // verificando se não há agendamento confirmado
														            \LogDialog\Model\Agenda::whereProfissionalId( $pessoa->id )
														            ->where('data_confirmacao', '!=', null)
														            ->where('data_cancelamento', '=', null)
														            ->where('data_compromisso', '=', $opcoesDias[0]->format('Y-m-d') . " {$item}:00")
														            ->count() == 0
																):
															?>
							                        			<li data-horario="{{ $item }}">{{ $item }}</li>
							                        		<?php else: ?>
							                        			<li class="color-bck">{{ $item }} - indisponível</li>
							                        		<?php endif; ?>
							                        	@endforeach

							                        @else
							                        	<li>Não há horários para este profissional</li>
							                        @endif
						                        </ul>

												<script>
													var horariosSistema = [
														<?php $tmp = HorarioAtendimento::lista('todos'); ?>

														@foreach( $tmp as $kHorario => $itemHorario )
															"{{ $itemHorario }}",
														@endforeach
													];

													$(document).ready(function(){
														$("#formAgendaModal_{{ $pessoa->id }} [name='data']").bind('change', function(){
															if( $(this).val() != '' )
															{
																// zerando os valores de horário
																// para carregar com os horários retornados pelo sistema
																$("#opcoesHorariosAgendaModal_{{ $pessoa->id }}").html( $('<li class="color-bck">').html('carregando...') );

																$.ajax({
																	url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $pessoa->usuario_id]) }}',
																	type: 'post',
																	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
																	data: {
																		_token: $('meta[name=_token]').attr('content'),
																		timestamp : $(this).val()
																	},
																	success: function(data){
																		// adicionando o primeiro elemento
																		$("#opcoesHorariosAgendaModal_{{ $pessoa->id }}").html('');

																		for( var i = 0; i < horariosSistema.length; i++ )
																		{
																			var classe = 'color-bck';

																			for( var k = 0; k < data.horarios.length; k++ )
																			{
																				if( data.horarios[k] == horariosSistema[i] )
																					classe = '';
																			}

																			if( classe != '' )
																				$("#opcoesHorariosAgendaModal_{{ $pessoa->id }}").append( $('<li>').html(horariosSistema[i] + " - indisponível").addClass(classe) );
																			else
																				$("#opcoesHorariosAgendaModal_{{ $pessoa->id }}").append( $('<li>').html(horariosSistema[i]).attr('data-horario', horariosSistema[i]).addClass(classe) );
																		}
																	}
																});
															}
														});

														// selecionar um horário na lista
														$("#opcoesHorariosAgendaModal_{{ $pessoa->id }} li").on('click', function(){
															if( !$(this).hasClass('color-bck') )
															{
																// removendo todos os itens selecionados
																$(this).parent().find('li').removeClass('color-bck2');

																$("#formAgendaModal_" + {{ $pessoa->id }}).find('[name="horarioSelecionado"]').val('');

																// selecionando o atual
																if( $(this).attr('data-horario') != '' )
																{
																	$(this).addClass('color-bck2');

																	$("#formAgendaModal_" + {{ $pessoa->id }}).find('[name="horarioSelecionado"]').val( $(this).attr('data-horario') );
																}
															}
														});
													});
												</script>

						                    </div><!-- lista-horarios -->
						                    <div class="check-box">
					                            <input type="checkbox" name="horarioAdicional" value="1" id="checkHorarioAdicional_{{ $pessoa->id }}">
					                            Quero enviar outras opções de horário caso o horário escolhido fique indisponível
						                    </div>

						                    @if( count($errors->all()) > 0 )
				                                <div id="errorBlockAgendarConsultaModal" class="help-block erroInputAvatar">{{ $errors->first('horarioIndisponivel') }}</div>
				                            @else
				                            	<div id="errorBlockAgendarConsultaModal" class="help-block erroInputAvatar"></div>
				                            @endif

						                    <div class="container agendaModalListaOne" id="blocoHorarioAdicional_{{ $pessoa->id }}_one">
						                        <div class="extrahorarios-item">
						                        <?php /*
					                                <select name="diaOpcao1" class="agendar-select" id="listaAgendaDia_{{ $pessoa->id }}_one">
					                                    <option value="">Escolha a data</option>

					                                    @foreach( $opcoesDias as $kDia => $dia  )
						                            		<option value="{{ $dia->timestamp }}">{{ $dia->format('d/m/Y') }}</option>
						                            	@endforeach
					                                </select> */ ?>

						                               <div class="input-group date datepicker" data-provide="datepicker">
													    <input type="text" class="form-control" name="diaOpcao1" placeholder="Clique para selecionar uma data" id="listaAgendaDia_{{ $pessoa->id }}_one">
														    <div class="input-group-addon">
														        <span class="glyphicon glyphicon-th"></span>
														    </div>
														</div>

					                                <select name="horarioOpcao1" class="scroll-horario agendar-select form-control" id="horarioAdicional_{{ $pessoa->id }}_one">
					                                    <option value="">Selecione o horário</option>
					                                </select>
						                            <button type="button" class="btn-more morehours">
						                                <img src="{{ asset("images/icon-plus.png") }}">
						                            </button>
						                        </div> <!-- /.extrahorarios-item -->
						                    </div><!-- extrahorarios-content -->

						                    <div class="container agendaModalListaTwo" id="blocoHorarioAdicional_{{ $pessoa->id }}_two">
						                        <div class="extrahorarios-item">
						                        <?php /*
					                                <select name="diaOpcao2" class="agendar-select" id="listaAgendaDia_{{ $pessoa->id }}_two">
					                                    <option value="">Escolha a data</option>

					                                    @foreach( $opcoesDias as $kDia => $dia  )
						                            		<option value="{{ $dia->timestamp }}">{{ $dia->format('d/m/Y') }}</option>
						                            	@endforeach
					                                </select> */?>
						                               <div class="input-group date datepicker" data-provide="datepicker">
													    <input type="text" class="form-control" name="diaOpcao2" placeholder="Clique para selecionar uma data" id="listaAgendaDia_{{ $pessoa->id }}_one">
														    <div class="input-group-addon">
														        <span class="glyphicon glyphicon-th"></span>
														    </div>
														</div>
					                                <select name="horarioOpcao2" class="scroll-horario agendar-select form-control" id="horarioAdicional_{{ $pessoa->id }}_two">
					                                    <option value="">Selecione o horário</option>
					                                </select>
						                        </div> <!-- /.extrahorarios-item -->
						                    </div><!-- extrahorarios-content -->

						                    <div class="container agendaModalListaThree" id="blocoHorarioAdicional_{{ $pessoa->id }}_three">
						                        <div class="extrahorarios-item">
						                        <?php /*
					                                <select name="diaOpcao3" class="agendar-select" id="listaAgendaDia_{{ $pessoa->id }}_three">
					                                    <option value="">Escolha a data</option>

					                                    @foreach( $opcoesDias as $kDia => $dia  )
						                            		<option value="{{ $dia->timestamp }}">{{ $dia->format('d/m/Y') }}</option>
						                            	@endforeach
					                                </select>*/ ?>

						                               <div class="input-group date datepicker" data-provide="datepicker">
													    <input type="text" class="form-control" name="diaOpcao3" placeholder="Clique para selecionar uma data" id="listaAgendaDia_{{ $pessoa->id }}_one">
														    <div class="input-group-addon">
														        <span class="glyphicon glyphicon-th"></span>
														    </div>
														</div>

					                                <select name="horarioOpcao3" class="scroll-horario agendar-select form-control" id="horarioAdicional_{{ $pessoa->id }}_three">
					                                    <option value="">Selecione o horário</option>
					                                </select>
						                        </div> <!-- /.extrahorarios-item -->
						                    </div><!-- extrahorarios-content -->

						                    <div class="modal-buttons">
							                    <button type="submit" class="btn btn-gradient">Agendar Consulta</button>
							                </div>

							                <script>
							                	$(document).ready(function(){
							                		$("#checkHorarioAdicional_" + {{ $pessoa->id }}).bind('change', function(){
							                			if( $(this).is(':checked') )
							                			{
							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaOne").show();
							                			}
							                			else
							                			{
							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaOne").hide();
							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaTwo").hide();
							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaThree").hide();

							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaOne").find('select').val('');
															$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaTwe").find('select').val('');
															$("#formAgendaModal_" + {{ $pessoa->id }}).find(".agendaModalListaThree").find('select').val('');
							                			}
							                		});

							                		$("#formAgendaModal_" + {{ $pessoa->id }}).find('.agendaModalListaOne').find('.morehours').bind('click', function(){
							                			if( !$("#formAgendaModal_" + {{ $pessoa->id }}).find('.agendaModalListaTwo').is(':visible') )
							                			{
							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find('.agendaModalListaTwo').show();
							                			}
							                			else
							                			{
							                				$("#formAgendaModal_" + {{ $pessoa->id }}).find('.agendaModalListaThree').show();
							                			}
							                		});

							                		$("#listaAgendaDia_" + {{ $pessoa->id }} + "_one").bind('change', function(){
							                			if( $(this).val() != '' )
							                			{
							                				// zerando os valores de horário
															// para carregar com os horários retornados pelo sistema
															$("#horarioAdicional_" + {{ $pessoa->id }} + "_one").html( $('<option>').html('carregando...').val('') );

															$.ajax({
																url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $pessoa->usuario_id]) }}',
																type: 'post',
																headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
																data: {
																	_token: $('meta[name=_token]').attr('content'),
																	timestamp : $(this).val()
																},
																success: function(data){
																	// adicionando o primeiro elemento
																	$("#horarioAdicional_" + {{ $pessoa->id }} + "_one").html( $('<option>').html('Escolha o horário').val('') );

																	for( var i = 0; i < data.horarios.length; i++ )
																	{
																		$("#horarioAdicional_" + {{ $pessoa->id }} + "_one").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
																	}
																}
															});
							                			}
							                		});

							                		$("#listaAgendaDia_" + {{ $pessoa->id }} + "_two").bind('change', function(){
							                			if( $(this).val() != '' )
							                			{
							                				// zerando os valores de horário
															// para carregar com os horários retornados pelo sistema
															$("#horarioAdicional_" + {{ $pessoa->id }} + "_two").html( $('<option>').html('carregando...').val('') );

															$.ajax({
																url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $pessoa->usuario_id]) }}',
																type: 'post',
																headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
																data: {
																	_token: $('meta[name=_token]').attr('content'),
																	timestamp : $(this).val()
																},
																success: function(data){
																	// adicionando o primeiro elemento
																	$("#horarioAdicional_" + {{ $pessoa->id }} + "_two").html( $('<option>').html('Escolha o horário').val('') );

																	for( var i = 0; i < data.horarios.length; i++ )
																	{
																		$("#horarioAdicional_" + {{ $pessoa->id }} + "_two").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
																	}
																}
															});
							                			}
							                		});

							                		$("#listaAgendaDia_" + {{ $pessoa->id }} + "_three").bind('change', function(){
							                			if( $(this).val() != '' )
							                			{
							                				// zerando os valores de horário
															// para carregar com os horários retornados pelo sistema
															$("#horarioAdicional_" + {{ $pessoa->id }} + "_three").html( $('<option>').html('carregando...').val('') );

															$.ajax({
																url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $pessoa->usuario_id]) }}',
																type: 'post',
																headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
																data: {
																	timestamp : $(this).val()
																},
																success: function(data){
																	// adicionando o primeiro elemento
																	$("#horarioAdicional_" + {{ $pessoa->id }} + "_three").html( $('<option>').html('Escolha o horário').val('') );

																	for( var i = 0; i < data.horarios.length; i++ )
																	{
																		$("#horarioAdicional_" + {{ $pessoa->id }} + "_three").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
																	}
																}
															});
							                			}
							                		});
							                	});
							                </script>
						                </form>
									</div>

						        </div><!--.modal-body -->
						    </div><!-- .modal-content -->
						</div> <!-- .modal-dialog -->
					</div><!-- .modal fade -->
				@endif
			@endif

		@endforeach
	@endif

	{{-- Modal --}}
	<div class="modal fade" id="modal-log" role="dialog">
	    <div class="modal-dialog modal-login">
	         {{-- Modal content --}}
	        <div class="modal-content">
	            <div class="modal-header-login">
	                <img src="{{ asset('images/logdialog-footer.png') }}">
	            </div>
	            <div class="modal-body-login">
	                <p>Bem vindo ao LogDialog</p>
	                <form id="loginform" action="{{ action('LoginController@index') }}" method="post">
						{{ csrf_field() }}

	                    <div class="form-group">
	                        <input type="text" class="form-control" name="login" id="usrname" placeholder="E-mail" value="{{ old('login') }}">

	                        @if( count($errors->all()) > 0 )
                                <span class="form_error" style="{{ $errors->has('login') ? 'display:block' : 'display:none' }}" >{{ $errors->first('login') }}</span>
                            @endif
	                    </div>
	                    <div class="form-group">
	                        <input type="password" class="form-control" name="senha" id="psw" placeholder="Senha">

	                        @if( count($errors->all()) > 0 )
                                <span class="form_error" style="{{ $errors->has('senha') ? 'display:block' : 'display:none' }}" >{{ $errors->first('senha') }}</span>
                            @endif
	                    </div>
	                    <div class="checkbox">
	                        <button type="submit" class="btn">Entrar</button>
	                    </div>
	                </form>
	            </div>
	            <div class="modal-footer-login">
	                <p>Ainda não criou sua conta? <a href="{{ action('CadastroController@registrar') }}">Cadastre-se agora</a></p>
	            </div>
	        </div>
	        <div class="modal-login-bottom">
	            <p onclick="javascript:home.modalEsqueciSenha();">Esqueci minha senha</p>
	        </div>
	    </div>
	</div>

	{{-- Modal - esqueci a senha --}}
    <div class="modal fade" id="esqueciSenha" role="dialog">
	    <div class="modal-dialog modal-md">
	    <!--Modal-content-->
	        <!-- HEADER-->
	        <div class="modal-alert modal-esqueci-senha">
	            <div class="header-alert">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <!-- <img src="{{ asset('images/alert.png') }}"> -->
	                <h4 class="modal-title">Redefinição de senha</h4>
	            </div>
	            <!-- BODY -->
	            <div class="modal-body">
	            	<div class="txt-alert"> 

		                <p class="centralizar-h">Informe seu e-mail de cadastro abaixo, enviaremos uma mensagem com as instruções para redefinição de senha.</p>

		                <form action="{{ action("LoginController@solicitarNovaSenha") }}" method="post" id="formEsqueciSenha">
							{{ csrf_field() }}

		                	<div class="form-group">
		                		<input type="text" id="emailNovaSenha" name="emailNovaSenha" class="form-control" placeholder="E-mail">

		                		@if( count($errors->all()) > 0 )
	                                <span class="form_error" style="{{ $errors->has('email') ? 'display:block' : 'display:none' }}" >{{ $errors->first('email') }}</span>
	                            @endif
		                	</div>

		                	<div class="row">
				            	<div class="col-md-12">
				            		<div class="col-md-3">&nbsp;</div>
				            		<div class="col-md-6 centralizar-h">
				            			<button type="submit" class="btn">
						                	ENVIAR E-MAIL
						                </button>
				            		</div>
				            		<div class="col-md-3">&nbsp;</div>
				            	</div>
				            </div>
		                </form>
		            </div>
	            </div>
	        </div>
	    </div> <!-- modal final -->
	</div>

	{{-- AGENDAR CONSULTA NL --}}
	{{-- Modal --}}

	{{-- esse é quando o usuário não está logado --}}
	@if( !Auth::check() )
		<div class="modal fade" id="agendarConsulta" role="dialog">
		    <div class="modal-dialog modal-md">
		         {{-- HEADER --}}
		        <div class="modal-content">
		            <div class="modal-header">
		              	<button type="button" class="close" data-dismiss="modal">&times;</button>
		              	<h4 class="modal-title">VOCÊ AINDA NÃO É CADASTRADO<br>NO LOGDIALOG?</h4>
		            </div>
		            {{-- BODY --}}
		            <div class="btn-nl">
		                <button type="button" class="btn btn-default">
		                	<a href="{{ action("CadastroController@registrar") }}">
		                		CADASTRAR
		                	</a>
		                </button>
		            </div>
		            {{-- FOOTER --}}
		           	<div class="modal-footer" id="nl-txt">
		               	<p>Se você ja é cadastrado,</p>
		               	<p id="nl-blue">
							<a data-dismiss="modal" data-toggle="modal" data-target="#modal-log">clique aqui para ir para a tela de Login</a>
		               	</p>
		           	</div>
		        </div>
		    </div>
		</div>
	{{-- caso seja menor de idade --}}
	@elseif( !AutorizacaoMenorIdade::agendamentoPermitido( Auth::user()->toArray()['id'] ) )
		<div class="modal fade" id="agendarConsulta" role="dialog">
		    <div class="modal-dialog modal-md">
		    <!--Modal-content-->
		        <!-- HEADER-->
		        <div class="modal-alert">
		            <div class="header-alert">
		                <button type="button" class="close" data-dismiss="modal">&times;</button>
		                <img src="{{ asset('images/alert.png') }}">
		                <h4 class="modal-title">ALERTA</h4>
		            </div>
		            <!-- BODY -->
		            <div class="txt-alert">
		                <h4>Não foi possível agendar sua consulta</h4>
		                <p class="box-alert">
		                @if(Auth::check() && Auth::user()->grupoSistema() == 'profissional')
		                	Para agendar uma consulta com um profissional, é preciso estar logado com uma conta de paciente
		                @else
		                	Identificamos que você é menor de idade, e que não tem uma<br> autorização dos resposáveis vinculada ao seu cadastro. Por<br> favor, entre no seu perfil e faça upload da autorização (que<br> pode ser <span>baixada aqui</span>) e consulte também os termos de uso<br> para consultas para menores de idade.
		                @endif
		                </p>
		            </div>
		            <!-- FOOTER -->
				            <div class="footer-alert">
				                @if(Auth::check() && Auth::user()->grupoSistema() == 'paciente')
						                <button type="button" class="btn btn-default">
						                	<a href="{{ action("PerfilController@meu") }}">
						                    	<img src="{{ asset('images/btn-meu-perfil.png') }}">IR PARA MEU PERFIL
						                    </a>
						                </button>
						        @else
						        	<br />
					            @endif
				            </div>
		        </div>
		    </div> <!-- modal final -->
		</div>
	@endif

	{{-- modal para editar perfil do paciente --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'paciente' )
	<?php
		// pegando dados do perfil
        $paciente = \LogDialog\Model\Paciente::whereUsuarioId( Auth::user()->toArray()['id'] )
                        ->first()
                        ->toArray();
	?>
		<div class="modal fade" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="modalPerfilLabel">
		    <div class="modal-dialog modal-lg" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                <h4 class="modal-title" id="modalPerfilLabel">Meu Perfil</h4>
		            </div>
		            <div class="modal-body modal-body-mt">
		                <div class="row">
							<form action="{{ action("PerfilController@editarPaciente", ['id' => Auth::user()->toArray()['id']]) }}" method="post" data-toggle="validator" role="form" id="formPerfilPaciente">
		                    <div class="col-lg-6">
		                        <div class="editarperfil-avatar">
		                            <div class="row">
		                                <div class="col-lg-4">
		                                    <img src="{{ asset( strlen($paciente['avatar']) > 0 ? "upload/user/" . Auth::user()->toArray()['id'] . "/" . $paciente['avatar'] : 'images/avatar-padrao.jpg' ) }}" alt="{{ $paciente['nome'] }} {{ $paciente['sobrenome'] }}" id="avatarPacienteImg" class="img-responsive">
		                                </div>
		                                <div class="col-lg-8 col-nopadding">
		                                    <div class="editarperfil-avatar-content">
												{{-- formulário para submeter a alteração da imagem --}} 
		                                        	<input type="hidden" id="inputRemoverAvatar" name="inputRemoverAvatar" value="0">

			                                        <h3>Avatar</h3>

			                                        <input type="file" class="file-loading" name="inputAvatarPaciente" id="inputAvatarPaciente">

			                                        <input type="hidden" class="file-loading" name="uploadAvatarPaciente" id="uploadAvatarPaciente">

			                                        <button type="button" tabindex="500" title="Clear selected files" class="btn btn-modal btn-modalalert fileinput-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i>  <span class="hidden-xs">Remover</span></button>

			                                     {{--   <div class="atualizar-btn">
			                                            <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="javascript:perfilPaciente.uploadAvatar(this);" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
			                                        </div> --}} 
		                                    </div>
		                                </div>

		                                <div id="errorBlockAvatar" class="help-block erroInputAvatar"></div>
		                            </div> <!-- /.row -->
		                        </div>
		                        <div class="editarperfil-senha">
		                            <h3>Alterar Senha</h3> 
		                                  <div class="checkbox">
									          <label>
									            <input type="checkbox" id="alterar_senha_paciente"> Alterar senha?
									          </label>
									       </div>
		                                <div class="form-group">
		                                	<label for="novasenha" class="control-label">Nova Senha</label>
		                                	<input type="password" id="novasenha" name="novasenha" placeholder="Digite sua nova senha..."  data-minlength="6" data-maxlength="150" maxlength="150" disabled="disabled">
		                                	<div class="help-block">A senha deve conter ente 6 e 150 dígitos.</div>
		                                </div>
		                                <div class="form-group">
		                                	<label for="conf_novasenha" class="control-label">Digite novamente</label>
		                                	<input type="password" id="conf_novasenha" name="conf_novasenha" placeholder="Digite novamente sua nova senha..." data-match="#novasenha" data-match-error="As senhas não coincidem" placeholder="Confirm"  disabled="disabled">
		                                	<div class="help-block with-errors"></div>
		                                </div>
		                                <script type="text/javascript">
		                                function habilitarSenha() {

												        $('#novasenha').val('').prop('disabled', false);
												        $('#novasenha').prop('required', true);
												        $('#conf_novasenha').val('').prop('disabled', false);
												        $('#conf_novasenha').prop('required', true);
		                                }
		                                function desabilitarSenha() {

												        $('#novasenha').val('').prop('disabled', true);
												        $('#novasenha').prop('required', false);
												        $('#conf_novasenha').val('').prop('disabled', true);
												        $('#conf_novasenha').prop('required', false);
		                                }
										desabilitarSenha();
		                                	$('#alterar_senha_paciente').change(function(){
												    if ($('#alterar_senha_paciente').is(':checked') == true){
												        habilitarSenha();

												    } else {
												    	desabilitarSenha();

												    }
												});
		                                </script>
		                                <!-- <div class="atualizar-btn">
		                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="perfilPaciente.trocarSenha(this); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
		                                </div> --> 
		                        </div>

								{{-- verificações para menor de idade --}}
		                        @if( AutorizacaoMenorIdade::isMenorIdade( Auth::user()->toArray()['id'] ) )
		                        	<div class="title-auto">
			                            <h3>Autorização dos responsáveis</h3>

			                            {{-- caso já possa agendar consultas, significa que a documentação está ok --}}
			                            @if( AutorizacaoMenorIdade::agendamentoPermitido( Auth::user()->toArray()['id'] ) )
				                            <div class="txt-auto">
				                                <p><i class="fa fa-check-circle-o"></i> AUTORIZAÇÃO APROVADA</p>
				                            </div>
				                        {{-- caso o envio já tenha sido feito, mas ainda esteja em aprovação --}}
				                        @elseif( AutorizacaoMenorIdade::documentoEnviado( Auth::user()->toArray()['id'] ) )
				                        	<div class="txt-reprov txt-aguarde">
				                                <p><i class="fa fa-clock-o"></i> AUTORIZAÇÃO EM APROVAÇÃO</p>
				                            </div>
				                            <div class="txt-infor">
				                            	<p>Sua autorização será avaliada, aguarde a finalização processo.</p>
				                            </div>
				                        {{-- caso o envio ainda não tenha sido feito, ou o admin recusou a documentação --}}
				                        @else
					                        <div class="txt-reprov">
				                                <p><i class="fa fa-times-circle-o"></i> AUTORIZAÇÃO PENDENTE</p>
				                            </div>
				                            <div class="txt-infor">
				                            	<form action="{{ action("PerfilController@anexarAutorizacaoMenor", ['id' => Auth::user()->toArray()['id']]) }}" method="post" data-toggle="validator" role="form" id="formAutorizacaoMenor" enctype="multipart/form-data">
				                            		<input type="file" class="file-loading" name="inputAutorizacao" id="inputAutorizacao">

				                            		@if( count($errors->all()) > 0 )
			                                            <span class="form_error" style="{{ $errors->has('inputAutorizacao') ? 'display:block' : 'display:none' }}" >{{ $errors->first('inputAutorizacao') }}</span>
			                                        @endif
			                                    </form>
				                            </div>

				                            <div id="errorBlockAutorizacao" class="help-block erroInputAvatar"></div>
				                        @endif
			                            <div class="txt-infor">
			                            	<p>O documento de autorização dos responsáveis é necessário para pacientes menores de idade. Assim como a presença na primeira consulta.</p>
			                            </div>
			                        </div>
		                        @endif
		                    </div> <!-- /.col -->

		                    <div class="col-lg-6">
		                        <h3>Informações Pessoais</h3>
									<input type="hidden" name="_token" id="_token-paciente" value="{{ csrf_token() }}"> 
		                            <div class="row">
		                                <div class="col-lg-12 form-group">
		                                    <label for="nome_paciente" class="control-label">Primeiro nome</label>
		                                    <input type="text" required="true" class="form-control" name="nome_paciente" id="nome_paciente" placeholder="Pedro" value="{{ old('nome_paciente') ?? $paciente['nome'] }}" maxlength="255">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="sobrebnome_paciente" class="control-label">Sobrenome</label>
		                                    <input type="text" required="true" class="form-control" name="sobrebnome_paciente" id="sobrebnome_paciente" placeholder="Sobrenome" value="{{ old('sobrebnome_paciente') ?? $paciente['sobrenome'] }}" maxlength="255">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="email_paciente">E-mail</label>
		                                    <input type="text" name="email_paciente" class="form-control" id="email_paciente" value="{{ Auth::user()->toArray()['email'] }}" placeholder="E-mail" disabled="disabled">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="nascimento_paciente" class="control-label">Data de nascimento</label>
		                                    <input type="text" required="true" class="form-control" disabled="disabled" name="nascimento_paciente" id="nascimento_paciente" placeholder="Data de nascimento" value="{{ old('nascimento_paciente') ?? date('d/m/Y', strtotime($paciente['nascimento'])) }}" data-mask="00/00/0000" data-mask-clearifnotmatch="true">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="cpf_paciente" class="control-label">CPF</label>

		                                    <input type="text" required="true" class="form-control" name="cpf_paciente" id="cpf_paciente" placeholder="CPF" value="{{ old('cpf_paciente') ?? $paciente['cpf'] }}" {{ $paciente['cpf'] != '' ? 'disabled="disabled"' : '' }} data-mask="000.000.000-00" data-mask-clearifnotmatch="true" data-error="Informe um CPF válido." data-remote="{{ action('Validador\DocumentoController@validar') }}">

		                                    <div class="help-block with-errors"></div>
		                                </div>

		                                <div class="col-lg-12 form-group">
		                                    <label for="cep_paciente" class="control-label">CEP</label>
		                                    <input type="text" name="cep_paciente" class="form-control" id="cep_paciente" placeholder="CEP" value="{{ old('cep_paciente') ?? $paciente['cep'] }}" data-mask="00000-000" data-mask-clearifnotmatch="true">
		                                </div>
		                                <div class="col-lg-9 form-group">
		                                    <label for="street_paciente" class="control-label">Rua</label>
		                                    <input type="text" name="street_paciente" class="form-control" id="street_paciente" placeholder="Rua" value="{{ old('street_paciente') ?? $paciente['endereco'] }}" maxlength="400" data-autocomplete-address>
		                                </div>
		                                <div class="col-lg-3 form-group">
		                                    <label for="rua_profissional">Nº</label>
		                                    <input type="text" name="numero_paciente" class="form-control" id="numero" placeholder="" value="{{  $paciente['end_numero'] }}" maxlength="250" >
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="neighborhood_paciente" class="control-label">Bairro</label>
		                                    <input type="text" name="neighborhood_paciente" class="form-control" id="neighborhood_paciente" placeholder="Bairro" value="{{ old('neighborhood_paciente') ?? $paciente['bairro'] }}" maxlength="255" data-autocomplete-neighborhood>
		                                </div>
		                                <div class="col-lg-7 form-group">
		                                    <label for="city_paciente" class="control-label">Cidade</label>
		                                    <input type="text" name="city_paciente" class="form-control" id="city_paciente" placeholder="Cidade" value="{{ old('city_paciente') ?? $paciente['cidade'] }}"  maxlength="400" data-autocomplete-city>
		                                </div>
		                                <div class="col-lg-5 form-group">
		                                    <label for="state_paciente" class="control-label">Estado</label>
		                                    <input type="text" name="state_paciente" class="form-control" id="state_paciente" placeholder="UF" maxlength="2" value="{{ old('state_paciente') ?? $paciente['estado'] }}" data-autocomplete-state>
		                                </div>

		                            </div> 
		                    </div> <!-- /.col -->
		                                <div class="atualizar-btn">
		                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="perfilPaciente.atualizarPerfil(this); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
		                                </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		{{-- Modal - Crop --}}
		<div class="modal fade" id="CropModal" role="dialog">
		    <div class="modal-dialog modal-md">
		         {{-- HEADER --}}
		        <div class="modal-content">
		            <div class="modal-header">
		              	<button type="button" class="close" data-dismiss="modal">&times;</button>
		              	<h4 class="modal-title">Avatar</h4>
		            </div>
		            {{-- BODY --}}
		            <div class="btn-nl modal-body">
		                <span id="CropImgTarget"></span>
		            </div>
		            {{-- FOOTER --}}
		           <div class="modal-footer" id="nl-txt">
		               <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:perfilPaciente._pegarCorteAvatar();">Cortar</button>
		           </div>
		        </div>
		    </div>
		</div>
	@endif
	{{-- fim - modal editar perfil do paciente --}}

	{{-- modal para editar o perfil do admin --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
	<?php
		// pegando dados do perfil
        $profissional = LogDialog\Model\Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )
                        ->first()
                        ->toArray();

        // formatando tratamento
        $profissional['tratamento'] = $profissional['sexo'] == 'feminino' ? 'Dra.' : 'Dr.';
	?>

			<div class="modal fade" id="profissional_{{ Auth::user()->toArray()['id'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		      	<div class="modal-dialog" role="document">
		        	<div class="modal-content">
		          		<div class="modal-header">
		                	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                	<h4 class="modal-title" id="myModalLabel">PERFIL DO PROFISSIONAL</h4>
		          		</div>
		          		<div class="modal-body">
		                	<div class="pt-top">
		                    	<div class="img-top">
		                       		<img src="{{ Avatar::urlImagem( $profissional['usuario_id'] ) }}">
		                    	</div>
		                    	<div class="txt-top">
		                        	<h4>{{ $profissional['tratamento'] }} {{ $profissional['nome'] }} {{ $profissional['sobrenome'] }}</h4>
		                        	<p>CRP {{ $profissional['numero_crp'] }}<br>
		                        	   {{ $profissional['cidade'] }} - {{ $profissional['estado'] }}<br>Valor da consulta: Consulta de 1h - R${{ number_format( \LogDialog\Model\Profissional::valorPorHora( $profissional['usuario_id'], 'menor' ), 2, ',', '.' ) }}</p>
		                    	</div>
		                	</div>
			                <div class="pt-one">
			                    <h4>Especialidades/áreas de atuação</h4>

								{{-- pegando as especialidades --}}
								<?php
									$especialidades =  \LogDialog\Model\Especialidade::whereProfissionalId($profissional['id'])
														 ->where('data_aprovado', '!=', null)
														 ->where('visibilidade', '!=', 'PRIVADO')
								                         ->pluck('especialidade')
								                         ->toArray();
								?>

			                    <ul>
			                    	@foreach( $especialidades as $k => $item )
			                    		<li>{{ $item }}.</li>
			                    	@endforeach
			                    </ul>
			                </div>
			                <div class="pt-two">
			                    <h4>Experiência Profissional</h4>
			                    <p>{!! nl2br($profissional['experiencia_profissional']) !!}</p>
			                </div>
			                <div class="pt-three">
			                    <h4>Formação Acadêmica</h4>
			                    <p>{!! nl2br($profissional['formacao_academica']) !!}</p>
			                </div>
			                <div class="pt-four">
			                    <h4>Afiliações</h4>
			                    <p>{!! nl2br($profissional['afiliacoes']) !!}</p>
			                </div> 
		          		</div>
		    		</div>
		      	</div>
		    </div>


		<div class="modal fade modalPerfilProfissional" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="modalPerfilLabel">
		    <div class="modal-dialog modal-lg" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                <h4 class="modal-title" id="modalPerfilLabel">Meu Perfil</h4>
		            </div>
		            <div class="modal-body">
		                <div class="row">
		                        <form id="formPerfilProfissional" action="{{ action("PerfilController@editarProfissional", ['id' => Auth::user()->toArray()['id']]) }}" method="post" data-toggle="validator" role="form">
		                    <div class="col-lg-6">
		                        <div class="editarperfil-avatar">
		                            <div class="row">
		                                <div class="col-lg-4">
		                                    <img src="{{ asset( strlen($profissional['avatar']) > 0 ? "upload/user/" . Auth::user()->toArray()['id'] . "/" . $profissional['avatar'] : 'images/avatar-padrao.jpg' ) }}" alt="{{ $profissional['tratamento'] }} {{ $profissional['nome'] }} {{ $profissional['sobrenome'] }}" id="avatarPacienteImg" class="img-responsive">
		                                </div>
		                                <div class="col-lg-8 col-nopadding">
		                                    <div class="editarperfil-avatar-content">
												{{-- formulário para submeter a alteração da imagem --}} 
		                                        	<input type="hidden" id="inputRemoverAvatar" name="inputRemoverAvatar" value="0">

			                                        <h3>Avatar</h3>

			                                        <input type="file" class="file-loading" name="inputAvatarProfissional" id="inputAvatarProfissional">

			                                        <input type="hidden" class="file-loading" name="uploadAvatarProfissional" id="uploadAvatarProfissional">

			                                        <button type="button" tabindex="500" title="Clear selected files" class="btn btn-modal btn-modalalert fileinput-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i>  <span class="hidden-xs">Remover</span></button>

			                                       {{-- <div class="atualizar-btn">
			                                            <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="javascript:perfilProfissional.uploadAvatar(this);" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
			                                        </div> --}} 
		                                    </div>
		                                </div>

		                                <div id="errorBlockAvatar" class="help-block erroInputAvatar"></div>
		                            </div> <!-- /.row -->
		                        </div>
		                        <div class="editarperfil-senha">
		                            <h3>Alterar Senha</h3> 
		                                  <div class="checkbox">
									          <label>
									            <input type="checkbox" id="alterar_senha_profissional"> Alterar senha?
									          </label>
									       </div>
		                                <div class="form-group">
		                                	<label for="novasenha" class="control-label">Nova Senha</label>
		                                	<input type="password" id="novasenha" name="novasenha" placeholder="Digite sua nova senha..." required="true" data-minlength="6" data-maxlength="150" maxlength="150">
		                                	<div class="help-block">A senha deve conter ente 6 e 150 dígitos.</div>
		                                </div>
		                                <div class="form-group">
		                                	<label for="conf_novasenha" class="control-label">Digite novamente</label>
		                                	<input type="password" id="conf_novasenha" name="conf_novasenha" placeholder="Digite novamente sua nova senha..." data-match="#novasenha" data-match-error="As senhas não coincidem" placeholder="Confirm" required="true">
		                                	<div class="help-block with-errors"></div>
		                                </div>
		                                <!-- <div class="atualizar-btn">
		                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="perfilProfissional.trocarSenha(this); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
		                                </div>  -->
		                                <script type="text/javascript">
		                                function habilitarSenha() {

												        $('#novasenha').val('').prop('disabled', false);
												        $('#novasenha').prop('required', true);
												        $('#conf_novasenha').val('').prop('disabled', false);
												        $('#conf_novasenha').prop('required', true);
		                                }
		                                function desabilitarSenha() {

												        $('#novasenha').val('').prop('disabled', true);
												        $('#novasenha').prop('required', false);
												        $('#conf_novasenha').val('').prop('disabled', true);
												        $('#conf_novasenha').prop('required', false);
		                                }
										desabilitarSenha();
		                                	$('#alterar_senha_profissional').change(function(){
												    if ($('#alterar_senha_profissional').is(':checked') == true){
												        habilitarSenha();

												    } else {
												    	desabilitarSenha();

												    }
												});
		                                </script>
		                        </div>
		                    </div> <!-- /.col -->
		                    <div class="col-lg-6">
		                        <h3>Informações Pessoais</h3>
		                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"> 
		                            <div class="row">
		                                <div class="col-lg-12 form-group">
		                                    <label for="nome_profissional"> Primeiro Nome</label>
		                                    <input type="text" name="nome_profissional" class="form-control" id="nome_profissional" value="{{ old('nome_profissional') ?? $profissional['nome'] }}" placeholder="Nome" maxlength="250">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="sobrenome_profissional">Sobrenome</label>
		                                    <input type="text" name="sobrenome_profissional" class="form-control" id="sobrenome_profissional" value="{{ old('sobrenome_profissional') ?? $profissional['sobrenome'] }}" placeholder="Sobrenome" maxlength="250">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="email_profissional">E-mail</label>
		                                    <input type="text" name="email_profissional" class="form-control" id="email_profissional" value="{{ Auth::user()->toArray()['email'] }}" placeholder="E-mail" disabled="disabled">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="nascimento_profissional" class="control-label">Data de nascimento</label>
		                                    <input type="text" required="true" class="form-control" disabled="disabled" name="nascimento_profissional" id="nascimento_profissional" placeholder="Data de nascimento" value="{{ old('nascimento_profissional') ?? date('d/m/Y', strtotime($profissional['nascimento'])) }}" data-mask="00/00/0000" data-mask-clearifnotmatch="true">
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="cpf_profissional">CPF</label>

		                                    <input type="text" required="true" class="form-control" name="cpf_profissional" id="cpf_profissional" placeholder="CPF" value="{{ old('cpf_profissional') ?? $profissional['cpf'] }}" {{ $profissional['cpf'] != '' ? 'disabled="disabled"' : '' }} data-mask="000.000.000-00" data-mask-clearifnotmatch="true" data-error="Informe um CPF válido." data-remote="{{ action('Validador\DocumentoController@validar') }}">

		                                    <div class="help-block with-errors"></div>
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="cep_profissional">CEP</label>
		                                    <input type="text" name="cep_profissional" class="form-control" id="cep_profissional" placeholder="CEP" value="{{ old('cep_profissional') ?? $profissional['cep'] }}" data-mask="00000-000" data-mask-clearifnotmatch="true">
		                                </div>
		                                <div class="col-lg-9 form-group">
		                                    <label for="rua_profissional">Rua</label>
		                                    <input type="text" name="rua_profissional" class="form-control" id="rua_profissional" placeholder="Rua" value="{{ old('rua_profissional') ?? $profissional['rua'] }}" maxlength="250" data-autocomplete-address>
		                                </div>
		                                <div class="col-lg-3 form-group">
		                                    <label for="rua_profissional">Nº</label>
		                                    <input type="text" name="numero_profissional" class="form-control" id="numero" placeholder="" value="{{  $profissional['numero'] }}" maxlength="250" >
		                                </div>
		                                <div class="col-lg-12 form-group">
		                                    <label for="bairro_profissional">Bairro</label>
		                                    <input type="text" name="bairro_profissional" class="form-control" id="bairro_profissional" placeholder="Bairro" value="{{ old('bairro_profissional') ?? $profissional['bairro'] }}" maxlength="250" data-autocomplete-neighborhood>
		                                </div>
		                                <div class="col-lg-7 form-group">
		                                    <label for="cidade_profissional">Cidade</label>
		                                    <input type="text" name="cidade_profissional" class="form-control" id="cidade_profissional" placeholder="Cidade" value="{{ old('cidade_profissional') ?? $profissional['cidade'] }}" maxlength="250" data-autocomplete-city>
		                                </div>
		                                <div class="col-lg-5 form-group">
		                                    <label for="estado_profissional">Estado</label>
		                                    <input type="text" name="estado_profissional" class="form-control" id="estado_profissional" placeholder="Estado" value="{{ old('estado_profissional') ?? $profissional['estado'] }}" maxlength="2" data-autocomplete-state>
		                                </div>

		                            </div>
		                    </div> <!-- /.col -->
		                                <div class="atualizar-btn">
		                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="perfilProfissional.atualizarPerfil(this); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
		                                </div>
		                        </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		{{-- Modal - Crop --}}
		<div class="modal fade" id="CropModalProfissional" role="dialog">
		    <div class="modal-dialog modal-md">
		         {{-- HEADER --}}
		        <div class="modal-content">
		            <div class="modal-header">
		              	<button type="button" class="close" data-dismiss="modal">&times;</button>
		              	<h4 class="modal-title">Avatar</h4>
		            </div>
		            {{-- BODY --}}
		            <div class="btn-nl modal-body">
		                <span id="CropImgTargetProfissional"></span>
		            </div>
		            {{-- FOOTER --}}
		           <div class="modal-footer" id="nl-txt">
		               <button type="button" class="btn btn-default" data-dismiss="modal" onclick="javascript:perfilProfissional._pegarCorteAvatar();">Cortar</button>
		           </div>
		        </div>
		    </div>
		</div>
	@endif
	{{-- fim - modal para editar o perfil do admin --}}

	{{-- modal para edição das especialidades do profissional --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
	<?php
		// carregando as especialidades aprovadas
		$especialidades = LogDialog\Model\Especialidade::listaUsuarioId( Auth::user()->toArray()['id'], true );
	?>
		<div class="modal fade" id="modalEspecialidades" tabindex="-1" role="dialog" aria-labelledby="modalPerfilLabel">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                <h4 class="modal-title" id="modalPerfilLabel">Minhas Especialidades</h4>
		            </div>
		            <div class="modal-body modal-body-mt">
		                <div class="row">
		                    <div class="col-lg-12">
		                        <div class="minhas-especialidades">
		                            <form id="formNovaEspecialidade" class="specialty" action="{{ action("PerfilController@novaEspecialidade", ["id" => Auth::user()->toArray()['id']]) }}" method="post" data-toggle="validator" role="form" enctype="multipart/form-data">
										<input type="hidden" name="_token" id="_token-especialidade" value="{{ csrf_token() }}"> 

		                                <label for="novasenha">Especialidades aprovadas</label>
		                                <ul class="list-">
		                                	@foreach( $especialidades as $k => $v )
		                                    	<li class="row">
		                                    		<div class="col-xs-8">
		                                    			{{ $v['especialidade'] }}
		                                    		</div>
		                                    		<div class="col-xs-4">
			                                         	<label class="control-label"><input type="checkbox" {!! $v['visibilidade'] == 'PUBLICO' ? 'checked="checked"' : '' !!} name="visibilidade_{{ $v['id'] }}" value="PUBLICO" class="horariosem"> Exibir no perfil</label>
				                                    </div> 
		                                    	</li>
		                                    	<br>
		                                    @endforeach
		                                </ul>
		                                <br>
		                                <label for="novasenha">Adicionar novas especialidades </label>
		                                <div class="row">
		                                    <div class="col-lg-12 form-group">
		                                        <H5 class="specialty-form">Título de especialidade</H5>

		                                        <input type="text" name="nomeEspecialidade" id="nomeEspecialidade" placeholder="Digite o nome de sua especialidade"  value="{{ old('nomeEspecialidade') }}"><br>

		                                        @if( count($errors->all()) > 0 )
		                                            <span class="form_error" style="{{ $errors->has('nomeEspecialidade') ? 'display:block' : 'display:none' }}" >{{ $errors->first('nomeEspecialidade') }}</span>
		                                            <br>
		                                        @endif

		                                        <div class="help-block with-errors"></div>
		                                    </div>
		                                    <div>
		                                        <div class="col-lg-9" id="specialty-row">
		                                            <H5 class="specialty-form">Necessário de envio de comprovante</H5>

		                                            <input id="nomeSelecionado" type="text" readonly="true" placeholder="Nenhum arquivo selecionado"><br>
		                                        </div>

		                                        <div class="col-md-3 comprovante-especialidade">
		                                        	<input type="file" class="file-loading" name="arquivoEspecialidade" id="arquivoEspecialidade">
		                                        </div>

		                                        @if( count($errors->all()) > 0 )
		                                            <span class="form_error" style="{{ $errors->has('arquivoEspecialidade') ? 'display:block' : 'display:none' }}" >{{ $errors->first('arquivoEspecialidade') }}</span>
		                                            <br>
		                                        @endif

		                                        <div id="errorBlockEspecialidade" class="help-block erroInputAvatar"></div>
		                                    </div>
		                                </div>
		                                <br>
		                                <div class="atualizar-btn">
		                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="javascript:perfilProfissional._novaEspecialidade(); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
		                                </div>
		                            </form>
		                        </div>
		                    </div> <!-- /.col -->
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
	{{-- fim - modal para edição das especialidades do profissional --}}

	{{-- modal para edição do curriculo do profissional --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
		<div class="modal fade" id="modalMeuCurriculo" tabindex="-1" role="dialog" aria-labelledby="modalPerfilLabel">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                <h4 class="modal-title" id="modalPerfilLabel">Meu Currículo</h4>
		            </div>
		            <div class="modal-body modal-body-mt">
		                <div class="row">
		                    <div class="col-lg-12">
		                        <div class="meu-curriculo">
		                            <form class="meucurriculo-space" id="formMeuCurriculo"   method="post">
		                            
		                            	<input type="hidden" name="_token" id="_id-curriculo" value="{{ Auth::user()->toArray()['id'] }}"> 
		                            	<input type="hidden" name="_token" id="_token-curriculo" value="{{ csrf_token() }}"> 
		                                <label for="novasenha">Experiência Profissional</label>
		                                <textarea name="experiencia" id="input_experiencia">{{ $profissional['experiencia_profissional'] }}</textarea>
		                                <br><br>
		                                <label for="novasenha">Formação Acadêmica</label>
		                                <textarea name="formacao" id="input_formacao">{{ $profissional['formacao_academica'] }}</textarea>
		                                <br><br>
		                                <label for="novasenha">Afiliações</label>
		                                <textarea name="afiliacao" id="input_afiliacao">{{ $profissional['afiliacoes'] }}</textarea>
		                                <br><br>
		                                <div class="atualizar-btn">
		                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="javascript:perfilProfissional._atualizarCarregando(this, 'carregando'); perfilProfissional.atualizarCurriculo(this); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
		                                </div>
		                            </form>
		                        </div>
		                    </div> <!-- /.col -->
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
	{{-- fim - modal para edição do curriculo do profissional --}}

	{{-- modal para controle dos valores dos serviços do profissional --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' && isset($servicos) && isset($usuario) )
		<div class="modal fade" id="modalValores" tabindex="-1" role="dialog" aria-labelledby="modalPerfilLabel">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                <h4 class="modal-title" id="modalPerfilLabel">Meus Valores</h4>
		            </div>
		            <div class="modal-body modal-body-mt">
		                <div class="prices">
		                    <form class="consultas-form" id="formAtualizarValores"  method="post">
		                            	<input type="hidden" name="_token" id="_token-valores" value="{{ csrf_token() }}"> 
		                            	<input type="hidden" name="_qtdServicos" id="_qtdServicos" value="{{ Count($servicos) }}"> 
		                            	<input type="hidden" name="valores" id="_id-valores" value="{{ Auth::user()->toArray()['id'] }}">  
		                        <h3>Valores</h3>
		                        <ul class="consultas">

			                        <?php $servicos = \LogDialog\Model\ProfissionalServico::whereProfissionalId(Auth::user()->id)->get()->toArray();  ?>
		                        	@foreach( $servicos as $k => $servico )
		                            	<input type="hidden" name="servico_id" id="_id-servico{{ $servico['id'] }} " value="{{ $servico['id'] }}">
		                        		<li class="{{ $k % 2 == 0 ? 'par' : 'impar' }}">
			                                <div class="row">
			                                    <div class="col-lg-5">
			                                        <span class="price-title">{{ $servico['servico'] }}</span>
			                                    </div>
			                                    <div class="col-lg-3">
													<input type="hidden" name="id[]" id="input-id" value="{{ $servico['id'] }}">

			                                        <input type="text" name="price_{{ $servico['id'] }}" id="input-valores" data-mask="#,##0.000.000.000,00" data-mask-reverse="true" placeholder="R$0,00" value="{{ number_format($servico['valor'], 2, ',', '.') }}">
			                                    </div>
			                                    <div class="col-lg-3 visibilidade">  
			                                         	<div class="control-label"><input type="checkbox" {!! $servico['visibilidade'] == 'PUBLICO' ? 'checked="checked"' : '' !!} name="visibilidade_{{ $servico['id'] }}" value="PUBLICO" class="horariosem"> Exibir</div>
			                                    </div>
			                                </div>
			                            </li>
		                        	@endforeach 
		                        </ul>
		                        <div class="consulta-gratis"> 
		                            <p><input type="checkbox" 
		                            {{ $usuario['profissional']['primeiro_contato_gratis'] ? 'checked' : '' }} name="primeiraConsultaGratis" id="input-primeira-consulta" 
		                            value="1"
		                            > Aceito a opção de primeira consulta grátis</p>
		                        </div>
		                        <div class="atualizar-btn">
                                    <a class="btn btn-modal btnmodal-update" href="javascript:;" onclick="javascript:perfilProfissional._atualizarCarregando(this, 'carregando'); perfilProfissional.atualizarValores(this); return false;" role="button"><i class="fa fa-check-circle-o"></i> Atualizar</a>
                                </div>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	@endif
	{{-- fim - modal para controle dos valores dos serviços do profissional --}}

	{{-- modal para definição dos dias e horários de atendimento --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
		<div class="modal fade" id="modalHorariosAtendimento" tabindex="-1" role="dialog" aria-labelledby="agendarProfissional">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                    <h4 class="modal-title" id="horarios-atendimento">Definição do horário de atendimento</h4>
		                </div>
		            <div class="modal-body">
		            	<form action="{{ action("PerfilController@atualiarHorarioAtendimento", ["id" => Auth::user()->toArray()['id']]) }}" id="formDefinicaoHorarioAtendimento" method="post" class="freeday">
			                <div class="container-modal">
			                    <div class="row">
			                        <ul id="workhours">
			                        	@foreach( HorarioAtendimento::diasSemana() as $diaSemana => $nomeDia )
			                        		<li>
				                                <div class="row">
				                                    <div class="col-xs-3">
				                                        <h3>{{ $nomeDia }}</h3>
				                                    </div>
				                                    <div class="col-xs-5 flex-center">
				                                        <div class="horarariosatend-select txt-center">
				                                        	<div class="col-md-5">
					                                        	<div class="input-group horario">
						                                            <select name="horario_de_{{ $diaSemana }}" id="horario_de_{{ $diaSemana }}" class="form-control">
						                                            	@foreach( HorarioAtendimento::lista( 'de' ) as $k => $de )
						                                                	<option value="{{ $de }}" {!! HorarioAtendimento::horarioAtendido( Auth::user()->toArray()['id'], $diaSemana, $de, 'de' ) ? 'selected="selected"' : '' !!}>{{ $de }}</option>
						                                                @endforeach
						                                            </select>
						                                         </div>
						                                     </div>
						                                     <div class="col-md-2">
				                                            	<span>às</span>
				                                            </div>
				                                            <div class="col-md-5">
					                                            <div class="input-group horario">
						                                            <select name="horario_ate_{{ $diaSemana }}" id="horario_de_{{ $diaSemana }}" class="form-control">
						                                            	@foreach( HorarioAtendimento::lista( 'ate' ) as $k => $ate )
						                                                	<option value="{{ $ate }}" {!! HorarioAtendimento::horarioAtendido( Auth::user()->toArray()['id'], $diaSemana, $ate, 'ate' ) ? 'selected="selected"' : '' !!}>{{ $ate }}</option>
						                                                @endforeach
						                                            </select>
						                                        </div>
					                                        </div>
				                                        </div>

				                                    </div>
				                                    <div class="col-xs-4">
			                                         	<label><input type="checkbox" {!! HorarioAtendimento::diaSemAtendimento( Auth::user()->toArray()['id'], $diaSemana ) ? 'checked="checked"' : '' !!} name="sem_atendimento_{{ $diaSemana }}" value="✓" class="horariosem"> Não atendo esse dia</label>
				                                    </div>
				                                </div>
				                            </li>
			                        	@endforeach
			                        </ul>
			                    </div>
			                </div>
			                <div class="agendar-btn">
			                    <button type="submit">Confirmar</button>
			                </div>
			            </form>
		            </div><!--.modal-body -->
		        </div><!-- .modal-content -->
		    </div> <!-- .modal-dialog -->
		</div><!-- .modal fade -->
	@endif
	{{-- fim - modal para definição dos dias e horários de atendimento --}}

	{{-- modal para adicionar um bloqueio de horário --}}
	@if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )

		<div class="modal fade" id="modalHorariosBloqueio" role="dialog">
		    <div class="modal-dialog modal-md">
		    <!--Modal-content-->
		        <!-- HEADER-->
		        <div class="modal-alert">
		        	<form id="formBloquearHorario" action="{{ action("PerfilController@salvarBloqueioHorario", ["id" => Auth::user()->toArray()['id']]) }}" method="post">
			            <div class="header-alert">
			                <button type="button" class="close" data-dismiss="modal">&times;</button>
			                <img src="{{ asset("images/alert.png") }}">
			                <h4 class="modal-title">Bloqueio de horários</h4>
			            </div>
			            <!-- BODY -->
			            <div class="modal-body">
			                <div class="container-modal">
			                    <div class="row">
		                        	<div class="col-lg-6" class"col-sm-12">
			                            <div class="date-consult">
			                            <h4>DATA</h4>
			                            <p>Selecione o dia</p>
			                            <?php /*
			                                <select name="diaBloqueio" id="diaBloqueio">
			                                	@foreach( Calendario::intervaloDias( Carbon\Carbon::now()->addDays(1), Carbon\Carbon::now()->addDays(120) ) as $k => $dt )
			                                		<option value="{{ $dt->format('Y-m-d') }}">{{ $dt->format('d/m/Y') }}</option>
			                                	@endforeach
			                                </select>
			                               */ ?>
			                               <div class="input-group date datepicker" data-provide="datepicker">
										    <input type="text" class="form-control" name="diaBloqueio" placeholder="Clique para selecionar uma data">
											    <div class="input-group-addon">
											        <span class="glyphicon glyphicon-th"></span>
											    </div>
											</div>
			                            </div> <!-- .container (dia desejado) -->
			                        </div>
			                        <div class="col-lg-6" class="col-sm-12">
			                            <div class="hour-consult">
			                                <h4>HORÁRIO</h4>
			                                <p>Selecione o horário para bloqueio</p> 
			                                <select name="horarioBloqueio" id="horarioBloqueio" class="form-control">
			                                	@foreach( array_merge(HorarioAtendimento::lista('de'), HorarioAtendimento::lista('ate')) as $k => $horario )
			                                		<option value="{{ $horario }}">{{ $horario }}</option>
			                                	@endforeach
			                                </select> 
			                                </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <!-- FOOTER -->
			            <div class="footer-alert">
			                <button type="button" onclick="$('#formBloquearHorario').submit();" class="btn btn-default btn-alert" data-dismiss="modal">Bloquear Horário
			                </button>
			            </div>
			        </form>
		        </div>
		    </div> <!-- modal final -->
		</div>
	@endif
	{{-- fim - modal para adicionar um bloqueio de horário --}}

	{{-- Avisos Javascript --}}
	{{-- Modal --}}
	<div class="modal fade" id="javascriptAlert" role="dialog">
	    <div class="modal-dialog modal-md">
	         {{-- HEADER --}}
	        <div class="modal-content">
	            <div class="modal-header">
	              	<button type="button" class="close" data-dismiss="modal">&times;</button>
	              	<h4 class="modal-title">&nbsp;</h4>
	            </div>
	            {{-- BODY --}}
	            <div class="btn-nl modal-body">
	                &nbsp;
	            </div>
	            {{-- FOOTER --}}
	           <div class="modal-footer" id="nl-txt">
	               <button type="button" class="btn btn-default" data-dismiss="modal">&nbsp;</button>
	           </div>
	        </div>
	    </div>
	</div>

	{{-- modal para agendamento de consulta, apenas na tela de agenda completa do profissional --}}
	@if( $view_name == 'profissionais-agenda-completa' && isset($servicos) &&
	     isset($horarioSistema) && isset($profissional) )
		<div class="modal fade" id="agendarConsulta-adm" tabindex="-1" role="dialog" aria-labelledby="agendarProfissional">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle-o fa-lg"></i></button>
		                    <h4 class="modal-title" id="agendarProfissional">AGENDAR CONSULTA</h4>
		                </div>
		            <div class="modal-body">
		            	<form id="formAgendarConsulta" action="{{ action("ProfissionaisController@agendarConsulta", ["profissionalUsuarioId" => $profissional['usuario_id']]) }}" method="post">
			                <div class="container-modal">
			                    <div class="row">
			                        <div class="col-lg-12">
			                            <div class="agendamento">
			                                <div class="consult"></div>
			                                <h4>CONSULTA</h4>
			                                <p>Escolha o tipo e duração da consulta que deseja agenciar</p>
			                        <?php $servicos = \LogDialog\Model\ProfissionalServico::whereProfissionalId($profissional['usuario_id'])->where('visibilidade', 'PUBLICO')->get()->toArray();  ?>
			                                @if(Count($servicos) > 0)
			                                <select name="servicoConsulta" class="form-control">
			                                	@foreach( $servicos as $k => $item ) 
			                                		<option value="{{ $item['id'] }}">{{ $item['servico'] }} - R$ {{ number_format($item['valor'], 2, ',', '.') }}</option> 
			                                	@endforeach
			                                </select>
			                                @else
								            <div class="txt-alert">
								                <p>No momento, o profissional não possui consultas disponíveis.</p>
								            </div>
			                                @endif
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                                @if(Count($servicos) > 0)
		                    <div class="container-modal">
		                        <div class="row">
		                            <div class="information-container">
		                                <div class="col-lg-6" class"col-sm-12">
		                                    <div class="date-consult">
		                                    <h4>DATA</h4>
		                                    <p>Selecione o dia da consulta</p>
			                               <div class="input-group date datepicker" data-provide="datepicker">
										    <input type="text" class="form-control" name="diaConsulta" placeholder="Clique para selecionar uma data">
											    <div class="input-group-addon">
											        <span class="glyphicon glyphicon-th"></span>
											    </div>
											</div>
<?php /*		                                        <select name="diaConsulta">
													<option value="">Escolha o dia</option>

		                                        	@foreach( $opcoesDias as $k => $dia )
		                                        		<option value="{{ $dia->timestamp }}">{{ $dia->format('d/m/Y') }}</option>
		                                        	@endforeach
		                                        </select> */ ?>
		                                    </div> <!-- .container (dia desejado) -->
		                                </div>
		                                <div class="col-lg-6" class="col-sm-12">
		                                    <div class="hour-consult">
		                                        <h4>HORÁRIO</h4>
		                                        <p>Selecione o horário da consulta</p>
		                                        <select name="horarioConsulta" class="form-control">
		                                        	<option value="">Selecione o horário</option>
		                                        </select>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>

			                <div class="check-box">
			                    <div class="row">
		                            <div class="col-lg-1">
		                                <input type="checkbox" name="enviarOpcoes" value="1" id="another-time">
		                            </div>
		                            <div class="col-lg-11">
		                                <p class="hour-options">
		                                	<label for="another-time">Quero enviar outras opções de horário caso o horário escolhido fique indisponível</label>
		                                </p>
		                            </div>
			                    </div>
			                </div>

							<script>
								$(document).ready(function(){
									$("#another-time").bind('change', function(){
										if( $(this).is(':checked') )
										{
											$("#another-time-content").show();
										}
										else
										{
											$("#another-time-content").hide();
										}
									});
								});
							</script>

			                <div class="" id="another-time-content" style="display: none;">
			                    <div class="row" id="one">
				                    <div class="col-lg-6 col-md-6 col-xs-12">
			                               <div class="input-group date datepicker" data-provide="datepicker">
										    <input type="text" class="form-control" name="diaOpcao1" placeholder="Clique para selecionar uma data">
											    <div class="input-group-addon">
											        <span class="glyphicon glyphicon-th"></span>
											    </div>
											</div>
									</div>
				                    <div class="col-lg-5 col-md-5 col-xs-12">
				                        <select class="scroll-horario form-control" name="horarioOpcao1" >
				                            <option value="">Selecione o horário</option>
				                        </select>
				                     </div>
				                     <div class="col-lg-1 col-md-1 col-xs-12">
				                        <button type="button" class="btn-more" onclick="javascript:novaData();">
				                            <img src="{{ asset('images/mais.png') }}">
				                        </button>
			                        </div>
			                    </div>
			                    <div class="row" id="two">

				                    <div class="col-lg-6 col-md-6 col-xs-12">
			                               <div class="input-group date datepicker" data-provide="datepicker">
										    <input type="text" class="form-control" name="diaOpcao2" placeholder="Clique para selecionar uma data">
											    <div class="input-group-addon">
											        <span class="glyphicon glyphicon-th"></span>
											    </div>
											</div>
 									</div>
				                    <div class="col-lg-5 col-md-5 col-xs-12">
				                        <select name="horarioOpcao2" class="form-control">
				                            <option value="">Selecione o horário</option>
				                        </select>
				                     </div>
				                     <div class="col-lg-1 col-md-1 col-xs-12">

				                        <button type="button" class="btn-more">
				                            <img src="{{ asset('images/menos.png') }}" onclick="javascript:removerData('two');">
				                        </button>
			                        </div>
			                    </div><!--two -->
			                    <div class="row" id="three">

				                    <div class="col-lg-6 col-md-6 col-xs-12">
			                               <div class="input-group date datepicker" data-provide="datepicker">
										    <input type="text" class="form-control" name="diaOpcao3" placeholder="Clique para selecionar uma data">
											    <div class="input-group-addon">
											        <span class="glyphicon glyphicon-th"></span>
											    </div>
											</div>
									</div>
				                    <div class="col-lg-5 col-md-5 col-xs-12">
				                        <select name="horarioOpcao3" class="form-control">
				                            <option value="">Selecione o horário</option>
				                        </select>
				                     </div>
				                     <div class="col-lg-1 col-md-1 col-xs-12">
				                        <button type="button" class="btn-more">
				                            <img src="{{ asset('images/menos.png') }}" onclick="javascript:removerData('three');">
				                        </button>
			                        </div>
			                    </div><!-- three -->
			                </div><!-- outrosHorarios-content -->

			                @if( count($errors->all()) > 0 )
                                <div id="errorBlockAgendarConsulta" class="help-block erroInputAvatar">{{ $errors->first('horarioIndisponivel') }}</div>
                            @else
                            	<div id="errorBlockAgendarConsulta" class="help-block erroInputAvatar"></div>
                            @endif

		                    <div class="agendar-btn">
		                        <button style="submit">AGENDAR CONSULTA</button>
		                    </div>
		                    @endif
		                </form>
		            </div><!--.modal-body -->
		        </div><!-- .modal-content -->
		    </div> <!-- .modal-dialog -->
		</div><!-- .modal fade -->
	@endif

	{{-- modal para reagendamento de consulta --}}
	<div class="modal fade" id="reagendarConsultaAviso" role="dialog">
	    <div class="modal-dialog modal-md">
	    <!--Modal-content-->
	        <!-- HEADER-->
	        <div class="modal-alert">
	            <div class="header-alert">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <!-- <img src="{{ asset('images/alert.png') }}"> -->
	                <h4 class="modal-title">Reagendamento de consulta</h4>
	            </div>
	            <!-- BODY -->
	            <div class="txt-alert"> 
	                <p>Deseja agendar outra consulta com este profissional?</p>
	            </div>
	            <!-- FOOTER -->
	            <div class="footer-alert centralizar-h multiplos-botoes">
	            	<div class="row">
	            		<div class="col-md-6">
	            			<button type="button" class="btn btn-success" data-dismiss="modal">Sim</button>
	            		</div>

	            		<div class="col-md-6">
	            			<button type="button" class="btn btn-info" data-dismiss="modal">Não</button>
	            		</div>
	            	</div>
	            </div>
	            <br />
	        </div>
	    </div> <!-- modal final -->
	</div>

	{{-- modal para cancelamento --}}

					<div class="modal fade" id="cancelarConsulta" tabindex="-1" role="dialog" aria-labelledby="cancelarConsulta">
						<div class="modal-dialog" role="document">
						    <div class="modal-content">
						        <div class="modal-header-cancel">
						            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						            <h4 class="modal-title" id="agendarProfissional">CANCELAR CONSULTA</h4>
						        </div>
						<div class="modal-body modal-body-mt">
						                <div class="row">
						                    <div class="col-lg-12">
						                        <div class="editarperfil-avatar">
						                            <div class="pt-top" id="cancelar-consulta-profissional">
						                            </div>                     
						                        </div>
						                    </div> <!-- /.col -->
						                </div>
						                <div class="question-title">
						                    <h3>Qual o motivo do cancelamento da consulta?</h3>
						                    <textarea rows="8" cols="64" id="cancelar-conulta-motivo" name="cancelar-conulta-motivo"></textarea>
						                </div>
						                <div class="btn-cancel">    
						                    <button>
						                        Cancelar consulta
						                    </button>
						                </div>
						            </div>
					            <!-- FOOTER --> 
							</div>
						</div>
					</div>
	{{-- modal para excluir nota --}}
	<div class="modal fade" id="excluirNotaModel" role="dialog">
	    <div class="modal-dialog modal-md">
	    <!--Modal-content-->
	        <!-- HEADER-->
	        <div class="modal-alert">
	            <div class="header-alert">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <!-- <img src="{{ asset('images/alert.png') }}"> -->
	                <h4 class="modal-title">Excluir nota</h4>
	            </div>
	            <!-- BODY -->
	            <div class="txt-alert"> 
	                <p>Deseja realmente excluir esta nota? Esta ação não pode ser revertida.</p>
	            </div>
	            <!-- FOOTER -->
	            <div class="footer-alert centralizar-h multiplos-botoes">
	            	<div class="row">
	            		<div class="col-md-6">
	            			<button type="button" class="btn btn-success" data-dismiss="modal" onclick="notas.processarExcluirNota();">Sim</button>
	            		</div>

	            		<div class="col-md-6">
	            			<button type="button" class="btn btn-info" data-dismiss="modal">Não</button>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div> <!-- modal final -->
	</div>

	{{-- modal para editar nota --}}
	<div class="modal fade" id="editarNotaModel" role="dialog">
	    <div class="modal-dialog modal-md">
	    <!--Modal-content-->
	        <!-- HEADER-->
	        <div class="modal-content">
	            <div class="header-alert">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <!-- <img src="{{ asset('images/alert.png') }}"> -->
	                <h4 class="modal-title">Editar nota</h4>
	            </div>
	            <!-- BODY -->
	            <div class="modal-body">
	                <textarea id="nota-editar-conteudo" cols="30" rows="10"></textarea>
	            </div>
	            <!-- FOOTER -->
	            <div class="footer-alert centralizar-h multiplos-botoes">
	            	<div class="row">
	            		<div class="col-md-6">
	            			<button type="button" class="btn btn-success" data-dismiss="modal" onclick="notas.processarSalvarNota();">Salvar</button>
	            		</div>

	            		<div class="col-md-6">
	            			<button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div> <!-- modal final -->
	</div>

	{{-- aviso do sistema --}}
	@if( isset($avisoSistema) )
		<div class="modal fade" id="avisoSistema" role="dialog">
		    <div class="modal-dialog modal-md">
		    <!--Modal-content-->
		        <!-- HEADER-->
		        <div class="modal-alert">
		            <div class="header-alert">
		                <button type="button" class="close" data-dismiss="modal">&times;</button>
		                <!-- <img src="{{ asset('images/alert.png') }}"> -->
		                <h4 class="modal-title">{!! $avisoSistema['titulo'] !!}</h4>
		            </div>
		            <!-- BODY -->
		            <div class="txt-alert"> 
		                <p>{!! $avisoSistema['mensagem'] !!}</p>
		            </div>
		            <!-- FOOTER -->
		            <div class="footer-alert centralizar-h">
		                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
		            </div>
		        </div>
		    </div> <!-- modal final -->
		</div>
	@endif
