
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                            <a href="#" class="site_title"><i><img src="<% asset('painel-admin/images/logdialog-icon.png') %>" alt=""></i> <span><img src="<% asset('painel-admin/images/logdialog-text.png') %>" alt=""></span></a>
                    </div>
                    <div class="clearfix"></div>
                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li   ui-sref-active="active" ui-sref="vantagens"><a href=""><i class="fa fa-line-chart" aria-hidden="true"></i> Vantagens</a></li>
                                <li  ui-sref-active="active" ui-sref="sliders"><a href=""><i class="fa fa-sliders" aria-hidden="true"></i> Slider</a></li>
                                <li  ui-sref-active="active" ui-sref="paginas"><a href=""><i class="fa fa-file-o" aria-hidden="true"></i> Páginas</a></li>
                                <li  ui-sref-active="active" ui-sref="profissionais"><a href=""><i class="fa fa-users" aria-hidden="true"></i> Profissionais</a></li>
                                <li  ui-sref-active="active" ui-sref="pacientes"><a href=""><i class="fa fa-user" aria-hidden="true"></i> Pacientes</a></li>
<!--                                 <li  ui-sref-active="active" ui-sref="servicos"><a href=""><i class="fa fa-th-list" aria-hidden="true"></i> Serviços</a></li>
 -->                                <li  ui-sref-active="active" ui-sref="servicosSolicitados"><a href=""><i class="fa fa-plus-square" aria-hidden="true"></i> Serviços Solicitados</a></li>
                                <li  ui-sref-active="active" ui-sref="especialidades"><a href=""><i class="fa fa-check-square-o" aria-hidden="true"></i> Especializações</a></li>
                                <li  ui-sref-active="active" ui-sref="denuncias"><a href=""><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Denúncias</a></li>
                            </ul>
                        </div>
                    </div> <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <!-- <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Logout">
                                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                            </a>
                    </div>  --><!-- /menu footer buttons -->
                </div>