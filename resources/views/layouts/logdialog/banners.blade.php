@section('banner-top-home')
    {{-- Banner Home --}}
    <section id="bannerhome" class="intro-section">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox"> 
            @if(isset($sliders))
            @for($i = 0; $i < Count($sliders); $i++)
                <div class="item {{$i == 0 ? 'active' : ''}}">
                    <div class="visible-md visible-lg"><img src="{{ $sliders[$i]->imagem }}" alt="" class="img-responsive"></div> 
                    <div class="carousel-caption">
                        <h1>{{$sliders[$i]->titulo}}</h1>
                        {{-- <p class="subtitle">o serviço psicológico <br>que você escolhe</p> --}}
                        <br>
                        <a href="{{$sliders[$i]->url}}">
                            <div class="btn btn-blue">
                            {{$sliders[$i]->botao_texto}}
                            </div>
                        </a>
                    </div>
                </div>
            @endfor
 
            @endif
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        {{-- <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner-content">
                        <h1>Tranquilidade e paz <br>ao seu alcance</h1>
                        <p class="subtitle">o serviço psicológico <br>que você escolhe</p>
                        <br>
                        <a href="{{ action('CadastroController@registrar') }}">
                            <div class="btn btn-blue">
                            CADASTRE-SEs
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div> --}}
    </section>
@stop()