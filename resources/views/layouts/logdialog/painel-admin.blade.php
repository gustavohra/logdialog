@include('layouts.logdialog.painel-admin-head')
<body class="nav-md" ng-app="logDialogAdmin">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                @include('layouts.logdialog.painel-admin-menu')
            </div>

            <%-- Header --%>
                @include('layouts.logdialog.painel-admin-header')
            <%-- Header --%>

                <%-- Conteúdo Principal --%>
                    <div class="right_col" role="main">
                        <ui-view ></ui-view> 
                    </div>
                 <%-- Conteúdo Principal --%>   

            <%-- Footer --%>
                @include('layouts.logdialog.painel-admin-footer')
            <%-- Footer --%>
        </div>
    </div>
    </script>
    <%-- Templates internos --%>
        @include('painel/vantagens')
        @include('painel/sliders')
        @include('painel/paginas')
        @include('painel/profissionais')
        @include('painel/profissional')
        @include('painel/pacientes')
        @include('painel/paciente')
        @include('painel/servicos')
        @include('painel/servicos-especializados')
        @include('painel/servico-especializado')
        @include('painel/especializacoes')
        @include('painel/especialidade')
        @include('painel/denuncias')
        @include('painel/formulario')

      <script type="text/ng-template" id="textarea-tinymce.html">
        <textarea ui-tinymce="options.data.tinymceOption"  ng-model="model[options.key]" class="form-control">
      </textarea>
    </script>
    <div class="clearfix"></div>
    <!-- jQuery -->
    <script src="<% asset('painel-admin/js/jquery.js') %>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<% asset('painel-admin/js/bootstrap.min.js') %>"></script>
    <script src="<% asset('painel-admin/js/custom.js') %>"></script>
</body>

</html>
