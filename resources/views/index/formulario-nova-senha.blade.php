{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
    {{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    @yield('nav-menu-principal')
    
    <section class="home-section">
        <div class="container">

            {{-- caso seja válido o token --}}
            @if( $valido )
                {{-- mensagem --}}
                <div class="row">
                    <div class="col-lg-12"><div class="col-md-8"></div>
                        <div class="section-title">
                            <h1>&nbsp;</h1>
                            <h1>Redefinição de senha.</h1>
                            <p class="subtitle">Preencha o formulário abaixo para trocar a sua senha.</p>
                        </div>{{-- /.section-title --}}
                    </div>
                </div>
                
                {{-- formulário para troca de senha --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3"></div>
                        
                        <div class="col-md-6">
                            <form action="{{ action("LoginController@telaNovaSenha", ["userId" => $userId, "token" => $token]) }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <input type="password" class="form-control" name="senha" placeholder="Senha">

                                    @if( count($errors->all()) > 0 )
                                        <span class="form_error" style="{{ $errors->has('senha') ? 'display:block' : 'display:none' }}" >{{ $errors->first('senha') }}</span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" name="conf_senha" placeholder="Confirmação de senha">

                                    @if( count($errors->all()) > 0 )
                                        <span class="form_error" style="{{ $errors->has('conf_senha') ? 'display:block' : 'display:none' }}" >{{ $errors->first('conf_senha') }}</span>
                                    @endif
                                </div>

                                <p class="subtitle">A senha deve conter ente 6 e 150 dígitos.</p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3">&nbsp;</div>
                                        <div class="col-md-6 centralizar-h">
                                            <button type="submit" class="btn">
                                                SALVAR
                                            </button>
                                        </div>
                                        <div class="col-md-3">&nbsp;</div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-md-3"></div>
                    </div>
                </div>
            @else
                
                {{-- caso a alteração tenha sido feita com sucesso --}}
                @if( isset($alterado) )
                    {{-- mensagem --}}
                    <div class="row">
                        <div class="col-lg-12"><div class="col-md-8"></div>
                            <div class="section-title">
                                <h1>&nbsp;</h1>
                                <h1>Sucesso!</h1>
                                <p class="subtitle">A senha foi alterada com sucesso, efetue o login normalmente.</p>
                            </div>{{-- /.section-title --}}
                        </div>
                    </div>
                @else
                    {{-- mensagem --}}
                    <div class="row">
                        <div class="col-lg-12"><div class="col-md-8"></div>
                            <div class="section-title">
                                <h1>&nbsp;</h1>
                                <h1>Redefinição de senha.</h1>
                                <p class="subtitle">Token inválido, solicite novamente a redefinição de senha.</p>
                            </div>{{-- /.section-title --}}
                        </div>
                    </div>
                @endif

            @endif

        </div>
    </section>

@stop
{{-- fim do conteúdo do yield atual --}}