{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
    {{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    @yield('nav-menu-principal')

    <section class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"><div class="col-md-8"></div>
                    <div class="section-title">
                        <h1>&nbsp;</h1>
                        <h1>E-mail enviado com sucesso!</h1>
                        <p class="subtitle">E-mail de redefinição de senha foi enviado com sucesso para o endereço: {{ $email }}.</p>
                    </div>{{-- /.section-title --}}
                </div>
            </div>
        </div>
    </section>

@stop
{{-- fim do conteúdo do yield atual --}}