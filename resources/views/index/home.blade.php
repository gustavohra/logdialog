{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
    {{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    @yield('nav-menu-principal')
    @yield('banner-top-home')

   <?php /*
    <section id="pergunta" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"><div class="col-md-8"></div>
                    <div class="section-title">
                        <h1>Você precisa de orientação psicológica</h1>
                        <p class="subtitle">Responda este breve questionário para ajudá-lo a determinar se você se beneficiaria de um cuidado psicológico</p>
                        <p class="notes">Esta não é uma ferramenta diagnóstica, apenas sintomas de alerta para a procura de ajuda psicológica.</p>
                    </div>{{-- /.section-title --}}
                    <a href="{{ action('OrientacaoController@pergunta', ['id' => 0, 'res' => 0]) }}">
                        <div id="btn-h" class="btn btn-gradient">
                        Descubra aqui
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section> */ ?>
    {{-- Saiba mais --}}
    <section id="pergunta" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"><div class="col-md-8"></div>
                    <div class="section-title">
                        <h1>Orientação Psicológica Online</h1>
                        <p class="subtitle">O Conselho Federal de Psicologia através da Resolução CFP n° 11/2012 regulamenta os serviços psicológicos realizados por meios tecnológicos de comunicação a distância desde que pontuais, informativos, focados no tema proposto.</p>
                        {{-- <p class="notes">Esta não é uma ferramenta diagnóstica, apenas sintomas de alerta para a procura de ajuda psicológica.</p> --}}
                    </div>{{-- /.section-title --}}
                    {{-- <a href="" data-toggle="modal" data-target="#modal-orientacao"> --}}
                    {{-- <a href=""> --}}
                        {{-- <div id="btn-h" class="btn btn-gradient">
                        Saiba mais
                        </div> --}}
                        <div id="btn-orientacao" class="btn btn-gradient">
                        Saiba mais
                        </div>
                    {{-- </a> --}}
                    <div class="div-orientacao">
                        <h4 class="modal-title">Entende-se por orientação:</h4> 
                        <p>Atendimento realizado em até 20 encontros ou contatos virtuais, síncronos ou assíncronos; <br /><br />
                        Processos prévios de Seleção de Pessoal; <br /><br />
                        Aplicação de Testes devidamente regulamentados por resolução pertinente; <br /><br />
                        Supervisão do trabalho de psicólogos, realizada de forma eventual ou complementar ao processo de sua formação profissional presencial; <br /><br />
                        Atendimento Eventual de clientes em trânsito e/ou de clientes que momentaneamente se encontrem impossibilitados de comparecer ao atendimento presencial.  <br /><br />
                        O uso de testes psicológicos informatizados são permitidos, desde que tenham avaliação favorável de acordo com Resolução CFP N° 002/03.<br /><br />
                        <b>
                        Casos graves, que representam algum risco para vida ou segurança do paciente, requerem e serão encaminhados para atendimento presencial. O Logdialog é um serviço de Orientação Psicológica Online, portanto se você está em risco ou pode colocar alguém em risco, orientamos para que não use o nosso serviço, mas  ficaremos satisfeitos em auxiliar no encaminhamento mais indicado para sua saúde e bem estar.</b> <br /><br />

                        Para ajuda no encaminhamento entre em contato através do <a href="mailto:encaminhamento@logdialog.com.br">encaminhamento@logdialog.com.br</a> ou pelo tel. +55 11 4561-6695.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Como funciona --}}
    <section id="comofunciona" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h1>Como funciona?</h1>
                        <p class="subtitle">Veja como é simples e rápido utilizar a LogDialog</p>
                    </div>
                </div>
            </div>
            <div class="row-eq-height">
                <div class="col-lg-3">
                    <div class="comofunciona-item">
                        <div class="item-icon">
                            <img src="{{ asset('images/comofunciona-icon02.png') }}" alt="">
                        </div>
                        <div class="item-content">
                            <h2>Escolha seu psicólogo</h2>
                            <p>Encontre o psicólogo que melhor atende às suas necessidades.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="comofunciona-item">
                        <div class="item-icon">
                            <img src="{{ asset('images/comofunciona-icon03.png') }}" alt="">
                        </div>
                        <div class="item-content">
                            <h2>Agende um horário</h2>
                            <p>Acesse a agenda do seu psicólogo e escolha o dia e horário mais conveniente para você.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="comofunciona-item">
                        <div class="item-icon">
                            <img src="{{ asset('images/comofunciona-icon01.png') }}" alt="">
                        </div>
                        <div class="item-content">
                            <h2>Pague</h2>
                            <p>O pagamento é simples, seguro e pode ser realizado com cartão de crédito.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="comofunciona-item">
                        <div class="item-icon">
                            <img src="{{ asset('images/comofunciona-icon04.png') }}" alt="">
                        </div>
                        <div class="item-content">
                            <h2>Log & Dialog</h2>
                            <p>Basta acessar o site do Logdialog, que seu psicólogo estará aguardando para atendê-lo no dia e horário agendado.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="video-home" data-toggle="modal" data-target="#modal-video-home">
                    <img src="{{ asset('images/video.jpg') }}" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </section>

    {{-- Profissionais --}}
    <section id="profissionais" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h1>Conheça nossos profissionais</h1>
                        <p class="subtitle">Clique no perfil do psicólogo que você deseja conhecer...</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="customNavigation">
                    <a class="btn prev"></a>
                    <a class="btn next"></a>
                </div>

                <div id="owl-profissionais" class="owl-carousel owl-theme">
                    @foreach( $profissionais as $k => $pessoa )
                        <div class="profissional">
                            <div class="profissional-image" data-toggle="modal" data-target="#profissional_{{ $pessoa->usuario_id }}">
                                <img src="{{ asset( strlen($pessoa->avatar) > 0 ? "upload/user/{$pessoa->usuario_id}/{$pessoa->avatar}" : 'images/avatar-padrao.jpg' ) }}" alt="{{ $pessoa->tratamento }} {{ $pessoa->nome }} {{ $pessoa->sobrenome }}">
                            </div>
                            <div class="profissional-content" data-toggle="modal" data-target="#profissional_{{ $pessoa->usuario_id }}">
                                <h3 title="{{ $pessoa->tratamento }} {{ $pessoa->nome }} {{ $pessoa->sobrenome }}">{{ $pessoa->tratamento }} {{ $pessoa->nome }} {{ $pessoa->sobrenome }}</h3>
                                
                                <?php //<p>{{ $pessoa->experiencia_profissional }}</p>
                                    $especialidades =  \LogDialog\Model\Especialidade::whereProfissionalId($pessoa->id)
                                                         ->where('data_aprovado', '!=', null)
                                                         ->where('visibilidade', '!=', 'PRIVADO')
                                                         ->pluck('especialidade')
                                                         ->take(3)
                                                         ->toArray();
                                ?>

                                <ul>
                                    @foreach( $especialidades as $k => $item )
                                        <li>{{ $item }}.</li><br />
                                    @endforeach
                                </ul>
                            </div>
                            <?php /* <div class="profissional-icons">
                                <ul>
                                    <li data-toggle="modal" data-target="#profissional_{{ $pessoa->usuario_id }}">
                                        <img src="{{ asset('images/icon-perfilprofissional.png') }}" alt="">
                                    </li>
                                    <li onclick="javascript:home.agendarConsulta('profissional', {{ $pessoa->usuario_id }}, {{ Auth::check() ? 1 : 0 }});">
                                        <img src="{{ asset('images/icon-agendarhorario.png') }}" alt="">
                                    </li>
                                </ul>
                            </div> */ ?>
                            {{-- //data-toggle="modal" data-target="#agendar-consulta" --}}
                        </div>{{-- /.profissional --}}
                    @endforeach
                </div> {{-- /#owl-profissionais --}}
                <a href="{{ action('ProfissionaisController@index') }}"><div class="btn btn-gradient">Ver todos os profissionais</div></a>
            </div> {{-- /.row --}}

        </div>
    </section>

    <style>
        .profissional-content h3{
            display: block;
            white-space: nowrap;
            /* Fallback for non-webkit */
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .profissional-content p{
            display: block;
            /* Fallback for non-webkit */
            display: -webkit-box;
            /* Fallback for non-webkit */
            line-height: 1.4;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>

    {{-- Sobre --}}
    <section id="sobre">
        <div id="sobre-inside">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6">
                        <div class="content-left">
                            <h1>Sobre</h1>
                            <p>Logdialog é uma tecnologia desenvolvida para os dias de hoje, possibilitando a realização de orientação psicológica online. Através do serviço de saúde mental aliado à tecnologia da informação, o Logdialog busca atender às necessidades de cada indivíduo.</p>
                            <p>A qualquer hora e local, nosso serviço proporciona de maneira simples, acessível e segura, uma solução para que seu atendimento seja realizado através de computador, tablet ou celular, por profissionais qualificados que você escolhe. </p>
                            <p>Atendimento profissional de alta qualidade, rigor ético, confidencialidade de nossos clientes praticidade e comodidade são os pilares do nosso compromisso em tornar a saúde mental acessível para a população e ter você melhor a cada dia.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    {{-- Vantagens --}}
    <section id="vantagens" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h1>Vantagens LogDialog</h1>
                        <p class="subtitle">Todas as facilidades sem sair do lugar.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="customNavigation">
                    <a class="btn prev"></a>
                    <a class="btn next"></a>
                </div>
                @if(isset($vantagens))
                     <span class="hide">{{$i=0}}</span>
                     <div id="owl-vantagens" class="owl-carousel owl-theme">
                     @foreach($vantagens as $vantagem) 
                            <div class="vantagem-item">
                                <a data-toggle="modal" data-target="#modal-v0{{$i++}}">
                                    <div class="vantagem-image" style="background-image: url({{ $vantagem->imagem }})">
                                    </div>
                                    <div class="vantagem-content">
                                        <h3>{{$vantagem->titulo}}</h3>
                                        <p>{{ strip_tags(str_limit($vantagem->conteudo, $limit = 110, $end = '...')) }}</p>
                                    </div>
                                </a>
                            </div>  
                    @endforeach
                </div>
                @endif
                <div class="row">
                    <div class="quote">
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Contato --}}
    <section id="contato" class="home-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-white">
                        <h1>Contato</h1>
                        {{-- <p class="subtitle">Sinta-se a vontade para nos enviar uma mensagem.</p> --}}
                        <p class="subtitle">Envie sua mensagem.<br>Ficaremos satisfeitos em poder ajudar.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div id="contato-form">
                        <form id="formContato" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row">
                                <p id="returnmessage"></p>
                            </div>
                            <div class="col-lg-12">
                                {{-- Nome(*):<br> --}}
                                <input type="text" name="nome" id="nome" placeholder="Nome*"/>
                            </div>
                            <div class="col-lg-12">
                                {{-- E-mail(*):<br> --}}
                                <input type="text" name="email" id="email" placeholder="Email*"/>
                            </div>
                            <div class="col-lg-12">
                                {{-- E-mail(*):<br> --}}
                                <input type="text" name="telefone" id="telefone" placeholder="Telefone"/>
                            </div>

                            <div class="col-md-12">
                                {{-- Mensagem:<br>  --}}
                                <textarea name="mensagem" id="mensagem" placeholder=" Messagem..."></textarea><br/>
                            </div>

                            <button type="button" id="submit" name="Submit" value="Enviar" class="btn btn-gradient" onclick="contato.enviar('formContato'); return false;">Enviar</button>
                            </div> {{-- /.row --}}
                        </form>
                </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Contact Form JavaScript --}}
    <script src="{{ asset('js/jqBootstrapValidation.js') }}"></script>
    {{-- <script src="{{ asset('js/contact_me.js') }}"></script> --}}

    <script>
        // scroll
        function scrollParaItem( id )
        {
            // pegando o posicionamento do item clicado
            var scroll = $(id).offset().top - 50;

            $('html, body').stop().animate({
                scrollTop : scroll
            }, 500);

            return false;
        }

        $(document).ready(function(){
            // ao clicar em um item para scroll
            $("[data-scroll]").bind("click", function(evt){
                var id = $(this).attr("data-scroll");

                history.pushState(null, null, $(this).attr('href'));

                evt.preventDefault();

                scrollParaItem( id );
            });

            var owl = $("#owl-profissionais");

            owl.owlCarousel({
                items : 6, //10 items above 1000px browser width
                itemsDesktop : [1000,6], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,5], // 3 items betweem 900px and 601px
                itemsTablet: [600,3], //2 items between 600 and 0;
                itemsMobile : [480,2], // itemsMobile disabled - inherit from itemsTablet option
                navigation : false,
                pagination: false
            });

            $(".prev").click(function(){
                owl.trigger('owl.prev');
            });

            $(".next").click(function(){
                owl.trigger('owl.next');
            });
        });

        $(document).ready(function(){
            var owl = $("#owl-vantagens");

            owl.owlCarousel({
                items : 3, //10 items above 1000px browser width
                itemsDesktop : [1000,3], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
                itemsTablet: [600,2], //2 items between 600 and 0;
                itemsMobile : [480,2], // itemsMobile disabled - inherit from itemsTablet option
                navigation : false,
                autoplay:true,
                autoplayTimeout:300,
                autoplayHoverPause:true
            });

            $(".prev").click(function(){
                owl.trigger('owl.prev');
            });

            $(".next").click(function(){
                owl.trigger('owl.next');
            }); 
        });

        $(document).ready(function(){
            $("#btn-orientacao").click(function(){
                if($('.div-orientacao').css('display') == 'none') {
                    $(".div-orientacao").attr("style", "display:block");
                    $("#btn-orientacao").html('Fechar');
                } else {
                    $(".div-orientacao").attr("style", "display:none");
                    $("#btn-orientacao").html('Saiba mais');
                }
            });
        });
    </script>

    {{-- carregando scripts utilitários --}}
    <script src="{{ asset('js/contato.js') }}"></script>
@stop