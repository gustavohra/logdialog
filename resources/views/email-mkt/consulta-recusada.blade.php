<html>
<head>
<title>Logdialog</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
</head>
<body bgcolor="#FAFAFA" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color:#FAFAFA">
<span style="color: #424242"></span><!-- Save for Web Slices (F.72.01 Email - Layout.psd) -->
<br>
<table width="600" height="618" border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01">
	<tr>
		<td>
			<a href="https://www.logdialog.com.br" target="_blank"
				onmouseover="window.status='Logdialog';  return true;"
				onmouseout="window.status='';  return true;">
				<img src="https://logdialog.com.br/images/email/header.png" width="600" height="64" border="0" alt="Logdialog"></a></td>
	</tr>
	<tr>
		<td>
			<a href="#"
				onmouseover="window.status='Banner';  return true;"
				onmouseout="window.status='';  return true;">
				<img src="https://logdialog.com.br/images/email/banner.png" width="600" height="200" border="0" alt="Banner Logdialog"></a></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td width="600" height="83" style="font-family: montserrat, sans-serif; font-style: normal; font-weight: bold; font-size:22px;text-align:center; color: #232973;text-transform:uppercase">Consulta recusada.</td>
	</tr>
	<tr bgcolor="#FFFFFF" valign="top" >
		<td width="600" height="271" style="font-family: montserrat, sans-serif; font-style: normal; font-weight: 100;padding:20px 30px;font-size:13px">
			<p>{{ $texto }}</p>

<p>
	<a style="background:#393f81;text-decoration:none;color:#ffffff;font-weight:bold;font-size:11px;padding:8px 15px;display:block;margin:auto;text-align:center;border-radius:10px" href="{{ action("PerfilController@meu") }}">Acessar painel</a>
</p>
			<p><strong>Equipe, Log Dialog</strong></p></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>
   