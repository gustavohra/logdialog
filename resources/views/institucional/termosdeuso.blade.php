{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>TERMOS DE USO DO LOGDIALOG</h4>
            </div>
            <div class="pages-content">
                <p>Este Contrato define e contém todos os termos e condições entre você, paciente ou psicólogo, doravante denominado  "USUÁRIO" e www.Logdialog.com.br (Logdialog). </p>
                <p>O USUÁRIO declara que, ao acessar o Logdialog ou utilizar quaisquer dos serviços disponibilizados, concorda de forma total e sem ressalvas com as condições previstas neste contrato.</p>
                <p>Logdialog reserva-se ao direito de alterar os Termos de Uso a qualquer momento, conforme julgar necessário. Tais alterações e sua data de atualização serão publicadas em www.logdialog.com.br para que o USUÁRIO possa ler e aceitar sem restrição as condições do Termo de Uso atuais neste site.</p>
                <p>O Logdialog é uma empresa de software que fornece uma plataforma digital, que pode ser acessada através de dispositivos móveis e outros meios digitais, por psicólogos e pacientes. </p>
                <p>Os psicólogos devidamente inscritos no Conselho Regional de Psicologia - CRP podem se cadastrar para realizar atendimento online, e o USUÁRIO paciente pode escolher dentre os psicólogos cadastrados, através do currículo, habilidades e experiência disponíveis e descritos no site, aquele que  julgar mais apropriado para realizar seu atendimento.</p> 
                <p>O USUÁRIO pode consultar o Logdialog para obter informações. No entanto, não é permitido copiar, distribuir, modificar, transmitir ou revisar o conteúdo deste site sem permissão por escrito do Logdialog. Os Termos de Uso estão disponíveis e podem ser acessados através de várias plataformas, de dispositivos móveis e do site www.logdialog.com.br. </p>
                <p>O uso ou acesso do Logdialog não transfere para terceiros o título ou os direitos de propriedade intelectual. Todos os direitos, títulos e interesses em e para todos os aspectos deste site continuam a ser propriedade exclusiva da Logdialog.</p>

                <h3>Renúncia de responsabilidade médica</h3>
                <p>O Logdialog, assim como, todos os profissionais cadastrados em nossa plataforma, não estão autorizados a realizar diagnóstico médico, prescrição de medicamentos, nem fornecer orientação médica.  </p>
                <p>Logdialog não emprega os psicólogos cadastrados através deste site. Os psicólogos atuam no Logdialog como prestadores de serviço. Os Psicólogos candidatos a prestação de serviço no do Logdialog são cuidadosamente avaliados por psicólogos registrados no Conselho Regional de Psicologia - CRP. </p>
                <p>O Logdialog não assume qualquer responsabilidade por qualquer ato, omissão ou transgressão de qualquer psicólogo, uma vez que tais profissionais não são empregados, nem representantes do Logdialog. </p>
                <p>Apesar do Logdialog ajudar o USUÁRIO a encontrar um psicólogo, ou, caso não haja empatia, encontrar um novo psicólogo, não garantimos que você encontrará o psicólogo adequado, ou que satisfaça suas necessidades.</p>
                <p>Os agendamentos serão realizados tendo como referência o fuso horário correspondente ao local de registro do Psicólogo. Portanto, os horários disponíveis na agenda do psicólogo tem como referência o fuso horário correspondente ao local de registro do profissional. Sendo assim, o USUÁRIO cadastrado no Logdialog está de acordo que qualquer atendimento será agendado de acordo com o local de registro profissional do psicólogo.</p>
                <p>O USUÁRIO está ciente de todos os riscos e benefícios relativos ao uso de dispositivos móveis para atendimento psicológico online. </p>

                <h3>Verificação de documentos</h3>
                <p>As credenciais, como formação acadêmica em psicologia e especializações, precisam ser comprovadas e são checadas pelo Logdialog. O Logdialog verifica todos os documentos enviados pelos psicólogos (diploma de formação acadêmica, registro profissional, certificados de especialização, mestrado, doutorado e outras experiências de atuação) antes de realizar o cadastramento e autorizar a utilização do nosso serviço. Ainda que toda documentação seja verificada, não podemos garantir a veracidade de tais documentos. 
                <p>Assuntos relacionados ao conteúdo da sessão de atendimento, abordagem teórica e técnicas utilizadas pelo psicólogo devem ser tratados somente com o psicólogo. O Logdialog não tem livre acesso a todos os documentos, portanto não pode controlar os assuntos abordados durante a sessão de atendimento.</p>
                <p>O Logdialog se empenha e toma todas as medidas razoáveis para segurança de seus USUÁRIOS, mas ainda assim, recomendamos que você tenha prudência para usar nosso serviço. Em alguns casos, nossos serviços não podem substituir uma sessão presencial com um psicólogo ou com outros profissionais. Recomendamos então que você se sinta confortável para buscar orientação online ou presencial de outro psicólogo, médico, ou qualquer outro profissional antes de tomar qualquer decisão que seja importante, ou que possa comprometer sua saúde e bem-estar.</p>

                <h3>ATENÇÃO!</h3>
                <p>SE VOCÊ ESTÁ PENSANDO SOBRE SUICÍDIO,</p>
                <p>SE VOCÊ CONSIDERA TER ALGUMA ATITUDE QUE PODE TE PREJUDICAR, OU PREJUDICAR OUTRAS PESSOAS,</p>
                <p>SE VOCÊ SE SENTE EM PERIGO, OU ACHA QUE PODE COLOCAR OUTRA PESSOA EM PERIGO,</p>
                <p>SE VOCÊ ESTÁ EM ALGUMA SITUAÇÃO DE EMERGÊNCIA MÉDICA, </p>
                <p>VOCÊ DEVE IMEDIATAMENTE LIGAR PARA UM DOS NÚMEROS DOS SERVIÇOS DE EMERGÊNCIA ABAIXO E AVISAR AS AUTORIDADES COMPETENTES. </p>
                <p>190 - POLÍCIA MILITAR</p>
                <p>192 - SERVIÇO PÚBLICO DE REMOÇÃO DE DOENTES (AMBUL NCIA)</p>
                <p>193 - CORPO DE BOMBEIROS</p>
                <p>Ainda que o Logdialog proteja sua Privacidade, os psicólogos cadastrados, que prestam serviços através do Logdialog, podem decidir pela quebra de sigilo, conforme regulamentado pelo Código de Ética do Psicólogo. O Cliente reconhece que o fato de ser atendido por um profissional cadastrado no Logdialog não diminui nosso compromisso ético e possibilidade de quebra de sigilo profissional através do fornecimento de informações necessárias (localização do USUÁRIO, endereço IP ou identificação pessoal) para as autoridades competentes.</p>

                <h3>Utilização de dados cadastrais</h3>
                <p>Compreendendo que tal consentimento pode afetar a privacidade do Cliente, o Cliente concorda que a equipe de Supervisão Clínica do Logdialog poderá analisar e ou fornecer suas informações de cadastro, sem que tal ato constitua qualquer violação de sigilo, para os seguintes propósitos:</p>
                <ul>
                    <li>Durante o login (entrada) do USUÁRIO no Logdialog</li>
                    <li>Durante o processo de agendamento </li>
                    <li>Para analisar reclamações realizadas pelo Cliente </li>
                    <li>Para analisar denúncias sobre o psicólogo cadastrado.</li>
                    <li>Para analisar queixas sobre a ética do psicólogo.</li>
                    <li>Em caso de preocupações com segurança do USUÁRIO</li>
                    <li>Para fazer a transição do USUÁRIO para um novo psicólogo</li>
                    <li>Se houver preocupação com a garantia na entrega da qualidade para o Cliente.</li>
                    <li>Em caso de indício de ato ilícito</li>
                </ul>
                <p>O Cliente está ciente e concorda que o Logdialog usa "Meta Dados" e outros termos de pesquisa que não identifica o USUÁRIO, para escanear transcrições, procurar tendências e padrões que possam afetar a qualidade de serviço ao cliente ou as práticas dos psicólogos que prestam serviço através do Logdialog.</p>
                <p>O USUÁRIO afirma que concede permissão ao Logdialog para que o psicólogo cadastrado forneça periodicamente avaliações clínicas do progresso do Cliente para Logdialog. O Cliente entende que as avaliações clínicas servem para fornecer informações sobre a sua saúde mental e bem-estar, e que os resultados podem ser analisados pelo seu psicólogo para que ele possa discutir com você.</p>
                <p>O USUÁRIO concorda afirmativamente com a revisão acima das comunicações protegidas por HIPAA do Cliente, pelo Logdialog, quando as condições acima estiverem presentes.
                Todos os dados não identificados, metadados e dados de pesquisa coletados pelo Logdialog através do Cliente do Logdialog continuam sendo propriedade exclusiva do Logdialog e de seus proprietários corporativos. O USUÁRIO não tem direitos sobre a propriedade, remoção ou exclusão de dados na Plataforma. Todos os dados mantidos pelo Logdialog e armazenados como parte do acordo com o psicólogo e com o USUÁRIO serão armazenados de forma segura.</p>

                <h3>Pagamento</h3>
                <p>O pagamento pelo uso do Logdialog é usado para remunerar o Logdialog por seu desenvolvimento de software, despesas gerais, serviços administrativos, custos corporativos, impostos e taxas, incluindo taxas de transação para uso de cartão de crédito. Os psicólogos cadastrados que atendem através do Logdialog também pagam pelo uso da tecnologia Logdialog e por taxas administrativas. </p>
                <p>Parte do seu pagamento é contabilizado separadamente e transferido ao psicólogo para pagamento dos serviços clínicos fornecidos a você. Logdialog não participa, divide ou assume uma percentagem desta parte do pagamento. Independentemente deste pagamento, Logdialog não é considerado um prestador de serviço como é o psicólogo cadastrado.</p> 
                <p>Tanto o psicólogo como o USUÁRIO concorda em indenizar o Logdialog por quaisquer reivindicações que um psicólogo ou USUÁRIO fizer contra o outro. O Cliente e o psicólogo cadastrado concorda em indenizar e isentar a Logdialog, suas entidades relacionadas, afiliadas e seus diretores, executivos, gerentes, funcionários, doadores, agentes e licenciadores, de e contra todas as perdas, despesas, danos e custos resultantes de qualquer violação destes Termos de Uso. </p>
                <p>Ao fornecer as informações sobre o cartão de crédito e de acordo com Política de Cobrança, o USUÁRIO está ciente, concorda e autoriza o Logdialog a faturar e cobrar todas as taxas e encargos (incluindo quaisquer impostos e taxas de atraso, conforme aplicável) correspondentes à sua conta de USUÁRIO, assim como se compromete em manter os dados do cartão de crédito atualizados. </p>

                <h3>Uso Adequado do Site</h3>
                <p>O USUÁRIO declara e garante possuir plena capacidade jurídica para a celebração do presente contrato, ser civilmente capaz, conforme a legislação brasileira, sendo que, caso seja constatado qualquer dano cometido por agentes total ou relativamente incapazes, com ou sem permissão de seus pais, tutores ou representantes legais, estes serão solidariamente responsáveis por todos os atos praticados pelos menores.</p>
                <p>O USUÁRIO está ciente que é obrigado, aceita e se compromete a fornecer informações necessárias, verdadeiras e atualizadas para se cadastrar no Logdialog, realizar pagamento de serviços contratados, ou de produtos disponíveis através do Logdialog.</p>
                <p>O USUÁRIO concorda em respeitar toda e qualquer legislação brasileira vigente e aplicável à utilização do Logdialog e aos atos do USUÁRIO dentro dele, bem como qualquer norma ou lei aplicável no país de onde se origina o acesso do USUÁRIO.</p>

                <h3>Proibições para o Uso do Logdialog</h3>
                <p>É proibido fazer publicidade, vender produtos serviço, coletar dados ou utilização do serviço para qualquer fim diferente do que é oferecido originalmente pelo Logdialog, sem nossa permissão expressa.</p>
                <p>É proibido o uso do Logdialog para qualquer finalidade que possa violar qualquer lei municipal, estadual, federal ou internacional</p>
                <p>É proibido publicar qualquer material que seja difamatório, abusivo ou obsceno, incluindo, mas sem limitação, aqueles que contenham conteúdo discriminatório em razão de sexo, raça, religião, condição social, idade, crença.</p>
                <p>É proibido utilizar senhas de caráter ofensivo ou ilegal, que induzam terceiros a erro, ou que já estejam em uso por outro USUÁRIO.</p>
                <p>É proibido o uso de qualquer material que encoraje conduta que constitua ofensa criminal, dê origem a responsabilidade civil ou de outra forma violar qualquer lei nacional ou internacional aplicável.</p>
                <p>É proibido ao USUÁRIO fingir ser outra pessoa.</p>
                <p>É proibido realizar ou divulgar qualquer tipo de gravação, de atendimento ou sessão, independente da tecnologia utilizada, em que apareçam imagens, sons, textos, ou outra linguagem criptografada ou não, sob pena de incorrer em responsabilidade civil por danos morais e até mesmo crime.</p>
                <p>É proibido agredir, caluniar, injuriar ou difamar os psicólogos, antes, durante ou após as sessões.</p>
                <p>É proibido infringir quaisquer direitos de propriedade industrial e autoral de terceiros na utilização do Logdialog.</p>
                <p>É proibido constituir solicitações de caridade, cartas em cadeia ou esquemas de pirâmide.</p>
                <p>É proibido o uso de "frame" para incluir qualquer página ou fragmento de página ou conteúdo do Logdialog dentro de outras páginas ou janela do browser. </p>
                <p>É proibido utilizar o Logdialog para obter, através de qualquer procedimento ilícito, acesso não autorizado a outro computador, servidor, ou rede; interromper serviço, servidores, ou rede de computadores; burlar qualquer sistema de autenticação ou de segurança, disseminar material que contenha vírus, worm, cavalo de Tróia, bomba de tempo ou qualquer outro programa ou componente prejudicial </p>
                <p>É proibido obter acesso não autorizado a áreas restritas do site, outras contas, sistemas de computador ou redes conectadas ao site, por meio de mineração por senha ou por qualquer outro meio.</p>
                <p>É proibido invadir a privacidade ou vigiar secretamente; acessar informações confidenciais, informações financeiras, números de cartões de crédito, contas bancária ou que possam causar prejuízos a qualquer pessoa.</p>

                <h3>Isenção de Responsabilidade</h3>
                <p>O Cliente concorda que o Logdialog e os psicólogos cadastrados não serão responsáveis ​​perante o Cliente ou qualquer outra pessoa por qualquer perda ou prejuízo causado, danos diretos ou indiretos e lucros cessantes decorrentes do uso, ou inaptidão para o uso do conteúdo ou de qualquer outra informação obtida através do Logdialog. 
                Estes prejuízos incluem, mas não se resumem a, danos médicos, legais e quaisquer outras reclamações ou indenizações de danos por negligência, dor e sofrimento, danos pessoais / morte ilícita, perda de renda, perda de consórcio, interrupção de negócios, contas médicas, danos por perda de dados, perda de programas e / ou custo de aquisição de serviços de substituição ou interrupções de serviço. </p>
                <p>A conexão do USUÁRIO com a internet não é responsabilidade do Logdialog. O acesso ao Logdialog depende de equipamentos tecnológicos, navegador e acesso à Internet, que é responsabilidade do USUÁRIO. </p>
                <p>O Logdialog não se responsabiliza e não garante que não ocorram falhas ou interrupções, uma vez que o funcionamento dependente de diversos fatores e de terceiros.</p>
                <p>O uso de plataformas de tecnologia desatualizadas não é seguro, portanto o Logdialog recomenda o uso de plataformas de acesso e tecnologia atualizadas e não se responsabiliza por danos de qualquer natureza.</p>
                <p>O uso inadequado do site exclui o Logdialog de possíveis perdas e danos.
                Logdialog poderá, por motivos técnicos, ou para melhorar a experiência do USUÁRIO, atualizar seu sistema, mesmo que possíveis alterações ocorram.</p>
                <p>O Logdialog não garante, sob nenhuma hipótese, nos serviços prestados por terceiros, disponíveis através do Logdialog, a ausência de vírus ou outros elementos nocivos que possam produzir alterações no sistema informático (software e hardware) do USUÁRIO, pois o Logdialog não exerce nenhuma verificação prévia sobre estes serviços e se encarrega apenas de intermediar. </p>
                <p>O Logdialog não tem controle sobre serviços e sites de terceiros, portanto não garante ausência de qualquer elemento nocivo nos documentos eletrônicos e arquivos que venham a ser instalados em seu sistema informático a partir de sites de terceiros. </p>

                <h3>Hiperlinks</h3>
                <p>Qualquer link (incluindo um hiperlink, botão ou dispositivo de referência de qualquer tipo) usado no Logdialog é fornecido para seu uso e conveniência e não constitui um endosso, recomendação ou certificação por nós, nem deve ser interpretado como uma sugestão de que qualquer site de terceiros tenha qualquer relação com Logdialog. </p>
                <p>O Logdialog não endossa o conteúdo de qualquer site de terceiros, não é responsável pelo conteúdo de sites, ou anúncio de terceiros, e não faz representações sobre o seu conteúdo ou precisão. O Logdialog não se associa a sites que possam infringir marcas registradas, marcas de serviço, direitos autorais ou patentes válidos e existentes.</p>
                <p>O uso de sites de terceiros por parte do Cliente é de responsabilidade do Cliente e está sujeito aos termos e condições de uso de tais websites.</p>

                <h3>Cookies</h3>
                <p>O USUÁRIO autoriza o uso de “cookies” (pacote de dados enviados de um website para o navegador, para notificar atividades prévias e lembrar informações da atividade do USUÁRIO), ou arquivos de natureza análoga, para acessar informações sobre a conta do USUÁRIO a fim de oferecer um serviço melhor e mais personalizado. O uso de “cookies” não é obrigatório e não prejudica o serviço oferecido caso seja desabilitado pelo USUÁRIO . No entanto, uma vez que os cookies são desligados, o USUÁRIO deverá fazer o acesso com login e senha cada vez que acessar áreas do Logdialog em que esses dados são requeridos.</p>

                <h3>Responsabilidade do USUÁRIO por Posts.</h3>
                <p>O Logdialog não tem a obrigação de responder mensagens abertas postadas no site, em redes sociais abertas ou qualquer outra mídia agora conhecida ou desenvolvida no futuro. Se o USUÁRIO enviar mensagem para Logdialog, postar ou publicar qualquer informação (texto, vídeo, fotografia, áudio) nas redes sociais vinculadas ao Logdialog, ele é responsável por tais informações, materiais e pelas as conseqüências de sua postagem. </p>
                <p>O USUÁRIO concorda que qualquer publicação que optar por fazer é de livre e espontânea vontade e está em conformidade com a lei. Logdialog reserva-se ao direito de editar ou excluir qualquer material postado no Logdialog. O Logdialog não endossa quaisquer opiniões expressas por terceiros. O USUÁRIO reconhece, se responsabiliza e assume o risco pelo uso que qualquer conteúdo postado por outras pessoas no Logdialog.</p>

                <h3>Modificações, Interrupções e Suspensões e Descontinuação  do Logdialog</h3>
                <p>O USUÁRIO do Logdialog está ciente e aceita que Logdialog pode modificar, suspender, interromper ou descontinuar Logdialog ou qualquer parte do site Logdialog, seja para todos os USUÁRIOs ou para algum USUÁRIO especificamente, a qualquer momento, com ou sem aviso prévio. </p>
                <p>O Logdialog fica disponível 24 (vinte e quatro) horas por dia, 7 (sete) dias por semana. No entanto, a tecnologia do Logdialog é tecnicamente complicada e depende de muitos fatores como engenharia, hospedagem, software, hardware, serviços de telecomunicações, fornecedores de serviços, diversas ferramentas tecnológicas e necessidade de manutenção. Diante dos motivos citados, o Logdialog não garante que seu funcionamento é ininterrupto, que será 100% seguro, consistente ou sem erros, uma vez que está sujeito a falhas em serviços e outras dificuldades técnicas que podem a qualquer momento comprometer ou interromper o funcionamento do serviço do Logdialog.</p>
                <p>O USUÁRIO está ciente e de acordo que o Logdialog não será responsável por nenhuma das ações acima mencionadas ou por quaisquer perdas ou danos que sejam causados por qualquer das ações mencionadas acima.</p>

                <h3>Vigência e rescisão de contrato </h3>
                <p>O contrato de uso do Logdialog tem prazo de validade indeterminado, a contar da data de cadastramento do USUÁRIO. 
                Em caso de uso inadequado ou descumprimento do Termo de Uso, o USUÁRIO poderá ter seu acesso cancelado imediatamente e sem aviso prévio. </p>
                <p>O uso continuado do Logdialog pelo USUÁRIO significará sua aceitação das mudanças feitas no website ou nos Termos de Uso. Se o Cliente não aceitar as alterações, o único e exclusivo recurso do Cliente é descontinuar o uso do Logdialog.</p>
                <p>O cancelamento da conta no Logdialog só será realizado e efetivado após o descadastramento, que deve ser realizado pelo USUÁRIO no Logdialog. </p>

                <h3>Propriedade intelectual do Logdialog </h3>
                <p>Todos os conteúdos e materiais como textos, desenhos, gráficos, ícones, algoritmos, nomes de domínio, código, imagens, mensagens, logotipos e o termo "Logdialog" que estão disponíveis no Logdialog, são propriedade intelectual da Logdialog e estão protegidos pelas leis de direitos autorais e propriedade industrial aplicáveis. Todos os direitos reservados.</p>
                <p>Os conteúdos das telas do Logdialog, assim como os programas, bancos e bases de dados, redes, arquivos, e quaisquer outras criações autorais e intelectuais dos prepostos do Logdialog, são de propriedade exclusiva do Logdialog e estão protegidos pelas leis e tratados internacionais, sendo vedada sua cópia, reprodução, ou qualquer outro tipo de utilização, ficando os infratores sujeitos às sanções civis e criminais correspondentes.</p>
                <p>O uso da marca registrada ou de qualquer outro conteúdo do Logdialog é rigorosamente proibido, exceto com autorização expressa e por escrito do Logdialog, conforme disposto nestes Termos de Uso.</p>
                <p>O uso ou acesso ao Logdialog não dá ao USUÁRIO o direito de utilizar marcas, nomes comerciais e logotipos disponíveis ou publicados no no Logdialog, uma vez que o Logdialog é o proprietário dos itens citados.</p>
                <p>O Cliente concede à Logdialog uma licença não exclusiva, mundial, isenta de royalties e perpétua, com direito a sublicenciar, a reproduzir, distribuir, transmitir, criar trabalhos derivados, exibir publicamente quaisquer materiais e outras informações que o USUÁRIO submeta nas redes sociais e outros meios de comunicação, agora conhecidos ou desenvolvidos e que não seja parte do processo de cadastramento onde estão protegidas por senha. Isso não se aplica para conteúdo informado durante a sessão de atendimento.</p>
                <p>Logdialog não oferece serviço, nem produto, assim como não faz uso de nome, logótipo ou marca em território onde não tenha direito de fazê-lo.</p> 
                <p>Qualquer uso inadequado, incluindo mas não se resumindo à reprodução, distribuição, exibição ou transmissão de qualquer conteúdo deste site é estritamente proibido.</p>

                <h3>Requisitos de idade, senha, e-mail e acesso ao Logdialog</h3>
                <p>Ao usar o Logdialog você afirma que tem 18 anos de idade ou mais. Logdialog não tem motivos para acreditar que o USUÁRIO não seja julgado um adulto competente de acordo com a lei.  </p>
                <p>No caso de USUÁRIOS menores de 18 anos, o responsável deve preencher o "Termo de Autorização para Atendimento de Menor de 18 anos", disponível no cadastro, para autorizar que seu dependente, menor, seja atendido pelo Logdialog.</p>
                <p>O USUÁRIO do Logdialog ou o responsável por menor de 18 anos USUÁRIO do Logdialog está ciente e concorda com os termos e condições deste Contrato, assim como está de acordo e aceita os termos da Política de Privacidade do Logdialog. O USUÁRIO ou o responsável por USUÁRIO menor de 18 anos, é responsável por manter a confidencialidade de sua Senha de USUÁRIO, e-mail de cadastro e quaisquer outras informações de segurança relacionadas à conta de USUÁRIO. Logdialog não será responsável por qualquer perda que o USUÁRIO ou USUÁRIO menor de 18 anos tiver ou acarretar a terceiro, caso outra pessoa, mesmo sem seu consentimento, utilize sua conta de USUÁRIO ou senha.</p>
                <p>As senhas de acesso ao Logdialog são pessoais, intransferíveis e de responsabilidade do próprio USUÁRIO, sendo que a transferência e utilização de senha por terceiros é proibida.</p>
                <p>O acesso a áreas restritas é expressamente proibido para pessoas não autorizadas. Para ter acesso a áreas restritas é necessário cadastro, login, senha e ser devidamente autorizado. O USUÁRIO está ciente de que pode sofrer sanções civis e criminais decorrentes de sua conduta.</p>

                <h3>Segurança</h3>
                <p>Para proteger suas informações pessoais, tomamos precauções razoáveis e seguimos as melhores práticas da indústria para nos certificar que elas não serão perdidas inadequadamente, usurpadas, acessadas, divulgadas, alteradas ou destruídas.
                </p>
                <p>Embora nenhum método de transmissão pela Internet ou armazenamento eletrônico seja 100% seguro, nós seguimos todos os requisitos e implementamos padrões adicionais geralmente aceitos pela indústria.
                </p>
                <p>O USUÁRIO está ciente que por questões de segurança o Logdialog aconselha que não sejam utilizados como senha números sequenciais, números de documentos, datas de aniversário, nomes próprios, de familiares ou de pessoas próximas. O Logdialog também recomenda que as senhas sejam de pelo menos seis dígitos e contenham ao menos um número, uma letra maiúscula, uma letra minúscula e um caractere especial (ex. ! @ * % _) e que sejam alteradas periodicamente. O Logdialog poderá enviar mensagens sugerindo que a senha seja alterada periodicamente. </p>


                <h3>Confidencialidade das informações na Orientação Psicológica</h3>

                <p>Logdialog tem um compromisso com a confidencialidade. As informações fornecidas e armazenadas são protegidas por um sistema avançado de criptografia que codifica as informações de forma que só quem envia e quem recebe as informações consigam decifrá-las.
                </p>
                <p>As únicas informações pessoais que serão enviadas pelo Logdialog ao psicólogo para identificação correta do paciente são, o nome completo do paciente e a data de nascimento. No caso de orientação para menores, também será disponibilizado para o psicólogo o nome completo do responsável e o documento de autorização já preenchido pelo responsável.
                </p>
                <p>Informações complementares que o psicólogo julgar necessárias poderão ser solicitadas pelo próprio psicólogo durante a sessão, onde as informações são criptografadas e o Logdialog não tem acesso. 
                </p>
                <p>O USUÁRIO fica ciente e concorda que qualquer informação, dado, texto, vídeo, mensagem ou qualquer outro material transmitido através do atendimento com o psicólogo será de total responsabilidade do USUÁRIO, pois não há possibilidade do Logdialog controlar ou ter acesso sobre essas informações. 
                </p>
                <p>Todas as informações sobre o paciente que o psicólogo armazenar em prontuário também estão protegidas por criptografia.
                </p>
                <p>É importante ressaltar que os psicólogos geralmente são obrigados a notificar as suspeitas de crimes, incluindo suspeita de maus-tratos e violência sexual contra crianças ou adultos, absoluta ou relativamente incapazes. 
                </p>
                <p>Além disso, o psicólogo poderá quebrar sigilo quando o paciente estiver em condições mentais ou emocionais que representem perigo para si próprio ou aos outros. 
                </p>
                <p>De forma geral, o psicólogo deverá respeitar a legislação atual e seguir as orientações contidas no Código de Ética do Conselho Federal de Psicologia vigente no Brasil.</p>

                <h3>Termos legais gerais</h3>
                <p>O presente Contrato será vinculativo para ambas as partes. </p>
                <p>Nenhuma emenda a este Contrato será efetiva a menos que seja feita por escrito. Os cabeçalhos dos parágrafos são exclusivamente por razões de conveniência e não serão aplicados na interpretação deste documento.</p>
                <p>Se qualquer disposição deste Contrato for considerada por um tribunal de jurisdição competente como ilegal, inválida, inexeqüível ou de outra forma contrária à lei, as restantes disposições deste Contrato permanecerão em pleno vigor e efeito.</p>
                <p>A tolerância de uma parte para com a outra, relativamente a descumprimento de qualquer das obrigações ora assumidas, não será considerada novação ou renúncia a qualquer direito, constituindo mera liberalidade, que não impedirá a parte tolerante de exigir da outra o fiel cumprimento deste contrato, a qualquer tempo.</p>
                <p>As atividades desenvolvidas pelo Logdialog, através do PORTAL, não são consideradas de risco, sendo inaplicável a responsabilidade objetiva disposta no artigo 927, parágrafo único, do Código Civil, o que o USUÁRIO declara concordar, neste ato.</p>
                <p>O USUÁRIO concorda com a obrigação de indenizar, em ação regressiva, qualquer prejuízo causado ao Logdialog em decorrência de ações que envolvam seus atos inclusive por meio da denunciação da lide, prevista no artigo 125, II do Código de Processo Civil. </p>
                <p>Este Acordo será interpretado apenas de acordo com as leis brasileiras.</p>
                <p>Fica eleito o Foro da Comarca de São Paulo, com renúncia expressa de qualquer outro, ainda que mais benéfico a qualquer das partes, para decidir controvérsia decorrente do presente contrato, que será regido pelas leis vigentes na República Federativa do Brasil.</p>

            </div>
        </div>
    </div>
@stop
{{-- fim do conteúdo do yield atual --}}