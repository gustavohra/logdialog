carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>PERGUNTAS FREQUENTES</h4>
            </div>
            <div class="pages-content">
                <h3 class="title-titlecase" style="margin-top:0;">Logdialog substitui a terapia?</h3>
                <p>Logdialog surgiu como mais uma opção para que as pessoas possam se cuidar e encontrar profissionais que realmente atendem a sua necessidade. Nosso objetivo é ampliar e facilitar acesso aos psicólogos e não substituir a psicoterapia presencial. </p>
                <hr>
                <h3 class="title-titlecase">O que é orientação psicológica online?</h3>
                <p>Orientação psicológica online é um serviço psicológico, autorizado e regulamentado pelo Conselho Federal de Psicologia - CFP, realizado em até 20 sessões, por meios tecnológicos de comunicação a distância desde que pontuais, informativos e focados no tema proposto.</p>
                <hr>
                <h3 class="title-titlecase">Orientação psicológica online é efetiva?</h3>
                <p>Sim, a orientação online é efetiva. Atualmente, muitos estudos científicos demonstram que se for realizada da maneira correta, por profissionais qualificados e no ambiente adequado, esta modalidade de atendimento tem grande potencial terapêutico.</p>
                <hr>
                <h3 class="title-titlecase">Qual a duração de cada consulta?</h3>
                <p>Você decide a duração da sua consulta de acordo com a sua necessidade. Basta escolher entre 30 minutos ou 50 minutos. Porém, o mais indicado é que isso seja conversado e decidido junto ao psicólogo, para atender a um planejamento terapêutico mais adequado.</p>
                <hr>
                <h3 class="title-titlecase">Qual a frequência das consultas?</h3>
                <p>Você também decide quando quer se consultar. No entanto, para atender a um planejamento terapêutico adequado, é importante que isso seja conversado com o profissional. </p>
                <hr>
                <h3 class="title-titlecase">E a privacidade?</h3>
                <p>A primeira preocupação do Logdialog é garantir o sigilo e a segurança dos nossos clientes. A sua tranquilidade e conforto é muito importante para nós e por esse motivo os profissionais cadastrados no site seguem o Código de Ética Profissional vigente no Brasil e a nossa tecnologia atende aos mais rigorosos padrões de segurança da informação. </p>
                <hr>
                <h3 class="title-titlecase">Os atendimentos são gravados?</h3>
                <p>Não. O único registro existente é o prontuário, que só pode ser acessado pelo psicólogo, e é protegido por tecnologia de segurança e codificação avançada.</p>
                <hr>
                <h3 class="title-titlecase">Qualquer um pode utilizar o serviço do Logdialog?</h3>
                <p>Sim. Qualquer pessoa com mais de 18 anos pode se cadastrar. Menores de 18 anos também podem, mas precisam de autorização dos pais. </p>
                <hr>
                <h3 class="title-titlecase">Os psicólogos são registrados?</h3>
                <p>Sim, todos eles. Nossa missão é oferecer orientação psicológica de qualidade. Para isso além do registro no conselho de psicologia, cada profissional passa por um rigoroso processo de seleção, checagem de documentos e treinamento.</p>
                <hr>
                <h3 class="title-titlecase">Quem são os psicólogos?</h3>
                <p>Nós temos profissionais especializados em diversas áreas da psicologia e treinados para atender em nossa plataforma. Temos também profissionais que monitoram todos os detalhes do nosso serviço.</p>
                <hr>
                <h3 class="title-titlecase">Como agendar um horário?</h3>
                <p>Para agendar é fácil. Basta entrar na agenda do profissional que você escolheu e clicar no horário disponível que desejar. Será enviado uma solicitação de horário ao profissional. O profissional poderá aceitar, ou enviar uma nova sugestão. Assim que a solicitação for aceita, você será notificado via e-mail e nos avisos da nossa plataforma. </p>
                <hr>
                <h3 class="title-titlecase">Como faço para mudar meu horário agendado?</h3>
                <p>Você pode alterar o horário da sua consulta com até 12 horas de antecedência sem precisar pagar a consulta nem taxa administrativa.</p>
                <hr>
                <h3 class="title-titlecase">E se eu precisar alterar meu horário com menos de 12 horas para o início da consulta?</h3>
                <p>Neste caso uma taxa administrativa de x% do valor da consulta será cobrada. </p>
                <hr>
                <h3 class="title-titlecase">Se por acaso eu me atrasar?</h3>
                <p>Não se preocupe. Seu psicólogo estará te esperando até o final do seu horário.</p>
                <hr>
                <h3 class="title-titlecase">E se eu faltar, a consulta será cobrada?</h3>
                <p>Sim. Ao agendar uma consulta, o horário do psicólogo fica bloqueado para atendimento de outro paciente.  </p>
                <hr>
                <h3 class="title-titlecase">Meu psicólogo pode alterar o horário ?</h3>
                <p>O psicólogo também pode alterar o horário da consulta. Assim como você, ele deve avisar com 12 horas de antecedência para não ser cobrado.</p>
                <hr>
                <h3 class="title-titlecase">E se ele avisar em cima da hora?</h3>
                <p>Você terá um desconto de x% no valor da sua consulta.</p>
                <hr>
                <h3 class="title-titlecase">O que devo fazer se meu psicólogo se atrasar?</h3>
                <p>É só aguardar e ele irá atendê-lo assim que possível. Também não se preocupe com o tempo, pois ele poderá ser compensado ao final da consulta, ou ser gerado um desconto proporcional para o pagamento da próxima sessão.</p>
                <hr>
                <h3 class="title-titlecase">Pode acontecer do psicólogo faltar. Como fica o valor que já paguei?</h3>
                <p>Você poderá agendar uma nova consulta sem custo.</p>
                <hr>
                <h3 class="title-titlecase">E se eu não gostar do meu psicólogo?</h3>
                <p>Para ajudar na escolha, o Logdialog oferece a primeira consulta para apresentação sem custo. O paciente e o profissional agendam um horário para conversarem e se conhecerem. Caso não se identifique com o profissional, o paciente pode agendar com outro profissional. </p>
                <p>Se preferir, também poderá entrar em contato com o Logdialog e tentaremos lhe ajudar a encontrar o profissional mais adequado ao seu perfil. Temos muito interesse em receber qualquer comentário que nos ajude a entender por qual motivo você não se sentiu confortável.</p>
                <hr>
                <h3 class="title-titlecase">O perfil do psicólogo é público?</h3>
                <p>Depende. O profissional pode optar por manter o seu perfil oculto, público ou divulgar apenas para quem desejar.</p>
                <hr>
                <h3 class="title-titlecase">Como funciona o pagamento?</h3>
                <p>O processo é simples e seguro. Todas as transações financeiras são realizadas dentro do nosso site, que está integrado ao PagSeguro, para que você possa efetuar o pagamento com tranquilidade. Além disso, todas as informações do cartão de crédito são mantidas em segurança e nós não divulgamos nenhuma informação pessoal.</p>
                <hr>
                <h3 class="title-titlecase">Eu não tenho conta no PagSeguro. Como fazer?</h3>
                <p>Não tem problema. Não é necessário ter conta no PagSeguro para efetuar o pagamento.</p>
                <hr>
                <h3 class="title-titlecase">Quais são os requisitos básicos para uma orientação psicológica online?</h3>
                <p>É necessário um computador ou dispositivo com acesso à Internet 4G ou Wi-Fi e estar cadastrado no site www.logdialog.com.br. </p>
                <hr>
                <h3 class="title-titlecase">Eu preciso fazer download ou instalar alguma coisa no meu computador?</h3>
                <p>Não. Basta estar logado, pois todo processo de atendimento acontece dentro do site.</p>
                <hr>

            </div>
        </div>
    </div>
@stop
{{-- fim do conteúdo do yield atual