carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>POLÍTICA DE PRIVACIDADE</h4>
            </div>
            <div class="pages-content">
                <p>Sua privacidade é muito importante para nós. Criamos nossa Política de Privacidade para divulgar informações importantes sobre como você pode usar o Logdialog.com.br e como coletamos e usamos seus conteúdos e informações. Recomendamos que leia a Política de Privacidade na íntegra.</p>

                <h3>Sobre as informações coletadas</h3>

                <p>Quando você utiliza nosso serviço, como parte do processo de contratação, coletamos  suas informações pessoais, por você fornecidas, tais como seu nome, endereço e endereço de e-mail.</p>

                <p>Quando você navega pelo nosso site, recebemos também automaticamente o protocolo de internet do seu computador, denominado endereço de IP, a fim de obtermos informações sobre seu navegador e sistema operacional.</p>

                <p>Com sua permissão, podemos lhe enviar emails sobre nosso serviço, novos produtos e outras atualizações.</p>

                <p>O Logdialog.com.br não irá comercializar quaisquer de suas informações pessoais com terceiros, no entanto, às vezes usamos terceiros para processar suas informações (por exemplo, fornecedores de pagamento). Exigimos que estes terceiros cumpram estritamente às suas instruções e que não utilizem as suas informações pessoais para os seus próprios fins comerciais.</p>

                <p>O Logdialog.com.br não poderá enviar informações ou utilizar informações sobre o cliente
                quando solicitado, excepcionalmente nos casos: </p>

                <p>Tiver o seu consentimento para compartilhar tais informações; </p>

                <p>Em resposta a processos judiciais;</p>

                <p>Se alguma de suas atitudes possam ter violado os Termos Uso do Logdialog.com.br; </p>

                <p>Responder às solicitações que os usuários possam fazer ao serviço de atendimento do
                Logdialog.com.br em relação à sua conta; </p>

                <p>Para proteger direitos, propriedades, interesses, ou manter a segurança do Logdialog.com.br, dos usuários e do público em geral.</p>

                <p>O Logdialog utiliza informações não-identificáveis para gerar estatísticas, para melhoria das ferramentas utilizadas, para divulgação dos serviços prestados, para melhorar a experiência de uso e navegação cujo conteúdo não permite a identificação dos usuários.​​</p>

                <p>O Logdialog utiliza informações não-identificáveis sobre as avaliações por parte de usuários e profissionais cadastrados e pesquisas de satisfação para resultados clínicos (avaliações pessoais), para a qualidade das ferramentas utilizadas pelo Logdialog.com.br. Esses esforços melhoram a avaliação do programa e melhorias tecnológicas.</p>

                <p>Os dados anônimos e agregados também poderão ser publicados através de várias plataformas de mídia / periódicos acadêmicos. Nenhuma informação de identificação pessoal será vinculada aos resultados e o Logdialog não compartilhará nada que possa ser usado para identificar sua conta ou suas informações privadas.</p>

                <p>De tempos em tempos, podemos usar as informações do cliente para novos e imprevistos usos não divulgados anteriormente em nosso aviso de privacidade. Se nossas práticas de informação mudarem em algum momento no futuro, entraremos em contato com você antes de usar seus dados para esses novos fins para notificá-lo sobre a alteração de política e para fornecer a você a capacidade de optar por esses novos usos.</p>

                <p>Nós armazenamos dados somente enquanto houver necessidade de fornecer produtos e serviços para você e outros, incluindo aqueles descritos acima e para proteções legais ou conforme exigido pelas leis e regulamentos aplicáveis.</p>

                <p>Podemos permitir o acesso a informações públicas que foram compartilhadas por meio de nossos serviços.</p>

                <p>Podemos permitir que os provedores de serviços acessem informações para que possam nos ajudar a fornecer serviços.</p>

                <p>Certas informações são necessárias para fornecer serviços, portanto, excluímos apenas essas informações depois que você excluir sua conta.
                </p> 
                <p>O Logdialog.com.br permite que você altere as suas informações cadastrais a qualquer instante. </p>

                <p>Você pode solicitar a exclusão da sua conta. Por favor, envie um e-mail para contato@Logdialog.com.br ou nos envie uma correspondência em: Logdialog - Rua do Rócio, 423 - Conj. 1411, São Paulo, SP, 04705-010, Brazil</p>

                <p>O Logdialog só fará a exclusão sem autorização do cliente mediante o recebimento de cópia da certidão de óbito, e assim proceder ao término da conta e todo o conteúdo eventualmente, disponível será apagado definitivamente. </p>

                <p>Algumas formas de processamento (envio de informações promocionais, perfis comerciais, publicidade comportamental, geo-localização, etc.) podem exigir o consentimento expresso do Usuário. Informações específicas podem ser mostradas nas páginas do site em conexão com determinados serviços ou processamento de dados fornecidos pelo usuário do site.</p>

                <p>Para conteúdo coberto por direitos de propriedade intelectual, como fotos e vídeos, você concede especificamente à Logdialog.com.br uma licença mundial, não exclusiva, transferível, sub-licenciável, livre de royalties para usar qualquer conteúdo de IP que você postar ou em conexão com Logdialog.com.br (Licença IP). Essa Licença IP termina quando você exclui seu conteúdo de IP ou sua conta, a menos que seu conteúdo tenha sido compartilhado com outras pessoas e não a tenha excluído.</p>

                <p>Quando você exclui conteúdo IP, ele é excluído de forma semelhante ao esvaziamento da lixeira em um computador. No entanto, você entende que o conteúdo removido pode persistir em cópias de backup por um período de tempo razoável (mas não estará disponível para outros).</p>


                <h3>Protegendo a privacidade de seus usuários do Logdialog</h3>

                <p>Durante o uso dos serviços de Logdialog.com.br, você:</p>

                <p>Não enviará ou não publicará comunicações comerciais não autorizadas (como spam) no Logdialog.com.br.</p>

                <p>Não irá coletar o conteúdo dos usuários ou informações, ou acessar Logdialog.com.br, usando meios automatizados não permitidos (bots de coleta, robôs ou spiders) sem nossa permissão.</p>

                <p>Não enviará vírus ou outro código malicioso.</p>

                <p>Não solicitará informações de login ou acessar uma conta pertencente a outra pessoa.</p>

                <p>Não vai intimidar ou assediar qualquer outro usuário.</p>

                <p>Não postará conteúdo odioso, ameaçador ou pornográfico; que incita à violência; ou que contenha nudez ou violência gráfica ou gratuita.</p>

                <p>Não fornecerá nenhuma informação pessoal falsa no Logdialog.com.br, ou criará uma conta para qualquer pessoa que não seja você mesmo sem permissão. </p>

                <p>Não criará mais de um perfil pessoal.</p>


                <h3>Direitos Autorais </h3>

                <p>Esperamos que respeite os direitos autorais, portanto você não postará conteúdo ou fará qualquer ação no Logdialog.com.br que infrinja ou viole os direitos de outra pessoa ou que de alguma maneira viole a lei. </p>

                <p>Podemos remover qualquer conteúdo ou informação que você postar no Logdialog.com.br se acreditarmos que viola esta Política de Privacidade ou os Termos de Uso deste Site. </p>

                <p>Você não usará nossa marca, ou nossos direitos autorais sem nossa permissão por escrito. </p>

                <p>Se você compartilhar dados pessoais com terceiros através do Logdialog, você assume total responsabilidade e deixa claro, que você (e não Logdialog.com.br) é aquele que compartilhou as informações e: </p>

                <p>Você declara possuir o direito de comunicar ou divulgar dados pessoais a terceiros e de ter recebido informações prévias, dispensando o Site de toda a responsabilidade pelo seu uso impróprio após a divulgação ou divulgação.</p>

                <p>Você não postará documentos de identificação de ninguém ou informações financeiras confidenciais no Logdialog.com.br. </p>

                <p>Você não marcará os usuários nem enviará convites por e-mail a não-usuários sem seu consentimento.</p>


                <h3>Dispositivos Eletrônicos Portáteis</h3>

                <p>A Logdialog.com.br está disponível em uma infinidade de dispositivos eletrônicos portáteis. </p>

                <p>Nós fornecemos nossa conexão a serviços móveis, mas lembre-se de que as taxas das operadoras de telefonia e internet ainda podem ser aplicadas. </p>

                <p>Você fornece todos os direitos necessários para permitir que os usuários sincronizem (inclusive por meio de um aplicativo) suas listas de contatos com quaisquer informações básicas e informações de contato que sejam visíveis no Logdialog.com.br, bem como seu nome e imagem de perfil.</p>


                <h3>Disputas</h3>

                <p>Você resolverá qualquer reclamação, causa de ação ou disputa (reclamação) que tenha conosco decorrentes ou relacionados a esta declaração ou Logdialog.com.br exclusivamente em um tribunal estadual ou federal localizado em São Paulo. </p>

                <p>As leis do Brasileiras regerão esta declaração, bem como qualquer reclamação que possa surgir entre você e nós.</p>

                <p>Se alguém apresentar uma reclamação contra nós relacionada às suas ações ou conteúdo, você o indenizará e nos manterá inofensivo contra todos os danos, perdas e despesas de qualquer tipo (incluindo honorários e custos legais razoáveis) vinculados a tal reivindicação.</p>


                <h3>Redes Sociais e Logdialog</h3>

                <p>Esses serviços permitem que o Logdialog.com.br acesse os dados do seu perfil nas redes sociais e interaja por meio de sua postagem. Estes serviços não são activados automaticamente e requerem autorização expressa do utilizador.</p>

                <p>Os usuários do Logdialog.com.br podem compartilhar informações e dados fornecidos, com as redes sociais em que está registrado, mas para isso deve aceitar a política de privacidade das redes sociais correspondentes. </p>

                <p>Postagens realizadas por pacientes nos perfis das redes sociais do Logdialog não serão consideradas informações confidenciais. </p>


                <h3>Pagamento</h3>

                <p>Quando você nos fornece as suas informações pessoais para completar uma transação ou verificar seu cartão de crédito, entendemos que você concorda com a nossa coleta e ouso desses dados será apenas para aquele motivo específico.</p>


                <h3>Serviços de terceiros</h3>

                <p>No geral, os serviços terceirizados usados por nós irão apenas coletar, usar e divulgar suas informações na medida do necessário para permitir que eles realizem os serviços que eles nos fornecem.</p>

                <p>Entretanto, certos fornecedores de serviços terceirizados, tais como gateways de pagamento e outros processadores de transação de pagamento, têm suas próprias políticas de privacidade com respeito à informação que somos obrigados a fornecer para eles de suas transações relacionadas com compras.</p>

                <p>Para esses fornecedores, recomendamos que você leia suas políticas de privacidade para que você possa entender de que maneira suas informações pessoais serão usadas por esses fornecedores.</p>

                <p>Em particular, lembre-se que certos fornecedores podem estar localizados em jurisdições diferentes que você ou nós. Assim, se você quer continuar com uma transação que envolve os serviços de um fornecedor de serviço terceirizado, então suas informações podem tornar-se sujeitas às leis da(s) jurisdição(ões) nas quais o fornecedor de serviço ou suas instalações estão localizados.</p>


                <h3>Segurança</h3>

                <p>Para proteger suas informações pessoais, tomamos precauções razoáveis e seguimos as melhores práticas da indústria para nos certificar que elas não serão perdidas inadequadamente, usurpadas, acessadas, divulgadas, alteradas ou destruídas.</p>

                <p>Embora nenhum método de transmissão pela Internet ou armazenamento eletrônico seja 100% seguro, nós seguimos todos os requisitos e implementamos padrões adicionais geralmente aceitos pela indústria.</p>


                <h3>Confidencialidade das informações na Orientação Psicológica</h3>

                <p>Logdialog.com.br tem um compromisso com a confidencialidade. As informações fornecidas e armazenadas são protegidas por sistema de criptografia que codifica as informações de forma que só o emissor e o receptor consigam decifrá-las</p>

                <p>As únicas informações pessoais que serão enviadas para o psicólogo são o nome completo do paciente e a data de nascimento, para a correta identificação. </p>

                <p>No caso de orientação para menores, também será disponibilizado para o profissional, o nome completo, do responsável e o documento de autorização prenchi, disponivel no logdialog.com.br e preenchido pelo responsavel.</p>

                <p>Informações complementares que o psicólogo julgar necessárias, poderão ser solicitadas pelo profissional durante a sessão. </p>

                <p>O cliente fica ciente e concorda que qualquer informação, dado, texto, vídeo,
                mensagem ou qualquer outro material transmitido através do atendimento com o psicólogo será de total responsabilidade do cliente, pois o Logdialog.com.br não irá controlar e ter acesso em hipótese alguma sobre essas informações. </p>

                <p>Todas as informações sobre o paciente que o psicólogo armazenar em prontuário, também então protegidas por criptografia.</p>

                <p>É importante ressaltar que os psicólogos geralmente são obrigados a notificar as suspeitas de crimes de qualquer instância, incluindo suspeita de maus-tratos e violência sexual contra crianças ou adultos dependentes. </p>

                <p>Além disso, o psicólogo poderá quebrar sigilo quando o usuário está em condições mentais ou emocionais, como ser um perigo para si próprio ou aos outros. </p>

                <p>De forma geral, o psicólogo deverá respeitar a legislação atual e seguir as orientações contidas no código de ética do Conselho Federal de Psicologia. </p>


                <h3>Registros documentais e Prontuário Eletrônico</h3>

                <p>Pensando nas melhores práticas de atendimento, o Logdialog desenvolveu um prontuário eletrônico que atende às exigências do Conselho Federal de Psicologia. </p>

                <p>Todas as informações trocadas entre psicólogos e pacientes são confidenciais e não são divulgadas. Informações armazenadas em prontuário são criptografadas e mantidas em "arquivo" por 5 anos para consulta em caso de disputa legal por ordem judicial de acordo com a resolução Resolução CFP Nº 001/2009.</p>

                <p>A retenção de arquivos protege a segurança do usuário e dos terapeutas. Os dados serão protegidos em altos níveis de segurança e privacidade, e se o usuário usar um pseudônimo, não incluirá seu nome real nos registros de conversa.</p>

                <p>Dados perdidos / expostos / usados ​​devido à natureza da Internet e ambientes digitais, ações ilegais de hackers e criminosos, falhas técnicas de servidores e banco de dados etc, não serão de responsabilidade do Logdialog.com.br </p>

                <p>Para garantir confidencialidade e segurança para o psicólogo e para o paciente, uma vez salva, as informações armazenadas em prontuário não poderão ser alteradas.</p>

                <p>Estas são as condições aceitas pelo usuário do Logdialog.com.br e usuários da Internet em geral. Você está concordando com estas condições ao usar nosso site.</p>


                <h3>Perfil do profissional e do paciente </h3>

                <p>O cliente do Logdialog.com.br deverá criar uma senha individual, confidencial e intransferível para acesso ao site, bem como para efetuar alterações nos dados cadastrais. </p>

                <p>É responsabilidade do cliente não permitir que sua conta seja acessada por outras pessoas.</p>

                <p>Em caso de suspeita de fraude ou uso não autorizado, o cliente deve comunicar imediatamente o Logdialog.com.br através do e-mail atendimento@logdialog.com.br.</p>

                <p>O logdialog não se responsabiliza pelas ações, conteúdos, informações ou dados de terceiros. De igual modo nossos diretores, funcionários e agentes também estão eximidos de quaisquer reclamações e danos, conhecidos e desconhecidos, decorrentes de qualquer reclamação contra terceiros.</p>


                <h3>O uso de cookies</h3>

                <p>Um cookie é uma pequena quantidade de dados, que muitas vezes inclui um identificador exclusivo que é enviado e armazenado no disco rígido do dispositivo para registram informações sobre suas preferências e que nos permitem adaptar Logdialog aos seus interesses. </p>

                <p>As informações fornecidas pelos cookies podem ajudar-nos a analisar o perfil dos nossos visitantes para que possamos proporcionar-lhe uma melhor experiência ao utilizar nosso site.</p>



                <h3>Idade e consentimento </h3>

                <p>Ao usar este site, você afirma que tem 18 anos ou mais, ou que nos deu seu consentimento para permitir que seu dependentes menor de idade use esse site.</p>


                <h3>Alterações da política de privacidade</h3>

                <p>Reservamos o direito de modificar essa política de privacidade a qualquer momento, então por favor, revise-a com frequência. Alterações e esclarecimentos vão surtir efeito imediatamente após sua publicação no site. Se fizermos alterações em nossa política, iremos notificá-lo sobre tais alterações, para que você sempre tenha ciência sobre quais informações coletamos, como e sob que circunstâncias poderão ser usadas,  ou divulgadas.</p>

            </div>
        </div>
    </div>
@stop
{{-- fim do conteúdo do yield atual