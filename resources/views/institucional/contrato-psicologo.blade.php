carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>CONTRATO COM O PSICÓLOGO</h4>
            </div>
            <div class="pages-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et nisl id elit accumsan pretium eget varius sapien. Praesent auctor, lorem sit amet venenatis porta, lacus turpis venenatis augue, id imperdiet massa risus eget magna. Quisque ultrices quis metus vel venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent non malesuada est. Nunc aliquam turpis quam, et viverra dolor dignissim mollis. Cras vehicula est at libero pharetra, in efficitur ex dapibus. Vestibulum nulla nibh, elementum sed auctor ut, mollis et urna.
                </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et nisl id elit accumsan pretium eget varius sapien. Praesent auctor, lorem sit amet venenatis porta, lacus turpis venenatis augue, id imperdiet massa risus eget magna. Quisque ultrices quis metus vel venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent non malesuada est. Nunc aliquam turpis quam, et viverra dolor dignissim mollis. Cras vehicula est at libero pharetra, in efficitur ex dapibus. Vestibulum nulla nibh, elementum sed auctor ut, mollis et urna.
                </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et nisl id elit accumsan pretium eget varius sapien. Praesent auctor, lorem sit amet venenatis porta, lacus turpis venenatis augue, id imperdiet massa risus eget magna. Quisque ultrices quis metus vel venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent non malesuada est. Nunc aliquam turpis quam, et viverra dolor dignissim mollis. Cras vehicula est at libero pharetra, in efficitur ex dapibus. Vestibulum nulla nibh, elementum sed auctor ut, mollis et urna.
                </p>

                <h3>Lorem Ipsum Dolor</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et nisl id elit accumsan pretium eget varius sapien. Praesent auctor, lorem sit amet venenatis porta, lacus turpis venenatis augue, id imperdiet massa risus eget magna. Quisque ultrices quis metus vel venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent non malesuada est. Nunc aliquam turpis quam, et viverra dolor dignissim mollis. Cras vehicula est at libero pharetra, in efficitur ex dapibus. Vestibulum nulla nibh, elementum sed auctor ut, mollis et urna.
                </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean et nisl id elit accumsan pretium eget varius sapien. Praesent auctor, lorem sit amet venenatis porta, lacus turpis venenatis augue, id imperdiet massa risus eget magna. Quisque ultrices quis metus vel venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent non malesuada est. Nunc aliquam turpis quam, et viverra dolor dignissim mollis. Cras vehicula est at libero pharetra, in efficitur ex dapibus. Vestibulum nulla nibh, elementum sed auctor ut, mollis et urna.
                </p>
            </div>
        </div>
    </div>
@stop
{{-- fim do conteúdo do yield atual