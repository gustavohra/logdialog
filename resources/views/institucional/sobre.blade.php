{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>SOBRE A EMPRESA</h4>
            </div>
            <div class="pages-content">
                <p>O Logdialog surgiu da tendência do uso da tecnologia para realizar atendimento psicológico, o que atualmente é regulamentado e sustentado por pesquisas científicas. Nesse cenário, um grupo de profissionais clínicos, especialistas, cientistas e empreendedores de diferentes áreas como psicologia, medicina, administração e tecnologia, uniram-se e desenvolveram uma solução através da qual psicólogos e pacientes podem oferecer e receber serviços de saúde mental de qualidade.</p>
                <h3 class="title-titlecase">Sendo assim nossa missão é: </h3>
                <ul>
                    <li>Você melhor a cada dia. </li>
                </ul>

                <h3 class="title-titlecase">Nossa visão é: </h3>
                <ul>
                    <li>Transformar a saúde mental de qualidade em um serviço disponível para todos e, através do uso de tecnologia, gerar resultados significantes para o bem-estar de toda a população.</li>
                </ul>

                <h3 class="title-titlecase">Na Logdialog trabalhamos seguindo alguns valores importantes:</h3>
                <ul>
                    <li>Trabalhar com rigor ético e transparência. </li>
                    <li>Assegurar a confidencialidade e privacidade de nossos clientes.</li>
                    <li>Disponibilizar atendimento profissional de alta qualidade. </li>
                    <li>Oferecer praticidade e comodidade para nossos clientes.</li>
                    <li>Resolver problemas com agilidade e eficiência.</li>
                </ul>
            </div>
        </div>
    </div>
@stop
{{-- fim do conteúdo do yield atual --}}