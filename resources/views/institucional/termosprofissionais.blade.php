{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>É PSICÓLOGO E DESEJA SE CADASTRAR?</h4>
            </div>
            <div class="pages-content">
                <p>O Logdialog é uma empresa de software que fornece uma plataforma digital e pode ser acessada através de dispositivos móveis, ou outros meios digitais, por psicólogos e pacientes. </p>
                <p>Se você é psicólogo e está inscrito no seu Conselho Regional de Psicologia - CRP, será simples realizar atendimento online (através do Logdialog).
                Basta fazer seu cadastro que (e) entraremos em contato. </p>
                <p>No Logdialog você:</p>
                <ul>
                    <li>Define o valor da sua consulta.</li>
                    <li>Disponibiliza os horário que deseja atender.</li>
                    <li>Realiza atendimento por videoconferência.</li>
                    <li>Registra o atendimento em prontuário, e nós o guardamos com segurança para você. </li>
                    <li>Recebe o pagamento na sua conta sem se preocupar em fazer cobrança ou inadimplência.</li>
                </ul>


            </div>
        </div>
    </div>
@stop
{{-- fim do conteúdo do yield atual --}}