carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando banners --}}
    @include('layouts.logdialog.banners')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    <div class="pages-wrap">
        <div class="container">
            <div class="pages-title"> 
                <h4>POLÍTICA DE PAGAMENTO E REEMBOLSO</h4>
            </div>
            <div class="pages-content pages-contrato">

                <h3 style="margin-top:0;">01.CANCELAMENTO E DEVOLUÇÃO DO VALOR PAGO</h3>
                <p>O pagamento da orientação psicológica é feito antecipadamente ao início da sessão, onde cada crédito equivale a uma sessão. Caso o USUÁRIO desista, ele poderá solicitar a devolução dos valores até 24 horas antes do atendimento, caso contrário, o valor não será devolvido. Será descontado do valor a ser devolvido, todas as taxas que foram cobradas na transação financeira do PayPal. Caso queira solicitar a devolução dos valores pagos, por favor, envie um e-mail com a solicitação para <a href="mailto:logdialog@logdialog.com.br">logdialog@logdialog.com.br.</a></p>
                <p class="notes" style="margin-top: 0;">OBSERVAÇÃO: No caso de alguma ocorrência de problemas técnicos do provedor Logdialog.com.br que prejudiquem a qualidade ou atéa mesmo a impossibilidade da realização do atendimento, a sessão será reagendada sem nenhum ônus para o USUÁRIO.</p>

                <h3>02.NÃO COMPARECIMENTO DO CLIENTE</h3>
                <p>Caso o USUÁRIO faça o agendamento com o psicólogo e não compareça no horário marcado, o crédito será debitado da conta do USUÁRIO e o valor da sessão será repassado ao psicólogo que disponibilizou a hora para o agendamento e ficou aguardando os 50 minutos no sistema. Lembramos que o USUÁRIO poderá cancelar o agendamento sem nenhum ônus 24 horas antes do atendimento.</p>

                <h3>03.NÃO COMPARECIMENTO DO PSICÓLOGO</h3>
                <p>No caso do não comparecimento do Psicólogo na sessão, o crédito será devolvido e poderá ser usado novamente para o agendamento de uma nova sessão. Neste caso pedimos que nos envie um e-mail e indique o ocorrido para entrarmos em contato com o psicólogo e verificar o motivo do não comparecimento.</p>

                <h3>04.MUDANÇA NO HORÁRIO DA SESSÃO</h3>
                <p>O USUÁRIO poderá mudar o horário da sessão sem nenhum custo até 24 horas antes do início da sessão marcada. Caso precise remarcar, é necessário cancelar o horário até 24 horas antes do início da sessão e em logo em seguida remarcar a sessão no novo horário escolhido.</p>

                <h3>05.AGENDAMENTO DA SESSÃO</h3>
                <p>Imediatamente, após o USUÁRIO solicitar uma sessão com o profissional escolhido, o psicólogo irá receber uma mensagem através do sistema onde deverá confirmar ou não este atendimento.</p>

                <h3>2. OS PSICÓLOGOS CADASTRADOS</h3>
                <p>O site Logdialog.com.br efetiva apenas o cadastro dos psicólogos devidamente registrados e autorizados pelo Conselho Federal de Psicologia a realizar atendimento psicológico e que estejam devidamente em dia com suas obrigações éticas e legais. Os dados fornecidos pelo psicólogo são verificados e a cada 30 dias será averiguado se houve alguma ocorrência, problema disciplinar ou até mesmo a perda do registro junto ao Conselho Federal de Psicologia. Não serão aceitos em hipótese alguma profissionais que não possuem a graduação concluída no curso de Psicologia.</p>
                 
                <h3>3. RESPONSABILIDADE DO Logdialog SOBRE O ATENDIMENTO REALIZADO PELO PSICÓLOGO</h3>
                 
                <p><span class="contrato-topico">3.1</span> O site Logdialog.com.br não se responsabiliza pelo conteúdo da troca de informações realizada no atendimento entre o psicólogo e o USUÁRIO. Tanto o direcionamento como a abordagem que é utilizada na orientação psicológica é de total responsabilidade do psicólogo que deve, obrigatoriamente, seguir o Código de Ética definido pelo Conselho Federal de Psicologia. Na ocorrência de má conduta ou falta grave pelo psicólogo cadastrado no Logdialog.com.br junto ao USUÁRIO, solicitamos que seja nos comunicado imediatamente para que as providências cabíveis sejam tomadas.</p>
                 
                <p><span class="contrato-topico">3.2</span> O site Logdialog.com.br não endossa ou recomenda qualquer profissional cadastrado no site, sendo que a escolha é feita de forma livre pelo USUÁRIO que irá utilizar o serviço.</p>
                 
                <p><span class="contrato-topico">3.3</span> É importante ressaltar que o Logdialog.com.br apenas disponibiliza e oferecer a utilização do espaço e das ferramentas para todos os USUÁRIOs que buscam uma orientação psicológica online realizando a intermediação entre o profissional psicólogo e seu USUÁRIO.</p>
                 
                <p><span class="contrato-topico">3.4</span> Como as sessões não são monitoradas, não temos a obrigação e nem o direito de dirigir os trabalhos do psicólogo, cabendo ao USUÁRIO não continuar com a sessão caso não fique satisfeito ou se julgue prejudicado, podendo optar por outro psicólogo nas próximas sessões.</p>
                 
                <p><span class="contrato-topico">3.5</span> No caso do USUÁRIO e o psicólogo optarem pelo atendimento presencial em consultório, o site Logdialog.com.br ficará isento de qualquer responsabilidade pela negociação bem como todo o acerto do contrato deste atendimento.</p>
                 
                <h3>4. VALOR DOS SERVIÇOS</h3>
                 
                <p><span class="contrato-topico">4.1</span> CHAT OU VIDEOCONFERÊNCIA: O valor minimo de atendimento no chat ou videoconferência disponibilizado em nosso site é de R$ 90,00 e a duração é de 50 minutos.</p>
                 
                <p><span class="contrato-topico">4.2</span> O site Logdialog.com.br é responsável em efetuar o pagamento dos valores pagos pelo USUÁRIO ao psicólogo.</p>
                 
                <p><span class="contrato-topico">4.3</span> O crédito só será liberado mediante confirmação de pagamento pelo sistema do PayPal.</p>
                 
                <h3>5. CANCELAMENTO E DEVOLUÇÃO DO VALOR PAGO</h3>
                 
                <p><span class="contrato-topico">5.1</span> O pagamento da orientação psicológica é feito antecipadamente ao início da sessão, onde cada crédito equivale a uma sessão. Caso o USUÁRIO desista, ele poderá solicitar a devolução dos valores até 24 horas antes do atendimento, caso contrário, o valor não será devolvido. Será descontado do valor a ser devolvido, todas as taxas que foram cobradas na transação financeira do PayPal. Caso queira solicitar a devolução dos valores pagos, por favor, envie um e-mail com a solicitação para logdialog@logdialog.com.br.
                 
                <p><span class="contrato-topico">5.2</span> NÃO COMPARECIMENTO DO CLIENTE: Caso o USUÁRIO faça o agendamento com o psicólogo e não compareça no horário marcado, o crédito será debitado da conta do USUÁRIO e o valor da sessão será repassado ao psicólogo que disponibilizou a hora para o agendamento e ficou aguardando os 50 minutos no sistema. Lembramos que o USUÁRIO poderá cancelar o agendamento sem nenhum ônus 3 horas antes do atendimento.
                 
                <p><span class="contrato-topico">5.3</span> NÃO COMPARECIMENTO DO PSICÓLOGO: No caso do não comparecimento do Psicólogo na sessão, o crédito será devolvido e poderá ser usado novamente para o agendamento de uma nova sessão. Neste caso pedimos que nos envie um e-mail e indique o ocorrido para entrarmos em contato com o psicólogo e verificar o motivo do não comparecimento.
                 
                <h3>6.MUDANÇA NO HORÁRIO DA SESSÃO</h3> 
                <p>O USUÁRIO poderá mudar o horário da sessão sem nenhum custo até 3 horas antes do início da sessão marcada. Caso precise remarcar, é necessário cancelar o horário até 3 horas antes do início da sessão e em logo em seguida remarcar a sessão no novo horário escolhido.</p>
                 
                <h3>7. FORMA DE PAGAMENTO</h3> 
                <p>As nossas operações financeiras são realizadas pela empresa PayPal. Para mais informações, por favor, clique no site do PayPal</p>
                 
                <h3>8. AGENDAMENTO DA SESSÃO</h3>
                <p>Imediatamente, após o USUÁRIO solicitar uma sessão com o profissional escolhido, o psicólogo irá receber uma mensagem através do sistema onde deverá confirmar ou não este atendimento. A sessão irá ocorrer independentemente do confirmação ou não do psicólogo. Caso ocorra o não comparecimento do psicólogo o crédito será estornado ao USUÁRIO.</p>
                 
                <h3>10. RESTRIÇÕES</h3> 
                <p><span class="contrato-topico">10.1</span> O Logdialog.com.br só aceitará o cadastramento de USUÁRIOs maiores de 18 anos, impreterivelmente.</p>
                 
                <p><span class="contrato-topico">10.2</span> Antes da compra do crédito para agendar a sessão, o USUÁRIO deverá confirmar que está ciente que o atendimento não poderá acontecer caso esteja em alguma das seguintes condições:</p>
                <ul>
                    <li>o USUÁRIO tiver vontade de se matar ou ter pensamentos suicida</li>
                    <li>o USUÁRIO tiver algum distúrbio psicológico grave, e estar sob forte medicação</li>
                    <li>o USUÁRIO ter utilizado, antes do atendimento, alguma droga ilícita ou estar alcoolizado, prejudicando assim a sua atenção, percepção e o raciocínio</li>
                    <li>o USUÁRIO não falar fluentemente o idioma português</li>
                    <li>o USUÁRIO estiver em crise ou descontrole emocional</li>
                </ul>
                 
                <h3>13. AVISO</h3>
                <p>Os Termos do Serviço constituem o acordo único e integral entre o USUÁRIO e o Logdialog.com.br, sendo que o relacionamento entre ambas as partes será regido pelas leis da República Federativa do Brasil. O USUÁRIO e o Logdialog.com.br concordam em submeter-se à competência única e exclusiva dos tribunais localizados no Brasil.</p>
                 
                <h3>14. ACEITE</h3>
                <p>Ao clicar em "Eu Aceito" e utilizar qualquer serviço do Logdialog.com.br, o USUÁRIO indica que leu e concordou, mesmo que tacitamente, com a versão mais recente dos Termos do Serviço, vinculando-se automaticamente às regras aqui contidas. </p>
            </div>
        </div>
    </div>
@stop
