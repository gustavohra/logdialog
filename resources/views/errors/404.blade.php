{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
    {{-- carregando menus --}}
    @include('layouts.logdialog.menus')
    <div class="clearfix"></div>
    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

    @yield('nav-menu-principal')
    @yield('banner-top-home')
 
    <section id="pergunta" class="home-section" style="margin:15vh 0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12"><div class="col-md-8"></div>
                    <div class="section-title">
                        <h1>404 / Página não encontrada</h1>
                        <p class="subtitle">Por favor, verifique o link digitado e tente novamente.</p> 
                    </div>{{-- /.section-title --}} 
                </div>
            </div>
        </div>
    </section>
  
@stop
{{-- fim do conteúdo do yield atual --}}