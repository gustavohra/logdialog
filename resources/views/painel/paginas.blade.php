
    <script type='text/ng-template' id='paginas.html'> 
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>Páginas</h3>
                            </div>

                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar..." ng-model="filtro">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="row">
                                        <button class="btn btn-success pull-right" ui-sref="paginaAdicionar()"><i class="fa fa-plus"></i> Adicionar Página</button>
                                    </div>
                                    <div class="x_content">
    
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action" ng-if="dados != null">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <input type="checkbox" id="check-all" class="flat">
                                                        </th>
                                                        <th class="column-title">Título </th>
                                                        <th class="column-title">Data </th> 
                                                        <th class="column-title">URL </th> 
                                                        <th class="column-title no-link last"><span class="nobr">Ações</span>
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr class="even pointer" dir-paginate="item in dados | itemsPerPage: 5 | filter:{titulo: filtro}" pagination-id="listagem" valign="middle">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" ng-model="selecao[$index]">
                                                        </td>
                                                        <td class=""><a ui-sref="pagina({id: item.id})">{{item.titulo}}</a></td>
                                                        <td class="">{{item.data_cadastro | formatarData}}</td>
                                                        <td class="">

                                                            <div class="form-group">
                                                              <div class="input-group">
                                                                <input type="text" class="form-control" ng-model="item.url" style="height:37px">
                                                                <span class="input-group-btn" uib-tooltip="Alterar URL da página" tooltip-position="bottom">
                                                                  <button class="btn btn-default" type="button" ng-click="alterarUrl(item)"><i class="fa fa-check"></i></button>
                                                                </span>
                                                              </div>
                                                            </div>

                                                        </td>
                                                        <td class="last" valign="middle">
                                                            <a ui-sref="pagina({id: item.id})"><i class="fa fa-pencil-square-o" aria-hidden="true" uib-tooltip="Editar página"></i></a>
                                                            <a href="" ng-click="excluir(item, $index)"><i class="fa fa-trash-o" aria-hidden="true" uib-tooltip="Excluir página"></i></a>
                                                            <a target="_blank" ng-href="<% asset('/') %>institucional/{{item.url}}"><i class="fa fa-globe" aria-hidden="true" uib-tooltip="Visualizar página"></i></a>
                                                        </td>
                                                    </tr> 
                                                </tbody>
                                            </table>
<!--                                             <label>
                                                <select class="form-control input-sm" ng-model="acao">
                                                    <option value="">Ações em massa</option> 
                                                    <option value="remove">Excluir</option>
                                                </select>
                                                <button type="button" class="btn btn-default btn-admin" ng-click="realizarAcao()">Aplicar</button>
                                            </label> -->
                                        </div>
                                                    <div class="alert alert-dismissible alert-info" ng-if="dados == null">
                                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                                              Nenhum registro encontrado.
                                            </div>
                                            <dir-pagination-controls pagination-id="listagem" class="pull-right"></dir-pagination-controls>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.row -->
                    </div>
                </script>