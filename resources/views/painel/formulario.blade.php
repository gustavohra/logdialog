	<script type='text/ng-template' id='formulario.html'> 

			<div class="row">
				<div class="col-md-12">
					<div class="alerta alerta2">
						<small class="esquerda" ng-click="$root.voltar()">< Voltar para a listagem</small> 
					<br clear="all" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br />
		<div class="formulario">
		<!-- Envio de imagem -->
		<div class="row" ng-if="vm.envioDeImagem">
			<div class="col-lg-6 col-md-6 col-xs-12 box-imagem">
				<img ng-src="{{vm.model.imagem}}" class="img-responsive" ng-if="vm.model.imagem != '' && vm.model.imagem != null" />
			</div>
			<br />
			<div class="clearfix"></div>
			<div class="col-lg-6 col-md-6 col-xs-12">
				<button  ngf-select="uploadImagem($file)"   ngf-pattern="'image/*'" accept="'image/*'" ng-model="vm.model.imagem" ngf-max-size="20MB" ngf-min-height="10" class="btn btn-primary" uib-tooltip="Clique aqui para anexar uma imagem" tooltip-placement="bottom" tooltip-trigger="mouseenter"><i class="fa fa-file-archive-o"></i> Anexar imagem</button>
			</div>
		</div>
		<!-- Envio de imagem -->
	<div class="clearfix"></div>
	<br />
	<div class="row-eq-height">
		<div class="col-md-12  no-padding">
			<div class="formulariocontainer-fluid"> 
					<div class"row"> 
					    <form ng-submit="enviar()" validate name="formulario" > 
							<formly-form model="vm.model" fields="vm.campos"></formly-form> 
				 		</form>
				</div>
				</div>

		</div>
	</div>
</div>
	<div class="clearfix"></div>
	<br />
	<button class="btn btn-success btn100 pull-right" ng-click="enviar(this.formulario)"><i class="ion-checkmark"></i> Enviar formulário</button>
	</script>
