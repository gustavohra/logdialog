	<script type='text/ng-template' id='especialidade.html'> 

			<div class="row">
				<div class="col-md-12">
					<div class="alerta alerta2">
						<small class="esquerda" ng-click="$root.voltar()">< Voltar para a listagem</small> 
					<br clear="all" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br />
		<div class="formulario">
			<div class="row">
				<div class="col-md-12">
					<h2><b>Especialidade:</b> {{vm.model.especialidade}}</h2>
					<a href="<% asset('/') %>/public/upload/user/{{vm.model.profissional.id}}/{{vm.model.documento}}" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> Documentação da especialidade</a><br />
					<small><b>Enviado por:</b> {{vm.model.profissional.nome}}</small>
				</div>
			</div>
			<br />
			<h5>Outros documentos do Profissional:</h5>
			<div class="row">
			<div class="col-md-12">
				<table width="100%" class="table table-striped table-hover ">
					<thead>
				      <th>Documento</th>
				      <th>Link</th>
				      </thead>
				  <tbody>
				    <tr>
				      <td>CRP</td>
				      <td>
				      	<a ng-href="<% asset('/') %>/public/upload/user/{{vm.model.profissional.id}}/{{vm.model.profissional.documento_crp}}" target="_blank" ng-if="vm.model.profissional.documento_crp != null">{{vm.model.profissional.documento_crp}}</a>
				      	<span ng-if="vm.model.profissional.documento_crp == null">Não enviado</span>
				      </td>
				    </tr>
				    <tr>
				      <td>Diploma</td>
				      <td>
				      	<a ng-href="<% asset('/') %>/public/upload/user/{{vm.model.profissional.id}}/{{vm.model.profissional.documento_diploma}}" target="_blank" ng-if="vm.model.profissional.documento_diploma != null">{{vm.model.profissional.documento_diploma}}</a>
				      	<span ng-if="vm.model.profissional.documento_diploma == null">Não enviado</span>
				      </td>
				    </tr>
				    <tr>
				      <td>Identidade</td>
				      <td>
				      	<a ng-href="<% asset('/') %>/public/upload/user/{{vm.model.profissional.id}}/{{vm.model.profissional.documento_identidade}}" target="_blank" ng-if="vm.model.profissional.documento_identidade != null">{{vm.model.profissional.documento_identidade}}</a>
				      	<span ng-if="vm.model.profissional.documento_identidade == null">Não enviado</span>
				      </td>
				    </tr>
				  </tbody>
				</table>
			</div>
			</div>

		</div>
	<div class="clearfix"></div>
	<br />
	<button ng-if="vm.model.data_aprovado == null || vm.model.data_aprovado == ''" class="btn btn-success btn100 pull-right" ng-click="desbloquear(vm.model)"><i class="ion-checkmark"></i> Aprovar especialidade</button> 
	</script>
