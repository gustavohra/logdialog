
    <script type='text/ng-template' id='especialidades.html'> 
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>Especializações Solicitadas</h3>
                            </div>

                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Buscar..." ng-model="filtro">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel"> 
                                    <div class="x_content">
    
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action" ng-if="dados != null">
                                                <thead>
                                                    <tr class="headings">
                                                        <th>
                                                            <input type="checkbox" id="check-all" class="flat">
                                                        </th>
                                                        <th class="column-title">Especialização Solicitada </th>
                                                        <th class="column-title">Solicitante</th>
                                                        <th class="column-title">Data do pedido</th> 
                                                        <th class="column-title">Status</th> 
                                                        <th class="column-title no-link last"><span class="nobr">Ações</span>
                                                        </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr class="even pointer" dir-paginate="item in dados | itemsPerPage: 5 | filter:{titulo: filtro}" pagination-id="listagem">
                                                        <td class="a-center ">
                                                            <input type="checkbox" class="flat" ng-model="selecao[$index]">
                                                        </td>
                                                        <td class=""> {{item.especialidade}}</td>
                                                        <td class=""> {{item.profissional}} </td>
                                                        <td class="">{{item.data_inserido | formatarData}}</td>
                                                        <td>{{item.data_aprovado != null ? 'Aprovado' : 'Não aprovado'}}</td>
                                                        <td class="last"> 
                                                            <a ng-if="item.data_aprovado == null" ui-sref="especialidade({id: item.id})"><i class="fa fa-unlock" aria-hidden="true" uib-tooltip="Aprovar especialidade"></i></a>
                                                            <a ng-if="item.data_aprovado != null" ng-click="bloquear(item, $index)"><i class="fa fa-lock" aria-hidden="true" uib-tooltip="Bloquear especialidade"></i></a>
                                                            <a href="" ng-click="excluir(item, $index)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr> 
                                                </tbody>
                                            </table>
<!--                                             <label>
                                                <select class="form-control input-sm" ng-model="acao">
                                                    <option value="">Ações em massa</option> 
                                                    <option value="remove">Excluir</option>
                                                </select>
                                                <button type="button" class="btn btn-default btn-admin" ng-click="realizarAcao()">Aplicar</button>
                                            </label> -->
                                        </div>
                                                    <div class="alert alert-dismissible alert-info" ng-if="dados == null">
                                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                                              Nenhum registro encontrado.
                                            </div>
                                            <dir-pagination-controls pagination-id="listagem" class="pull-right"></dir-pagination-controls>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.row -->
                    </div>
                </script>