	<script type='text/ng-template' id='paciente.html'> 

			<div class="row">
				<div class="col-md-12">
					<div class="alerta alerta2">
						<small class="esquerda" ng-click="$root.voltar()">< Voltar para a listagem</small> 
					<br clear="all" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br />
		<div class="formulario">
			<div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12">
					<h4>Nome do Paciente</h4>
					<span>{{vm.model.nome}} {{vm.model.sobrenome}}</span> 
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<h4>Sexo</h4>
					<span>{{vm.model.sexo | validarInformacao}}</span> 
				</div>
				<div class="col-lg-2 col-md-2 col-xs-12">
					<h4>CPF</h4>
					<span>{{(vm.model.cpf | validarInformacao)  }}</span> 
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-lg-4 col-md-6 col-xs-12">
					<h4>Cidade</h4>
					<span>{{vm.model.cidade | validarInformacao}}</span>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<h4>Estado</h4>
					<span>{{vm.model.estado | validarInformacao}}</span>
				</div>
				<div class="col-lg-2 col-md-2 col-xs-12">
					<h4>CEP</h4>
					<span>{{vm.model.cep | validarInformacao}}</span>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-lg-6 col-md-6 col-xs-12">
					<h4>Endereço</h4>
					<span>{{vm.model.endereco | validarInformacao}}</span>
				</div>
				<div class="col-lg-2 col-md-2 col-xs-12">
					<h4>Nº</h4>
					<span>{{vm.model.numero | validarInformacao}}</span>
				</div>
				<div class="col-lg-4 col-md-2 col-xs-12">
					<h4>Bairro</h4>
					<span>{{vm.model.bairro | validarInformacao}}</span>
				</div>
			</div>
		</div>
	<div class="clearfix"></div>
	<br />
	<!-- <button class="btn btn-success btn100 pull-right" ng-click="enviar(this.formulario)"><i class="ion-checkmark"></i> Enviar formulário</button> -->
	</script>
