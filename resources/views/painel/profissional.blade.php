	<script type='text/ng-template' id='profissional.html'> 

			<div class="row">
				<div class="col-md-12">
					<div class="alerta alerta2">
						<small class="esquerda" ng-click="$root.voltar()">< Voltar para a listagem</small> 
					<br clear="all" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br />
		<div class="formulario">
			<div class="row">
				<div class="col-md-12">
					<h2>{{vm.model.nome}} {{vm.model.sobrenome}}</h2>
					<small><b>CRP:</b> {{vm.model.numero_crp}}</small>
				</div>
			</div>
			<br />
			<h5>Documentos do Profissional:</h5>
			<div class="row">
			<div class="col-md-12">
				<table width="100%" class="table table-striped table-hover ">
					<thead>
				      <th>Preview</th>
				      <th>Documento</th>
				      <th>Link</th>
				      </thead>
				  <tbody>
				    <tr>
				      <td class="profissional-preview" style="background-image: url(<% asset('/upload/user/{{vm.model.id}}/{{vm.model.documento_crp}}') %>)"></td>
				      <td>CRP</td>
				      <td>
				      	<a ng-href="<% asset('/upload/user/{{vm.model.id}}/{{vm.model.documento_crp}}') %>" target="_blank" ng-if="vm.model.documento_crp != null">{{vm.model.documento_crp}}</a>
				      	<span ng-if="vm.model.documento_crp == null">Não enviado</span>
				      </td>
				    </tr>
				    <tr>
				      <td class="profissional-preview" style="background-image: url(<% asset('/upload/user/{{vm.model.id}}/{{vm.model.documento_diploma}}') %>)"></td>
				      <td>Diploma</td>
				      <td>
				      	<a ng-href="<% asset('/upload/user/{{vm.model.id}}/{{vm.model.documento_diploma}}') %>" target="_blank" ng-if="vm.model.documento_diploma != null">{{vm.model.documento_diploma}}</a>
				      	<span ng-if="vm.model.documento_diploma == null">Não enviado</span>
				      </td>
				    </tr>
				    <tr>
				      <td class="profissional-preview" style="background-image: url(<% asset('/upload/user/{{vm.model.id}}/{{vm.model.documento_identidade}}') %>)"></td>
				      <td>Identidade</td>
				      <td>
				      	<a ng-href="<% asset('/upload/user/{{vm.model.id}}/{{vm.model.documento_identidade}}') %>" target="_blank" ng-if="vm.model.documento_identidade != null">{{vm.model.documento_identidade}}</a>
				      	<span ng-if="vm.model.documento_identidade == null">Não enviado</span>
				      </td>
				    </tr>
				  </tbody>
				</table>
			</div>
			</div>
		</div>
	<div class="clearfix"></div>
	<br />
	<!-- <button class="btn btn-success btn100 pull-right" ng-click="enviar(this.formulario)"><i class="ion-checkmark"></i> Enviar formulário</button> -->
	</script>
