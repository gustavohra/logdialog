{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">
@stop

@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')
	
	{{-- menu principal --}}
    @yield('nav-menu-principal')
	
	<div class="admincontent-wrap">
        <div class="container pagina-notificacoes">
            <div class="admin-content-main">
<div class="admin-content-title ">
  <div class="row">
  <div class="col-xs-8">
                    <h1><a href="{{asset('/')}}notificacoes">Notificações</a></h1>
  </div>
  <div class="col-xs-4">
	                    @if( sizeof($notificacoes) == 1 )
    <div class="notification-actions"> 
 	<a class="btn btn-actions btn-notificacoes" href="{{asset('/')}}notificacoes">Todas as notificações</a>
			                                    </div>
			                                    @endif
  </div>
   </div>
                </div>
                <div class="admin-content-body">
                    <div class="notifications-box">
	                    @if( sizeof($notificacoes) > 0 )
	                    	
	                    	@foreach( $notificacoes as $item )
		                    	<?php
		                            // pegando dados mais específicos da notificação
		                            $item = \LogDialog\Model\Notificacao::detalharItem( $item->id, $item->tipo ); 

		                        ?>
								
								{{-- apenas para consultas solicitadas --}}
								@if( isset($item['data_compromisso']) )

		                            {{-- Notification Item --}}
			                        <div class="notifications-item {{!$item['exibir'] ? 'item-inativo' : ''}}">
			                            <div class="row">
			                                <div class="col-lg-1">
			                                    <div class="notification-image">
			                                        <img src="{{ asset($item['avatar_notificacao']) }}" alt="{{ $item['nome_usuario'] }}" class="img-responsive">
			                                    </div>
			                                </div>
			                                <div class="col-lg-7">
			                                    <div class="notification-content">
			                                        <p><span class="bold">{{ $item['nome_usuario'] }}</span> {{ $item['texto'] }}</p>
			                                        <p><span class="bold">Dia: </span> {{ $item['data_compromisso']->formatLocalized("%d de %B de %Y") }}</p>
			                                        <p><span class="bold">Horário: </span>{{ $item['data_compromisso']->format("H\hi") }}</p>
			                                    </div>
			                                
			                                
			                                </div>
			                                <div class="col-lg-4">
			                                    <div class="notification-actions"> 
                                                     @if($item["tipo"] == 'agendamento_confirmado')
                                                    	<a {{ \Carbon\Carbon::parse($item['data_compromisso'])->isPast() ? 'disabled="true" title="Consulta expirada" data-toggle="tooltip"' : '' }} class="btn btn-actions btn-irconsulta" href="{{ url('/meu/perfil?diaAgenda='.$item['data_compromisso'].'#minhaagenda') }} "><i class="fa fa-play-circle-o" aria-hidden="true"></i> Ir para consulta</a>
                                                    @endif
                                                    @if($item["tipo"] != 'agendamento_confirmado')
			                                        	<a {{ \Carbon\Carbon::parse($item['data_compromisso'])->isPast() ? 'disabled="true" title="Consulta expirada" data-toggle="tooltip" ' : '' }}  class="btn btn-actions btn-confirm" href="javascript:;" onclick="javascript:calendario.confirmarConsulta(this, {{ $item['relacionado_agenda_id'] }});" role="button"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Confirmar</a>
			                                        	<a {{ \Carbon\Carbon::parse($item['data_compromisso'])->isPast() ? 'disabled="true" title="Consulta expirada" data-toggle="tooltip"' : '' }}  class="btn btn-actions btn-decline" href="javascript:;" onclick="javascript:calendario.recusarConsulta(this, {{ $item['relacionado_agenda_id'] }});" role="button"><i class="fa fa-times" aria-hidden="true"></i> Recusar</a>
			                                        @endif
			                                    </div>
			                                </div>
			                            </div>
			                        </div> {{-- /.notifications-item --}}
			                        {{-- Notification Item --}}
			                    @elseif( $item['tipo'] == 'novo_inbox' )
			                    	{{-- Notification Item --}}
			                        <div class="notifications-item">
			                            <div class="row">
			                                <div class="col-lg-1">
			                                    <div class="notification-image">
			                                        <img src="{{ asset($item['avatar_notificacao']) }}" alt="{{ $item['nome_usuario'] }}" class="img-responsive">
			                                    </div>
			                                </div>
			                                <div class="col-lg-7">
			                                    <div class="notification-content">
			                                        <p>
			                                        	{{ $item['texto'] }}
			                                        </p>
			                              <small class="horario-notificacao">          
		                                    @if(date('d/m/Y') == $item['data_registro']->format('d/m/Y'))
		                                        {{$item['data_registro']->format('h:i')}}
		                                    @else
		                                        {{$item['data_registro']->format('d \d\e M')}}
		                                    @endif
	                                    </small>
			                                    </div>
			                                </div>

			                                <div class="col-lg-4">
			                                    <div class="notification-actions">
														<a class="btn btn-actions btn-confirm"  href="{{ action('NotificacoesController@visualizar', ['idNotificacao' => $item['id'], 'idUsuario' => $item['para_usuario_id'], 'tipo' => $item['tipo']]) }}"> Ver mensagem</a>
			                                    </div>
			                                </div>
			                            </div>
			                        </div> {{-- /.notifications-item --}}
			                        {{-- Notification Item --}}
			                    @endif
	                        @endforeach
	                    @else
	                    	{{-- Notification Item --}}
	                        <div class="notifications-item">
	                            <div class="row">
	                                <div class="col-lg-7">
	                                    <div class="notification-content">
	                                        <p>
	                                        	Não há notificações
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div> {{-- /.notifications-item --}}
	                        {{-- Notification Item --}}
	                    @endif
                    </div>
                </div> <!-- /.box -->
                @if(Count($notificacoes) > 1)
                <div class="pagination-wrap">
                    {{ $notificacoes->links() }}
                </div>
                @endif
            </div>     
        </div>
    </div> <!-- /.admincontent-wrap -->
@stop