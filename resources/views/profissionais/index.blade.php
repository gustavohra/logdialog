{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">
@stop

@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')
	
	{{-- menu principal --}}
    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')
	
	<div class="pages-wrap">
	    <div class="container">
	        <div class="col-lg-9">
	            <div class="profissionais-wrap">
	                <div class="pages-title"> 
	                    <h4>TODOS OS PROFISSIONAIS</h4>
	                </div>
	                <div class="pages-content">
	                    <div class="profissionais-wrap">
	                        {{-- PROFISSIONAL --}}
	                        @foreach( $profissionais as $pessoa )
		                        <div class="profissionais-item">
		                            <div class="row">
		                                <div class="col-lg-2">
		                                    <div class="profissionais-image">
		                                        <img src="{{ Avatar::urlImagem( $pessoa->usuario_id ) }}" class="img-responsive">
		                                    </div>
		                                </div>
		                                <div class="col-lg-6">
		                                    <div class="profissionais-content">
												{{-- pegando as especialidades --}}
												<?php
													$especialidades =  \LogDialog\Model\Especialidade::whereProfissionalId($pessoa->id)
																		 ->where('data_aprovado', '!=', null)
												                         ->pluck('especialidade')
												                         ->toArray();
												?>

		                                        <h3>{{ $pessoa->tratamento }} {{ $pessoa->nome }} {{ $pessoa->sobrenome }}</h3>
		                                        <p>
		                                        CRP {{$pessoa->numero_crp}} <br />
		                                        {{ $pessoa->cidade }}-{{ $pessoa->estado }}<br>Valor da consulta: Consulta de 1h - R${{ number_format( \LogDialog\Model\Profissional::valorPorHora( $pessoa->usuario_id, 'menor' ), 2, ',', '.' ) }}<br>Especialidades: {{ implode(', ', $especialidades) }}.
		                                        </p>
		                                    </div>
		                                </div>
		                                <div class="col-lg-4">
		                                	{{-- não exibir quando for um profisisonal logado --}}
											@if( Auth::check() )
												@if( Auth::user()->grupoSistema() != 'profissional' )
				                                    <div class="profissionais-actions">
				                                        <button class="btn btn-actions"><img src="{{ asset("images/marca-consult.png") }}"> <a href="{{ action('ProfissionaisController@agendaCompleta', ["usuarioId" => $pessoa->usuario_id]) }}">MARCAR CONSULTA</a></button><br><br>
				                                        <button class="btn btn-actions" onclick="javascript:home.enviarMensagem('profissional', {{ $pessoa->usuario_id }}, {{ Auth::check() ? 1 : 0 }});"><img src="{{ asset("images/enviar-msg.png") }}"> ENVIAR MENSAGEM</button>
				                                    </div>
				                                @endif
			                                @else
			                                	<div class="profissionais-actions">
			                                        <button class="btn btn-actions" onclick="javascript:home.agendarConsulta('profissional', {{ $pessoa->usuario_id }}, {{ Auth::check() ? 1 : 0 }});"><img src="{{ asset("images/marca-consult.png") }}"> MARCAR CONSULTA</button><br><br>
			                                        <button class="btn btn-actions" onclick="javascript:home.enviarMensagem('profissional', {{ $pessoa->usuario_id }}, {{ Auth::check() ? 1 : 0 }});"><img src="{{ asset("images/enviar-msg.png") }}"> ENVIAR MENSAGEM</button>
			                                    </div>
			                                @endif
		                                </div>
		                            </div>
		                        </div> <!-- /.item -->
		                    @endforeach
	                    </div> <!-- /.profissionais-wrap -->
	                    <div class="pagination-wrap">
	                        {{ $profissionais->links() }}
	                    </div>
	                </div>
	            </div> <!-- /.profissionais-wrap -->
	        </div> <!-- /.col -->
	        
	        <!-- FILTRO -->
	        <div class="col-lg-3">
	            <div class="filter-wrap">
	                <div class="pages-title"> 
	                    <h4>FILTRO</h4>
	                </div>
	                <!-- FILTRO POR NOME-->
	                <div class="filter-content">
	                    <form action="{{ action('ProfissionaisController@index') }}" id="search2" method="GET">
	                        <div id="filter-name">
	                            <h5>BUSCAR POR NOME</h5>
	                            <input type="text" name="nome" value="{{ Input::get('nome') }}" class="filter-input" placeholder="Busque por nome/sobrenome"/>
	                            <div class="search-button">
	                                <button class="btn btn-filters btn-confirm" type="submit">
	                                    <i class="fa fa-search" aria-hidden="true"></i> BUSCAR
	                                </button>
	                            </div>
	                        </div>
	                    </form>

	                    <form action="{{ action('ProfissionaisController@index') }}" method="GET">
							<input type="hidden" class="valor_max" name="valor_max" value="{{ $valorMaximoStart }}">
							<input type="hidden" class="valor_min" name="valor_min" value="{{ $valorMinimoStart }}">

	                        <!-- BUSCA POR FILTRO -->
	                        <div class="busca-filtro">
	                            <h5>BUSCA POR FILTRO</h5>
	                            <div class="gender-filter">
	                                <div class="gender-radio">
	                                    <input type="checkbox" name="sexo[]" value="masculino" {{ Input::get('sexo') ? in_array('masculino', Input::get('sexo')) ? 'checked="checked"' : '' : '' }}> Homem
	                                </div>
	                                <div class="gender-radio">
	                                    <input type="checkbox" name="sexo[]" value="feminino" {{ Input::get('sexo') ? in_array('feminino', Input::get('sexo')) ? 'checked="checked"' : '' : '' }}> Mulher
	                                </div>
	                            </div>
	                        </div>
	                        <div>
	                            <!-- PRICE RANGER -->
	                            <div class="price-filter">
	                                <h6>Valor <br>
	                                	De: R$<span id="textoValorMinimo">{{ $valorMinimoStart }}</span> <br>
	                                	Até: R$<span id="textoValorMaximo">{{ $valorMaximoStart }}</span></h6>
	                                <div class="price-selector">
	                                    <div id="connect"></div>
	                                </div>
	                            </div>

	                            <!--SELECTOR -->
	                            <div class="freecontact-filter">
	                                <!-- <p><input type="checkbox" value="✓" name="primeiro_gratis" {{ Input::get('primeiro_gratis') ? 'checked="checked"' : '' }}> Opção de primeiro contato grátis</p> --> 
	                            </div>
	                        </div>

	                        <!-- ESPECIALIDADE -->
	                        <div class="expertise-filter">
	                            <h6>Especialidade</h6>
	                            <select name="servico" id="">
	                            	<option value="">Selecione</option>
	                                @foreach( $servicos as $k => $item )
	                                	<option value="{{ $item['servico'] }}" {{ Input::get('servico') == $item['servico'] ? 'selected="selected"' : '' }}>{{ $item['servico'] }}</option>
	                                @endforeach
	                            </select>
	                        </div>
	                        <div class="search-button">
	                            <button class="btn btn-filters btn-confirm" type="submit">
	                                <i class="fa fa-search" aria-hidden="true"></i> BUSCAR
	                            </button>
	                        </div>
	                    </form>
	                </div> <!-- /.body-container -->
	            </div><!-- /.filter-wrap -->
	        </div><!-- /.col -->

	    </div><!-- /.container -->
	</div><!-- /.pages-wrap -->

	<script src="{{ asset('js/wnumb-1.0.2/wNumb.js') }}"></script>
	<script src="{{ asset('filter-slider/nouislider.min.js') }}"></script>

    <script>
	    var connectSlider = document.getElementById('connect');

	    noUiSlider.create(connectSlider, {
	        start: [{{ $valorMinimoStart }}, {{ $valorMaximoStart }}],
	        connect: false,
	        format: wNumb({
				decimals: 0
			}),
	        range: {
	            'min': {{ $valorMinimo }},
	            'max': {{ $valorMaximo }}
	        }
	    });

	    var connectBar = document.createElement('div'),
	        connectBase = connectSlider.querySelector('.noUi-base');

	    // Give the bar a class for styling and add it to the slider.
	    connectBar.className += 'connect';
	    connectBase.appendChild(connectBar);

	    connectSlider.noUiSlider.on('update', function( values, handle, a, b, handlePositions ) {

	        var offset = handlePositions[handle];

	        // Right offset is 100% - left offset
	        if ( handle === 1 ) {
	            offset = 100 - offset;
	        }

	        // Pick left for the first handle, right for the second.
	        connectBar.style[handle ? 'right' : 'left'] = offset + '%';

	        // valores nos campos para busca
	        $("input.valor_min").val(values[0]);
	        $("input.valor_max").val(values[1]);

	    	// html
	    	$("#textoValorMinimo").html(values[0] + ",00");
	    	$("#textoValorMaximo").html(values[1] + ",00");
	    });
	</script>
@stop