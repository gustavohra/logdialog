{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo.css') }}" rel="stylesheet">

    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">
@stop

@section('conteudo')
	{{-- carregando menus --}}
    @include('layouts.logdialog.menus')

	{{-- menu principal --}}
    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

	    <div class="container page-profissionais-agenda-completa">
	        <div class="agenda">
	            <div class="row">
	                <div class="col-lg-3">
	                    <div class="agenda-perfil">
	                        <img src="{{ Avatar::urlImagem( $profissional['usuario_id'] ) }}">
	                        <h4>{{ $profissional['tratamento'] }} {{ $profissional['nome'] }} {{ $profissional['sobrenome'] }}</h4>
	                        <p>CRP {{ $profissional['numero_crp'] }}<br>{{ $profissional['cidade'] }}-{{ $profissional['estado'] }}</p>
	                    </div><!-- .agenda-perfil -->
	                    <div class="btn-agenda-content">
	                        
	                        <button data-toggle="modal" data-target="#profissional_{{ $profissional['usuario_id'] }}"><img src="{{ asset("images/btn-person.png") }}"> PERFIL COMPLETO</button>
	                    </div><!-- .button -->
	                </div><!-- .col -->
	                <div class="container">
	                    <div class="col-lg-9">
	                        <div class="admincontent-wrap">
	                            <div class="agenda-content-main">
	                                <div class="agenda-content-title">
	                                    <div class="title-box">
	                                        <h4 class="text-left">AGENDA COMPLETA</h4>
	                                    </div>
	                                    <div class="btns-header">
	                                    	{{-- não exibir o botão de voltar, caso a semana carregada seja a atual do ano --}}
	                                    	@if( \Carbon\Carbon::today()->weekOfYear <= $semana[0]->weekOfYear )
		                                        <a href="{{ action("ProfissionaisController@agendaCompleta", ["usuarioId" => $profissional['usuario_id'], "timestamp" => $semana[0]->copy()->subDays(1)->timestamp]) }}" class="voltar">
	                                                <i class="fa fa-arrow-circle-o-left fa-2x" aria-hidden="true"></i>
	                                            </a>
	                                        @endif

	                                        <a href="{{ action("ProfissionaisController@agendaCompleta", ["usuarioId" => $profissional['usuario_id'], "timestamp" => end($semana)->copy()->addDays(1)->timestamp]) }}" class="avancar">
                                                <i class="fa fa-arrow-circle-o-right fa-2x" aria-hidden="true"></i>
                                            </a>
	                                    </div>
	                                </div>
	                                <div class="row header-month"> 
	                                	<div class="col-md-5">
	                                		<h3 class="data-calendario text-left">{{ utf8_encode($semana[0]->formatLocalized("%B %Y")) }}</h3>
	                                	</div>
	                                	<div class="col-md-7">
	                                		<div class="btn-agendar">
			                                    <button type="button" data-toggle="modal" data-target="#agendarConsulta-adm">AGENDAR NOVA CONSULTA</button>
			                                </div>
	                                	</div>
	                                </div>
	                                <div class="agenda-table">
	                                	<div class="row">
	                                		<div class="col-md-12">
	                                			<div id="calendar-week">

		                                            <div id="templateVisaoSemana">
		                                                <ul class="weekly-hours-title">
		                                                    <li class="hour"></li>
		                                                    @foreach( $semana as $k => $dia )
		                                                    	<li>{{ utf8_encode($dia->formatLocalized('%a (%d/%m)')) }}</li>
		                                                    @endforeach
		                                                </ul>

		                                                @foreach( $horarioSistema as $k => $horario )
			                                                <ul class="weekly-hours {{ $horario == end($horarioSistema) ? 'weekly-borderb' : '' }}">
			                                                    <li class="hour">{{ $horario }}</li>

			                                                    @foreach( $semana as $key => $dia )
			                                                    	@if( in_array($dia->format("Y-m-d {$horario}"), $indisponiveis) )
			                                                    		<li class="indisponivel">
			                                                    			<span>Indisponível</span>
			                                                    		</li>
			                                                    	@elseif( in_array($dia->format("Y-m-d {$horario}"), $ocupados) )
			                                                    		<li class="ocupado">
			                                                    			<span>Ocupado</span>
			                                                    		</li>
			                                                    	@elseif( HorarioAtendimento::diaSemAtendimento( $profissional['usuario_id'], $dia->dayOfWeek ) ||
			                                                    	         !HorarioAtendimento::horarioAtendido( $profissional['usuario_id'], $dia->dayOfWeek, $horario ) )
			                                                    		<li class="indisponivel">
			                                                    			<span>Indisponível</span>
			                                                    		</li>
			                                                    	@else
			                                                    		<li onclick="calendario.agendarDia('{{$dia->format("d/m/Y")}}', '{{$horario}}')" title="Clique para agendar no dia {{$dia->format("d/m/Y")}}, às {{$horario}}" data-toggle="tooltip" class="cursor bloco-calendario-disponivel">
			                                                    		&nbsp;
			                                                    		</li>
			                                                    	@endif
			                                                    @endforeach
			                                                </ul>
		                                                @endforeach

		                                            </div>
		                                            <div class="clear"></div>
		                                        </div>
	                                		</div>
	                                	</div>
	                                </div>
	                            </div>
	                        </div> <!-- /.admincontent-wrap -->
	                    </div><!-- col -->
	                </div><!-- container -->
	            </div><!-- .row -->
	        </div><!-- agenda -->
	    </div><!-- .container -->

	<script>
		$(document).ready(function(){
			$("#formAgendarConsulta [name='diaConsulta']").bind('change', function(){
				if( $(this).val() != '' )
				{
					// zerando os valores de horário
					// para carregar com os horários retornados pelo sistema
					$("#formAgendarConsulta [name='horarioConsulta']").html( $('<option>').html('carregando...').val('') );

					$.ajax({
						url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $profissional['usuario_id']]) }}',
						type: 'post',
						headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
						data: {
							_token: $('meta[name=_token]').attr('content'),
							timestamp : $(this).val()
						},
						success: function(data){
							// adicionando o primeiro elemento
							$("#formAgendarConsulta [name='horarioConsulta']").html( $('<option>').html('Escolha o horário').val('') );
							if(data.horarios.length > 0){
								for( var i = 0; i < data.horarios.length; i++ )
								{
									$("#formAgendarConsulta [name='horarioConsulta']").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
								}
							}
							else
								alert("Não existem horários disponíveis na data selecionada.");
						}
					});
				}
			});

			// primeira opcao de horario
			$("#formAgendarConsulta [name='diaOpcao1']").bind('change', function(){
				if( $(this).val() != '' )
				{
					// zerando os valores de horário
					// para carregar com os horários retornados pelo sistema
					$("#formAgendarConsulta [name='horarioOpcao1']").html( $('<option>').html('carregando...').val('') );

					$.ajax({
						url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $profissional['usuario_id']]) }}',
						type: 'post',
						headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
						data: {
							_token: $('meta[name=_token]').attr('content'),
							timestamp : $(this).val()
						},
						success: function(data){
							// adicionando o primeiro elemento
							$("#formAgendarConsulta [name='horarioOpcao1']").html( $('<option>').html('Escolha o horário').val('') );

							for( var i = 0; i < data.horarios.length; i++ )
							{
								$("#formAgendarConsulta [name='horarioOpcao1']").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
							}
						}
					});
				}
			});

			$("#formAgendarConsulta [name='diaOpcao2']").bind('change', function(){
				if( $(this).val() != '' )
				{
					// zerando os valores de horário
					// para carregar com os horários retornados pelo sistema
					$("#formAgendarConsulta [name='horarioOpcao2']").html( $('<option>').html('carregando...').val('') );

					$.ajax({
						url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $profissional['usuario_id']]) }}',
						type: 'post',
						headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
						data: {
							_token: $('meta[name=_token]').attr('content'),
							timestamp : $(this).val()
						},
						success: function(data){
							// adicionando o primeiro elemento
							$("#formAgendarConsulta [name='horarioOpcao2']").html( $('<option>').html('Escolha o horário').val('') );

							for( var i = 0; i < data.horarios.length; i++ )
							{
								$("#formAgendarConsulta [name='horarioOpcao2']").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
							}
						}
					});
				}
			});

			$("#formAgendarConsulta [name='diaOpcao3']").bind('change', function(){
				if( $(this).val() != '' )
				{
					// zerando os valores de horário
					// para carregar com os horários retornados pelo sistema
					$("#formAgendarConsulta [name='horarioOpcao3']").html( $('<option>').html('carregando...').val('') );

					$.ajax({
						url: '{{ action("ProfissionaisController@horariosDisponiveis", ["profissionalUsuarioId" => $profissional['usuario_id']]) }}',
						type: 'post',
						headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
						data: {
							_token: $('meta[name=_token]').attr('content'),
							timestamp : $(this).val()
						},
						success: function(data){
							// adicionando o primeiro elemento
							$("#formAgendarConsulta [name='horarioOpcao3']").html( $('<option>').html('Escolha o horário').val('') );

							for( var i = 0; i < data.horarios.length; i++ )
							{
								$("#formAgendarConsulta [name='horarioOpcao3']").append( $('<option>').html(data.horarios[i]).val(data.horarios[i]) );
							}
						}
					});
				}
			});
		});

		/**
		 * Função para adicionar opção de data
		 */
		function novaData()
		{
			if( !$('#three').is(':visible') )
				$('#three').show();
			else if( !$('#two').is(':visible') )
				$('#two').show();
		}

		/**
		 * Função para ocultar um campo de data
		 */
		function removerData( id )
		{
			$('#' + id).hide();
			$('#' + id).find('[name="diaOpcao2"]').eq(0).val('');
			$('#' + id).find('[name="diaOpcao3"]').eq(0).val('');
			$('#' + id).find('[name="horarioOpcao2"]').eq(0).val('');
			$('#' + id).find('[name="horarioOpcao3"]').eq(0).val('');
		}

		/**
		 * Ao submeter o formulário
		 */
		$(document).ready(function(){
			$("#formAgendarConsulta").bind('submit', function(){
				if( $('[name="diaConsulta"]').val() == '' )
				{
					$("#errorBlockAgendarConsulta").html('Selecione o dia da consulta');

					return false;
				}
				else if( $('[name="horarioConsulta"]').val() == '' )
				{
					$("#errorBlockAgendarConsulta").html('Selecione o horário da consulta');

					return false;
				}

				// caso marque o campo de envio de sugestões
				if( $('[name="enviarOpcoes"]').is(':checked') )
				{
					// verificando se algum dos campos foi preenchido
					// e se foi, deve ser preenchido a data e hora
					if(
						( $('[name="diaOpcao1"]').val() != '' && $('[name="horarioOpcao1"]').val() == '' ) ||
						( $('[name="diaOpcao1"]').val() == '' && $('[name="horarioOpcao1"]').val() != '' ) ||
						( $('[name="diaOpcao2"]').val() != '' && $('[name="horarioOpcao2"]').val() == '' ) ||
						( $('[name="diaOpcao2"]').val() == '' && $('[name="horarioOpcao2"]').val() != '' ) ||
						( $('[name="diaOpcao3"]').val() != '' && $('[name="horarioOpcao3"]').val() == '' ) ||
						( $('[name="diaOpcao3"]').val() == '' && $('[name="horarioOpcao3"]').val() != '' )
					)
					{
						$("#errorBlockAgendarConsulta").html('Uma das opções de data alternativa, não foi preenchida corretamente com data e hora');

						return false;
					}
				}
			});
		});
	</script>

@stop