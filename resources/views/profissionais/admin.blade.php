{{-- carregando o template padrão --}}
@extends('layouts.logdialog.padrao')

@section('stylesheet')
    <link href="{{ asset('css/criaturo-admin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/criaturo-calendar.css') }}" rel="stylesheet">
@stop

{{-- colocando o conteúdo do yield 'conteudo' --}}
@section('conteudo')
    {{-- carregando menus --}}
    @include('layouts.logdialog.menus')

    @yield('nav-menu-principal')

    {{-- carregando modal --}}
    @include('layouts.logdialog.modal')

                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js"></script>  
                            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script> 
                            <script type="text/javascript">

                                var getUrlParameter = function getUrlParameter(sParam) {
                                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                                        sURLVariables = sPageURL.split('&'),
                                        sParameterName,
                                        i; 
                                    for (i = 0; i < sURLVariables.length; i++) {
                                        sParameterName = sURLVariables[i].split('=');

                                        if (sParameterName[0] === sParam) {
                                            return sParameterName[1] === undefined ? true : sParameterName[1];
                                        }
                                    }
                                };
                                
                                var app = angular.module('profissionais', [ 
                                    ], function($interpolateProvider) {
                                        $interpolateProvider.startSymbol('<%');
                                        $interpolateProvider.endSymbol('%>');
                                    });

                                    app.factory('socket', function ($rootScope) {
                                        var socket = io.connect('https://logdialog.com.br:49999');
                                        return {
                                            on: function (eventName, callback) {
                                                socket.on(eventName, function () {
                                                    var args = arguments;
                                                    $rootScope.$apply(function () {
                                                        callback.apply(socket, args);
                                                    });
                                                });
                                            },
                                            emit: function (eventName, data, callback) {
                                                socket.emit(eventName, data, function () {
                                                    var args = arguments;
                                                    $rootScope.$apply(function () {
                                                        if (callback) {
                                                            callback.apply(socket, args);
                                                        }
                                                    });
                                                })
                                            }
                                        };
                                    });
                                    app.factory('transformRequestAsFormPost', [
                                        function () {
                                            'use strict';


                                            function transformRequest( data, getHeaders ) {
                                                var headers = getHeaders();
                                                    headers[ "Content-type" ] = "application/x-www-form-urlencoded; charset=utf-8";
                                                return( serializeData( data ) );
                                            }
                                            function serializeData( data ) {
                                                if ( ! angular.isObject( data ) ) {
                                                    return( ( data == null ) ? "" : data.toString() );
                                                }
                                                var buffer = [];
                                                for ( var name in data ) {
                                                    if ( ! data.hasOwnProperty( name ) ) {
                                                        continue;
                                                    }
                                                    var value = data[ name ];
                                                    buffer.push(
                                                        encodeURIComponent( name ) +
                                                        "=" +
                                                        encodeURIComponent( ( value == null ) ? "" : value )
                                                    );
                                                }
                                                var source = buffer
                                                    .join( "&" )
                                                    .replace( /%20/g, "+" )
                                                ;
                                                return( source );
                                            }

                                            return {
                                                transformRequest: transformRequest
                                            };
                                        }
                                    ]);

                                app.controller('CalendarioAnoCtrl', ['$scope', 'transformRequestAsFormPost', '$http', function ($scope, transformRequestAsFormPost, $http) {
                                    console.log("Calendário iniciado");
                                    $scope.anoAtual     = moment();
                                    $scope.calendario   = {};
                                    function carregarDatas() {
                                        console.log($scope.anoAtual);
                                        var timestampInicio = $scope.anoAtual.startOf('year').unix();
                                        var timestampFim    = $scope.anoAtual.endOf('year').unix();
                                        $scope.carregando = true;
                                        $http({ 
                                                method: "POST",
                                                url:  "/calendario/intervalo/" + timestampInicio + "/" + timestampFim,
                                                transformRequest: transformRequestAsFormPost,
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded',
                                                    'X-CSRF-Token' : token_fix
                                                },
                                                data: {
                                                    _token: token_fix,
                                                    id: id_fix
                                                }
                                            }).then(function(response) {
                                                $scope.calendario = response.data;
                                            })
                                            .catch(function(response) {})
                                            .finally(function() { });       
                                    };

                                    carregarDatas();

                                    $scope.abrirConsulta = function(dia) {
                                        changeCalendarView(1, dia);
                                    };
                                }]);
                                app.controller('MensagensCtrl', ['$scope','transformRequestAsFormPost', '$http', '$interval', 'socket', function ($scope,transformRequestAsFormPost, $http, $interval, socket) {
                                        $scope.contatos     = [];
                                        $scope.paginacao    = 1;
                                        $scope.contatoatual = null;
                                        $scope.mensagens    = [];
                                        $scope.novamensagem = '';
                                        $scope.digitando    = false;
                                        $scope.carregando   = true;
                                        $scope.chatIniciado = false;

                                        $interval(verificarNovaMensagem, 1000);

                                        socket.on('startup', function(data) { 
                                            console.log("Chat iniciado");
                                            $scope.chatIniciado = true;
                                        });

                                        function scroll() {
                                            $("#usuarioMensagemChat .mensagens-chat").stop().animate({
                                                scrollTop: Math.abs($("#usuarioMensagemChat .mensagens-chat").prop('scrollHeight') + $("#usuarioMensagemChat .mensagens-chat").scrollTop())+200
                                            }, 500);
                                        }
                                        function getMensagens() {
                                            $scope.carregando = true;
                                            $http({ 
                                                    method: "POST",
                                                    url: '{{ action('MensagemController@APICarregarMensagens') }}/?page=' + $scope.paginacao + "&com_usuario_id="+ $scope.contatoatual,
                                                    transformRequest: transformRequestAsFormPost,
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded',
                                                        'X-CSRF-Token' : token_fix
                                                    },
                                                    data: {
                                                        _token: token_fix,
                                                        'com_usuario_id' : $scope.contatoatual,
                                                        'paginar' : 15 
                                                    }
                                                }).then(function(response) {
                                                    $scope.mensagens = response.data.mensagem;
                                                })
                                                .catch(function(response) {})
                                                .finally(function() {
                                                            $("#usuarioMensagemChat .mensagens-chat").stop().animate({
                                                                scrollTop: Math.abs($("#usuarioMensagemChat .mensagens-chat").prop('scrollHeight') + $("#usuarioMensagemChat .mensagens-chat").scrollTop())
                                                            }, 500);
                                                            $scope.carregando   = false;
                                                                $scope.usuariosOnline = data.usuariosOnline;
                                                                console.log(data.message);
                                                                socket.emit('chatEntrar', id_fix, function(socket, args){

                                                                });
                                                    });                                                                                                                                        
                                        }
                                        function verificarMensagens(msg) {
                                            var retorno = false;
                                            for (var i = 0; i < $scope.mensagens.length; i++) {
                                                    if($scope.mensagens[i].id == msg.id || msg.minha_mensagem)
                                                        retorno = true;
                                            }
                                            return retorno;
                                        }
                                        function verificarNovaMensagem() {
                                            if(!$scope.chatIniciado){
                                                $scope.carregando = true;
                                                $http({ 
                                                        method: "POST",
                                                        url: '{{ action('MensagemController@APICarregarMensagens') }}/?page=' + $scope.paginacao + "&com_usuario_id="+ $scope.contatoatual,
                                                        transformRequest: transformRequestAsFormPost,
                                                        headers: {
                                                            'Content-Type': 'application/x-www-form-urlencoded',
                                                            'X-CSRF-Token' : token_fix
                                                        },
                                                        data: {
                                                            _token: token_fix,
                                                            'ultimas': true,
                                                            'com_usuario_id' : $scope.contatoatual
                                                        }
                                                    }).then(function(response) {
                                                        var mensagens = response.data.mensagem; 
                                                        for (var i = 0; i < mensagens.length; i++) {

                                                            if(!verificarMensagens(mensagens[i])){
                                                                $scope.mensagens.push(mensagens[i]);
                                                                $("#usuarioMensagemChat .mensagens-chat").stop().animate({
                                                                    scrollTop: Math.abs($("#usuarioMensagemChat .mensagens-chat").prop('scrollHeight') + $("#usuarioMensagemChat .mensagens-chat").scrollTop())
                                                                }, 500);
                                                            }
                                                        }
                                                    })
                                                    .catch(function(response) {})
                                                    .finally(function() {
                                                        $scope.carregando   = false;
                                                        });    
                                            }                                                                                                                                    
                                        }
                                        function iniciar() {
                                            $scope.carregando = true;
                                            $http({ 
                                                    method: "POST",
                                                    url: '{{ action('MensagemController@APICarregarContatos') }}',
                                                    transformRequest: transformRequestAsFormPost,
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded',
                                                        'X-CSRF-Token' : token_fix
                                                    },
                                                    data: {
                                                        _token: token_fix,
                                                        usuario_id : id_fix,
                                                        'com_usuario_id' : $scope.contatoatual
                                                    }
                                                }).then(function(response) {
                                                    $scope.contatos = response.data.contatos;
                                                    console.log($scope.contatos);
                                                    $scope.contatoatual = $scope.contatos[$scope.contatos.length-1].usuario_id;
                                                })
                                                .catch(function(response) {})
                                                .finally(function() {getMensagens();$scope.carregando   = false;}
                                                );  
                                                // $interval(function() { 
                                                //             if(!$scope.carregando)
                                                //                 verificarNovaMensagem();
                                                // }, 1000); 

                                        }

                                        socket.on('chatDigitando', function(data) {      
                                            console.log(data);
                                            if(data != null && data.hash != null){ 
                                                var hash = data.hash.split("-");  
                                                if(hash[1] == id_fix){ 
                                                    $scope.digitando = data.digitando;
                                                } 
                                            }
                                        }); 
                                        socket.on('receberMensagem', function(data) {
                                            var hash = data.hash.split("-"); 

                                            if(hash[1] == id_fix){
                                                console.log("Mensagem recebida");
                                                var mensagem = {'mensagem': data.mensagem,
                                                                data_mensagem: data.data_mensagem,
                                                                de_nome: data.nome,
                                                                de_avatar: data.avatar,
                                                                minha_mensagem: 0
                                                            };

                                                    $scope.mensagens.push(mensagem);
                                            }
                                        });
                                        $scope.$watch('novamensagem', function(value) { 
                                            if(value != ''){ 
                                                $scope.hash = id_fix + '-' + $scope.contatoatual;
                                                socket.emit('chatDigitando',  {'hash': $scope.hash, 'flag': true}, function(socket, args){

                                                });
                                            }
                                            else {
                                                socket.emit('chatDigitando', {'hash': $scope.hash, 'flag': false}, function(socket, args){
                                                });                        
                                            }
                                                
                                        }, true);
                                        iniciar();

                                        $scope.ativarConversa = function(usuario) {
                                            $scope.contatoatual = usuario.usuario_id;
                                            $scope.tituloatual = 'Mensagens com '+usuario.nome;

                                            $scope.mensagens = [];
                                            getMensagens();
                                        };
                                        $scope.getHorario = function(data) {
                                            return moment(data).format("HH:mm DD/MM/YYYY");
                                        };
                                        $scope.enviarMensagem = function(nome, avatar) {
                                            $scope.carregando = true;
                                            var mensagem = {'mensagem': $scope.novamensagem,
                                                            data_mensagem: moment().format("YYYY-MM-DD HH:mm:ss"),
                                                            de_nome: nome,
                                                            de_avatar: avatar,
                                                            minha_mensagem: 1
                                                        };

                                                            $scope.mensagens.push(mensagem);
                                                            var indice = $scope.mensagens.length-1;

                                                $("#usuarioMensagemChat .mensagens-chat").stop().animate({
                                                    scrollTop: Math.abs($("#usuarioMensagemChat .mensagens-chat").prop('scrollHeight') + $("#usuarioMensagemChat .mensagens-chat").scrollTop())
                                                }, 500);

                                            $scope.novamensagem = '';
                                            $http({ 
                                                    method: "POST",
                                                    url: '{{ action('MensagemController@APINovaMensagem') }}',
                                                    transformRequest: transformRequestAsFormPost,
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded',
                                                        'X-CSRF-Token' : token_fix
                                                    },
                                                    data: {
                                                        _token: token_fix, 
                                                        paraUsuarioId: $scope.contatoatual,
                                                        mensagem: mensagem.mensagem
                                                    }
                                                }).then(function(response) {
                                                    $scope.mensagens[indice]["id"] = response.data.id; 
                                                    mensagem.hash = id_fix + '-' + $scope.contatoatual;

                                                    socket.emit('enviarMensagem', mensagem, function(socket, args){
                                                        console.log("Mensagem enviada");
                                                    });   
                                                })
                                                .catch(function(response) {})
                                                .finally(function() {$scope.carregando = false;});                                                
                                        }
                                }])

                            </script>

    <div class="admincontent-wrap" ng-app="profissionais">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-nopadding">
                    <div class="sidebar-admin">
                        <div class="professional-photo">
                            <img src="{{ asset( strlen($usuario['profissional']['avatar']) > 0 ? "upload/user/" . Auth::user()->toArray()['id'] . "/" . $usuario['profissional']['avatar'] : 'images/avatar-padrao.jpg' ) }}" alt="{{ $usuario['profissional']['nome'] }} {{ $usuario['profissional']['sobrenome'] }}" alt="">
                            <h3>{{ $usuario['profissional']['tratamento'] }} {{ $usuario['profissional']['nome'] }} {{ $usuario['profissional']['sobrenome'] }}</h3>
                            <p>
                                @if(
                                    strlen( $usuario['profissional']['cidade'] ) > 0 &&
                                    strlen( $usuario['profissional']['estado'] ) > 0
                                )
                                    {{ $usuario['profissional']['cidade'] }}, {{ $usuario['profissional']['estado'] }}
                                @endif
                            </p>
                        </div>
                        <div class="visible-md-block visible-lg-block">
                            <div class="sidebarblock sidebar-block01">
                                <h2>Próximas Consultas</h2>
                                <div class="list-proximasconsultas">
                                    @if( sizeof($proximasConsultas) > 0 )
                                        @foreach( $proximasConsultas as $consulta )
                                            <div class="proximaconsulta">
                                                <div class="pccontent">
                                                    <div class="pcdata">{{ date("d/m", strtotime($consulta->data_compromisso)) }} as {{ date("H\hi", strtotime($consulta->data_compromisso)) }}</div>
                                                    <div class="pcdescricao">{{ $consulta->tratamento_profissional }} {{ $consulta->nome_profissional }} {{ $consulta->sobrenome_profissional }}</div>
                                                </div>
                                            </div>
                                        @endforeach

                                        <div class="sidebar-vermais"><a href="{{ action('AgendaController@minhasConsultas') }}">Ver todas <i class="fa fa-angle-double-right"></i></a></div>
                                    @else
                                        <div class="proximaconsulta">
                                            <div>Não há consultas agendadas</div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{-- carregando notificações do usuário logado --}}
                            <?php
                                // pegando notificações pendentes
                                // apenas algumas para exibir no dropdown
                                $notificacoes = \LogDialog\Model\Notificacao::listarNotificacoes( Auth::user()->toArray()['id'], 3 );
                            ?>
                            @if( count($notificacoes) > 0 )
                                <div class="sidebarblock sidebar-block02">
                                    <h2>Avisos</h2>
                                    <div class="list-avisossidebar">

                                        @foreach( $notificacoes as $item )
                                            <?php
                                                // pegando dados mais específicos da notificação
                                                $item = \LogDialog\Model\Notificacao::detalharItem( $item->id, $item->tipo );
                                            ?>

                                            <div class="avisossidebar" onclick="window.location.href='{{ action('NotificacoesController@visualizar', ['idNotificacao' => $item['id'], 'idUsuario' => $item['para_usuario_id'], 'tipo' => $item['tipo']]) }}'">
                                                <div class="ascontent">
                                                    <div class="asdescricao">
                                                    <!-- <strong>{{ $item['nome_usuario'] }}</strong><br> --> {{ $item['texto'] }}</div>
                                                    <div class="asdata">{{ $item['data_registro']->formatLocalized("%d/%m") }}</div>
                                                </div>
                                            </div>
                                        @endforeach

                                        <div class="sidebar-vermais"><a href="{{ action('NotificacoesController@index') }}">Ver todas <i class="fa fa-angle-double-right"></i></a></div>
                                    </div>
                                </div>
                            @endif
                        </div> <!-- hidden -->
                        <div class="visible-xs-block visible-sm-block">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                Próxima Consultas
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="list-proximasconsultas">
                                                <div class="proximaconsulta">
                                                    <div class="pccontent">
                                                        <div class="pcdata">02/12 as 14h00</div>
                                                        <div class="pcdescricao">Dr. Lorem Ipsum</div>
                                                    </div>
                                                </div>
                                                <div class="proximaconsulta">
                                                    <div class="pccontent">
                                                        <div class="pcdata">02/12 as 14h00</div>
                                                        <div class="pcdescricao">Dr. Lorem Ipsum</div>
                                                    </div>
                                                </div>
                                                <div class="sidebar-vermais"><a href="">Ver todas <i class="fa fa-angle-double-right"></i></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                Próxima Consultas
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="list-proximasconsultas">
                                                <div class="proximaconsulta">
                                                    <div class="pccontent">
                                                        <div class="pcdata">02/12 as 14h00</div>
                                                        <div class="pcdescricao">Dr. Lorem Ipsum</div>
                                                    </div>
                                                </div>
                                                <div class="proximaconsulta">
                                                    <div class="pccontent">
                                                        <div class="pcdata">02/12 as 14h00</div>
                                                        <div class="pcdescricao">Dr. Lorem Ipsum</div>
                                                    </div>
                                                </div>
                                                <div class="sidebar-vermais"><a href="">Ver todas <i class="fa fa-angle-double-right"></i></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> <!-- hidden -->
                    </div> <!-- /.sidebar-admin -->
                </div>

                <div class="col-lg-10">
                    <div class="maincontent-admin">
                        <ul class="nav nav-tabs">
                            <li class="active"><a class="menu01" href="#meuperfil">Meu Painel</a></li>
                            <li><a class="menu02" href="#minhaagenda">Minha Agenda</a></li>
                            <li><a class="menu03" href="#historico">Histórico</a></li>
                            <li><a class="menu04" href="#mensagens">Mensagens</a></li>
                            <li><a class="menu05" href="#bloconotas">Prontuário</a></li>
                        </ul>

                        <div class="tab-content">

                            <div id="meuperfil" class="tab-pane fade in active">
                                <div class="tabcontent-inside">
                                    <div class="submenu">
                                        <ul>
                                        <li><a type="button" data-toggle="modal"  data-target="#profissional_{{Auth::user()->toArray()['id']}}">Visualizar Perfil</a></li>
                                            <li><a type="button" data-toggle="modal" data-target="#modalPerfil">Editar Perfil</a></li>
                                            <li><a type="button" data-toggle="modal" data-target="#modalMeuCurriculo">Currículo</a></li>
                                            <li><a type="button" data-toggle="modal" data-target="#modalEspecialidades">Especialidades</a></li>
                                            <li><a type="button" data-toggle="modal" data-target="#modalValores">Meus valores</a></li>
                                            {{-- <button id="meusValores" type="button" data-toggle="modal" data-target="#modalValores">MEUS VALORES</button> --}}
                                        </ul>
                                    </div> 

                                    @if(Session::get('retornoMoip') == 'success')
                                        <div class="alert alert-dismissible alert-success">
                                          <button type="button" class="close" data-dismiss="alert">×</button>
                                          <h4 class="alert-heading">Integração realizada com sucesso!</h4>
                                          <p class="mb-0">Sua conta de profissional foi integrada com sucesso ao Moip!</p>
                                        </div>
                                    @endif
                                    @if(!LogDialog\Http\Controllers\PerfilController::verificarMoip())
                                        <div class="alert alert-dismissible alert-danger">
                                          <button type="button" class="close" data-dismiss="alert">×</button>
                                          <h4 class="alert-heading">Aviso importante</h4>
                                          <p class="mb-0">Para finalizar o seu cadastro, por favor, <a target="_self" href="{{url("/profissionais/financeiro/gerarToken") }}"><b>clique aqui</b></a> e integre a sua conta profissional ao sistema de pagamentos Moip..</p>
                                        </div>
                                    @endif
                                    <h3>Histórico</h3>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h4>Total de Consultas</h4>
                                            <div class="ct-chart ct-golden-section" id="chart1"></div>
                                        </div>
                                        <div class="col-lg-6">
                                            <h4>Pacientes por faixa etária</h4>
                                            <div class="ct-chart ct-golden-section" id="chart2"></div>
                                        </div>
                                        <div class="col-lg-6">
                                            <h4>Pacientes por sexo</h4>
                                            <div class="ct-chart ct-golden-section" id="chart3"></div>
                                        </div>
                                        <div class="col-lg-6">
                                            <h4>Tipos de consultas</h4>
                                            <div class="ct-chart ct-golden-section" id="chart4"></div>
                                        </div>
                                    </div>

                                    <div class="clear"></div>

                                </div> <!-- /.tabcontent-inside -->
                            </div> <!-- /#meuperfil -->

                            <div id="minhaagenda" class="tab-pane fade">
                                <div class="tabcontent-inside">
                                    <div class="submenu">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <ul>
                                                    <li><a type="button" id="calendarview-1" class="active" onclick="changeCalendarView(1);">Dia</a></li>
                                                    <li><a type="button" id="calendarview-2" onclick="changeCalendarView(2);">Semana</a></li>
                                                    <li><a type="button" id="calendarview-3" onclick="changeCalendarView(3);">Mês</a></li>
                                                    <li><a type="button" id="calendarview-4" onclick="changeCalendarView(4);">Ano</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-8">
                                                <ul style="text-align:right;">
                                                    <li><a type="button" data-toggle="modal" data-target="#modalHorariosAtendimento">Definição de horários de atendimento</a></li>
                                                    <li><a type="button" data-toggle="modal" data-target="#modalHorariosBloqueio">Bloqueio de horários</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  <div id='calendar'></div> -->
                                    <div id="admin-calendar">
                                        <div id="calendar-day">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <div class="daily-left">
                                                        <div class="calendar-header">
                                                            <div class="row">
                                                                <div class="col-lg-7">
                                                                    <h3 class="data-calendario">
                                                                        {{ Carbon\Carbon::now()->formatLocalized('%d de %B de %Y') }}
                                                                    </h3>
                                                                </div>
                                                                <div class="col-lg-5">
                                                                    <div class="controls-calendar">
                                                                        <a href="javascript:;" class="voltar" data-timestamp="{{ Carbon\Carbon::yesterday()->timestamp }}" onclick="javascript:calendario.detalharDia(this);">
                                                                            <i class="fa fa-arrow-circle-o-left fa-2x" aria-hidden="true"></i>
                                                                        </a>
                                                                        <a href="javascript:;" class="avancar" data-timestamp="{{ Carbon\Carbon::tomorrow()->timestamp }}" onclick="javascript:calendario.detalharDia(this);">
                                                                            <i class="fa fa-arrow-circle-o-right fa-2x" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="detalheAgendaDia">
                                                            <ul class="daily-hours">
                                                                <li><span class="hour">08:00</span></li>
                                                                <li><span class="hour">09:00</span></li>
                                                                <li>
                                                                    <span class="hour">10:00</span>
                                                                </li>
                                                                <li><span class="hour">11:00</span></li>
                                                                <li><span class="hour">12:00</span></li>
                                                                <li><span class="hour">13:00</span></li>
                                                                <li><span class="hour">14:00</span></li>
                                                                <li><span class="hour">15:00</span></li>
                                                                <li><span class="hour">16:00</span></li>
                                                                <li><span class="hour">17:00</span></li>
                                                                <li><span class="hour">18:00</span></li>
                                                                <li><span class="hour">19:00</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div> <!-- /.col -->
                                                <div class="col-lg-5">
                                                    <div class="small-calendar" id="small-calendar">{{-- .small-calendar --}}
                                                        <div class="backstage small-calendar-title">
                                                            <p class="arrow">
                                                                <?php $mes = Calendario::mesCompleto(Calendario::date(time())); ?>
                                                                <a href="javascript:;" data-small-calendar="true" id="iniciarMesSmall" class="voltar" onclick="javascript:calendario.detalharMes(this, false);" data-timestamp="{{ $mes[0]->timestamp }}">
                                                                    <i class="fa fa-caret-left" aria-hidden="true"></i>
                                                                </a>

                                                                <span class="month-title"> {{ Carbon\Carbon::now()->formatLocalized('%B %Y') }} </span>

                                                                <a href="javascript:;" data-small-calendar="true" class="avancar" onclick="javascript:calendario.detalharMes(this, false);" data-timestamp="{{ end($mes)->timestamp }}">
                                                                    <i class="fa fa-caret-right" aria-hidden="true"></i>
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <ul class="dia-mes">
                                                            <ul class="semana">
                                                                @foreach( Calendario::semanaCompleta(Carbon\Carbon::now()) as $k => $dia )
                                                                    <li class="border-sab {{ $k > 0 ? $k == 6 ? 'tdborderr' : '' : 'tdborderl' }}">{{ $dia->formatLocalized('%a')[0] }}</li>
                                                                @endforeach
                                                            </ul>
                                                            <ul id="dias-small-calendar">
                                                                <li><div class="daynumber tdborderl">1</div></li>
                                                                <li><div class="daynumber">2</div></li>
                                                                <li><div class="daynumber">3</div></li>
                                                                <li><div class="daynumber">4</div></li>
                                                                <li><div class="daynumber">5</div></li>
                                                                <li><div class="daynumber">6</div></li>
                                                                <li class="tdborderr"><div class="daynumber">7</div></li>
                                                                <li><div class="daynumber tdborderl">8</div></li>
                                                                <li><div class="daynumber">9</div></li>
                                                                <li class="active"><div class="daynumber">10</div></li>
                                                                <li><div class="daynumber">11</div></li>
                                                                <li><div class="daynumber">12</div></li>
                                                                <li><div class="daynumber">13</div></li>
                                                                <li class="tdborderr"><div class="daynumber">14</div></li>
                                                                <li><div class="daynumber tdborderl">15</div></li>
                                                                <li><div class="daynumber">16</div></li>
                                                                <li><div class="daynumber">17</div></li>
                                                                <li><div class="daynumber">18</div></li>
                                                                <li><div class="daynumber">19</div></li>
                                                                <li><div class="daynumber">20</div></li>
                                                                <li class="tdborderr"><div class="daynumber">21</div></li>
                                                                <li><div class="daynumber tdborderl">22</div></li>
                                                                <li><div class="daynumber">23</div></li>
                                                                <li><div class="daynumber">24</div></li>
                                                                <li><div class="daynumber">25</div></li>
                                                                <li><div class="daynumber">26</div></li>
                                                                <li><div class="daynumber">27</div></li>
                                                                <li class="tdborderr"><div class="daynumber">28</div></li>
                                                                <li class="tdborderb ultimos-dias"><div class="daynumber tdborderl">29</div></li>
                                                                <li class="tdborderb ultimos-dias"><div class="daynumber">30</div></li>
                                                                <li class="othermonth tdborderb ultimos-dias"><div class="daynumber">1</div></li>
                                                                <li class="othermonth tdborderb ultimos-dias"><div class="daynumber">2</div></li>
                                                                <li class="othermonth tdborderb ultimos-dias"><div class="daynumber">3</div></li>
                                                                <li class="othermonth tdborderb ultimos-dias"><div class="daynumber">4</div></li>
                                                                <li class="othermonth tdborderr tdborderb ultimos-dias"><div class="daynumber">5</div></li>
                                                            </ul>

                                                            <div class="clear"></div>
                                                        </ul>
                                                    </div>
                                                    {{-- /.small-calendar --}}
                                                    <div id="anotacoes-consulta-visao-dia" class="daily-details">
                                                        <h3 class="nome-profissional">&nbsp;</h3>
                                                        <p class="details-hourdate">
                                                            <span class="data">{{-- Quarta-feira, 02/11 --}}</span>
                                                            <br>
                                                            <span class="hora">{{-- 14h30 as 15h30 --}}</span>
                                                        </p>
                                                        <div class="daily-notes">
                                                            <h5>Anotações
                                                                <button class="btn btn-editar" style="display: none;">
                                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar
                                                                </button>
                                                            </h5>

                                                            <p class="texto">
                                                                <span style="text-align: center; font-style: italic;">Sem anotações até o momento.</span>
                                                            </p>
                                                        </div>
                                                        <div class="daily-actionbuttons">
                                                        </div>
                                                    </div>
                                                </div><!-- /.col -->
                                            </div> <!-- /.row -->
                                        </div>

                                        <div id="calendar-week">
                                            <div class="calendar-header">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h3 class="data-calendario">{{ Calendario::date(time())->formatLocalized("%B %Y") }}</h3>
                                                        <div class="controls-calendar">
                                                            <?php $semana = Calendario::semanaCompleta(Calendario::date(time())); ?>
                                                            <a href="javascript:;" class="voltar" data-timestamp="{{ $semana[0]->timestamp }}" onclick="javascript:calendario.detalharSemana(this, false);">
                                                                <i class="fa fa-arrow-circle-o-left fa-2x" aria-hidden="true"></i>
                                                            </a>
                                                            <a href="javascript:;" id="iniciarSemana" class="avancar" data-timestamp="{{ end($semana)->timestamp }}" onclick="javascript:calendario.detalharSemana(this, false);">
                                                                <i class="fa fa-arrow-circle-o-right fa-2x" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="templateVisaoSemana">
                                                <ul class="weekly-hours-title">
                                                    <li class="hour"></li>
                                                    <li>DOM(17/01)</li>
                                                    <li>SEG(18/01)</li>
                                                    <li>TER(19/01)</li>
                                                    <li>QUA(20/01)</li>
                                                    <li>QUI(21/01)</li>
                                                    <li>SEX(22/01)</li>
                                                    <li>SAB(23/01)</li>
                                                </ul>

                                                <ul class="weekly-hours">
                                                    <li class="hour">08:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">09:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">10:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">11:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">12:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">13:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">14:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">15:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">16:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">17:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours">
                                                    <li class="hour">18:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                                <ul class="weekly-hours weekly-borderb">
                                                    <li class="hour">19:00</li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
                                                </ul>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>

                                        <div id="calendar-month">
                                            <div class="calendar-header">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h3 class="data-calendario">{{ Calendario::date(time())->formatLocalized("%B %Y") }}</h3>
                                                        <div class="controls-calendar">
                                                            <?php $mes = Calendario::mesCompleto(Calendario::date(time())); ?>
                                                            <a href="javascript:;" id="iniciarMes" class="voltar" onclick="javascript:calendario.detalharMes(this, false);" data-timestamp="{{ $mes[0]->timestamp }}">
                                                                <i class="fa fa-arrow-circle-o-left fa-2x" aria-hidden="true"></i>
                                                            </a>
                                                            <a href="javascript:;" class="avancar" onclick="javascript:calendario.detalharMes(this, false);" data-timestamp="{{ end($mes)->timestamp }}">
                                                                <i class="fa fa-arrow-circle-o-right fa-2x" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="templateVisaoMes">
                                                <ul class="weekdays">
                                                    <li>Dom</li>
                                                    <li>Seg</li>
                                                    <li>Ter</li>
                                                    <li>Qua</li>
                                                    <li>Qui</li>
                                                    <li>Sex</li>
                                                    <li>Sab</li>
                                                </ul>

                                                <ul class="days">
                                                    <li><div class="daynumber">1</div></li>
                                                    <li><div class="daynumber">2</div></li>
                                                    <li><div class="daynumber">3</div></li>
                                                    <li><div class="daynumber">4</div></li>
                                                    <li><div class="daynumber">5</div></li>
                                                    <li>
                                                        <div class="daynumber">6</div>
                                                        <div class="daymonth-event event-request">
                                                            <a href="">Consulta - 11:00</a>
                                                        </div>
                                                    </li>
                                                    <li class="tdborderr"><div class="daynumber">7</div></li>
                                                    <li>
                                                        <div class="daynumber">8</div>
                                                        <div class="daymonth-event event-confirmed">
                                                            <a href="">Consulta - 11:00</a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="daynumber">9</div>
                                                        <div class="daymonth-event event-approved">
                                                            <a href="">Consulta - 11:00</a>
                                                        </div>
                                                    </li>
                                                    <li class="active"><div class="daynumber">10</div></li>
                                                    <li><div class="daynumber">11</div></li>
                                                    <li><div class="daynumber">12</div></li>
                                                    <li><div class="daynumber">13</div></li>
                                                    <li class="tdborderr"><div class="daynumber">14</div></li>
                                                    <li><div class="daynumber">15</div></li>
                                                    <li><div class="daynumber">16</div></li>
                                                    <li><div class="daynumber">17</div></li>
                                                    <li><div class="daynumber">18</div></li>
                                                    <li><div class="daynumber">19</div></li>
                                                    <li><div class="daynumber">20</div></li>
                                                    <li class="tdborderr"><div class="daynumber">21</div></li>
                                                    <li><div class="daynumber">22</div></li>
                                                    <li><div class="daynumber">23</div></li>
                                                    <li><div class="daynumber">24</div></li>
                                                    <li><div class="daynumber">25</div></li>
                                                    <li><div class="daynumber">26</div></li>
                                                    <li><div class="daynumber">27</div></li>
                                                    <li class="tdborderr"><div class="daynumber">28</div></li>
                                                    <li class="tdborderb"><div class="daynumber">29</div></li>
                                                    <li class="tdborderb"><div class="daynumber">30</div></li>
                                                    <li class="othermonth tdborderb"><div class="daynumber">1</div></li>
                                                    <li class="othermonth tdborderb"><div class="daynumber">2</div></li>
                                                    <li class="othermonth tdborderb"><div class="daynumber">3</div></li>
                                                    <li class="othermonth tdborderb"><div class="daynumber">4</div></li>
                                                    <li class="othermonth tdborderr tdborderb"><div class="daynumber">5</div></li>
                                                    <div class="clear"></div>
                                                </ul>
                                            </div>
                                        </div>

                                        <div id="calendar-year" ng-controller="CalendarioAnoCtrl">
                                            <div class="calendar-header">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <h3 id="calendarioAnoHeader">ANO {{ date("Y") }}</h3>
                                                        <div class="controls-calendar">
                                                            <?php $ano = Calendario::anoCompleto(Calendario::date(time())); ?>
                                                            <a href="javascript:;" id="iniciarAno" class="voltar" onclick="javascript:calendario.detalharAno(this, false);" data-timestamp="{{ $ano[0]->timestamp }}">
                                                                <i class="fa fa-arrow-circle-o-left fa-2x" aria-hidden="true"></i>
                                                            </a>
                                                            <a href="javascript:;" class="avancar" onclick="javascript:calendario.detalharAno(this, false);" data-timestamp="{{ end($ano)->timestamp }}">
                                                                <i class="fa fa-arrow-circle-o-right fa-2x" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="calendar"> 
                                                <div class="row" id="templateVisaoAno"> 
                                                <!-- exibir mes a mes do ano --> 
                                                            <div class="col-lg-3" ng-repeat="ano in calendario.nome_meses">
                                                                <div class="yearcalendar-item">
                                                                    <header>                
                                                                        <h4><% ano %></h4>
                                                                    </header>
                                                                    <div class="year-calendar">
                                                                        <ul class="dia-mes"> 
                                                                            <ul class="yearview-semana">
                                                                                <!-- cabeçalho com os dias da semana --> 
                                                                                    <li ng-repeat="sem in calendario.letra_dia_semana track by $index"><% sem %></li> 
                                                                            </ul>
                                                                            
                                                                            <!-- dias do mês -->
                                                                            <div ng-repeat="dia in calendario.dias"> 
                                                                                <div ng-if="dia.numero_mes == $parent.$index+1">  
                                                                                    <div ng-repeat="diaSemana in calendario.dia_semana">   
                                                                                        <div ng-if="dia.dia_semana == diaSemana">   
                                                                                            <li>
                                                                                                    <div ng-if="dia.tem_consulta" class="yearview-day calendario-dia-evento" ng-click="abrirConsulta(dia)"><% dia.dia_mes %></div>
                                                                                                    <div ng-if="!dia.tem_consulta" class="yearview-day" ><% dia.dia_mes %></div>
                                                                                            </li>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="clear"></div>
                                                                        </ul>
                                                                    </div>
                                                                </div> <!-- /.yearcalendar-item -->
                                                            </div> <!-- /.col --> 
                                                </div> <!-- /.row -->
                                            </div> <!-- end calendar -->
                                        </div> <!-- /#calendar-year -->

                                    </div>
                                </div> <!-- /.tabcontent-inside -->
                            </div> <!-- /#minhaagenda -->

                            <div id="historico" class="tab-pane fade">
                                <div class="tabcontent-inside">
                                    <h3>Histórico</h3>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Data</th>
                                                    <th>Horário</th>
                                                    <th>Paciente</th>
                                                    <th>Status</th>
                                                    <th>Ações</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                    @foreach( $consultas as $k => $consulta )
                                                <tr>
                                                    <td>{{Carbon\Carbon::parse((isset($consulta->data_compromisso) ? $consulta->data_compromisso : ''))->format('d/m/Y')}}</td>
                                                    <td>{{Carbon\Carbon::parse((isset($consulta->data_compromisso) ? $consulta->data_compromisso : ''))->format('h:i')}}</td>
                                                    <td>{{$consulta->nome_paciente . ' ' . $consulta->sobrenome_paciente}}</td>
                                                    <td>
                                                    @if($consulta->data_encerramento != null)
                                                    Encerrada
                                                    @elseif($consulta->data_cancelamento != null)
                                                    Cancelada
                                                    @else
                                                    Pendente
                                                    @endif
                                                    </td>
                                                    <td>
                                                        <!-- <a class="btn btn-default btn-pagar" href="#" role="button"><i class="fa fa-credit-card"></i> Pagar</a> -->
                                                        <a class="btn btn-default btn-enviarmensagem" href="{{asset('/meu/perfil/')}}#mensagens" role="button"><i class="fa fa-commenting-o"></i> Enviar Mensagem</a>
                                                    </td>
                                                </tr> 
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div> <!-- /.tabcontent-inside -->
                            </div> <!-- /#historico -->
                            <div id="mensagens" class="tab-pane fade">
                                <div class="tabcontent-inside" id="usuarioMensagemChat" ng-controller="MensagensCtrl">
                                    <h3>Mensagens</h3>
                                    <div class="padding-25">
                                        <div class="row">
                                            <div class="col-lg-4 no-padding mensagemblock-left">
                                                <h4>Todas as mensagens</h4>
                                                <ul class="mensagens-list contatos-list">
                                                    <li ng-repeat="contato in contatos" ng-click="ativarConversa(contato)" ng-class="{'selecionado': contatoatual == contato.usuario_id}">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <img ng-src="<% contato.avatar %>" alt="Dr. Gustavo Aguiar" class="img-responsive">
                                                            </div>
                                                            <div class="col-lg-8 no-padding-left"><span class="bold"><% contato.nome %></span><br><p>15</p></div>
                                                        </div> 
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-8 no-padding">
                                                <div class="mensagemblock-right">
                                                    <h4 class="nomeContatoMensagem"><%tituloatual%></h4>
                                                    <ul class="mensagens-chat">
                                                        <li ng-repeat="mensagem in mensagens" ng-class="{'chat-right': mensagem.minha_mensagem, 'chat-left': !mensagem.minha_mensagem}">
                                                        <!-- Remetente -->
                                                            <div class="row" ng-if="mensagem.minha_mensagem">
                                                                <div class="col-lg-10 no-padding-right">
                                                                    <span class="bold"><% mensagem.de_nome %></span>
                                                                    <span class="mensagem-time"><% getHorario(mensagem.data_mensagem) %></span>
                                                                    
                                                                    <div class="mensagem-text-right mensagem-tip">
                                                                        <div class="mensagem-text-inside"><% mensagem.mensagem %></div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="col-lg-2">
                                                                    <img src="<% mensagem.de_avatar %>" alt="<% mensagem.de_nome %>" class="img-responsive">
                                                                </div>
                                                            </div>
                                                        <!-- Destinatário -->
                                                        <div class="row" ng-if="!mensagem.minha_mensagem">
                                                            <div class="col-lg-2">
                                                                <img src="<% mensagem.de_avatar %>" alt="<% mensagem.de_nome %>" class="img-responsive">
                                                            </div>
                                                            <div class="col-lg-10 no-padding-left">
                                                                <span class="bold"><% mensagem.de_nome %></span>
                                                                <span class="mensagem-time"><% getHorario(mensagem.data_mensagem) %></span>
                                                                
                                                                <div class="mensagem-text-left mensagem-tip">
                                                                    <div class="mensagem-text-inside"><% mensagem.mensagem %></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </li>   
                                                        <div class="typing-indicator" ng-if="digitando">
                                                          <span></span>
                                                          <span></span>
                                                          <span></span>
                                                        </div>
                                                    </ul>
                                                </div> <!-- /.mensagemblock-right -->
                                                <div id="messagefield-wrap">
                                                    <div class="message-input">
                                                        <input type="text" class="textoNovaMensagem" ng-model="novamensagem">
 

                                                        <button class="btn btn-sendmessage"  data-avatar="" ng-click="enviarMensagem('{{ $usuario['profissional']['tratamento'] }} {{ $usuario['profissional']['nome'] }} {{ $usuario['profissional']['sobrenome'] }}', '{{ Avatar::urlImagem($usuario['user']['id']) }}')" ng-disabled="contatoatual == null || novamensagem == ''">
                                                            Enviar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div>
                                    </div>
                                </div> <!-- /.tabcontent-inside -->
                            </div> <!-- /#mensagens -->

                            <div id="bloconotas" class="tab-pane fade">
                                <div class="tabcontent-inside">
                                    <h3>Prontuário</h3>
                                    <div class="padding-25">
                                        <div class="row">
                                            <div class="col-lg-4 no-padding anotacoesblock-left">
                                                <h4>
                                                    Anotações
                                                </h4>
                                                <ul class="anotacoes-list">
                                                    @foreach( $notas as $k => $item )

                                                    @if(isset($nota) && $nota["agenda"]["data_encerramento"] != null)
                                                        <li id="nota_profissional_{{ $k }}"
                                                            data-titulo="{{ base64_encode( utf8_decode($item['titulo'])) }}"
                                                            data-conteudo="{{ base64_encode( json_encode($item['conteudo']) ) }}"
                                                            onclick="notas.selecionarNotaProfissional(this, {{ $k }} )"
                                                        >
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <span class="bold">{{ $item['titulo'] }} </span>
                                                                    <br>
                                                                    <p>
                                                                        {{ sizeof($item['conteudo']) }}

                                                                        @if( sizeof($item['conteudo']) > 1 )
                                                                            notas
                                                                        @else
                                                                            nota
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="col-lg-8 no-padding anotacoesblock-right">
                                                @if( sizeof($notas) > 0 )
                                                    <h4 class="nota-titulo">{{ $notas[0]['titulo'] }}</h4>
                                                    <div class="anotacao-content">
                                                        @foreach( $notas[0]['conteudo'] as $k => $item )
                                                            <div class="nota-header">
                                                                <div class="align-left">
                                                                    <p><span class="nota-data">Criado em {{ $item['data'] }}</span></p>
                                                                </div>
                                                            </div>
                                                            <div class="nota-body">
                                                                {!! nl2br($item['conteudo']) !!}
                                                            </div>
                                                            @if( isset($notas[0]['conteudo'][( $k + 1 )]) )
                                                                <br>
                                                                <br>
                                                                <br>
                                                                <span>
                                                                    ===== fim da nota de {{ $item['data'] }} =====
                                                                </span>
                                                                <br>
                                                                <br>
                                                                <br>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <h4>Não há notas salvas</h4>
                                                    <div class="anotacao-content">
                                                        <div class="nota-header">
                                                            <div class="align-left">
                                                                <p>&nbsp;</p>
                                                            </div>
                                                            <div class="align-right">
                                                                <ul style="display: none;">
                                                                    <li><img src="{{ asset('images/icon-edit.png') }}" alt=""></li>
                                                                    <li><img src="{{ asset('images/icon-delete.png') }}" alt=""></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="nota-body" style="display: none;">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus.</p>
                                                            <p>Consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus.</p>
                                                            <p>Ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus.</p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus.</p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus.</p>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus neque, laboriosam nesciunt ullam non voluptatem odio a ut tempora explicabo? Quia numquam esse autem fugiat alias provident iste, repellat natus.</p>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- /.tabcontent-inside -->
                            </div> <!-- /#bloconotas -->
                        </div>
                    </div> <!-- /.maincontent-admin -->
                </div>
            </div><!-- /.row -->
        </div>
    </div> <!-- /.admincontent-wrap -->

    <script>
        $(function(){
            // POPOVER CONSULTA 01
            $("#popover-consulta01").popover({
                html : true,
                content: function() {
                  return $("#popover-consulta01-content").html();
                },
                title: function() {
                  return $("#example-popover-2-title").html();
                }
            });

            // POPOVER CONSULTA 02
            $("#popover-consulta02").popover({
                html : true,
                content: function() {
                  return $("#popover-consulta02-content").html();
                }
            });

            // POPOVER CONSULTA 02
            $("#popover-consulta03").popover({
                html : true,
                content: function() {
                  return $("#popover-consulta03-content").html();
                }
            });

        });
    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).tab('show');
            });
            $('.nav-tabs a').on('shown.bs.tab', function(event){
                var x = $(event.target).text();         // active tab
                var y = $(event.relatedTarget).text();  // previous tab
                $(".act span").text(x);
                $(".prev span").text(y);
            });

            // ao iniciar o documento
            // carregar as informações da agenda com a data atual
            var btn = $('<span></span>').attr('data-timestamp', ( new Date().getTime() / 1000 ));
            calendario.detalharDia( btn, (moment(getUrlParameter('diaAgenda'))) );

            // ao iniciar o documento
            // processar a semana atual
            calendario.detalharSemana( $("#iniciarSemana"), true );

            // ao iniciar o documento
            // processar o mês do small calendar
            calendario.detalharMes( $("#iniciarMesSmall"), true );

            // ao iniciar o documento
            // processar o mês atual
            calendario.detalharMes( $("#iniciarMes"), true );

            // ao iniciar o documento
            // processar o ano atual
            calendario.detalharAno( $("#iniciarAno"), true );

            // caso tenha alguma hash na url
            // executar o click
            if( $.trim(window.top.location.hash).length > 0 )
                $("a[href='" + window.top.location.hash + "']").eq(0).trigger('click');
        });
    </script>

    <script>
        $(document).ready(function () {
            var carousel = $("#owl-humor-top");
            carousel.owlCarousel({
                navigation:true,
                navigationText: [
                  "<span class='fa-stack fa-lg'><i class='fa fa-circle-o fa-stack-2x'></i><i class='fa fa-chevron-left fa-stack-1x'></i></span>",
                  "<span class='fa-stack fa-lg'><i class='fa fa-circle-o fa-stack-2x'></i><i class='fa fa-chevron-right fa-stack-1x'></i></span>"
                ],
                center: true,
                loop: false,
                margin: 20,
                items: 3,
                pagination: false

            });
        });

        $(document).ready(function () {
            var carousel = $("#owl-humor-bottom");
            carousel.owlCarousel({
                navigation:true,
                navigationText: [
                  "<span class='fa-stack fa-lg'><i class='fa fa-circle-o fa-stack-2x'></i><i class='fa fa-chevron-left fa-stack-1x'></i></span>",
                  "<span class='fa-stack fa-lg'><i class='fa fa-circle-o fa-stack-2x'></i><i class='fa fa-chevron-right fa-stack-1x'></i></span>"
                ],
                center: true,
                loop: false,
                margin: 20,
                items: 3,
                pagination: false

            });
        });

    </script>

    <script type="text/javascript" src="{{ asset('js/logDialog/changeCalendarView.js') }}"></script>
    <script type="text/javascript" src="{{ asset('chartist/chartist.js') }}"></script>

    <script> 
        // GRAFICO 01
        new Chartist.Line('#chart1', {
            labels: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
            series: [
               <?php echo json_encode($atendimentosAno); ?>
            ]
        }, {
            fullWidth: true,
            chartPadding: {
                right: 40
            }
        });

        // GRAFICO 02
        new Chartist.Bar('#chart2', {
            labels: ['0-9', '10-19', '20-29', '30-39', '40-49', '50-59', '60-69', '70-79', '80+'],
            series: [
                <?php echo json_encode($atendimentosIdade); ?>
            ]
        }, {
            stackBars: true,
            axisY: {
                labelInterpolationFnc: function(value) {
                    return (value);
                }
            }
        }).on('draw', function(data) {
            if(data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 30px'
                });
            }
        });


        // GRAFICO 03
        var data = {
            labels: ['Homens', 'Mulheres'],
            series: 
                <?php echo json_encode($atendimentosSexo); ?>
        };

        var options = {
            labelInterpolationFnc: function(value) {
                return value[0]
            }
        };

        var responsiveOptions = [
            ['screen and (min-width: 640px)', {
                chartPadding: 30,
                labelOffset: 100,
                labelDirection: 'explode',
                labelInterpolationFnc: function(value) {
                    return value;
                }
            }],
            ['screen and (min-width: 1024px)', {
                labelOffset: 65,
                chartPadding: 20
            }]
        ];

        new Chartist.Pie('#chart3', data, options, responsiveOptions);

        // GRAFICO 04
        new Chartist.Bar('#chart4', {
            labels: ['Consulta Individual', 'Consulta tipo 2', 'Consulta para casais'],
            series: [
                <?php echo json_encode($atendimentosTipo); ?>
            ]
        }, {
            stackBars: true,
            axisY: {
                labelInterpolationFnc: function(value) {
                    return (value);
                }
            }
        }).on('draw', function(data) {
            if(data.type === 'bar') {
                data.element.attr({
                    style: 'stroke-width: 30px'
                });
            }
        });
    </script>
@stop
{{-- fim do conteúdo do yield atual --}}