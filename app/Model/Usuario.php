<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_usuario';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'username',
		'email',
		'senha',
		'grupo_sistema_id',
		'data_registro',
		'via_facebook',
		'via_linkedin',
		'ultimo_login',
		'inativo',
        'data_ativado'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
    	'username',
    	'grupo_sistema_id',
    	'data_registro',
    	'via_linkedin',
    	'via_facebook'
    ];

    /**
     * Retorna true ou false, caso o email informado já esteja no sistema
     *
     * @param string $email E-mail que será verificado, int $grupo Código do grupo
     *
     * @return boolean Status de cadastro do sistema
     */
    public static function emailJaCadastrado( string $email, int $grupo = null )
    {
        if($grupo != null)
            return self::where('email', $email)->where('grupo_sistema_id', $grupo)->count() > 0;
        else
            return self::where('email', $email)->count() > 0;
    } 
    /**
     * Retorna true ou false, caso o usuario_id informado já esteja no sistema
     *
     * @param string $usuario_id ID que será verificado, int $grupo Código do grupo
     *
     * @return boolean Tipo de cadastro
     */
    public static function getUsuarioTipo( string $usuario_id, int $grupo )
    {
        return self::where('id', $usuario_id)->where('grupo_sistema_id', $grupo)->count() > 0;
    } 
}