<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

// carregando os models
// necessários para grande parte das consultas
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\GrupoSistema as GrupoSistema;
use LogDialog\Model\ProfissionalServico as ProfissionalServico;
use LogDialog\Model\AdminServicoPadrao as AdminServicoPadrao;

// utilitários
use Hash;
use DB;
use Carbon\Carbon;

class Profissional extends Model
{
	public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_profissional';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'usuario_id',
		'nome',
		'sobrenome',
		'nascimento',
		'numero_crp',
		'sexo',
		'documento_crp',
		'documento_identidade',
		'documento_diploma',
		'avatar',
        'experiencia_profissional',
        'afiliacoes',
        'formacao_academica',
        'cpf',
        'rua',
        'numero',
        'bairro',
        'cidade',
        'estado',
        'cep',
        'data_aprovada_documentacao'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
    	'usuario_id',
    	'nascimento',
    	'sexo',
    	'documento_crp',
    	'documento_identidade',
    	'documento_diploma'
    ];

    /**
     * Retorna true ou false, caso o CRP informado já esteja no sistema
     *
     * @param string $crp CRP que será verificado
     *
     * @return boolean Status de cadastro do sistema
     */
    public static function crpJaCadastrado( string $crp )
    {
        return self::join('tb_usuario', 'tb_usuario.id', '=', 'tb_profissional.usuario_id')
                   ->where('tb_profissional.numero_crp', $crp)
                   ->where('tb_usuario.inativo', 0) // verificar apenas os cadastros ativos
                   ->count() > 0;
    }

    /**
     * Função para executar o cadastro do usuário
     * Retorna o status do cadastro em booleano
     *
     * @return boolean|string [Retorna TRUE no caso de sucesso, no caso de erro, retorna uma string com o erro]
     */
    public static function cadastrar(array $dados)
    {
    	$retorno = true;

    	// verificando se o email já está cadastrado no sistema
    	if( Usuario::emailJaCadastrado($dados['prof_email'], 2) )
    	{
    		// erro
    		$retorno = ["prof_email" => ["E-mail já cadastrado no sistema"]];
    	}
    	// verificando se o crp já está cadastrado no sistema
    	else if( self::crpJaCadastrado($dados['nr_crp']) )
    	{
    		// erro
            $retorno = ["nr_crp" => ["Número do CRP já cadastrado no sistema"]];
    	}
    	// caso seja único, prosseguir com o cadastro
    	else
    	{
		    // iniciando transaction
		    \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		    try{
                \DB::beginTransaction();

                // Verifica se já existe um e-mail cadastrado como profissional
                if(Usuario::emailJaCadastrado($dados['prof_email'], 2))
                    $usuario = "profissional_".$dados["prof_email"];
                else
                    $usuario = $dados["prof_email"];

		    	// cadastrando na tabela de usuário, e pegando o ID inserido
	    		$userId = Usuario::insertGetId([
	    				'username' => $usuario,
	    				'email' => $dados['prof_email'],
	    				'senha' => Hash::make($dados['prof_password']),
	    				'grupo_sistema_id' => GrupoSistema::select('id')
			                                         ->where('grupo', 'profissional')
                                                     ->where('inativo', 0)
			                                         ->take(1)
			                                         ->get()
			                                         ->toArray()[0]['id'],
	    			]);

	    		// convertendo data de nascimento
	    		$dados['prof_birth'] = Carbon::createFromFormat('d/m/Y', $dados['prof_birth']);
	    		$dados['prof_birth'] = $dados['prof_birth']->toDateString();

                // sexo
                if( $dados['prof_gender'] == 'female' )
                    $dados['prof_gender'] = 'feminino';
                else
                    $dados['prof_gender'] = 'masculino';

	    		// cadastrando o profissional
	    		self::insert([
	    				'usuario_id' => $userId,
	    				'nome' => $dados['prof_name'],
						'sobrenome' => $dados['prof_lastname'],
						'nascimento' => $dados['prof_birth'],
						'numero_crp' => $dados['nr_crp'],
                        'sexo' => $dados['prof_gender']
	    			]);

	    		// retornar o ID do usuário
	    		// será necessário para salvar o upload de documentos
	    		$retorno = $userId;

	    		\DB::commit();
		    }
		    catch( Exception $e )
		    {
		    	$retorno = "Erro ao efetivar seu cadastro, tente novamente em breve";

		    	\DB::rollback();
		    }

		    \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    	}

        // se o retorno não for um array
        // e sim um inteiro
        // prosseguir com o cadastro de serviços
        if( is_int($retorno) )
        {
            AdminServicoPadrao::atribuirProfissional( $retorno );
        }

    	return $retorno;
    }

    /**
     * Esta função retorna uma lista de profissionais
     * em array
     */
    public static function carregarLista( int $quantidade = 2, bool $randon = true ) : array
    {
        $tmp = DB::table('tb_profissional')
                ->select(DB::raw("tb_profissional.id,
                         tb_profissional.usuario_id,
                         tb_profissional.nome,
                         tb_profissional.sobrenome,
                         tb_profissional.sexo,
                         tb_profissional.avatar,
                         
                         tb_profissional.experiencia_profissional,
                         tb_profissional.afiliacoes,
                         tb_profissional.formacao_academica,
                         tb_profissional.cpf,
                         tb_profissional.rua,
                         tb_profissional.bairro,
                         tb_profissional.cidade,
                         tb_profissional.estado,
                         tb_profissional.cep,

                         tb_profissional.numero_crp,
                         
                         tb_profissional.primeiro_contato_gratis,

                         IF( sexo = 'feminino', 'Dra.', 'Dr.' ) AS tratamento"))
                ->join('tb_usuario', 'tb_profissional.usuario_id', '=', 'tb_usuario.id')
                ->where('tb_usuario.inativo', '=', 0)
                ->where('tb_profissional.data_aprovada_documentacao', '!=', null);

        if( $randon )
            $tmp->orderByRaw("RAND()");

        $tmp->limit($quantidade);

        return $tmp->get();
    }

    /**
     * Esta função retorna uma lista de profissionais
     * em array, com paginação
     */
    public static function carregarListaPaginada( int $quantidade = 0, bool $randon = false )
    {
        $tmp = DB::table('tb_profissional')
                ->select(DB::raw("tb_profissional.id,
                         tb_profissional.usuario_id,
                         tb_profissional.nome,
                         tb_profissional.sobrenome,
                         tb_profissional.sexo,
                         tb_profissional.avatar,
                         tb_profissional.cidade,
                         tb_profissional.estado,
                         
                         tb_profissional.experiencia_profissional,
                         tb_profissional.afiliacoes,
                         tb_profissional.formacao_academica,
                         tb_profissional.cpf,
                         tb_profissional.rua,
                         tb_profissional.bairro,
                         tb_profissional.cidade,
                         tb_profissional.estado,
                         tb_profissional.cep,

                         tb_profissional.numero_crp,
                         
                         tb_profissional.primeiro_contato_gratis,

                         IF( sexo = 'feminino', 'Dra.', 'Dr.' ) AS tratamento"))
                ->join('tb_usuario', 'tb_profissional.usuario_id', '=', 'tb_usuario.id')
                ->where('tb_usuario.inativo', '=', 0)
                ->where('tb_profissional.data_aprovada_documentacao', '!=', null);

        return $tmp;
    }

    /**
     * Esta função retorna um valor booleano
     * Ao verificar duplicidade de CPF na tabela de profissionais
     */
    public static function cpfJaExiste( $cpf, $usuarioId = 0 ) : bool
    {
        // verificando se o CPF já está em uso por outro paciente
        // que não o informado
        $count = Profissional::where('usuario_id', '!=', $usuarioId)  
                              ->whereCpf($cpf)
                              ->get()
                              ->count();

        return (int)$count != 0;
    }

    /**
     * Esta função retorna o valor já aceito pelo profissional
     * Apenas para os serviços padrões do sistema
     *
     * Esta informação é exibia em todos os lugares que exibem o valor por hora do profissional
     *
     * @param int    $usuarioId [Recebe o ID do usuário, relacionado ao profissional]
     * @param string $tipoValor [Recebe os valores (menor, maior, media), que determinam qual valor retornar]
     *
     * @return float [retorna o valor decimal com ponto flutuante]
     */
    public static function valorPorHora( int $usuarioId, $tipoValor = 'menor' ) : float
    {
        $retorno = 0.00;

        $valor = self::whereUsuarioId( $usuarioId )
                    ->where('tb_profissional_servico.servico_padrao_id', '!=', null)
                    ->where('tb_profissional_servico.data_modificacao', '!=', null)
                    ->join('tb_profissional_servico', 'tb_profissional.id', '=', 'tb_profissional_servico.profissional_id')
                    ->groupBy('tb_profissional_servico.profissional_id');

        // verificando o tipo de busca
        if( $tipoValor == 'menor' )
        {
            $valor->select( DB::raw('MIN(valor) AS valor') );
        }
        else if( $tipoValor == 'maior' )
        {
            $valor->select( DB::raw('MAX(valor) AS valor') );
        }
        else if( $tipoValor == 'media' )
        {
            $valor->select( DB::raw('AVG(valor) AS valor') );
        }
        
        $valor = $valor->first();

        if( $valor )
            $retorno = $valor->toArray()['valor'];

        return $retorno;
    }

    /**
     * Função para carregar as informações de um profissional específico
     */
    public static function verDetalhes( int $usuarioId ) : array
    {
        $retorno = [];

        $profissional = self::whereUsuarioId( $usuarioId )->get();

        if( $profissional->first() )
        {
            $retorno = $profissional->first()->toArray();

            $retorno['email'] = Usuario::find( $usuarioId )->toArray()['email'];
            $retorno['tratamento'] = $retorno['sexo'] == 'feminino' ? 'Dra.' : 'Dr.';
        }

        return $retorno;
    }
}
