<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

class TokenResetSenha extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_token_reset_senha';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'de_usuario_id',
		'token',
		'data_expiracao',
		'usado'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
        'id',
        'token',
        'de_usuario_id'
    ];

    /**
     * Verifica se ainda existe um token válido para o usuário informado
     * Caso não exista, gera uma nova
     */
    public static function token( int $usuarioId ) : string
    {
    	// gerando token de redefinição de senha

    	$token = self::whereDeUsuarioId( $usuarioId )
    	             ->whereUsado(0)
    	             ->where('data_expiracao', '>=', strtotime('+1 day'))
    	             ->first();

    	// caso exista
    	if( $token )
    	{
    		$token = $token->toArray();
    		$token = $token['token'];
    	}
    	else
    	{
    		// inserir no banco
    		$token = str_random(4);

    		self::insert([
    			"token" => $token,
    			"de_usuario_id" => $usuarioId,
    			"data_expiracao" => date("Y-m-d H:i:s", strtotime("+2 days"))
    		]);
    	}

    	// retornando o token válido
    	return $token;
    }
}
