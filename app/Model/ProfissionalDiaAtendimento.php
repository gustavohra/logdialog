<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

//utilitário
use DB;

class ProfissionalDiaAtendimento extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_profissional_dia_atendimento';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'profissional_id',
        'dia_semana',
        'horario_inicio',
        'horario_fim',
        'sem_atendimento'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
        'profissional_id',
        'id'
    ];

    /**
     * If the register exists in the table, it updates it. 
     * Otherwise it creates it
     * @param array $data Data to Insert/Update
     * @param array $keys Keys to check for in the table
     * @return Object
     */
    public static function createOrUpdate($data, $keys)
    {
        $record = self::where($keys)->first();
        
        if( is_null($record) )
        {
            return self::create($data);
        }
        else
        {
            return self::where($keys)->update($data);
        }
    }

    /**
     * Esta função retorna os horários de atendimento para o profisisonal informado
     */
    public static function horarios( int $usuarioId ) : array
    {
        $retorno = [];

        $horarios = self::select('tb_profissional_dia_atendimento.*')
                        ->join('tb_profissional', 'tb_profissional.id', '=', 'tb_profissional_dia_atendimento.profissional_id')
                        ->where('tb_profissional.usuario_id', '=', $usuarioId)
                        ->groupBy('dia_semana')
                        ->orderBy('dia_semana')
                        ->get();

        if( $horarios )
            $retorno = $horarios->toArray();

        return $retorno;
    }

    /**
     * Esta função retorna um valor booleano
     * para checar se o profissional informado atende no dia da semana e horário informado
     *
     * O parâmetro $intervalo, recebe (intervalo, de, ate), para filtrar se estou verificando o horário de inicio de atentimento, limite ou dentro de um intervalo
     */
    public static function atendeHorario( int $usuarioId, int $diaSemana, string $horario, string $intervalo = 'intervalo' ) : bool
    {
        $horarios = DB::table('tb_profissional_dia_atendimento AS pda')
                ->select(DB::raw("COUNT(pda.id) AS total"))
                
                ->join('tb_profissional AS prof', 'prof.id', '=', 'pda.profissional_id');

                if( $intervalo == 'intervalo' )
                {
                    $horarios->whereRaw(
                        DB::raw("( pda.horario_inicio <= '{$horario}' AND pda.horario_fim >= '{$horario}' ) AND
                                 prof.usuario_id = '{$usuarioId}' AND
                                 pda.dia_semana = '{$diaSemana}'")
                    );
                }
                else if( $intervalo == 'de' )
                {
                    $horarios->where("pda.horario_inicio", "=", $horario)
                             ->where("prof.usuario_id", "=", $usuarioId)
                             ->where("pda.dia_semana", "=", $diaSemana);
                }
                else if( $intervalo == 'ate' )
                {
                    $horarios->where("pda.horario_fim", "=", $horario)
                             ->where("prof.usuario_id", "=", $usuarioId)
                             ->where("pda.dia_semana", "=", $diaSemana);
                }
        
        $horarios = $horarios->first();

        if( $horarios )
            $retorno = $horarios->total > 0;
        else
            $retorno = false;

        return $retorno;
    }

    /**
     * Esta função retorna um valor booleano
     * para checar se o profissional informado atende ou não neste dia
     */
    public static function diaSemAtendimento( int $usuarioId, int $diaSemana ) : bool
    {
        $horarios = DB::table('tb_profissional_dia_atendimento AS pda')
                ->select(DB::raw("COUNT(pda.id) AS total"))
                
                ->join('tb_profissional AS prof', 'prof.id', '=', 'pda.profissional_id')
                
                ->where('prof.usuario_id', '=', $usuarioId)
                ->where('pda.dia_semana', '=', $diaSemana)
                ->where('pda.sem_atendimento', '=', 0)
                ->first();

        if( $horarios )
            $retorno = $horarios->total == 0;
        else
            $retorno = false;

        return $retorno;
    }
}
