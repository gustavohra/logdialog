<?php

namespace LogDialog\Model;

// utilitários
use DB;
use File;
use Carbon\Carbon;
use Avatar;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_agenda';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'paciente_id',
        'profissional_id',
        'nome_compromisso',
        'data_compromisso',
        'data_pagamento',
        'token_pagamento',
        'confirmado_usuario_convidado',
        'data_confirmacao',
        'data_cancelamento',
        'data_encerramento',
        'valor',
        'inativo',
        'status_pagamento',
        'id_pagamento'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id'
    ];

    /**
     * Esta função retorna a lista de consultas (objeto), paginável
     * Com a quantidade de itens solicitados
     *
     * E recebe o ID do usuário envolvido na consulta, independente se
     * ele é o paciente ou o profissional
     *
     * E recebe um timestamp ou Zero, para buscar a partir de uma data
     */
    public static function listaConsultas( int $usuarioId, int $quantidade = 2, int $timestamp = 0, string $ordem = 'ASC' )
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    ag.id AS agenda_id,
                    ag.nome_compromisso,
                    ag.data_compromisso,
                    ag.data_confirmacao,
                    ag.data_solicitacao,

                    ag.data_encerramento,
                    ag.data_cancelamento,

                    ag.encerrada_por_usuario_id,
                    ag.cancelada_por_usuario_id,
                    
                    paciente.nome AS nome_paciente,
                    paciente.sobrenome AS sobrenome_paciente,
                    paciente.nascimento AS nascimento_paciente,
                    paciente.sexo AS sexo_paciente,
                    paciente.avatar AS avatar_paciente,
                    paciente.id AS paciente_id,
                    paciente.usuario_id AS paciente_usuario_id,
                    
                    profissional.nome AS nome_profissional,
                    profissional.sobrenome AS sobrenome_profissional,
                    profissional.nascimento AS nascimento_profissional,
                    profissional.sexo AS sexo_profissional,
                    profissional.avatar AS avatar_profissional,
                    profissional.id AS profissional_id,
                    profissional.usuario_id AS profissional_usuario_id,
                    
                    IF(profissional.sexo = 'feminino', 'Dra.', 'Dr.') AS tratamento_profissional
                    "))
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' OR paciente.usuario_id = '{$usuarioId}' )") )
                ->where('ag.inativo', '=', 0)
                ->where('ag.data_confirmacao', '!=', null)
                ->where('ag.data_compromisso', '>=', date("Y-m-d H:i:s", $timestamp))
                ->orderBy('ag.data_compromisso', $ordem)
                ->paginate($quantidade);

        return $tmp;
    }

    public static function qtdConsultasPorPeriodo( int $usuarioId, string $timestampInicio = "", string $timestampFinal = "")
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    Count(ag.id) AS qtdConsultas,
                    profissional.usuario_id AS profissional_usuario_id
                    ")) 
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' )") )
                ->where('ag.inativo', '=', 0)
                ->where('ag.data_confirmacao', '!=', null)
                ->where('ag.data_compromisso', '>=', $timestampInicio)
                ->where('ag.data_compromisso', '<=', $timestampFinal);

        return $tmp;
    }
    public static function qtdConsultasPorIdade( int $usuarioId)
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    Count(ag.id) AS qtdConsultas,
                    profissional.usuario_id AS profissional_usuario_id,
                    TIMESTAMPDIFF(YEAR, paciente.nascimento, CURDATE()) AS idade
                    ")) 
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' )") )
                ->where('ag.inativo', '=', 0);

        return $tmp;
    }

    public static function qtdConsultasPorSexo( int $usuarioId, string $sexo)
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    Count(ag.id) AS qtdConsultas
                    ")) 
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' )") )
                ->where('paciente.sexo', '=', $sexo)
                ->where('ag.inativo', '=', 0);

        return $tmp;
    }

    public static function qtdConsultasPorTipo( int $usuarioId, string $tipo)
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    Count(ag.id) AS qtdConsultas
                    ")) 
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' )") )
                ->where('ag.nome_compromisso', '=', $tipo)
                ->where('ag.inativo', '=', 0);

        return $tmp;
    }

    /**
     * Detalhar um item
     * Retorna um objeto
     */
    public static function detalharItem( int $id ) : \stdClass
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    ag.id AS agenda_id,
                    ag.nome_compromisso,
                    ag.data_compromisso,
                    ag.data_confirmacao,
                    ag.data_solicitacao,
                    ag.data_pagamento,
                    ag.token_pagamento,
                    ag.valor,
                    ag.servico_id,
                    ag.status_pagamento,
                    
                    ag.data_encerramento,
                    ag.data_cancelamento,

                    ag.encerrada_por_usuario_id,
                    ag.cancelada_por_usuario_id,
                    
                    paciente.id AS paciente_id,
                    paciente.nome AS nome_paciente,
                    paciente.sobrenome AS sobrenome_paciente,
                    paciente.nascimento AS nascimento_paciente,
                    paciente.sexo AS sexo_paciente,
                    paciente.avatar AS avatar_paciente,
                    paciente.usuario_id AS paciente_usuario_id,
                    paciente.cpf AS cpf_paciente,
                    
                    profissional.id AS profissional_id,
                    profissional.nome AS nome_profissional,
                    profissional.sobrenome AS sobrenome_profissional,
                    profissional.nascimento AS nascimento_profissional,
                    profissional.sexo AS sexo_profissional,
                    profissional.avatar AS avatar_profissional,
                    profissional.usuario_id AS profissional_usuario_id,
                    profissional.moip_auth AS moip_auth,
                    profissional.moip_id AS moip_id,
                    
                    IF(profissional.sexo = 'feminino', 'Dra.', 'Dr.') AS tratamento_profissional
                    "))
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->where('ag.id', '=', $id)
                ->first();

        // pegando avatares
        if( $tmp )
        {
            $tmp->avatar_profissional = Avatar::urlImagem( $tmp->profissional_usuario_id );
            $tmp->avatar_paciente     = Avatar::urlImagem( $tmp->paciente_usuario_id );
        }
        else
        {
            $tmp = new \stdClass;
        }
            if(isset($tmp->data_compromisso))
                $tmp->data_compromisso = new Carbon($tmp->data_compromisso); 
 
        return $tmp;
    }

    /**
     * Esta função retorna a lista de consultas (objeto), paginável
     * Com a quantidade de itens solicitados
     *
     * E recebe o ID do usuário envolvido na consulta, independente se
     * ele é o paciente ou o profissional
     *
     * E recebe um timestamp ou Zero, para buscar a partir de uma data
     */
    public static function listaConsultasEncerradas( int $usuarioId, int $quantidade = 2, string $ordem = 'ASC' )
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    ag.id AS agenda_id,
                    ag.nome_compromisso,
                    ag.data_compromisso,
                    ag.data_confirmacao,
                    ag.data_solicitacao,
                    
                    ag.data_encerramento,
                    ag.data_cancelamento,

                    ag.encerrada_por_usuario_id,
                    ag.cancelada_por_usuario_id,
                    
                    paciente.nome AS nome_paciente,
                    paciente.sobrenome AS sobrenome_paciente,
                    paciente.nascimento AS nascimento_paciente,
                    paciente.sexo AS sexo_paciente,
                    paciente.avatar AS avatar_paciente,
                    paciente.id AS paciente_id,
                    paciente.usuario_id AS paciente_usuario_id,
                    
                    profissional.nome AS nome_profissional,
                    profissional.sobrenome AS sobrenome_profissional,
                    profissional.nascimento AS nascimento_profissional,
                    profissional.sexo AS sexo_profissional,
                    profissional.avatar AS avatar_profissional,
                    profissional.id AS profissional_id,
                    profissional.usuario_id AS profissional_usuario_id,
                    
                    IF(profissional.sexo = 'feminino', 'Dra.', 'Dr.') AS tratamento_profissional
                    "))
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' OR paciente.usuario_id  = '{$usuarioId}' )") )
                ->where('ag.inativo', '=', 0)
                ->where('ag.data_encerramento', '>', 0)
                ->orderBy('ag.data_compromisso', $ordem)
                ->paginate($quantidade);

        return $tmp;
    }

    /**
     * Buscar todas as consultas do dia informado
     * em array
     */
    public static function listarConsultasDia( int $usuarioId, int $timestamp = 0, string $ordem = 'ASC' ) : array
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    ag.id AS agenda_id,
                    ag.nome_compromisso,
                    ag.data_compromisso,
                    ag.data_confirmacao,
                    ag.data_solicitacao,
                    
                    ag.data_encerramento,
                    ag.data_cancelamento,

                    ag.encerrada_por_usuario_id,
                    ag.cancelada_por_usuario_id,
                    
                    paciente.nome AS nome_paciente,
                    paciente.sobrenome AS sobrenome_paciente,
                    paciente.nascimento AS nascimento_paciente,
                    paciente.sexo AS sexo_paciente,
                    paciente.avatar AS avatar_paciente,
                    paciente.id AS paciente_id,
                    paciente.usuario_id AS paciente_usuario_id,
                    
                    profissional.nome AS nome_profissional,
                    profissional.sobrenome AS sobrenome_profissional,
                    profissional.nascimento AS nascimento_profissional,
                    profissional.sexo AS sexo_profissional,
                    profissional.avatar AS avatar_profissional,
                    profissional.id AS profissional_id,
                    profissional.usuario_id AS profissional_usuario_id,
                    
                    IF(profissional.sexo = 'feminino', 'Dra.', 'Dr.') AS tratamento_profissional
                    "))
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' OR paciente.usuario_id  = '{$usuarioId}' )") )
                ->where('ag.inativo', '=', 0)
                ->where('ag.data_compromisso', '>=', date("Y-m-d 00:00:00", $timestamp))
                ->where('ag.data_compromisso', '<=', date("Y-m-d 23:59:59", $timestamp))
                ->orderBy('ag.data_compromisso', $ordem);

        return $tmp->get();
    }

    /**
     * Buscar todas as consultas do intervalo de dias informado
     * em array
     */
    public static function listarConsultasIntervaloDias( int $usuarioId, int $timestampInicio = 0, int $timestampFim = 0, string $ordem = 'ASC' ) : array
    {
        $tmp = DB::table('tb_agenda AS ag')
                ->select(DB::raw("
                    ag.id AS agenda_id,
                    ag.nome_compromisso,
                    ag.data_compromisso,
                    ag.data_confirmacao,
                    ag.data_solicitacao,
                    
                    ag.data_encerramento,
                    ag.data_cancelamento,

                    ag.encerrada_por_usuario_id,
                    ag.cancelada_por_usuario_id,
                    
                    paciente.nome AS nome_paciente,
                    paciente.sobrenome AS sobrenome_paciente,
                    paciente.nascimento AS nascimento_paciente,
                    paciente.sexo AS sexo_paciente,
                    paciente.avatar AS avatar_paciente,
                    paciente.id AS paciente_id,
                    paciente.usuario_id AS paciente_usuario_id,
                    
                    profissional.nome AS nome_profissional,
                    profissional.sobrenome AS sobrenome_profissional,
                    profissional.nascimento AS nascimento_profissional,
                    profissional.sexo AS sexo_profissional,
                    profissional.avatar AS avatar_profissional,
                    profissional.id AS profissional_id,
                    profissional.usuario_id AS profissional_usuario_id,
                    
                    IF(profissional.sexo = 'feminino', 'Dra.', 'Dr.') AS tratamento_profissional
                    "))
                ->join('tb_paciente AS paciente', 'paciente.id', '=', 'ag.paciente_id')
                ->join('tb_profissional AS profissional', 'profissional.id', '=', 'ag.profissional_id')
                ->whereRaw( DB::raw("( profissional.usuario_id = '{$usuarioId}' OR paciente.usuario_id  = '{$usuarioId}' )") )
                ->where('ag.inativo', '=', 0)
                ->where('ag.data_compromisso', '>=', date("Y-m-d 00:00:00", $timestampInicio))
                ->where('ag.data_compromisso', '<=', date("Y-m-d 23:59:59", $timestampFim))
                ->orderBy('ag.data_compromisso', $ordem);
 
        return $tmp->get();
    }
}