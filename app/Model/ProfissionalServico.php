<?php

namespace LogDialog\Model;

// utilitários
use DB;

use Illuminate\Database\Eloquent\Model;

class ProfissionalServico extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_profissional_servico';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'profissional_id',
        'servico',
        'valor',
        'inativo',
        'visibilidade',
        'data_modificacao'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
        'profissional_id',
        'data_inserido'
    ];

    /**
     * Esta função retorna os dados de todos os serviços prestados pelo profissional informado
     */
    public static function listarProfissional( int $usuarioId, bool $apenasAtivos = true ) : array
    {
        $retorno = [];

        // recuperando o ID do profissional
        $id = \LogDialog\Model\Profissional::whereUsuarioId( $usuarioId )
                        ->first();

        if( $id )
        {
            $lista = self::whereProfissionalId( $id->toArray()['id'] );

            if( $apenasAtivos )
                  $lista->where('inativo', 0);
                  
            $lista = $lista->orderBy('servico', 'ASC')
                           ->get();

            // caso exista lista
            if( $lista )
                $retorno = $lista->toArray();
        }

        return $retorno;
    }
}
