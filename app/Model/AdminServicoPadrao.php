<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

class AdminServicoPadrao extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_admin_servico_padrao';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'nome',
		'valor_sugerido',
		'criado_por_usuario_id',
		'data_criacao',
		'inativo'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
    	'data_criacao'
    ];

    /**
     * Esta função, atribui ao profissional informado (userId)
     * Todos os serviços setados como padrão
     */
    public static function atribuirProfissional( int $userId ) : bool
    {
        // verificando se o usuário informado é mesmo um profissional
        // ativo
        $profissional = \LogDialog\Model\Profissional::whereUsuarioId( $userId )->first();

        if( $profissional )
        {
            $profissional = $profissional->toArray();

            // adicionando todos os serviços do sistema que estejam ativos como padrão
            $servicosPadrao = AdminServicoPadrao::whereInativo( 0 )->get()->toArray();

            // cadastrando todos os serviços para o usuário
            foreach( $servicosPadrao as $k => $servico )
            {
                // apenas se o serviço atual
                // ainda não estiver atrelado ao usuário informado
                if( !\LogDialog\Model\ProfissionalServico::whereServicoPadraoId( $servico['id'] )->whereProfissionalId( $profissional['id'] )->first() )
                {
                    $inserir = new \LogDialog\Model\ProfissionalServico;
                    $inserir->profissional_id = $profissional['id'];
                    $inserir->servico_padrao_id = $servico['id'];
                    $inserir->valor = $servico['valor_sugerido'];
                    $inserir->servico = $servico['nome'];
                    $inserir->save();
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
