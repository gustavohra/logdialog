<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

// carregando os models
// necessários para grande parte das consultas
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\GrupoSistema as GrupoSistema;

// utilitários
use Hash;
use DB;
use Carbon\Carbon;

class Paciente extends Model
{
	public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_paciente';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'usuario_id',
		'nome',
		'sobrenome',
		'nascimento',
		'endereco',
		'end_numero',
		'end_complemento',
		'bairro',
		'cidade',
		'estado',
		'cep',
		'avatar'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
    	'usuario_id',
    	'nascimento'
    ];

    /**
     * Função para executar o cadastro do usuário
     * Retorna o status do cadastro em booleano
     *
     * @return boolean|string [Retorna TRUE no caso de sucesso, no caso de erro, retorna uma string com o erro]
     */
    public static function cadastrar(array $dados)
    {
    	$retorno = true;

    	// verificando se o email já está cadastrado no sistema
        if( Usuario::emailJaCadastrado($dados['email'], 3) )
    	{
    		// erro
    		$retorno = ["email" => ["E-mail já cadastrado no sistema"]];
    	}
    	// caso seja único, prosseguir com o cadastro
    	else
    	{
		    // iniciando transaction
		    \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		    try{
		    	\DB::beginTransaction();

                // Verifica se já existe um e-mail cadastrado como profissional
                if(Usuario::emailJaCadastrado($dados['email'], 2))
                    $usuario = "paciente_".$dados["email"];
                else
                    $usuario = $dados["email"];

		    	// cadastrando na tabela de usuário, e pegando o ID inserido
	    		$userId = Usuario::insertGetId([
	    				'username' => $usuario,
	    				'email' => $dados['email'],
	    				'senha' => Hash::make($dados['password']),
	    				'grupo_sistema_id' => GrupoSistema::select('id')
			                                         ->where('grupo', 'paciente')
			                                         ->take(1)
			                                         ->get()
			                                         ->toArray()[0]['id'],
	    			]);

	    		// convertendo data de nascimento
	    		$dados['birth'] = Carbon::createFromFormat('d/m/Y', $dados['birth']);
	    		$dados['birth'] = $dados['birth']->toDateString();

	    		// sexo
                if( $dados['gender'] == 'female' )
                    $dados['gender'] = 'feminino';
                else
                    $dados['gender'] = 'masculino';

	    		// cadastrando o paciente
	    		self::insert([
	    				'usuario_id' => $userId,
	    				'nome' => $dados['name'],
						'sobrenome' => $dados['lastname'],
						'nascimento' => $dados['birth'],
						'sexo' => $dados['gender']
	    			]);

	    		\DB::commit();
		    }
		    catch( Exception $e )
		    {
		    	$retorno = "Erro ao efetivar seu cadastro, tente novamente em breve";

		    	\DB::rollback();
		    }

		    \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    	}

    	return $retorno;
    }

    /**
     * Esta função retorna um valor booleano
     * Ao verificar duplicidade de CPF na tabela de pacientes
     */
    public static function cpfJaExiste( $cpf, $usuarioId = 0 ) : bool
    {
    	// verificando se o CPF já está em uso por outro paciente
    	// que não o informado
        $count = Paciente::where('usuario_id', '!=', $usuarioId) 
                              ->whereCpf($cpf)
                              ->get()
                              ->count();

        return (int)$count != 0;
    }

    /**
     * Função para carregar as informações de um paciente específico
     */
    public static function verDetalhes( int $usuarioId ) : array
    {
        $retorno = [];

        $paciente = self::whereUsuarioId( $usuarioId )->get();

        if( $paciente->first() )
        {
            $retorno = $paciente->first()->toArray();
            $retorno['email'] = Usuario::find( $usuarioId )->toArray()['email'];
        }

        return $retorno;
    }
}
