<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_newsletter_email';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'email',
		'inativo'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
        'email'
    ];

    /**
     * Retorna true ou false, caso o email informado já esteja no sistema
     *
     * @param string $email E-mail que será verificado
     *
     * @return boolean Status de cadastro do sistema
     */
    public static function emailJaCadastrado( string $email )
    {
        return self::where('email', $email)->count() > 0;
    }
}
