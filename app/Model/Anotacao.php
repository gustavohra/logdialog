<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

use LogDialog\Model\Agenda as Agenda;

class Anotacao extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_anotacao';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'id',
		'usuario_id',
		'relacionado_agenda_id',
		'titulo',
		'conteudo',
		'data_anotacao'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
    	'usuario_id',
    	'data_anotacao'
    ];

    /**
     * If the register exists in the table, it updates it. 
     * Otherwise it creates it
     * @param array $data Data to Insert/Update
     * @param array $keys Keys to check for in the table
     * @return Object
     */
    public static function createOrUpdate($data, $keys)
    {
        $record = self::where($keys)->first();
        
        if( is_null($record) )
        {
            return self::create($data);
        }
        else
        {
            return self::where($keys)->update($data);
        }
    }

    /**
     * Listar anotações, para exição para o paciente
     */
    public static function listarNotasPaciente( int $usuario_id ) : array
    {
        $notas = self::whereUsuarioId( $usuario_id )
                    ->where('relacionado_agenda_id', '!=', null) // apenas com relacionamento de agenda
                    ->where('conteudo', '!=', null)
                    ->orderBy('id', 'DESC')
                    ->get();

        $retorno = $notas ? $notas->toArray() : [];

        // complementando os dados das notas
        if( sizeof($retorno) > 0 )
        {
            foreach( $retorno as $k => $nota )
            {
                $retorno[$k]['agenda'] = (array)Agenda::detalharItem( $nota['relacionado_agenda_id'] );
            }
        }

        return $retorno;
    }

    /**
     * Listar anotações, para exição para o profissional
     * este precisa de um agrupamento por paciente
     */
    public static function listarNotasProfissional( int $usuario_id ) : array
    {
        $notas = self::whereUsuarioId( $usuario_id )
                    ->where('relacionado_agenda_id', '!=', null) // apenas com relacionamento de agenda
                    ->where('conteudo', '!=', null)
                    ->orderBy('id', 'DESC')
                    ->get();

        $notas = $notas ? $notas->toArray() : [];
        $retorno = [];

        // complementando os dados das notas
        if( sizeof($notas) > 0 )
        {
            foreach( $notas as $k => $nota )
            {
                $agenda = (array)Agenda::detalharItem( $nota['relacionado_agenda_id'] );

                if(!isset($agenda['paciente_usuario_id']))
                    $agenda['paciente_usuario_id'] = array();
                else{
                    $retorno[$agenda['paciente_usuario_id']]['paciente'] = [
                        'paciente_id'       => isset($agenda['paciente_id']) ? $agenda['paciente_id'] : null,
                        'usuario_id'        => isset($agenda['paciente_usuario_id']) ? $agenda['paciente_usuario_id'] : null,
                        'nome'              => isset($agenda['nome_paciente']) ? $agenda['nome_paciente'] : null,
                        'sobrenome'         => isset($agenda['sobrenome_paciente']) ? $agenda['sobrenome_paciente'] : null
                    ];
                $retorno[$agenda['paciente_usuario_id']]['lista'][$k]['agenda'] = $agenda;
                $retorno[$agenda['paciente_usuario_id']]['lista'][$k]['nota'] = $nota;
                }
            }
        }

        $retorno = array_values($retorno);

        return $retorno;
    }

    /**
     * Listar as anotações que o usuário informado
     * Tem em relação com o outro usuário
     * As notas retornadas, são apenas as criadas pelo usuário 1
     */
    public static function minhaComUsuarioId( int $meuUsuarioId, int $relacionadoUsuarioId )
    {
        $retorno = [];

        // listando as notas que este usuário possui
        $lista = self::whereUsuarioId( $meuUsuarioId )
                    ->where('relacionado_agenda_id', '!=', null)
                    ->orderBy('id', 'desc')
                    ->get();

        $lista = $lista ? $lista->toArray() : [];

        // percorrendo as consultas
        // para separar apenas as notas que o outro usuário informado está relacionado
        if( sizeof( $lista ) > 0 )
        {
            foreach( $lista as $k => $nota )
            {
                $agenda = (array)Agenda::detalharItem( $nota['relacionado_agenda_id'] );
                if(isset($agenda['paciente_usuario_id'])){
                    if( $agenda['paciente_usuario_id'] == $relacionadoUsuarioId || $agenda['profissional_usuario_id'] == $relacionadoUsuarioId )
                    {
                        $retorno[] = $nota;
                    }
                }
            }
        }

        return $retorno;
    }
}
