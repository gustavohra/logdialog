<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

class VideoChamada extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_videochamada';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'agenda_id',
		'data_inicio_chamada',
		'data_fim_chamada'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
    	'agenda_id'
    ];
}
