<?php

namespace LogDialog\Model;

// utilitários
use DB;
use Auth;
use Carbon\Carbon;
use Avatar;

use Illuminate\Database\Eloquent\Model;

// carregando models
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\TipoNotificacao as TipoNotificacao;

class Notificacao extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_notificacao';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'para_usuario_id',
        'relacionado_usuario_id',
        'tipo_notificacao_id',
        'texto',
        'exibir',
        'alerta'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'id',
        'data_registro',
        'para_usuario_id',
        'relacionado_usuario_id',
        'tipo_notificacao_id',
        'texto',
        'exibir',
        'alerta'
    ];

    /**
     * Função para retornar as notificações para o usuário informado
     * Retorna uma lista paginável
     */
    public static function listarNotificacoes( int $usuarioId, int $quantidade = 4, bool $apenasNovas = false )
    {
        $tmp = Notificacao::whereParaUsuarioId( $usuarioId )
                ->select('tb_notificacao.*', 'tb_tipo_notificacao.tipo')
                ->join('tb_tipo_notificacao', 'tb_tipo_notificacao.id', '=', 'tb_notificacao.tipo_notificacao_id');

        // if( $apenasNovas )
        //     $tmp->where('tb_notificacao.data_visualizada', null);
        
        $tmp = $tmp
        // ->where('tb_notificacao.exibir', 1)
                ->orderBy('tb_notificacao.id', 'DESC')
                ->paginate($quantidade);

        return $tmp;
    }
    /**
     * Função para retornar as notificações para o usuário informado
     * Retorna uma lista paginável
     */
    public static function getNotificacao( int $usuarioId, int $notificacaoId )
    {
        $tmp = Notificacao::whereParaUsuarioId( $usuarioId )
                ->select('tb_notificacao.*', 'tb_tipo_notificacao.tipo')
                ->join('tb_tipo_notificacao', 'tb_tipo_notificacao.id', '=', 'tb_notificacao.tipo_notificacao_id')
                ->where('tb_notificacao.id', $notificacaoId);
  

        return $tmp;
    }

    /**
     * Função para retornar o detalhamento do item informado
     * retorna um array
     */
    public static function detalharItem( int $item, string $tipo ) : array
    {
        $tmp = Notificacao::find( $item )->toArray();

        // pegando o tipo de notificação
        $tipo = TipoNotificacao::find( $tmp['tipo_notificacao_id'] );

        // adicionando itens comuns
        $tmp['avatar_notificacao'] = 'images/avatar-padrao.jpg';

        $tmp['nome_usuario'] = '';

        $tmp['tipo'] = $tipo['tipo'];

        // tranando o retorno de agendamento solicitado
        // de consulta
        if( 
            $tmp['relacionado_agenda_id'] > 0 &&
            $tmp['relacionado_agenda_id'] > 0
        )
        {
            $agenda = Agenda::detalharItem( $tmp['relacionado_agenda_id'] );

            // verificando se o usuário logado é paciente ou profissional
            if( Auth::user()->gruposistema() == 'paciente' )
            {
                $tmp['nome_usuario'] = "{$agenda->tratamento_profissional} {$agenda->nome_profissional} {$agenda->sobrenome_profissional}";

                $tmp['avatar_notificacao'] = strlen($agenda->avatar_profissional) > 0 ? $agenda->avatar_profissional : $tmp['avatar_notificacao'];
            }
            else
            {
                $tmp['nome_usuario'] = "{$agenda->nome_paciente} {$agenda->sobrenome_paciente}";

                $tmp['avatar_notificacao'] = strlen($agenda->avatar_paciente) > 0 ? $agenda->avatar_paciente : $tmp['avatar_notificacao'];
            }

            $tmp['data_compromisso'] = new Carbon($agenda->data_compromisso);
            $tmp['agenda_id'] = $agenda->agenda_id;

            $tmp['data_registro'] = new Carbon($tmp['data_registro']);
        }
        // quando o tipo for um novo inbox
        else if( $tmp['tipo'] == 'novo_inbox' )
        {
            $tmp['avatar_notificacao'] = Avatar::urlImagem( $tmp['relacionado_usuario_id'] );

            $paciente = Paciente::verDetalhes( $tmp['relacionado_usuario_id'] );

            if( sizeof($paciente) > 0 )
            {
                $tmp['nome_usuario'] = "{$paciente['nome']} {$paciente['sobrenome']}";
            }
            else
            {
                $profissional = Profissional::verDetalhes( $tmp['relacionado_usuario_id'] );

                $tmp['nome_usuario'] = "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}";
            }

            $tmp['data_registro'] = new Carbon($tmp['data_registro']);
        }
        // neste else, vão os campos padões, caso o item não seja encontrado no banco de dados
        // por qualquer motivo, talvez pelo tipo errado
        else
        {
            //
        }

        // retorno do resultado
        return $tmp;
    }
}
