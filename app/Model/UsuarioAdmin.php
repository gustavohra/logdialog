<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsuarioAdmin extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_usuario_has_administrador';

    // informando o campo de chave primária
    protected $primarykey = 'tb_usuario_id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = ['tb_usuario_id'];
 
}