<?php

namespace LogDialog\Model;

// utilitários
use DB;

use Illuminate\Database\Eloquent\Model;

class Especialidade extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_especialidade';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'profissional_id',
        'especialidade',
        'documento',
        'data_registro',
        'data_aprovado',
        'aprovado_usuario_id',
        'visibilidade'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
    	'aprovado_usuario_id',
        'profissional_id',
        'data_registro'
    ];

    /**
     * Função para carregar a lista com todas as especialidades do usuário id informado
     * 
     * @param id      $userId         [Usuário ID do profissional]
     * @param boolean $apenasAprovado [Recebe TRUE para listar apenas especialidades aprovadas]
     */
    public static function listaUsuarioId( int $userId, bool $apenasAprovado = false ) : array
    {
        $retorno = [];

        // recuperando id de profissional
        $id = \LogDialog\Model\Profissional::whereUsuarioId( $userId )
                        ->first();

        if( $id )
        {
            $lista = self::whereProfissionalId( $id->toArray()['id'] );

            if( $apenasAprovado )
                  $lista->where('data_aprovado', '!=', null);
                  
            $lista = $lista->orderBy('especialidade', 'ASC')
                           ->get();

            // caso exista lista
            if( $lista )
                $retorno = $lista->toArray();
        }

        return $retorno;
    }

    /**
     * Esta função retorna um valor booleano, para indicar se o profissional tem ou não a especialidade informada
     */
    public static function possuiEspecialidade( int $userId, string $especialidade, bool $apenasAprovado = false ) : bool
    {
        $retorno = false;

        // recuperando id de profissional
        $id = \LogDialog\Model\Profissional::whereUsuarioId( $userId )
                        ->first();

        if( $id )
        {
            $lista = self::whereProfissionalId( $id->toArray()['id'] );

            if( $apenasAprovado )
                  $lista->where('data_aprovado', '!=', null);
            
            $lista->where('especialidade', '=', $especialidade);
            $lista->first();

            $lista = $lista->first();

            // caso exista lista
            if( $lista )
                $retorno = true;
        }

        return $retorno;
    }
}
