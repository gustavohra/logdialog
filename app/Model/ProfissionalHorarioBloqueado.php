<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

class ProfissionalHorarioBloqueado extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_profissional_horario_bloqueado';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'profissional_id',
        'dia_horario'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
        'profissional_id',
        'id'
    ];

    /**
     * If the register exists in the table, it updates it. 
     * Otherwise it creates it
     * @param array $data Data to Insert/Update
     * @param array $keys Keys to check for in the table
     * @return Object
     */
    public static function createOrUpdate($data, $keys)
    {
        $record = self::where($keys)->first();
        
        if( is_null($record) )
        {
            return self::create($data);
        }
        else
        {
            return self::where($keys)->update($data);
        }
    }
}
