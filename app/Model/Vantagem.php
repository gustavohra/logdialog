<?php

namespace LogDialog\Model;

use Illuminate\Database\Eloquent\Model;

class Vantagem extends Model
{
    public $timestamps = false;

    // definindo um nome customizado para a tabela
    protected $table = 'tb_vantagem';

    // informando o campo de chave primária
    protected $primarykey = 'id';

    // lista de campos que podem ser modificados
    // necessário para garantir que apenas esses campos sejam modificados ou inseridos no banco
    // os demais serão ignorados, mesmo se informados pelo usuário
    protected $fillable = [
		'titulo',
        'conteudo',
        'imagem',
        'criado_por_usuario_id'
    ];

    // listagem de campos, que não podem ser modificados, mesmo se informados pelo usuário
    // é um tipo de black-list do sistema, para evitar updates, uma vez inseridos não podem ser modificados
    protected $guarded = [
        'id',
        'data_cadastro',
        'criado_por_usuario_id'
    ]; 
}
