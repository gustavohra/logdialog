<?php

namespace LogDialog;

use Illuminate\Foundation\Auth\User as Authenticatable;

// carregando models
use LogDialog\Model\GrupoSistema;

/**
 * Classe para controle de autenticação
 * Utilizado como padrão pelo laravel
 */
class User extends Authenticatable
{
    public $timestamps = false;
    protected $table = 'tb_usuario';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'senha',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senha', 'remember_token',
    ];

    /**
     * Retorna uma string
     * com o grupo do sistema
     */
    public function grupoSistema() : string
    {
        return GrupoSistema::select('grupo')->whereId( $this->grupo_sistema_id )->take(1)->get()->toArray()[0]['grupo'];
    }

    public function getAuthPassword()
    {
        return $this->senha;
    }
}
