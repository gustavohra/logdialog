<?php

namespace LogDialog\Providers;

use Illuminate\Support\ServiceProvider;

use Carbon\Carbon;

class CarbonLocaleProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // definindo i18n para o Carbon
        // isso faz com que o Carbon funcione com o mesmo locale que o Laravel
        setlocale(LC_ALL, 'pt_BR' );
        Carbon::setLocale('pt_BR');
    }
}
