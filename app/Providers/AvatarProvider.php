<?php

namespace LogDialog\Providers;

use Illuminate\Support\ServiceProvider;

use Auth;
use LogDialog\Model\Profissional;
use LogDialog\Model\Paciente;

class AvatarProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Esta função retorna o caminho URL do avatar do usuário informado
     * Se não tiver avatar, retorna a imagem padrão
     */
    public static function urlImagem( int $usuarioId ) : string
    {
        // caso esteja um número válido
        if( $usuarioId > 0 )
        {
            // verificando se está na tabela de profissionais
            $profissional = Profissional::whereUsuarioId( $usuarioId )->first();

            if( $profissional )
            {
                $profissional = $profissional->toArray();

                return asset( strlen($profissional['avatar']) > 0 ? "upload/user/{$usuarioId}/{$profissional['avatar']}" : 'images/avatar-padrao.jpg' );
            }
            // verificando paciente
            else
            {
                $paciente = Paciente::whereUsuarioId( $usuarioId )->first();

                if( $paciente )
                {
                    $paciente = $paciente->toArray();

                    return asset( strlen($paciente['avatar']) > 0 ? "upload/user/{$usuarioId}/{$paciente['avatar']}" : 'images/avatar-padrao.jpg' );
                }
                else
                {
                    // retornando padrão
                    return asset("images/avatar-padrao.jpg");
                }
            }
        }
        else
        {
            return asset("images/avatar-padrao.jpg");
        }
    }
}
