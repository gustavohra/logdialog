<?php

namespace LogDialog\Providers;

use Illuminate\Support\ServiceProvider;

// utilitários
use Carbon\Carbon;
use Calendario;

// models
use LogDialog\Model\ProfissionalDiaAtendimento;

class HorarioAtendimentoProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Esta função retorna um array com a listagem de horários de atendimento do sistema
     *
     * @param string $intervalo [recebe a definição 'de', 'ate', 'todos', 'primeiro', 'ultimo']
     */
    public static function lista( $intervalo = 'todos' ) : array
    {
        $horario = [];

        // o dia não importa neste caso
        // será considerado apenas o intervalo de horas
        $inicio = Carbon::parse("07:00:00");
        $fim    = Carbon::parse("22:00:00");

        $lista = Calendario::intervaloHoras( $inicio, $fim );

        foreach( $lista as $k => $item )
            $horario[] = $item->formatLocalized("%H:%M");

        // no caso de querer a listagem 'DE'
        $intervalo == 'de' ? array_pop( $horario ) : null;
        $intervalo == 'ate' ? array_shift( $horario ) : null;

        // no caso de querer apenas o primeiro horário
        $intervalo == 'primeiro' ? $horario = [ $horario[0] ] : null;

        // no caso de querer apenas o último horário
        $intervalo == 'ultimo' ? $horario = [ $horario[ count($horario) -1 ] ] : null;

        return $horario;
    }

    /**
     * Esta função simplesmente retorna um array com os dias da semana
     * De qualquer forma, é importante estar aqui para i18n
     * Utilizando qualquer data apenas como base para o Carbon
     */
    public static function diasSemana() : array
    {
        $dias = ['Domingo', 'Segunda', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];
        return $dias; // é importante essa ordem para o array, pois a chave inciando em 0, indica o primeiro dia da semana
    }

    /**
     * Esta função verifica se o horário informado é atendido pelo profissional informado
     *
     * Retorna um valor booleano
     *
     * O parâmetro $intervalo, recebe (intervalo, de, ate), para filtrar se estou verificando o horário de inicio de atentimento, limite ou dentro de um intervalo
     */
    public static function horarioAtendido( int $usuarioId, int $diaSemana, string $horario, string $intervalo = 'intervalo' ) : bool
    {
        return ProfissionalDiaAtendimento::atendeHorario( $usuarioId, $diaSemana, $horario, $intervalo );
    }

    /**
     * Esta função retorna um valor booleano, para indicar se um dia possui ou não atendimento
     * se o profissional trabalha ou não neste dia
     */
    public static function diaSemAtendimento( int $usuarioId, int $diaSemana ) : bool
    {
        return ProfissionalDiaAtendimento::diaSemAtendimento( $usuarioId, $diaSemana );
    }
}
