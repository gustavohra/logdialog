<?php

namespace LogDialog\Providers;

use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // copiando assets para o diretório público
        $this->publishes([
                     __DIR__ . '/../../vendor/kartik-v/bootstrap-fileinput/css' => public_path('bootstrap-fileinput/css'),
                     __DIR__ . '/../../vendor/kartik-v/bootstrap-fileinput/js' => public_path('bootstrap-fileinput/js'),
                     __DIR__ . '/../../vendor/kartik-v/bootstrap-fileinput/img' => public_path('bootstrap-fileinput/img'),
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
