<?php

namespace LogDialog\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class CarbonCalendarGenerateProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Esta função retorna o dia informado (timestamp)
     * No formato Carbon
     */
    public static function date( $timestamp ) : \Carbon\Carbon
    {
        $date = new \DateTime();

        if(strstr($timestamp,'/')){ 
            $timestamp = \DateTime::createFromFormat('d/m/Y', $timestamp);
            return Carbon::parse( $timestamp->format('Y-m-d H:i:s.u e'));
        }
        else
            return Carbon::parse( $date->setTimestamp($timestamp)->format('Y-m-d H:i:s.u e') );
    }

    /**
     * Esta função retorna o objeto do dia no formato Carbon
     * Mas a partir da data informada já no formato carbon
     * Avança um dia e retorna o objeto
     */
    public static function diaSeguinte( \Carbon\Carbon $dia ) : \Carbon\Carbon
    {
        return $dia->tomorrow();
    }

    /**
     * Esta função retorna o objeto do dia no formato Carbon
     * Mas a partir da data informada já no formato carbon
     * Volta um dia e retorna o objeto
     */
    public static function diaAnterior( \Carbon\Carbon $dia ) : \Carbon\Carbon
    {
        return $dia->yesterday();
    }

    /**
     * Esta função retorna um array com a listagem de dias
     * Do início e final da semana, primeiro e último dia
     * Com base no dia informado, já no formato Carbon
     *
     * A semana começa no domingo
     */
    public static function semanaCompleta( \Carbon\Carbon $dia ) : array
    {
        $tmp = Carbon::parse( $dia->toDateTimeString() );

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $sunday   = $dia->startOfWeek();
        $saturday = $tmp->endOfWeek();

        $numberOfDays = $sunday->diffInDays($saturday);
        
        $dates = [];

        foreach(range(0, $numberOfDays) as $day)
        {
            $dates[] = $saturday->copy()->subDays($day);
        }

        return array_reverse($dates);
    }

    /**
     * Esta função retorna um array com a listagem de dias
     * Do primeiro ao último dia do mês
     * Com base na data informada no timestamp
     * É entendido como mês corrente
     */
    public static function mesCompleto( \Carbon\Carbon $dia ) : array
    {
        $tmp = Carbon::parse( $dia->toDateTimeString() );

        $start = $dia->startOfMonth();
        $end   = $tmp->endOfMonth();

        $numberOfDays = $start->diffInDays($end);
        
        $dates = [];

        foreach(range(0, $numberOfDays) as $day)
        {
            $dates[] = $end->copy()->subDays($day);
        }

        return array_reverse($dates);
    }
 
    /**
     * Esta função retorna um array com a listagem de dias
     * Do primeiro ao último dia do ano
     * Com base na data informada no timestamp
     * É entendido como ano corrente
     */
    public static function anoCompleto( \Carbon\Carbon $dia ) : array
    {
        $tmp = Carbon::parse( $dia->toDateTimeString() );

        $start = $dia->startOfYear();
        $end   = $tmp->endOfYear();

        $numberOfDays = $start->diffInDays($end);
        
        $dates = [];

        foreach(range(0, $numberOfDays) as $day)
        { 
            $dates[] = $end->copy()->subDays($day);
        }

        return array_reverse($dates);
    }

    /**
     * Esta função retorna um array com a listagem de dias
     * Com base nos intervalos informados
     * já no formato Carbon
     */
    public static function intervaloDias( \Carbon\Carbon $diaInicio, \Carbon\Carbon $diaFim ) : array
    {
        $numberOfDays = $diaFim->diffInDays($diaInicio);

        $dates = [];

        foreach(range(0, $numberOfDays) as $day)
        {
            $dates[] = $diaFim->copy()->subDays($day);
        }

        return array_reverse($dates);
    }

    /**
     * Esta função retorna um array com a listagem de horários
     * Com base nos intervalos informados
     * já no formato Carbon
     */
    public static function intervaloHoras( \Carbon\Carbon $horaInicio, \Carbon\Carbon $horaFim ) : array
    {
        $numberOfHours = $horaFim->diffInHours($horaInicio);

        $hours = [];

        foreach(range(0, $numberOfHours) as $hour)
        {
            $hours[] = $horaFim->copy()->subHours($hour);
        }

        return array_reverse($hours);
    }
}
