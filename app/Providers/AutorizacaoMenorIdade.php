<?php

namespace LogDialog\Providers;

use Illuminate\Support\ServiceProvider;

// models
use LogDialog\Model\GrupoSistema;
use LogDialog\Model\Usuario;
use LogDialog\Model\Profissional;
use LogDialog\Model\Paciente;

// utilitários
use Carbon\Carbon;

class AutorizacaoMenorIdade extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Esta função verifica para o usuário id informado
     * Se ele é menor de idade
     * retorna um valor booleano para o resultado
     */
    public static function isMenorIdade( int $usuarioId ) : bool
    {
        // retorno
        $isMenor = false;

        $nascimento = Paciente::whereUsuarioId($usuarioId)
                        ->limit(1)
                        ->get()
                        ->toArray()[0]['nascimento'] ?? date('Y-m-d');

        $hoje = Carbon::now();
        $nascimento = Carbon::parse($nascimento);

        $isMenor = $hoje->diffInYears( $nascimento ) < 18;

        return $isMenor;
    }

    /**
     * Esta função verifica se o usuário é menor de idade
     * Se for, analisa se o agendamento é permitido
     * retorna um valor booleano
     */
    public static function agendamentoPermitido( int $usuarioId ) : bool
    {
        $allowed = true;

        // apenas é necessário checagem, se ele for menor de idade
        if( self::isMenorIdade( $usuarioId ) )
        {
            // por padão fica bloqueado
            $allowed = false;

            // verificando apenas no perfil de paciente se ele possui autorização
            // devidamente aprovada no sistema
            $tmp = Paciente::whereUsuarioId($usuarioId)
                        ->limit(1)
                        ->get()
                        ->toArray();

            $allowed = isset($tmp[0]) ? $tmp[0]['documento_menor_aceito_por_usuario_id'] > 0 : $allowed;
        }

        return $allowed;
    }

    /**
     * Esta função verifica se o documento já foi enviado
     */
    public static function documentoEnviado( int $usuarioId ) : bool
    {
        $enviado = true;

        // apenas é necessário checagem, se ele for menor de idade
        if( self::isMenorIdade( $usuarioId ) )
        {
            // por padão fica negado
            $enviado = false;

            // verificando apenas no perfil de paciente se enviou o documento
            $tmp = Paciente::whereUsuarioId($usuarioId)
                        ->limit(1)
                        ->get()
                        ->toArray();

            $enviado = isset($tmp[0]) ? strlen( $tmp[0]['documento_autorizacao_menor'] ) > 0 : $enviado;
        }

        return $enviado;
    }
}
