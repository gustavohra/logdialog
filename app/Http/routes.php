<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middlewareGroups' => ['web']], function(){

    /**
     * Não permitir a submissão de tags html
     * Para nenhuma requisição
     */
    /*$input = Request::all();

    if( sizeof($input) > 0 && is_array($input) )
    {
        array_walk_recursive($input, function(&$input){
            $input = strip_tags($input);
        });

        Request::merge($input);
    }*/


    /**
     * Rota da tela inicial (home)
     */
    Route::get('/', 'IndexController@index');

    
    /**
     * Rota das páginas
     */
    Route::get('institucional', 'IndexController@index');
    Route::get('institucional/{all}', 'PaginaController@index');

    /**
     * Rota da tela de login (admin)
     */
    Route::get('admin', 'AdminController@index');
    Route::match(['post', 'get'], 'admin/login', 'AdminController@Login');

    /**
     * Rota quando há um aviso na home
     */
    Route::group(['prefix' => 'aviso'], function(){
        Route::get('/{aviso?}', 'IndexController@aviso');
    });

    /**
     * Executar login
     */
    Route::match(['post', 'get'], '/login', 'LoginController@login');
    Route::match(['post', 'get'], '/logout', 'LoginController@logout');

    /**
     * Tela de ativação de perfil
     * Quando o cadastro é feito
     */
    Route::get('/ativar/{userId}', 'IndexController@ativarPerfil');

    /**
     * Redefinição de senha
     */
    Route::group(['prefix' => 'senha'], function(){
        /**
         * Solicitar redefinição
         */
        Route::post('/', 'LoginController@solicitarNovaSenha');

        /**
         * Acesso para redefinição
         */
        Route::match(['get', 'post'], '/redefinir/{userId}/{token}', 'LoginController@telaNovaSenha');
    });

    /**
     * Rota para a tela de cadastro
     * Que pode receber requisições GET e POST
     */
    Route::match(['get', 'post'], '/cadastro', 'CadastroController@registrar');

    Route::match(['post'], '/cadastro/paciente', 'CadastroController@paciente');
    Route::match(['get'], '/cadastro/paciente', 'CadastroController@registrar');

    Route::match(['post'], '/cadastro/profissional', 'CadastroController@profissional');
    Route::match(['get'], '/cadastro/profissional', 'CadastroController@registrar');

    /**
     * Rota para a tela de perguntas (orientação)
     * Que pode receber requisições GET e POST
     */
    Route::match(['get', 'post'], '/orientacao/resultado', 'OrientacaoController@resultado');

    /**
     * Rota para a tela de perguntas (orientação)
     */
    Route::match(['get'], '/orientacao/id/{pergunta}/res/{resposta}', 'OrientacaoController@pergunta');

    /**
     * Tela de como funciona
     */
    Route::get('/como-funciona', 'ComoFuncionaController@index');

    /**
     * Tela com a listagem de profissionais
     */
    Route::get('/profissionais', 'ProfissionaisController@index');

    /**
     * Tela de contato
     */
    Route::match(['post'], '/contato/enviar', 'ContatoController@enviar');

    /**
     * Tela de login
     */
    Route::match(['get', 'post'], '/login', 'LoginController@index');

    /**
     * Cadastro de newsletter
     */
    Route::match(['post'], '/newsletter/cadastro', 'NewsletterController@cadastro');

    /**
     * Telas institucionais
     */
    // Route::get('/institucional', 'InstitucionalController@index');
    // Route::get('/institucional/sobre', 'InstitucionalController@sobre');
    // Route::get('/institucional/seja-nosso-parceiro', 'InstitucionalController@sejaNossoParceiro');
    // Route::get('/institucional/politica-privacidade', 'InstitucionalController@politicaPrivacidade');
    // Route::get('/institucional/seguranca', 'InstitucionalController@seguranca');
    // Route::get('/institucional/trabalhe-conosco', 'InstitucionalController@trabalheConosco');
    // Route::get('/institucional/vendas-corporativas', 'InstitucionalController@vendasCorporativas');
    // Route::get('/institucional/termos-de-uso', 'InstitucionalController@termosdeuso');
    // Route::get('/institucional/termos-profissionais', 'InstitucionalController@termosprofissionais');
    // Route::get('/institucional/politica-pagamento-reembolso', 'InstitucionalController@politicaPagamentoReembolso');
    // Route::get('/institucional/contrato-psicologo', 'InstitucionalController@contratoPsicologo');
    // Route::get('/institucional/perguntas-frequentes', 'InstitucionalController@perguntasFrequentes');

    /**
     * Telas de informacoes
     */
    Route::get('/informacoes', 'InformacoesController@index');
    Route::get('/informacoes/trocas-devolucoes', 'InformacoesController@trocasDevolucoes');
    Route::get('/informacoes/formas-pagamento', 'InformacoesController@formasPagamento');
    Route::get('/informacoes/opcoes-entrega', 'InformacoesController@opcoesEntrega');
    Route::get('/informacoes/cancelamentos', 'InformacoesController@cancelamentos');
    Route::get('/informacoes/oferta-relampago', 'InformacoesController@ofertaRelampago');
    Route::get('/informacoes/seja-fornecedor', 'InformacoesController@sejaFornecedor');
 
    /**
     * Área onde as ligações são feitas
     */
    Route::group(['prefix' => 'conferencia'], function(){

        /**
         * Rota para sala de espera da consulta
         */
        Route::match(['get', 'post'], '/sala-de-espera/{agendaId}', 'ConferenciaController@espera');

        /**
         * Rota de consulta real via video conferência
         */
        Route::match(['get', 'post'], '/consulta/{agendaId}', 'ConferenciaController@consulta');

        /**
         * Rota apenas para verificação se nos próximos minutos haverá uma conferência
         */
        Route::match(['get', 'post'], '/verificar/proxima', 'ConferenciaController@verificarProxima');

        /**
         * Encerramento de consulta
         */
        Route::match(['get', 'post'], '/encerrar/{agendaId}', 'ConferenciaController@encerrar');

        /**
         * As rotas abaixo são somente de teste, devem ser removidas para produção
         * Não são acessíveis para produção
         */
        if( App::environment() != 'production' )
        {
            /**
             * Área onde as ligações são feitas
             */
            Route::match(['get'], '/conferencia', 'ConferenciaController@index');

            Route::match(['get', 'post'], '/voz', 'ConferenciaController@voz');
            Route::match(['get', 'post'], '/mensagem', 'ConferenciaController@mensagem');
            Route::match(['get', 'post'], '/video', 'ConferenciaController@video');
            Route::match(['get', 'post'], '/teste-video', 'ConferenciaController@testeVideo');
        }
    });

    /**
     * Rotas de acesso à área logada
     */
    Route::group(['prefix' => 'meu'], function(){
         /**
          * Rotas de acesso ao perfil
          */
         Route::group(['prefix' => 'perfil'], function(){
            /**
             * Acesso à tela de perfil
             */
            Route::get('/', 'PerfilController@meu');

            /**
             *
             *
             * Itens relacionados ao paciente
             *
             *
             */

            /**
             * Salvar nova foto do paciente
             */
            Route::post('/trocar/avatar/paciente/{id}', 'PerfilController@trocarFotoPaciente');

            /**
             * Editar perfil do paciente
             */
            Route::post('/editar/paciente/{id}', 'PerfilController@editarPaciente');

            /**
             * Anexar autorização para menor de idade
             */
            Route::post('/anexar/autorizacao/menor/{id}', 'PerfilController@anexarAutorizacaoMenor');

            /**
             *
             *
             * Itens relacionados ao profisional
             *
             *
             */
 
            /**
             * Salvar nova foto do profissional
             */
            Route::post('/trocar/avatar/profissional/{id}', 'PerfilController@trocarFotoProfissional');

            /**
             * Editar perfil do profissional
             */
            Route::post('/editar/profissional/{id}', 'PerfilController@editarProfissional');

            /**
             * Nova especialidade do profissional
             */
            Route::post('/nova/especialidade/{id}', 'PerfilController@novaEspecialidade');

            /**
             * Editar currículo
             */
            Route::post('/atualizar/curriculo/{id}', 'PerfilController@atualizarCurriculo');

            /**
             * Editar valores
             */
            Route::post('/atualizar/valores', 'PerfilController@atualizarValores');

            Route::post('/atualizar/valores/{id}', 'PerfilController@atualizarValores');

            /**
             * Atualizar horário de atendimento
             */
            Route::post('/atualizar/horario-atendimento/{id}', 'PerfilController@atualiarHorarioAtendimento');

            /**
             * Salvar um bloqueio de horário
             */
            Route::post('/gravar/horario-bloqueado/{id}', 'PerfilController@salvarBloqueioHorario');

            /**
             *
             *
             * Itens gerais
             *
             *
             */

            /**
             * Salvar nova senha
             */
            Route::post('/trocar/senha/{id}', 'PerfilController@trocarSenha');
         });
    });

    /**
     * Tela com a listagem de notificações
     */
    Route::get('/notificacoes', 'NotificacoesController@index');
    /**
     * Visualização de notificação
     */
    Route::group(['prefix' => 'visualizar'], function(){
        Route::get('/{visualizar?}', 'NotificacoesController@visualizar');
    });

    /**
     * Controller API
     */
    Route::get('/api', 'ApiController@index');
    Route::post('/api', 'ApiController@index');

    /**
     * Acesso à tela com a lista de minhas consultas
     */
    Route::get('/minhas/consultas', 'AgendaController@minhasConsultas');
    /**
     * Acesso à tela com a lista de minhas consultas
     */
    Route::get('/minhas/consultas/cancelar', 'AgendaController@cancelarConsulta');

    /**
     * Rotas relacionadas aos profissionais
     */
    Route::group(['prefix' => 'profissionais'], function(){
        /**
         * Tela com a listagem de profissionais
         */
        Route::get('/', 'ProfissionaisController@index');

        /**
         * Tela com a agenda completa do profissional
         */
        Route::get('/agenda/{usuarioId}/{timestamp?}', 'ProfissionaisController@agendaCompleta');

        /**
         * Tela para executar o agendamento com o profissional
         */
        Route::match(['get', 'post'], '/agendar-consulta/{profissionalUsuarioId}', 'ProfissionaisController@agendarConsulta');

        /**
         * Tela para executar o agendamento com o profissional, utilizando o modal do sistema
         */
        Route::match(['get', 'post'], '/agendar-consulta-modal/{profissionalUsuarioId}', 'ProfissionaisController@agendarConsultaModal');

        /**
         * Esta rota serve apenas para retornar em formato JSON
         * As ifnormações de valores e agenda do profissional.
         * Para as telas onde será solicitado agendamento.
         */
        Route::get('/api/agenda/{id}', 'ProfissionaisController@APIAgenda');

        /**
         * Tela para gerar token financeiro
         */
        Route::get('/financeiro/gerarToken', 'FinanceiroController@gerar_token');

        /**
         * Tela para callback financeiro
         */
        Route::get('/financeiro/callback', 'FinanceiroController@token_callback');

    });

    Route::group(['prefix' => 'paciente'], function(){
        Route::get('/financeiro/realizarPagamento', 'FinanceiroController@realizarPagamento');
        Route::get('/financeiro/gerarBoleto', 'FinanceiroController@gerarBoleto');
        Route::get('/financeiro/visualizarBoleto', 'FinanceiroController@visualizarBoleto');
        Route::post('/financeiro/pagamentoCartao', 'FinanceiroController@pagamentoCartao');
        Route::get('/financeiro/webook', 'FinanceiroController@webook');

        
    });
    /**
     * API para validações remotas de formulário
     */
    Route::group(['prefix' => 'validar'], function(){
        // validação de documentos
        Route::get('/documento', 'Validador\DocumentoController@validar');

        // validação duplicidade de CPF
        Route::get('/documento/paciente/cpf', 'Validador\DocumentoController@duplicidadeCpfPaciente');

        // validação duplicidade de CPF
        Route::get('/documento/profissional/cpf', 'Validador\DocumentoController@duplicidadeCpfProfissional');

        // validação de duplicidade de especialidade do profissional
        Route::get('/especialidade/profissional', 'PerfilController@duplicidadeEspecialidade');
    });

    /**
     * API para requisições referentes ao calendário
     */
    Route::group(['prefix' => 'calendario'], function(){
        // agenda do dia informado
        Route::post('/dia/{timestamp}', 'Calendario\CalendarioController@dia');

        // agenda do intervalo informado
        Route::post('/intervalo/{timestampInicio}/{timestampFim}', 'Calendario\CalendarioController@intervalo');

        // confirmar consulta
        Route::post('/confirmar-consulta', 'AgendaController@confirmarConsulta');

        // recusar consulta
        Route::post('/recusar-consulta', 'AgendaController@recusarConsulta');
    });

    /**
     * API para requisições referentes ao calendário
     */
    Route::group(['prefix' => 'anotacao'], function(){
        // solicitar dados de anotações
        Route::match(['get', 'post'], '/consulta', 'AgendaController@carregarAnotacoesConsulta');

        // excluir anotação
        Route::match(['get', 'post'], '/excluir', 'AgendaController@excluirNota');

        // atualizar anotação
        Route::match(['get', 'post'], '/atualizar', 'AgendaController@atualizarNota');

        // anotação durante a consulta
        Route::match(['get', 'post'], '/durante-consulta', 'AgendaController@notaConsulta');
    });

    /**
     * API para requisições referentes à agenda do profissional
     */
    Route::group(['prefix' => 'agenda-profissional'], function(){
        // retornar os horários disponíveis para o profisisonal e o dia informado
        Route::post('/horarios-disponiveis/{profissionalUsuarioId}', 'ProfissionaisController@horariosDisponiveis');
    });

    /**
     * Requisições referentes às funções de inbox
     */
    Route::group(['prefix' => 'inbox'], function(){
        // retornar as mensagens
        Route::match(['get', 'post'], '/carregar-mensagens', 'MensagemController@APICarregarMensagens');

        // retornar os contatos
        Route::post('/carregar-contatos', 'MensagemController@APICarregarContatos');

        // nova mensagem
        Route::post('/nova-mensagem', 'MensagemController@APINovaMensagem');

        // carregar dados do usuário
        // para complementar a tela no caso de nova mensagem
        // com um contato novo
        Route::post('/dados-usuario', 'MensagemController@APIDadosUsuario');
    });

    /**
     * Redirecionamento pagseguro do checkout
     */
    Route::get('/pagseguro/redirect', [
        'as' => 'pagseguro.redirect',
        'uses' => 'PagSeguro@redirect'
    ]);

    /**
     * Redirecionamento pagseguro de notifications
     */
    Route::get('/pagseguro/notification', [
        'as' => 'pagseguro.notification',
        'uses' => 'PagSeguro@notification'
    ]);
});
// por enquanto, essa rota não é necessária
//Route::get('/home', 'HomeController@index');