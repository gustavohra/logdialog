<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando models
use LogDialog\Model\OrientacaoPergunta as OrientacaoPergunta;
use LogDialog\Model\OrientacaoResposta as OrientacaoResposta;
use LogDialog\Model\UsuarioOrientacao as UsuarioOrientacao;

// carregando utilitários
use Auth;
use Session;

class OrientacaoController extends Controller
{
    /**
     * Carregar e processar perguntas
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function pergunta(Request $request, int $perguntaId, int $respostaId)
    {
        // chegando até aqui, verifica se o usuário possui algum id de sessão
        // se não tiver, gera para colocar no cadastro
        session_start();
        
        if( !isset($_SESSION['id']) )
            $_SESSION['id'] = \Session::getId();

        // pegando a pergunta inicial
        // caso não seja passado no parâmetro
        if( $perguntaId < 1 )
        {
            $pergunta = OrientacaoPergunta::where('inativo', 0)
                                               ->first()
                                               ->toArray();

            return view('orientacao.index')
                       ->with('pergunta', $pergunta);
        }
        // caso tenha respondido
        // armazenar no banco
        // e pegar a próxima que ele não respondeu
        // se responseu tudo, renderiza o resultado
        else
        {
            // pegando respostas do usuário
            $respUser = UsuarioOrientacao::where('sessao_token', $_SESSION['id'])->get();

            if( $respUser )
            {
                $respUser = $respUser->toArray();

                $tmp = [];

                foreach( $respUser as $k => $v )
                    $tmp[] = $v['pergunta_id'];

                $respUser = $tmp;
            }
            else
            {
                $respUser = array();
            }

            // cadastrando a resposta deste
            // apenas se o usuário ainda não tiver
            $verificar = UsuarioOrientacao::where('sessao_token', $_SESSION['id'])
                                                ->where('pergunta_id', $perguntaId)
                                                ->first();
            
            // verificando se a pergunta e resposta existem
            $existePergunta = OrientacaoPergunta::find($perguntaId);
            $existeResposta = OrientacaoResposta::find($respostaId);

            // cadastrando se não existir
            if( !$verificar )
            {
                $resposta = new UsuarioOrientacao;
                $resposta->sessao_token = $_SESSION['id'];

                // caso exista a pergunta
                if( $existePergunta )
                    $resposta->pergunta_id = $perguntaId;

                // caso exista a resposta
                if( $existeResposta )
                    $resposta->resposta_id = $respostaId;
                else
                    $resposta->resposta_id = 0;

                // verificar se tem autenticação
                if( Auth::check() )
                    $resposta->respondido_usuario_id = Auth::id();

                $resposta->save();
            }

            // verificando se há mais a ser respondido
            $pergunta = OrientacaoPergunta::where('inativo', 0)
                                               ->where('id', '!=', $perguntaId)
                                               ->whereNotIn('id', $respUser)
                                               ->first();

            // se ainda tiver pergunas
            if( $pergunta )
            {
                // pegando array
                $pergunta = $pergunta->toArray();

                return view('orientacao.index')
                       ->with('pergunta', $pergunta);
            }
            // ou caso seja necessário mostrar o resultado
            else
            {
                // pegando resultados do usuário
                $resultado = UsuarioOrientacao::where('tb_usuario_orientacao.sessao_token', $_SESSION['id'])
                                   ->join('tb_orientacao_resposta', function($join){
                                        $join->on('tb_orientacao_resposta.id', '=', 'tb_usuario_orientacao.resposta_id');
                                   })
                                   ->join('tb_orientacao_pergunta', function($join){
                                        $join->on('tb_orientacao_pergunta.resposta_sim', '=', 'tb_orientacao_resposta.id');
                                   })
                                   ->get([
                                        'tb_orientacao_pergunta.titulo',
                                        'tb_orientacao_resposta.resposta'
                                    ])
                                   ->toArray();

                // renovando token, para que seja possível responder novamente
                \Session::regenerate();

                unset($_SESSION['id']);

                // retornando view
                return view('orientacao.resposta')
                       ->with('resultado', $resultado);
            }
        }
    }
}
