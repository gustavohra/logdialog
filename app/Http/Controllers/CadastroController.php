<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

use Validator;
use Redirect;
use View;
use Carbon\Carbon;

// carregando model
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\AdminServicoPadrao as AdminServicoPadrao;

// carregando utilitários
use Auth;
use Hash;
use Illuminate\Support\Facades\Input;
use \Illuminate\Support\MessageBag as MessageBag;

// utilitário para disparo de e-mail
use Mail;

class CadastroController extends Controller
{
    /**
     * Tela para cadastro
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function registrar()
    {
    	return view('cadastro.registrar');
    }

    /**
     * Recebe os dados de cadastro de pacientes
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function paciente(Request $request)
    {
    	$dados = $request->all();

    	// montando a data mínima
    	$hoje = Carbon::today();
    	$maiorIdade = $hoje->subYears(18);

    	$rules = [
            'email' => 'required|email',
            'password' => 'required|min:6|max:150',
            'name' => 'required',
            'lastname' => 'required',
            'birth' => 'required|date_format:"d/m/Y"|before:' . $maiorIdade,
            'gender' => 'required',
            'passwordconfirmation' => 'required'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.',
            'email' => 'O campo :attribute deve conter um email válido.',

            // personalização de campo, por causa de tradução
            'password.required' => 'O campo senha é obrigatório.',
            'password.min' => 'O campo senha deve conter no mínimo :min caracteres.',
            'password.max' => 'O campo senha deve conter no máximo :max caracteres.',
            'passwordconfirmation.required' => 'O campo de confirmação de senha é obrigatório.',

            'name.required' => 'O campo nome é obrigatório.',
            'lastname.required' => 'O campo sobrenome é obrigatório.',
            'birth.required' => 'O campo data de nascimento é obrigatório.',
            'birth.before' => 'O cadastro é permitido apenas para maiores de 18 anos.',
            'birth.format' => 'A data de nascimento possui um formato inválido.',
            'gender.required' => 'O campo sexo é obrigatório.',
        ];

    	$validation = Validator::make(
		    $dados,
		    $rules,
		    $messages
		);

        // chegando até aqui, verifica se o usuário possui algum id de sessão
        // se não tiver, gera para colocar no cadastro
        session_start();
        
        if( !isset($_SESSION['id']) )
        {
            $dados['sessao_token_registro'] = \Session::getId();
            $_SESSION['id'] = $dados['sessao_token_registro'];
        }
        else
        {
            $dados['sessao_token_registro'] = $_SESSION['id'];
        }

    	// no caso de falha de validação
		if( $validation->fails() )
		{
		    $errors = $validation->messages();

		    // redirecionando com os erros
		    // para exibir ao usuário
		   	$request->flash();

			return View::make('cadastro.registrar')
						->withInput($request->all())
						->withErrors($errors)
						->with('tab', 'register-user');
		}
		// agora no caso de estar tudo válido
		else
		{
			// executando cadastro
			$status = Paciente::cadastrar( $dados );

			// caso seja uma string, trata-se de um erro, renderizar a view com a mensagem
			if( is_array($status) )
			{
                $errors = new MessageBag($status);

				return View::make('cadastro.registrar')
						->withInput($request->all())
						->with('tab', 'register-user')
						->withErrors($errors);
			}
			else
			{
				// recuperando dados da requisição
		    	$credenciais = array(
		    			'username' => $dados['email'],
		    			'senha' => $dados['password']
		    		);

		    	$usuario = \LogDialog\User::whereUsername($request->get('email'))->first();

		    	if($usuario && Hash::check($request->get('password'), $usuario->senha))
		    	{
                    // disparando e-mail de ativação de perfil de paciente
                    Mail::send('email-mkt.ativar-perfil-paciente', ["userId" => $usuario->id], function ($mail) use ($request, $dados) {
                        $mail->from('logdialog@criaturo.com', 'LogDialog');

                        $mail->to($request->get('email'), "{$dados['name']} {$dados['lastname']}")->subject("{$dados['name']}, Bem vindo ao LogDialog!");
                    });

                    // redirecionando para a home
				    return redirect(action('IndexController@aviso', ["aviso" => "ativar-email"]));
				}
				else
				{ 
				   return redirect(action('CadastroController@registrar'));
				}
			}
		}
    }
    
    /**
     * Recebe os dados de cadastro de profissionais
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function profissional(Request $request)
    {
        $dados = $request->all();

        // montando a data mínima
        $hoje = Carbon::today();
        $maiorIdade = $hoje->subYears(18);

        $rules = [
            'prof_email' => 'required|email',
            'prof_password' => 'required|min:6|max:150',
            'prof_name' => 'required',
            'prof_lastname' => 'required',
            'prof_birth' => 'required|date_format:"d/m/Y"|before:' . $maiorIdade,
            'prof_gender' => 'required',
            'prof_passwordconfirmation' => 'required',

            // documentos
            'nr_crp' => 'required',
            'crp' => 'required|image|mimes:jpeg,png,jpg,gif,svg,tiff,targa,bmp|max:4000',
            'rg_cnh' => 'required|image|mimes:jpeg,png,jpg,gif,svg,tiff,targa,bmp|max:4000',
            'diploma' => 'required|image|mimes:jpeg,png,jpg,gif,svg,tiff,targa,bmp|max:4000',
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.',
            'email' => 'O campo :attribute deve conter um email válido.',
            'image' => 'Só é permitido o envio de arquivos de imagem',
            'mimes' => 'Só são permitidos arquivos do tipo: :values',

            // personalização de campo, por causa de tradução
            'prof_password.required' => 'O campo senha é obrigatório.',
            'prof_password.min' => 'O campo senha deve conter no mínimo :min caracteres.',
            'prof_password.max' => 'O campo senha deve conter no máximo :max caracteres.',
            'prof_passwordconfirmation.required' => 'O campo de confirmação de senha é obrigatório.',

            'prof_name.required' => 'O campo nome é obrigatório.',
            'prof_lastname.required' => 'O campo sobrenome é obrigatório.',
            'prof_birth.required' => 'O campo data de nascimento é obrigatório.',
            'prof_birth.before' => 'O cadastro é permitido apenas para maiores de 18 anos.',
            'prof_birth.format' => 'A data de nascimento possui um formato inválido.',
            'prof_gender.required' => 'O campo sexo é obrigatório.',

            'nr_crp.required' => 'O número do CRP é obrigatório',

            'crp.required' => 'O arquivo do CRP é obrigatório',
            'rg_cnh.required' => 'O arquivo do RG ou CNH é obrigatório',
            'diploma.required' => 'O arquivo do Diploma é obrigatório',
        ];

        $validation = Validator::make(
            $request->all(),
            $rules,
            $messages
        );

        // chegando até aqui, verifica se o usuário possui algum id de sessão
        // se não tiver, gera para colocar no cadastro
        session_start();
        
        if( !isset($_SESSION['id']) )
        {
            $dados['sessao_token_registro'] = \Session::getId();
            $_SESSION['id'] = $dados['sessao_token_registro'];
        }
        else
        {
            $dados['sessao_token_registro'] = $_SESSION['id'];
        }

        // no caso de falha de validação
        if( $validation->fails() )
        {
            $errors = $validation->messages();

            // redirecionando com os erros
            // para exibir ao usuário
            // mantendo os campos
            $request->flash();

            return View::make('cadastro.registrar')
                        ->withInput($request->all())
                        ->withErrors($errors)
                        ->with('tab', 'register-psicologo');
        }
        // agora no caso de estar tudo válido
        else
        {
            // executando cadastro
            $status = Profissional::cadastrar( $dados );

            // caso seja uma string, trata-se de um erro, renderizar a view com a mensagem
            if( is_array($status) )
            {
                $errors = new MessageBag($status);

                // mantendo os campos
                $request->flash();

                return View::make('cadastro.registrar')
                        ->withInput($request->all())
                        ->withErrors($errors)
                        ->with('tab', 'register-psicologo');
            }
            else
            {
                $uploadPath = "upload/user/{$status}";

                // salvando documentos
                if( Input::file('crp')->isValid() )
                {
                    // armazenando na pasta
                    $file = "crp_" . microtime() . "." . pathinfo(Input::file('crp')->getClientOriginalName(), PATHINFO_EXTENSION);

                    $file = str_replace(' ', '-', $file);

                    Input::file('crp')->move($uploadPath, $file );

                    // atualizando com o caminho do arquivo
                    \LogDialog\Model\Profissional::where('usuario_id', $status)
                                                ->update(['documento_crp' => $file]);
                }

                // salvando documentos
                if( Input::file('diploma')->isValid() )
                {
                    // armazenando na pasta
                    $file = "diploma_" . microtime() . "." . pathinfo(Input::file('diploma')->getClientOriginalName(), PATHINFO_EXTENSION);

                    $file = str_replace(' ', '-', $file);

                    Input::file('diploma')->move($uploadPath, $file );

                    // atualizando com o caminho do arquivo
                    \LogDialog\Model\Profissional::where('usuario_id', $status)
                                                ->update(['documento_diploma' => $file]);
                }

                // salvando documentos
                if( Input::file('rg_cnh')->isValid() )
                {
                    // armazenando na pasta
                    $file = "rg_cnh_" . microtime() . "." . pathinfo(Input::file('rg_cnh')->getClientOriginalName(), PATHINFO_EXTENSION);

                    $file = str_replace(' ', '-', $file);

                    Input::file('rg_cnh')->move($uploadPath, $file );

                    // atualizando com o caminho do arquivo
                    \LogDialog\Model\Profissional::where('usuario_id', $status)
                                                ->update(['documento_identidade' => $file]);
                }

                // recuperando dados da requisição
                $credenciais = array(
                        'username' => $dados['prof_email'],
                        'senha' => $dados['prof_password']
                    );

                $usuario = \LogDialog\User::whereUsername($request->get('prof_email'))->first();

                // disparando e-mail de ativação de perfil de profissional
                Mail::send('email-mkt.ativar-perfil-profissional', ["userId" => $usuario->id], function ($mail) use ($request, $dados) {
                    $mail->from('logdialog@criaturo.com', 'LogDialog');

                    $mail->to($request->get('prof_email'), "{$dados['prof_name']} {$dados['prof_lastname']}")->subject("{$dados['prof_name']}, Bem vindo ao LogDialog!");
                });

                // redirecionando para a home
                return redirect(action('IndexController@aviso', ["aviso" => "ativar-email"]));
            }
        }
    }
}
