<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;
use Validator;
use Mail;
use DB;
use HorarioAtendimento;
use Calendario;
use \Illuminate\Support\MessageBag as MessageBag;

// carregando models
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Especialidade as Especialidade;
use LogDialog\Model\ProfissionalServico as ProfissionalServico;
use LogDialog\Model\ProfissionalHorarioBloqueado as ProfissionalHorarioBloqueado;
use LogDialog\Model\ProfissionalDiaAtendimento as ProfissionalDiaAtendimento;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\TipoNotificacao as TipoNotificacao;
use LogDialog\Model\Notificacao as Notificacao;

class ProfissionaisController extends Controller
{
    /**
     * Tela com os profissionais
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function index( Request $request )
    {
        // buscando todos os servicos prestados
        $servicos = ProfissionalServico::select(DB::raw('DISTINCT servico'))
                                        // apenas serviços já editados, pois ele mesmo salvou os valores
                                        ->where('data_modificacao', '!=', null)
                                        ->orderBy('servico', 'ASC')
                                        ->get()
                                        ->toArray();

        // buscando profissionais
        $profissionais = Profissional::select('*')
                         ->where('data_aprovada_documentacao', '!=', null); // apenas os que já possuem a documentação aprovada

        // no caso de ter filtros
        if( $request->has('nome') )
            $profissionais->where('nome', 'like', "%{$request->input('nome')}%");

        if( $request->has('sexo') )
            $profissionais->whereRaw("sexo IN('" . implode("','", $request->input('sexo')) . "')");

        // filtro por servicos prestados
        if( $request->has('servico') )
        {
            if( strlen($request->input('servico')) > 0 )
            {
                $lista = ProfissionalServico::select(DB::raw('DISTINCT profissional_id'))
                                            ->where('servico', '=', $request->input('servico'))
                                            // apenas serviços já editados, pois ele mesmo salvou os valores
                                            ->where('data_modificacao', '!=', null)
                                            // pegando apenas a coluna
                                            // retorna um array com apenas uma dimensão
                                            ->pluck('profissional_id')
                                            ->toArray();

                $profissionais->whereIn('id', $lista);
            }
        }

        // filtro de primeiro contato gratis
        if( $request->has('primeiro_gratis') )
            $profissionais->where('primeiro_contato_gratis', 1);

        // filtro por valor
        if( $request->has('valor_min') && $request->has('valor_max') )
        {
            $lista = ProfissionalServico::select('profissional_id')
                                        ->where('valor', '>=', $request->input('valor_min'))
                                        ->where('valor', '<=', $request->input('valor_max'))
                                        // apenas serviços já editados, pois ele mesmo salvou os valores
                                        ->where('data_modificacao', '!=', null)
                                        // pegando apenas a coluna
                                        // retorna um array com apenas uma dimensão
                                        ->pluck('profissional_id')
                                        ->toArray();

            $profissionais->whereIn('id', $lista);
        }

        // recuperando o valor mínimo dos serviços padrões
        $minimo = ProfissionalServico::where('servico_padrao_id', '>', 0)->orderBy('valor','ASC')->first();
        
        // recuperando o valor máximo dos serviços padrões
        $maximo = ProfissionalServico::where('servico_padrao_id', '>', 0)->orderBy('valor','DESC')->first();

        // caso não existam, setar um valor padrão
        $minimo = $minimo ? $minimo->toArray()['valor'] : 1;
        $maximo = $maximo ? $maximo->toArray()['valor'] : 1000;

        // pegar o valor mínimo e máximo informados como parâmetro
        $minimoStart = $request->input('valor_min') ?? $minimo;
        $maximoStart = $request->input('valor_max') ?? $maximo;

        if( $minimoStart > $maximoStart )
        {
            $tmp = $maximoStart;
            $maximoStart = $minimoStart;
            $minimoStart = $tmp;
        }

        // retornando view
    	return view('profissionais.index')
    			->with('profissionais', $profissionais->paginate(10))
                ->with('servicos', $servicos)
                // valores para filtro
                ->with('valorMinimo', floor($minimo))
                ->with('valorMaximo', ceil($maximo))

                ->with('valorMinimoStart', $minimoStart)
                ->with('valorMaximoStart', $maximoStart);
    }

    /**
     * Esta função
     * Executa uma consulta às opções de agendamento do profissional informado
     * E retorna sua agenda, para verificação de disponibilidade
     */
    public function APIAgenda( Request $request, $usuarioId )
    {
        // pegando o perfil do profissional
        $profissional = Profissional::whereUsuarioId($usuarioId)
                            ->limit(1)
                            ->get()
                            ->toArray()[0];

        // pegando os serviços prestados
        $servicos = ProfissionalServico::select(['id', 'servico', 'valor'])
                            ->whereProfissionalId($profissional['id'])
                            ->whereInativo(0)
                            ->get()
                            ->toArray();

        // @TODO: Aguardando definição de como listar a opção dos dias
        $diasDisponiveis = [
            '11/04/2016',
            '12/04/2016',
            '13/04/2016',
            '14/04/2016'
        ];

        echo '<pre>';
        print_r( $profissional );
        print_r( $servicos );
        print_r( $diasDisponiveis );

        // consultando os serviços prestados pelo profissional
    }

    /**
     * Esta função retorna os dados necessários
     * Para exibir as opções de agenda do profissional para o paciente
     */
    public static function opcoesAgendaProfissional( Request $request, int $usuarioId, $timestamp = null ) : array
    {
        //
    }

    /**
     * Tela com a agenda completa do profissional informado
     */
    public function agendaCompleta( Request $request, int $usuarioId, $timestamp = null )
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // verificando se o informado é um profissional
        if( Profissional::whereUsuarioId( $usuarioId )->first() )
        {
            // recuperando a semana completa
            // se foi informado um timestamp de inicio
            if( $timestamp )
                $semana = Calendario::semanaCompleta( Calendario::date( $timestamp ) );
            else
                $semana = Calendario::semanaCompleta( \Carbon\Carbon::today() );

            // recuperando id do profissional
            $profissionalId = Profissional::whereUsuarioId( $usuarioId )->first()->toArray()['id'];

            // buscando todos os dias e horários que ele marcou como bloqueado
            $horarioBloqueado = ProfissionalHorarioBloqueado::whereProfissionalId( $profissionalId )
                                ->where('dia_horario', '>=', $semana[0]->format('Y-m-d H:i:s')) // filtro inicia com o primeiro dia da semana carregada
                                ->groupBy('dia_horario')
                                ->limit(8) // não é necessário carregar todos os dias, pois será exibida apenas uma semana por vez
                                ->pluck('dia_horario');

            if( $horarioBloqueado )
                $horarioBloqueado = $horarioBloqueado->toArray();
            else
                $horarioBloqueado = [];

            // verificando os agendamentos
            $consultas = Agenda::whereProfissionalId( $profissionalId )
                         ->where('data_compromisso', '>=', $semana[0]->format('Y-m-d H:i:s')) // filtro inicia com o primeiro dia da semana carregada
                         ->where('data_cancelamento', '=', null)
                         ->where('data_confirmacao', '!=', null)
                         ->groupBy('data_compromisso')
                         ->limit(8) // não é necessário carregar todos os dias, pois será exibida apenas uma semana por vez
                         ->pluck('data_compromisso'); // não é necessário carregar todos os dias, pois será exibida apenas uma semana por vez

            if( $consultas )
                $consultas = $consultas->toArray();
            else
                $consultas = [];

            // verificando os horarios marcados como dia de atendimento do profissional
            $diaAtendimento = ProfissionalDiaAtendimento::whereProfissionalId( $profissionalId )
                              ->whereSemAtendimento(0)
                              ->where('horario_inicio', '!=', null)
                              ->where('horario_fim', '!=', null)
                              ->orderBy('dia_semana')
                              ->get();

            if( $diaAtendimento )
                $diaAtendimento = $diaAtendimento->toArray();
            else
                $diaAtendimento = [];

            // recuperando a listagem de horários de atendimento do sistema
            $horariosSistema = HorarioAtendimento::lista('todos');

            // monstando uma lista com todos os dias e horários indisponíveis
            $indisponiveis = [];
            $ocupados = [];

            foreach( $horarioBloqueado as $k => $v )
                $indisponiveis[] = \Carbon\Carbon::parse( $v )->format("Y-m-d H:00"); // não considerar os minutos, pois por enquando o sistema trabalha apenas com horas inteiras

            foreach( $consultas as $k => $v )
                $ocupados[] = \Carbon\Carbon::parse( $v )->format("Y-m-d H:00"); // não considerar os minutos, pois por enquando o sistema trabalha apenas com horas inteiras

            // selecionando os serviços prestados pelo profissional selecionado
            $servicos = ProfissionalServico::whereProfissionalId( $profissionalId )->get();

            if( $servicos )
                $servicos = $servicos->toArray();
            else
                $servicos = [];

            // variável com os próximos 60 dias
            // para dar as opções de dia ao usuário
            $opcoesDias = [];

            $tmp = Calendario::intervaloDias( \Carbon\Carbon::today(), \Carbon\Carbon::today()->addDays(60) );

            foreach( $tmp as $k => $dia )
                if( !HorarioAtendimento::diaSemAtendimento( $usuarioId, $dia->dayOfWeek ) )
                    $opcoesDias[] = $dia;
            
            // saída da view
            return view('profissionais.agenda-completa')
                   ->with('profissionais', Profissional::whereId($profissionalId)->get())
                   ->with('profissional', Profissional::verDetalhes( $usuarioId ))
                   ->with('indisponiveis', $indisponiveis)
                   ->with('ocupados', $ocupados)
                   ->with('semana', $semana)
                   ->with('servicos', $servicos)
                   ->with('opcoesDias', $opcoesDias)
                   ->with('horarioSistema', $horariosSistema);
        }
        else
        {
            // direcionando para a tela com a listagem de profissionais
            return redirect()->action('ProfissionaisController@index');
        }
    }

    /**
     * Esta função retorna um json com os horários disponíevis para o dia informado e o profissional
     */
    public function horariosDisponiveis( Request $request, int $profissionalUsuarioId )
    {
        if( !$request->has('timestamp') )
        {
            return response('Parâmetros incorretos', 401);
        }

        $retorno = [];

        // recuperando id do profissional
        $profissionalId = Profissional::whereUsuarioId( $profissionalUsuarioId )->first()->toArray()['id'];

        // verificando os horarios marcados como dia de atendimento do profissional
        $diaAtendimento = ProfissionalDiaAtendimento::whereProfissionalId( $profissionalId )
                          ->whereSemAtendimento(0)
                          ->where('horario_inicio', '!=', null)
                          ->where('horario_fim', '!=', null)
                          ->where('dia_semana', '=', Calendario::date($request->input('timestamp'))->dayOfWeek)
                          ->select('horario_inicio', 'horario_fim')
                          ->get(); 
        if($diaAtendimento != null)
        {
            if($diaAtendimento->first() != null):
                $horarios = $diaAtendimento->first()->toArray();

                // pegando um intervalo de horários
                $horarios = Calendario::intervaloHoras( \Carbon\Carbon::parse( Calendario::date($request->input('timestamp'))->format("Y-m-d {$horarios['horario_inicio']}") ), \Carbon\Carbon::parse( Calendario::date($request->input('timestamp'))->format("Y-m-d {$horarios['horario_fim']}") ) );

                // verificando os agendamentos
                $consultas = Agenda::whereProfissionalId( $profissionalId )
                             ->where('data_compromisso', '>=', Calendario::date($request->input('timestamp'))->format('Y-m-d 00:00:00'))
                             ->where('data_compromisso', '<', Calendario::date($request->input('timestamp'))->copy()->addDays(1)->format('Y-m-d 00:00:00'))
                             ->where('data_cancelamento', '=', null)
                             ->where('data_confirmacao', '!=', null)
                             ->pluck('data_compromisso');

                if( $consultas )
                    $consultas = $consultas->toArray();
                else
                    $consultas = [];

                // verificando os horarios
                // para montar o retorno
                foreach( $horarios as $k => $horario )
                {
                    if( !in_array( $horario->format('Y-m-d H:i:00'), $consultas )  )
                        $retorno[] = $horario->format('H:i');
                }
            else:
                $retorno = [];
            endif;
        }

        return response()->json(['horarios' => $retorno]);
    }

    /**
     * Esta função recebe os dados para validar e agendar ou não a consulta
     */
    public function agendarConsulta( Request $request, int $profissionalUsuarioId )
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // recuperando id do profissional
        $profissionalId = Profissional::whereUsuarioId( $profissionalUsuarioId )->first()->toArray()['id'];

        $dados = $request->all();

        // caso não tenham todos os campos obrigatórios
        // retornar mensagem de erro
        $rules = [
            'diaConsulta' => 'required',
            'horarioConsulta' => 'required',
            'servicoConsulta' => 'required'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.'
        ];

        $validation = Validator::make(
            $dados,
            $rules,
            $messages
        );

        // no caso de falha de validação
        if( $validation->fails() )
        {
            $errors = new MessageBag(['horarioIndisponivel' => ['Houve um problema ao realizar o agendamento, verifique as informações passadas e tente novamente.']]);

            $view = new ProfissionaisController;
            $view = $view->agendaCompleta( $request, $profissionalUsuarioId )
                    ->withInput($request->all())
                    ->withErrors($errors)
                    ->with('abrirModal', 'agendarConsulta-adm');

            return $view;
        }

        // antes de continuar
        // verifica se já existe uma solicitação de agendamento com o profissional
        $paciente = Paciente::verDetalhes( Auth::user()->toArray()['id'] );

        $profissional = Profissional::verDetalhes( $profissionalUsuarioId );

        // buscando na agenda
        // $consulta = Agenda::wherePacienteId( $paciente['id'] )
        //                 ->whereProfissionalId( $profissionalId )
        //                 ->where('data_confirmacao', '=', null)
        //                 ->where('data_cancelamento', '=', null)
        //                 ->count();

        // caso algum registro seja encontrado
        // significa que ainda não foi nem aprovado e nem cancelado
        // sendo assim, está pendente
        // if( $consulta > 0 )
        // {
        //     $errors = new MessageBag(['horarioIndisponivel' => ['Já existe uma solicitação de consulta com este profissional, por favor, aguarde até que sua solicitação seja analisada pelo profissional.']]);

        //     $view = new ProfissionaisController;
        //     $view = $view->agendaCompleta( $request, $profissionalUsuarioId )
        //             ->withInput($request->all())
        //             ->withErrors($errors)
        //             ->with('abrirModal', 'agendarConsulta-adm');

        //     return $view;
        // }

        // data e hora que pode ser atendida pelo profissional
        $dados['data_hora_aprovado'] = null;

        // verificando se o horário informado é atendido pelo profissional
        if(  
            // verificando atendimento nesta data e hora
            HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaConsulta'] )->dayOfWeek, $dados['horarioConsulta'] ) &&

            // verificando se não há agendamento confirmado
            Agenda::whereProfissionalId( $profissionalId )
            ->where('data_confirmacao', '!=', null)
            ->where('data_cancelamento', '=', null)
            ->where('data_compromisso', '=', Calendario::date( $dados['diaConsulta'] )->format('Y-m-d') . " {$dados['horarioConsulta']}:00")
            ->count() == 0
        )
        {
            $dados['data_hora_aprovado'] = Calendario::date( $dados['diaConsulta'] )->format('Y-m-d') . " {$dados['horarioConsulta']}:00";
        }
        // verificando as demais opções de agendamento
        else if( strlen($dados['diaOpcao1']) && strlen($dados['horarioOpcao1']) )
        {
            if(
                // verificando atendimento nesta data e hora
                HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaOpcao1'] )->dayOfWeek, $dados['horarioOpcao1'] ) &&

                // verificando se não há agendamento confirmado
                Agenda::whereProfissionalId( $profissionalId )
                ->where('data_confirmacao', '!=', null)
                ->where('data_cancelamento', '=', null)
                ->where('data_compromisso', '=', Calendario::date( $dados['diaOpcao1'] )->format('Y-m-d') . " {$dados['horarioOpcao1']}:00")
                ->count() == 0
            )
            {
                $dados['data_hora_aprovado'] = Calendario::date( $dados['diaOpcao1'] )->format('Y-m-d') . " {$dados['horarioOpcao1']}:00";
            }
        }
        // verificando as demais opções de agendamento
        else if( strlen($dados['diaOpcao2']) && strlen($dados['horarioOpcao2']) )
        {
            if(
                // verificando atendimento nesta data e hora
                HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaOpcao2'] )->dayOfWeek, $dados['horarioOpcao2'] ) &&

                // verificando se não há agendamento confirmado
                Agenda::whereProfissionalId( $profissionalId )
                ->where('data_confirmacao', '!=', null)
                ->where('data_cancelamento', '=', null)
                ->where('data_compromisso', '=', Calendario::date( $dados['diaOpcao2'] )->format('Y-m-d') . " {$dados['horarioOpcao2']}:00")
                ->count() == 0
            )
            {
                $dados['data_hora_aprovado'] = Calendario::date( $dados['diaOpcao2'] )->format('Y-m-d') . " {$dados['horarioOpcao2']}:00";
            }
        }
        // verificando as demais opções de agendamento
        else if( strlen($dados['diaOpcao3']) && strlen($dados['horarioOpcao3']) )
        {
            if(
                // verificando atendimento nesta data e hora
                HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaOpcao3'] )->dayOfWeek, $dados['horarioOpcao3'] ) &&

                // verificando se não há agendamento confirmado
                Agenda::whereProfissionalId( $profissionalId )
                ->where('data_confirmacao', '!=', null)
                ->where('data_cancelamento', '=', null)
                ->where('data_compromisso', '=', Calendario::date( $dados['diaOpcao3'] )->format('Y-m-d') . " {$dados['horarioOpcao3']}:00")
                ->count() == 0
            )
            {
                $dados['data_hora_aprovado'] = Calendario::date( $dados['diaOpcao3'] )->format('Y-m-d') . " {$dados['horarioOpcao3']}:00";
            }
        }

        // caso não tenha um horário aprovado
        // retornar erro
        if( !$dados['data_hora_aprovado'] )
        {
            $errors = new MessageBag(['horarioIndisponivel' => ['Desculpe, mas os horários selecionados não estão mais disponíveis.']]);

            $view = new ProfissionaisController;
            $view = $view->agendaCompleta( $request, $profissionalUsuarioId )
                    ->withInput($request->all())
                    ->withErrors($errors)
                    ->with('abrirModal', 'agendarConsulta-adm');
        }
        else
        {
            // recuperando informações de consulta
            $servico = ProfissionalServico::find( $dados['servicoConsulta'] )->toArray();

            $paciente = Paciente::verDetalhes( Auth::user()->toArray()['id'] );

            $profissional = Profissional::verDetalhes( $profissionalUsuarioId );

            // antes de continuar
            // verifica se já existe uma consulta neste mesmo horário com qualquer profissional
            // buscando na agenda
            // $consulta = Agenda::wherePacienteId( $paciente['id'] )
            //                 ->where('data_cancelamento', '=', null)
            //                 ->where('data_compromisso', '=', $dados['data_hora_aprovado'])
            //                 ->count();

            // // caso algum registro seja encontrado
            // // significa que o paciente não pode fazer dois agendamentos no mesmo horário
            // if( $consulta > 0 )
            // {
            //     $errors = new MessageBag(['horarioIndisponivel' => ['Desculpe, mas já existe um registro de solicitação de agendamento no horário marcado, não é possível agendar duas consultas para o mesmo dia e hora. Por favor, selecione outro horário..']]);

            //     $view = new ProfissionaisController;
            //     $view = $view->agendaCompleta( $request, $profissionalUsuarioId )
            //             ->withInput($request->all())
            //             ->withErrors($errors)
            //             ->with('abrirModal', 'agendarConsulta-adm');

            //     return $view;
            // }

            // inserindo no banco de dados
            $agendaId = Agenda::insertGetId([
                            "paciente_id"       => $paciente['id'],
                            "profissional_id"   => $profissionalId,
                            "nome_compromisso"  => "{$servico['servico']}",
                            "valor"             => "{$servico['valor']}",
                            "servico_id"        => "{$servico['id']}",
                            "data_compromisso"  => $dados['data_hora_aprovado']
                        ]);

            // notificando o cadastro da agenda
            // avisando o profissional
            Notificacao::insert([
                "para_usuario_id" => $profissionalUsuarioId,
                "tipo_notificacao_id" => TipoNotificacao::whereTipo('solicitacao_agenda')->first()->toArray()['id'],
                "relacionado_usuario_id" => Auth::user()->toArray()['id'],
                "relacionado_agenda_id" => $agendaId,
                "texto" => "{$paciente['nome']} {$paciente['sobrenome']}, solicitou agendamento com você para o serviço ({$servico['servico']})"
            ]);

            // notificando por e-mail
            // Mail::send('email-mkt.consulta-solicitada', ["texto" => "{$paciente['nome']} {$paciente['sobrenome']}, solicitou agendamento com você para o serviço ({$servico['servico']})"], function ($mail) use ($profissional) {
            //     $mail->from('logdialog@criaturo.com', 'LogDialog');

            //     $mail->to($profissional['email'], "")->subject('LogDialog, solicitação de consulta.');
            // });

            $view = new ProfissionaisController;
            $view = $view->agendaCompleta( $request, $profissionalUsuarioId )
                    ->with('avisoSistema', [
                            "titulo" => "Solicitação de agendamento",
                            "mensagem" => "Sua solicitação de agendamento foi registrada com sucesso, agora aguarde até que o profissional confirme sua solicitação." // @TODO: Remover esse final da mensagem para entrar em produção
                        ])
                    ->with('abrirModal', 'avisoSistema');
        }

        return $view;
    }
    /**
     * Esta função recebe os dados para validar e agendar ou não a consulta
     */
    public function agendarConsultaModal( Request $request, int $profissionalUsuarioId )
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // recuperando id do profissional
        $profissionalId = Profissional::whereUsuarioId( $profissionalUsuarioId )->first()->toArray()['id'];

        $dados = $request->all();

        // caso não tenham todos os campos obrigatórios
        // retornar mensagem de erro
        $rules = [
            'data' => 'required',
            'horarioSelecionado' => 'required',
            'servico' => 'required'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.'
        ];

        $validation = Validator::make(
            $dados,
            $rules,
            $messages
        );

        // no caso de falha de validação
        if( $validation->fails() )
        {
            $errors = $validation->messages();

            // por enquanto apenas um alerta geral
            $view = new IndexController;
            $view = $view->index( $request )
                    ->with('avisoSistema', [
                            "titulo" => "Falha no agendamento",
                            "mensagem" => "Houve um problema ao realizar o agendamento, verifique as informações passadas e tente novamente." // @TODO: Remover esse final da mensagem para entrar em produção
                        ])
                    ->with('abrirModal', 'avisoSistema');

            return $view;
        }

        // antes de continuar
        // verifica se já existe uma solicitação de agendamento com o profissional
        $paciente = Paciente::verDetalhes( Auth::user()->toArray()['id'] );

        $profissional = Profissional::verDetalhes( $profissionalUsuarioId );

        // buscando na agenda
        // $consulta = Agenda::wherePacienteId( $paciente['id'] )
        //                 ->whereProfissionalId( $profissionalId )
        //                 ->where('data_confirmacao', '=', null)
        //                 ->where('data_cancelamento', '=', null)
        //                 ->count();

        // // caso algum registro seja encontrado
        // // significa que ainda não foi nem aprovado e nem cancelado
        // // sendo assim, está pendente
        // if( $consulta > 0 )
        // {
        //     $view = new IndexController;
        //     $view = $view->index( $request )
        //             ->with('avisoSistema', [
        //                     "titulo" => "Por favor, aguarde",
        //                     "mensagem" => "Já existe uma solicitação de consulta com este profissional, por favor, aguarde até que sua solicitação seja analisada pelo profissional." // @TODO: Remover esse final da mensagem para entrar em produção
        //                 ])
        //             ->with('abrirModal', 'avisoSistema');

        //     return $view;
        // }

        // inserindo no banco de dados
        $agendaId = Agenda::insertGetId([
                        "paciente_id" => $paciente['id'],
                        "profissional_id" => $profissionalId,
                        "nome_compromisso" => "{$servico['servico']}",
                        "data_compromisso" => $dados['data_hora_aprovado']
                    ]);

        // data e hora que pode ser atendida pelo profissional
        $dados['data_hora_aprovado'] = null;

        // verificando se o horário informado é atendido pelo profissional
        if(
            // verificando atendimento nesta data e hora
            HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['data'] )->dayOfWeek, $dados['horarioSelecionado'] ) &&

            // verificando se não há agendamento confirmado
            Agenda::whereProfissionalId( $profissionalId )
            ->where('data_confirmacao', '!=', null)
            ->where('data_cancelamento', '=', null)
            ->where('data_compromisso', '=', Calendario::date( $dados['data'] )->format('Y-m-d') . " {$dados['horarioSelecionado']}:00")
            ->count() == 0
        )
        {
            $dados['data_hora_aprovado'] = Calendario::date( $dados['data'] )->format('Y-m-d') . " {$dados['horarioSelecionado']}:00";
        }
        // verificando as demais opções de agendamento
        else if( strlen($dados['diaOpcao1']) && strlen($dados['horarioOpcao1']) )
        {
            if(
                // verificando atendimento nesta data e hora
                HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaOpcao1'] )->dayOfWeek, $dados['horarioOpcao1'] ) &&

                // verificando se não há agendamento confirmado
                Agenda::whereProfissionalId( $profissionalId )
                ->where('data_confirmacao', '!=', null)
                ->where('data_cancelamento', '=', null)
                ->where('data_compromisso', '=', Calendario::date( $dados['diaOpcao1'] )->format('Y-m-d') . " {$dados['horarioOpcao1']}:00")
                ->count() == 0
            )
            {
                $dados['data_hora_aprovado'] = Calendario::date( $dados['diaOpcao1'] )->format('Y-m-d') . " {$dados['horarioOpcao1']}:00";
            }
        }
        // verificando as demais opções de agendamento
        else if( strlen($dados['diaOpcao2']) && strlen($dados['horarioOpcao2']) )
        {
            if(
                // verificando atendimento nesta data e hora
                HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaOpcao2'] )->dayOfWeek, $dados['horarioOpcao2'] ) &&

                // verificando se não há agendamento confirmado
                Agenda::whereProfissionalId( $profissionalId )
                ->where('data_confirmacao', '!=', null)
                ->where('data_cancelamento', '=', null)
                ->where('data_compromisso', '=', Calendario::date( $dados['diaOpcao2'] )->format('Y-m-d') . " {$dados['horarioOpcao2']}:00")
                ->count() == 0
            )
            {
                $dados['data_hora_aprovado'] = Calendario::date( $dados['diaOpcao2'] )->format('Y-m-d') . " {$dados['horarioOpcao2']}:00";
            }
        }
        // verificando as demais opções de agendamento
        else if( strlen($dados['diaOpcao3']) && strlen($dados['horarioOpcao3']) )
        {
            if(
                // verificando atendimento nesta data e hora
                HorarioAtendimento::horarioAtendido( $profissionalUsuarioId, Calendario::date( $dados['diaOpcao3'] )->dayOfWeek, $dados['horarioOpcao3'] ) &&

                // verificando se não há agendamento confirmado
                Agenda::whereProfissionalId( $profissionalId )
                ->where('data_confirmacao', '!=', null)
                ->where('data_cancelamento', '=', null)
                ->where('data_compromisso', '=', Calendario::date( $dados['diaOpcao3'] )->format('Y-m-d') . " {$dados['horarioOpcao3']}:00")
                ->count() == 0
            )
            {
                $dados['data_hora_aprovado'] = Calendario::date( $dados['diaOpcao3'] )->format('Y-m-d') . " {$dados['horarioOpcao3']}:00";
            }
        }

        // caso não tenha um horário aprovado
        // retornar erro
        if( !$dados['data_hora_aprovado'] )
        {
            $errors = new MessageBag(['horarioIndisponivel' => ['Desculpe, mas os horários selecionados não estão mais disponíveis.']]);

            $view = new IndexController;
            $view = $view->index( $request )
                    ->withInput($request->all())
                    ->withErrors($errors)
                    ->with('abrirModal', "agendarProfissional_{$profissionalUsuarioId}");
        }
        else
        {
            // recuperando informações de consulta
            $servico = ProfissionalServico::find( $dados['servico'] )->toArray();

            $paciente = Paciente::verDetalhes( Auth::user()->toArray()['id'] );

            $profissional = Profissional::verDetalhes( $profissionalUsuarioId );

            // antes de continuar
            // verifica se já existe uma consulta neste mesmo horário com qualquer profissional
            // buscando na agenda
            $consulta = Agenda::wherePacienteId( $paciente['id'] )
                            ->where('data_cancelamento', '=', null)
                            ->where('data_compromisso', '=', $dados['data_hora_aprovado'])
                            ->count();

            // caso algum registro seja encontrado
            // significa que o paciente não pode fazer dois agendamentos no mesmo horário
            if( $consulta > 0 )
            {
                $view = new IndexController;
                $view = $view->index( $request )
                        ->with('avisoSistema', [
                                "titulo" => "Duplicidade de horário",
                                "mensagem" => "Desculpe, mas já existe um registro de solicitação de agendamento no horário marcado, não é possível agendar duas consultas para o mesmo dia e hora. Por favor, selecione outro horário." // @TODO: Remover esse final da mensagem para entrar em produção
                            ])
                        ->with('abrirModal', 'avisoSistema');

                return $view;
            }

            // inserindo no banco de dados
            $agendaId = Agenda::insertGetId([
                            "paciente_id" => $paciente['id'],
                            "profissional_id" => $profissionalId,
                            "nome_compromisso" => "{$servico['servico']}",
                            "data_compromisso" => $dados['data_hora_aprovado']
                        ]);

            // notificando o cadastro da agenda
            // avisando o profissional
            Notificacao::insert([
                "para_usuario_id" => $profissionalUsuarioId,
                "tipo_notificacao_id" => TipoNotificacao::whereTipo('solicitacao_agenda')->first()->toArray()['id'],
                "relacionado_usuario_id" => Auth::user()->toArray()['id'],
                "relacionado_agenda_id" => $agendaId,
                "texto" => "{$paciente['nome']} {$paciente['sobrenome']}, solicitou agendamento com você para o serviço ({$servico['servico']})"
            ]);

            // notificando por e-mail
            // Mail::send('email-mkt.consulta-solicitada', ["texto" => "{$paciente['nome']} {$paciente['sobrenome']}, solicitou agendamento com você para o serviço ({$servico['servico']})"], function ($mail) use ($profissional) {
            //     $mail->from('logdialog@criaturo.com', 'LogDialog');

            //     $mail->to($profissional['email'], "")->subject('LogDialog, solicitação de consulta.');
            // });

            $view = new IndexController;
            $view = $view->index( $request )
                    ->with('avisoSistema', [
                            "titulo" => "Solicitação de agendamento",
                            "mensagem" => "Sua solicitação de agendamento foi registrada com sucesso, agora aguarde até que o profissional confirme sua solicitação." // @TODO: Remover esse final da mensagem para entrar em produção
                        ])
                    ->with('abrirModal', 'avisoSistema');
        }

        return $view;
    }
}
