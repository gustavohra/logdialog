<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use LogDialog\Model\Slider as Slider; 
use LogDialog\Model\Pagina as Pagina; 

// carregando utilitários
use Auth;

class PaginaController extends Controller
{
	public function index(Request $request) {
		$pagina = request()->segment(2);
		if($pagina == null || $pagina == '')
			return  view('home');
		else { 
			$dados 	= Pagina::where('url', $pagina)->get()->first();

			if($dados == null)
				abort(404);
			else{
				return view('pagina.padrao')->with('pagina', $dados)->with('sliders', Slider::get());
			}

		}
	}
}