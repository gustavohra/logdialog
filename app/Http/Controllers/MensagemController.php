<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;
use Carbon\Carbon;
use DB;
use Avatar;

// carregando model
use LogDialog\Model\Inbox as Inbox;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\TipoNotificacao as TipoNotificacao;
use LogDialog\Model\Notificacao as Notificacao;

class MensagemController extends Controller
{
    /**
     * Esta função é chamada pela API
     * Retorna um json com os dados solicitados
     *
     * O usuário precisa estar logado
     */
    public function APICarregarMensagens( Request $request )
    {
        // caso não esteja logado
        // if( !Auth::check() )
        // {
        //     return response('Autenticação necessária', 401);
        // }

        // caso não tenham os parâmetros necessários
        if(
            !$request->has('com_usuario_id')
        )
        {
            return response('Parâmetros incorretos', 401);
        }

        // verificando a paginação
        // se não estiver definida, cria um
        // padrão de 10 itens
        // apenas se não for informado um timestamp
        if( !$request->has('timestamp') )
        {
            $paginacao = $request->has('paginar') ? (int)$request->input('paginar') : 1000;
        }

        // usuário logado
        $usuarioId = Auth::user()->toArray()['id'];
        if($request->has('ultimas') && $request->input('ultimas'))
            $ordem      = 'DESC';
        else
            $ordem      = 'ASC';

        $inbox = Inbox::whereRaw( DB::raw("(( de_usuario_id = '{$usuarioId}' AND para_usuario_id = '{$request->input('com_usuario_id')}' ) OR
                                           ( para_usuario_id = '{$usuarioId}' AND de_usuario_id = '{$request->input('com_usuario_id')}' ))") )
                 ->orderBy('id', $ordem);

                // se houver paginação
                if( isset( $paginacao ) )
                {
                    $inbox->paginate($paginacao);
                }
                else if( $request->has('timestamp') )
                {
                    $inbox->where('data_mensagem', '>', date("Y-m-d H:i:s", $request->input('timestamp')));
                }

        $responseTmp = [];
        $response = [];
        $pagination = [];

        if( $inbox )
        {
            $response = $inbox->get()->toArray();
        }

        foreach( $response as $k => $v )
        {
            $response[$k]['de_avatar']   = Avatar::urlImagem( $response[$k]['de_usuario_id'] );
            $response[$k]['para_avatar'] = Avatar::urlImagem( $response[$k]['para_usuario_id'] );

            // pegando dados
            $deTmp = Paciente::verDetalhes( $response[$k]['de_usuario_id'] );

            if( sizeof($deTmp) == 0 )
            {
                $deTmp = Profissional::verDetalhes($response[$k]['de_usuario_id']);
            }

            // pegando dados
            $paraTmp = Paciente::verDetalhes( $response[$k]['para_usuario_id'] );

            if( sizeof($paraTmp) == 0 )
            {
                $paraTmp = Profissional::verDetalhes($response[$k]['para_usuario_id']);
            }

            if( isset($deTmp['tratamento']) )
               $response[$k]['de_nome'] = "{$deTmp['tratamento']} {$deTmp['nome']} {$deTmp['sobrenome']}";
            else
               $response[$k]['de_nome'] = "{$deTmp['nome']} {$deTmp['sobrenome']}";

            if( isset($paraTmp['tratamento']) )
               $response[$k]['para_nome'] = "{$paraTmp['tratamento']} {$paraTmp['nome']} {$paraTmp['sobrenome']}";
            else
               $response[$k]['para_nome'] = "{$paraTmp['nome']} {$paraTmp['sobrenome']}";

            // flag se a mensagem foi enviada pelo logado
            $response[$k]['minha_mensagem'] = (int)($response[$k]['de_usuario_id'] == Auth::user()->toArray()['id']);
        }

        return response()->json(['mensagem' => $response, 'pagination' => $pagination]);
    }

    /**
     * Esta função é chamada pela API
     * Retorna um json com os dados solicitados
     *
     * O usuário precisa estar logado
     */
    public function APICarregarContatos( Request $request )
    {
        // caso não esteja logado
        if( Auth::check() ): 
            // usuário logado
            $usuarioId = Auth::user()->toArray()['id'];

            $lista      = [];
            $contatos   = [];

            // recuperando as listagens de pessoas que já se comunicaram com o usuário logado
            $tmp = Inbox::whereDeUsuarioId( $usuarioId )
                      ->select('para_usuario_id')
                      ->groupBy('para_usuario_id')
                      ->orderBy('id', 'DESC')
                      ->get();

            if( $tmp )
            {
                $tmp = $tmp->toArray();

                foreach( $tmp as $k => $v )
                    $lista[] = $v['para_usuario_id'];
            }

            $tmp = Inbox::whereParaUsuarioId( $usuarioId )
                      ->select('de_usuario_id')
                      ->groupBy('de_usuario_id')
                      ->orderBy('id', 'DESC')
                      ->get();

            if( $tmp )
            {
                $tmp = $tmp->toArray();

                foreach( $tmp as $k => $v )
                    $lista[] = $v['de_usuario_id'];
            }

            $lista = array_unique($lista);

            // contagem de contatos
            $contatoIdx = 0;

            // pegando dados de pacientes
            $paciente = Paciente::whereIn('usuario_id', $lista)
                        ->orderBy('nome')
                        ->get();

            if( $paciente )
            {
                $paciente = $paciente->toArray();

                foreach( $paciente as $k => $v )
                {
                    $contatos[$contatoIdx]['usuario_id'] = $v['usuario_id'];
                    $contatos[$contatoIdx]['nome'] = "{$v['nome']} {$v['sobrenome']}";
                    $contatos[$contatoIdx]['avatar'] = Avatar::urlImagem( $v['usuario_id'] );
                    $contatos[$contatoIdx]['ultima_mensagem'] = '';
                    $contatos[$contatoIdx]['ultimo_id_inbox'] = 0;

                    // pegando a ultima mensagem com este contato
                    $inbox = Inbox::whereRaw( DB::raw("( de_usuario_id = '{$usuarioId}' AND para_usuario_id = '{$v['usuario_id']}' ) OR
                                                  ( para_usuario_id = '{$usuarioId}' AND de_usuario_id = '{$v['usuario_id']}' )") )
                             ->orderBy('id', 'DESC')
                             ->first();

                    if( $inbox )
                    {
                        $contatos[$contatoIdx]['ultima_mensagem'] = $inbox->toArray()['mensagem'];
                        $contatos[$contatoIdx]['ultimo_id_inbox'] = $inbox->toArray()['id'];
                    }

                    $contatoIdx++;
                }
            }

            // pegando dados de profissionais
            $profissional = Profissional::whereIn('usuario_id', $lista)
                            ->orderBy('nome')
                            ->get();

            if( $profissional )
            {
                $profissional = $profissional->toArray();

                foreach( $profissional as $k => $v )
                {
                    $tratamento = "Dra.";

                    if( $v['sexo'] == 'masculino' )
                        $tratamento = "Dr.";

                    $contatos[$contatoIdx]['usuario_id'] = $v['usuario_id'];
                    $contatos[$contatoIdx]['nome'] = "{$tratamento} {$v['nome']} {$v['sobrenome']}";
                    $contatos[$contatoIdx]['avatar'] = Avatar::urlImagem( $v['usuario_id'] );
                    $contatos[$contatoIdx]['ultima_mensagem'] = '';
                    $contatos[$contatoIdx]['ultimo_id_inbox'] = 0;

                    // pegando a ultima mensagem com este contato
                    $inbox = Inbox::whereRaw( DB::raw("( de_usuario_id = '{$usuarioId}' AND para_usuario_id = '{$v['usuario_id']}' ) OR
                                                  ( para_usuario_id = '{$usuarioId}' AND de_usuario_id = '{$v['usuario_id']}' )") )
                             ->orderBy('id', 'DESC')
                             ->first();

                    if( $inbox )
                    {
                        $contatos[$contatoIdx]['ultima_mensagem'] = $inbox->toArray()['mensagem'];
                        $contatos[$contatoIdx]['ultimo_id_inbox'] = $inbox->toArray()['id'];
                    }

                    $contatoIdx++;
                }
            }

            usort($contatos, function($a, $b){
                if ($a['ultimo_id_inbox'] == $b['ultimo_id_inbox'])
                  return 1;
                else
                  return $a['ultimo_id_inbox'] < $b['ultimo_id_inbox'] ? 1 : -1;
            });

            return response()->json(['contatos' => $contatos]);
        else:
            return response('Autenticação necessária', 401);
        endif;
    }

    /**
     * Salvar nova mensagem
     */
    public function APINovaMensagem( Request $request )
    {
        // caso não esteja logado
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        // campos
        if( !$request->has('paraUsuarioId') || !$request->has('mensagem') )
        {
            return response('Parâmetros incorretos', 401);
        }

        if( strlen($request->input('mensagem')) == 0 || $request->input('paraUsuarioId') < 1 )
        {
            return response('Parâmetros incorretos', 401);
        }

        // gravando no banco
        Inbox::insert([
            "de_usuario_id" => Auth::user()->toArray()['id'],
            "para_usuario_id" => $request->input('paraUsuarioId'),
            "mensagem" => $request->input('mensagem')
        ]);

        $paciente = Paciente::verDetalhes( Auth::user()->toArray()['id'] );
        $profissional = Profissional::verDetalhes( Auth::user()->toArray()['id'] );

        $nomeRemetente = '';

        if( sizeof($paciente) > 0 )
        {
            $nomeRemetente = "{$paciente['nome']} {$paciente['sobrenome']}";
        }
        else
        {
            $nomeRemetente = "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}";
        }

        // notificando nova mensagem
        $novaNotificacao = Notificacao::insertGetId([
            "para_usuario_id" => $request->input('paraUsuarioId'),
            "tipo_notificacao_id" => TipoNotificacao::whereTipo('novo_inbox')->first()->toArray()['id'],
            "relacionado_usuario_id" => Auth::user()->toArray()['id'],
            "texto" => "{$nomeRemetente}, te enviou uma nova mensagem"
        ]);

        return response()->json(array('id' => $novaNotificacao));
    }

    /**
     * Buscar os dados de um usuário
     * No caso de nova mensagem
     */
    public function APIDadosUsuario( Request $request )
    {
        // caso não esteja logado
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        // campos
        if( !$request->has('usuarioId') )
        {
            return response('Parâmetros incorretos', 401);
        }

        $retorno = [];

        $paciente = Paciente::verDetalhes( $request->input('usuarioId') );

        if( sizeof($paciente) == 0 )
        {
            $profissional = Profissional::verDetalhes( $request->input('usuarioId') );

            $retorno["nome"] = "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}";
        }
        else
        {
            $retorno["nome"] = "{$paciente['nome']} {$paciente['sobrenome']}";
        }

        $retorno["avatar"] = Avatar::urlImagem( $request->input('usuarioId') );

        return response()->json(['dados' => $retorno]);
    }

    /**
     * Esta função carrega as mensagens
     */
}
