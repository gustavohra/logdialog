<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;

class InformacoesController extends Controller
{
    /**
     * Tela com a listagem de links
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function index()
    {
    	echo "Tela inicial com os links";
    }

    /**
     * Tela Trocas e Devoluções
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function trocasDevolucoes()
    {
    	echo "Tela Trocas e Devoluções";
    }

    /**
     * Tela Formas de Pagamento
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function formasPagamento()
    {
    	echo "Formas de Pagamento";
    }

    /**
     * Tela Opções de Entrega
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function opcoesEntrega()
    {
    	echo "Opções de Entrega";
    }

    /**
     * Tela Cancelamentos
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function cancelamentos()
    {
    	echo "Cancelamentos";
    }

    /**
     * Tela Oferta Relâmpago
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function ofertaRelampago()
    {
    	echo "Oferta Relâmpago";
    }

    /**
     * Tela Seja um Fornecedor
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function sejaFornecedor()
    {
    	echo "Seja um Fornecedor";
    }
}
