<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use Blade;
// carregando utilitários
use Auth;
use Carbon\Carbon;
use Hash;
use Input;
use File;
use Image;
use Validator;
use View;
use \Illuminate\Support\MessageBag as MessageBag;

use Calendario;
use HorarioAtendimento;
use Avatar;

// carregando models
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\ProfissionalServico as ProfissionalServico;
use LogDialog\Model\Especialidade as Especialidade;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\Notificacao as Notificacao;
use LogDialog\Model\ProfissionalDiaAtendimento as ProfissionalDiaAtendimento;
use LogDialog\Model\ProfissionalHorarioBloqueado as ProfissionalHorarioBloqueado;
use LogDialog\Model\Anotacao as Anotacao;
use LogDialog\Model\Slider as Slider;

class PerfilController extends Controller
{
    /**
     * Esta função retorna as informações padrões da view para pacientes
     * Home do admin
     */
    public function padraoViewPaciente( int $usuarioId )
    {

        if( Auth::check() ):
            // pegando dados do perfil
            $paciente = Paciente::whereUsuarioId( $usuarioId )
                            ->first();

            if($paciente != null):

            $paciente = $paciente->toArray();

            // pegando dados das duas próximas consultas
            $proximasConsultas = Agenda::listaConsultas( $usuarioId, 2, time() );

            // pegando dados das duas últimas consultas encerradas
            $consultasEncerradas = Agenda::listaConsultasEncerradas( $usuarioId, 2, time(), 'DESC' );

            $consultas = array_merge($proximasConsultas->toArray(), $consultasEncerradas->toArray())["data"];

            // pegando dados das anotações
            $notas = Anotacao::listarNotasPaciente( $usuarioId );

            // saída da view
            return view('paciente.admin')
                   ->with('usuario', [
                        'user' => Auth::user(),
                        'paciente' => $paciente
                    ])
                   ->with('proximasConsultas', $proximasConsultas)
                   ->with('consultasEncerradas', $consultasEncerradas)
                   ->with('consultas', $consultas)
                   ->with('notas', $notas);
            else:
                 Auth::logout();
                $errors = new MessageBag(['login' => ['Perfil inativo.']]);

                return View::make('index.home')
                        ->withErrors($errors)
                        ->with('sliders', Slider::get())
                        ->with('abrirModal', 'modal-log')
                        ->with('profissionais', Profissional::carregarLista(18));

            endif;

        endif;
    }

    function getAtendimentosPorMes() {

            $consultas = array();
            $profissionalUsuarioId = Auth::user()->toArray()['id'];
            $oneMonth = new \DateInterval('P1M');
            $startDate = new \DateTime(date("Y")."-01-01");
            $period = new \DatePeriod($startDate, $oneMonth, 11);
            $i = 0;
            foreach($period as $date){
                $mes        = $date->format('m');
                $data       = $date->format('Y-m-d');
                $final      = $date->add(new \DateInterval('P1M'));
                $resultado  = Agenda::qtdConsultasPorPeriodo($profissionalUsuarioId, $data, $final->format('Y-m-d'))->first();
                $consultas[$i] = $resultado->qtdConsultas;
                $i++;
            }

        return $consultas;
    }

    function getAtendimentosPorIdade() {

        $profissionalUsuarioId = Auth::user()->toArray()['id'];
        $atendimentos = Agenda::qtdConsultasPorIdade($profissionalUsuarioId)->get();
        $retorno        = array();
        for ($i=0; $i < 9; $i++) {
            $retorno[$i] = 0;
        }
        for ($i=0; $i < Count($atendimentos); $i++) {
            if($atendimentos[$i]->idade >= 0 && $atendimentos[$i]->idade <= 9)
                $retorno[0] += 1;
            else if($atendimentos[$i]->idade >= 10 && $atendimentos[$i]->idade <= 19)
                $retorno[1] += 1;
            else if($atendimentos[$i]->idade >= 20 && $atendimentos[$i]->idade <= 29)
                $retorno[2] += 1;
            else if($atendimentos[$i]->idade >= 30 && $atendimentos[$i]->idade <= 39)
                $retorno[3] += 1;
            else if($atendimentos[$i]->idade >= 40 && $atendimentos[$i]->idade <= 49)
                $retorno[4] += 1;
            else if($atendimentos[$i]->idade >= 50 && $atendimentos[$i]->idade <= 59)
                $retorno[5] += 1;
            else if($atendimentos[$i]->idade >= 60 && $atendimentos[$i]->idade <= 69)
                $retorno[6] += 1;
            else if($atendimentos[$i]->idade >= 70 && $atendimentos[$i]->idade <= 79)
                $retorno[7] += 1;
            else if($atendimentos[$i]->idade > 79)
                $retorno[8] += 1;
        }
        return $retorno;
    }
    function getAtendimentosPorSexo() {
        $consultas = array();
        $profissionalUsuarioId = Auth::user()->toArray()['id'];
        $consultas[0] = Agenda::qtdConsultasPorSexo($profissionalUsuarioId, 'masculino')->first()->qtdConsultas;
        $consultas[1] = Agenda::qtdConsultasPorSexo($profissionalUsuarioId, 'feminino')->first()->qtdConsultas;
        return $consultas;
    }

    function getAtendimentosPorTipo() {
        $consultas = array();
        $profissionalUsuarioId = Auth::user()->toArray()['id'];
        $consultas[0] = Agenda::qtdConsultasPorTipo($profissionalUsuarioId, 'Consulta individual')->first()->qtdConsultas;
        $consultas[1] = Agenda::qtdConsultasPorTipo($profissionalUsuarioId, 'Consulta tipo 2')->first()->qtdConsultas;
        $consultas[2] = Agenda::qtdConsultasPorTipo($profissionalUsuarioId, 'Consulta para casais')->first()->qtdConsultas;
        return $consultas;

    }
    /**
     * Esta função retorna as informações padrões da view para profisisonais
     * Home do admin
     */
    public function padraoViewProfissional( int $usuarioId )
    {
        // pegando dados do perfil
        $profissional = Profissional::whereUsuarioId( $usuarioId )
                        ->first();

        if($profissional != null):

        $profissional = $profissional->toArray();

        // formatando tratamento
        $profissional['tratamento'] = $profissional['sexo'] == 'feminino' ? 'Dra.' : 'Dr.';

        // pegando dados das duas próximas consultas
        $proximasConsultas = Agenda::listaConsultas( $usuarioId, 2, time() );

        // pegando dados das duas últimas consultas encerradas
        $consultasEncerradas = Agenda::listaConsultasEncerradas( $usuarioId, 2, time(), 'DESC' );

        $consultas = array_merge($proximasConsultas->toArray(), $consultasEncerradas->toArray())["data"];

        // pegando dados dos serviços prestados por este profissional
        $servicos = ProfissionalServico::listarProfissional( $usuarioId );

        // pegando dados das anotações
        $lista = Anotacao::listarNotasProfissional( $usuarioId );

        // processando para o layout
        // como o profissional não pdoe editar as notas
        // e as anotações são agrupadas por paciente
        // mesclar o conteúdo em um array simplificado
        // apenas para exibição

        $notas = [];
        foreach( $lista as $k => $paciente )
        {
            $tmp = [];

            $tmp['titulo'] = "{$paciente['paciente']['nome']} {$paciente['paciente']['sobrenome']}";

            // concatendando conteúdo
            // em um array simplifidado para este paciente
            $tmp['conteudo'] = [];

            foreach( $paciente['lista'] as $idx => $item )
            {
                $tmp['conteudo'][] = [
                    'data' => Carbon::parse($item['nota']['data_anotacao'])->format('d/m/Y H:i'),
                    'conteudo' => $item['nota']['conteudo']
                ];
            }

            $notas[] = $tmp;
        }

        // saída da view
        return view('profissionais.admin')
               ->with('usuario', [
                    'user' => Auth::user(),
                    'profissional' => $profissional
                ])
               ->with('proximasConsultas', $proximasConsultas)
               ->with('atendimentosAno', $this->getAtendimentosPorMes())
               ->with('atendimentosSexo', $this->getAtendimentosPorSexo())
               ->with('atendimentosIdade', $this->getAtendimentosPorIdade())
               ->with('atendimentosTipo', $this->getAtendimentosPorTipo())
               ->with('consultasEncerradas', $consultasEncerradas)
                ->with('consultas', $consultas)
               ->with('servicos', $servicos)
               ->with('notas', $notas);

        else:
             Auth::logout();
            $errors = new MessageBag(['login' => ['Perfil inativo.']]);

            return View::make('index.home')
                    ->withErrors($errors)
                    ->with('sliders', Slider::get())
                    ->with('abrirModal', 'modal-log')
                    ->with('profissionais', Profissional::carregarLista(18));

        endif;
    }

    /**
     * Tela inicial
     * Para detectar o tipo de perfil logado
     * E direcionar corretamente
     */
    public function meu(Request $request)
    {
        // testando calendário
        // echo '<pre>';

        // print_r( Calendario::date(strtotime('now')) );
        // print_r(Calendario::diaSeguinte( Calendario::date(strtotime('now')) ));
        // print_r(Calendario::diaAnterior( Calendario::date(strtotime('now')) ));
        // print_r(Calendario::semanaCompleta( Calendario::date(strtotime('now')) ));
        // print_r(Calendario::mesCompleto( Calendario::date(strtotime('now')) ));

        // exit;

        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // recuperando ID logado
        $usuarioId = Auth::user()->toArray()['id'];

        // pegando os dados dos usuários envolvidos
        // tanto da tabela usuários
        // quanto

    	// dependendo do perfil
        // carregar o layout correspondente
        // verificando o perfil
        $grupoSistema = Auth::user()->grupoSistema();
        switch ($grupoSistema) {
            case 'paciente':
                return $this->padraoViewPaciente( $usuarioId );
            case 'profissional':
                return $this->padraoViewProfissional( $usuarioId );
            default:
                echo 'grupo não encontrado';
                break;
        }
    }

    /**
     * Função para salvar os dados do perfil do paciente
     */
    public function editarPaciente( Request $request )
    {
        $existe = false;

        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // array para salvar os dados
        $data = [
                    'nome' => $request->input('nome_paciente'),
                    'sobrenome' => $request->input('sobrebnome_paciente'),
                    'endereco' => $request->input('street_paciente'),
                    'end_numero'    => $request->input('numero_paciente'),
                    'bairro' => $request->input('neighborhood_paciente'),
                    'cidade' => $request->input('city_paciente'),
                    'estado' => $request->input('state_paciente'),
                    'cep' => $request->input('cep_paciente')
                ];

        // criptografando a senha
        if( $request->has('novasenha') )
        {
            $senha = $request->input('novasenha');

            if($senha != null && $senha != ''){
                // atualizando no banco de dados
                Usuario::whereId( Auth::user()->toArray()['id'] )
                            ->update([
                                'senha' => Hash::make($senha)
                            ]);
            }
        }

        // caso exista CPF
        // e o paciente ainda esteja sem CPF no cadastro
        if(
            $request->has('cpf_paciente') &&
            strlen( Paciente::whereUsuarioId( Auth::user()->toArray()['id'] )->first()->toArray()['cpf'] ) == 0
        )
        {
            // verificando se o CPF é válido
            $cpf = $request->input('cpf_paciente') ?? $request->input('cpf');
            $cpf = preg_replace('/[^0-9]/', '', $cpf);

            $existe = Paciente::cpfJaExiste( $cpf, Auth::user()->toArray()['id'] );

            $data['cpf'] = $cpf;
        }

        // apenas se o CPF não existir
        if( !$existe )
        {
            Paciente::whereUsuarioId( Auth::user()->toArray()['id'] )
                         ->update($data);
        }

        // redirecionando para o perfil do usuário logado
        return redirect()->action('PerfilController@meu');
    }

    /**
     * Esta função recebe a alteração da foto do paciente
     */
    public function trocarFotoPaciente( Request $request )
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        $userId = Auth::user()->toArray()['id'];

        $uploadPath = "upload/user/{$userId}";

        // gaantindo a criação do diretório
        File::makeDirectory($uploadPath, 0777, true, true);

        // salvando avatar
        if( $request->has('uploadAvatarPaciente') && $request->input('inputRemoverAvatar') == 0 )
        {
            $data = trim($request->input('uploadAvatarPaciente'));

            if( strlen($data) > 0 )
            {
                list($type, $data) = explode(';', $data);

                list($type, $data) = explode(',', $data);
                $data = base64_decode($data);

                // armazenando na pasta
                $file = "avatar_" . microtime() . ".png";

                $file = str_replace(' ', '-', $file);

                file_put_contents("{$uploadPath}/{$file}", $data);

                // otimizando compressão da imagem
                $image = Image::make("{$uploadPath}/{$file}");
                $image->resize(700, 700);
                $image->save();

                // atualziando tabela de paciente
                // com o avatar
                Paciente::whereUsuarioId( $userId )
                           ->update([
                                'avatar' => $file
                            ]);
            }
        }

        // caso tenha solicitado para remover o avatar
        if( $request->input('inputRemoverAvatar') == 1 )
        {
            // atualziando tabela de paciente
            // com o avatar
            Paciente::whereUsuarioId( $userId )
                       ->update([
                            'avatar' => null
                        ]);
        }

        // redirecionando para a tela do perfil
        return response()->json(false);
    }

    /**
     * Esta função recebe o documento de autorização para menor de idade
     */
    public function anexarAutorizacaoMenor( Request $request )
    {
        // apenas se o usuário logado for paciente
        if( Auth::check() && Auth::user()->grupoSistema() == 'paciente' )
        {
            $dados = $request->all();

            // verificando nova especialidade
            $rules = [
                // documentos
                'inputAutorizacao' => 'required|mimes:jpg,jpeg,png,bmp,pdf,doc,docx,rtf|max:3000'
            ];

            $messages = [
                'required' => 'O campo :attribute é obrigatório.',
                'mimes' => 'Só são permitidos arquivos do tipo: :values',

                'inputAutorizacao.max' => 'A autorização deve conter no máximo :max caracteres.',

                // personalização de campo, por causa de tradução
                'inputAutorizacao.required' => 'É obrigatório anexar uma autorizacão dos responsáveis pelo menor de idade.'
            ];

            $validation = Validator::make(
                $dados,
                $rules,
                $messages
            );

            // no caso de falha de validação
            if( $validation->fails() )
            {
                $errors = $validation->messages();

                // redirecionando com os erros
                // para exibir ao usuário
                // mantendo os campos
                $request->flash();

                // retornando view
                return $this->padraoViewPaciente( Auth::user()->toArray()['id'] )
                            ->withInput($request->all())
                            ->withErrors($errors)
                            ->with('abrirModal', 'modalPerfil');
            }
            else
            {
                $uploadPath = "upload/user/" . Auth::user()->toArray()['id'];

                // armazenando na pasta
                $file = "autorizacao_menor_" . microtime() . "." . pathinfo(Input::file('inputAutorizacao')->getClientOriginalName(), PATHINFO_EXTENSION);

                $file = str_replace(' ', '-', $file);

                Input::file('inputAutorizacao')->move($uploadPath, $file);

                // atualziando tabela de profissional
                // com o avatar
                Paciente::whereUsuarioId( Auth::user()->toArray()['id'] )
                               ->update([
                                    'documento_autorizacao_menor' => $file,
                                    'documento_menor_aceito_por_usuario_id' => null,
                                    'data_aceito_autorizacao_menor' => null
                                ]);
            }
        }

        // redirecionando para a tela do perfil
        return redirect()->action("PerfilController@meu");
    }

    /**
     * Esta função salva a alteração de senha para o usuário logado
     */
    public function trocarSenha( Request $request )
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // criptografando a senha
        if( $request->has('novasenha') )
        {
            $senha = $request->input('novasenha');

            // atualizando no banco de dados
            Usuario::whereId( Auth::user()->toArray()['id'] )
                        ->update([
                            'senha' => Hash::make($senha)
                        ]);
        }

        // redirecionando ao perfil
        return redirect()->action('PerfilController@meu');
    }

    /**
     * Função para edição da foto do profissional
     */
    public function trocarFotoProfissional( Request $request )
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        $userId = Auth::user()->toArray()['id'];

        $uploadPath = "upload/user/{$userId}";

        // gaantindo a criação do diretório
        File::makeDirectory($uploadPath, 0777, true, true);

        // salvando avatar
        if( $request->has('uploadAvatarProfissional') && $request->input('inputRemoverAvatar') == 0 )
        {
            $data = trim($request->input('uploadAvatarProfissional'));

            if( strlen($data) > 0 )
            {
                list($type, $data) = explode(';', $data);

                list($type, $data) = explode(',', $data);
                $data = base64_decode($data);

                // armazenando na pasta
                $file = "avatar_" . microtime() . ".png";

                $file = str_replace(' ', '-', $file);

                file_put_contents("{$uploadPath}/{$file}", $data);

                // otimizando compressão da imagem
                $image = Image::make("{$uploadPath}/{$file}");
                $image->resize(700, 700);
                $image->save();

                // atualziando tabela de profissional
                // com o avatar
                Profissional::whereUsuarioId( $userId )
                               ->update([
                                    'avatar' => $file
                                ]);
            }
        }

        // caso tenha solicitado para remover o avatar
        if( $request->input('inputRemoverAvatar') == 1 )
        {
            // atualziando tabela de profissional
            // com o avatar
            Profissional::whereUsuarioId( $userId )
                           ->update([
                                'avatar' => null
                            ]);
        }

        // redirecionando para a tela do perfil
        return response()->json(true);
    }

    /**
     * Função para editar os dados do profissional
     */
    public function editarProfissional( Request $request )
    {
        $existe = false;

        if( Auth::check() ):

        // array para salvar os dados
        $data = [
                    'nome' => $request->input('nome_profissional'),
                    'sobrenome' => $request->input('sobrenome_profissional'),
                    'rua' => $request->input('rua_profissional'),
                    'numero'    => $request->input('numero_profissional'),
                    'bairro' => $request->input('bairro_profissional'),
                    'cidade' => $request->input('cidade_profissional'),
                    'estado' => $request->input('estado_profissional'),
                    'cep' => $request->input('cep_profissional')
                ];

        // criptografando a senha
        if( $request->has('novasenha') )
        {
            $senha = $request->input('novasenha');

            if($senha != null && $senha != ''){
                // atualizando no banco de dados
                Usuario::whereId( Auth::user()->toArray()['id'] )
                            ->update([
                                'senha' => Hash::make($senha)
                            ]);
            }
        }

        // caso exista CPF
        // e o paciente ainda esteja sem CPF no cadastro
        if(
            $request->has('cpf_profissional') &&
            strlen( Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )->first()->toArray()['cpf'] ) == 0
        )
        {
            // verificando se o CPF é válido
            $cpf = $request->input('cpf_profissional') ?? $request->input('cpf');
            $cpf = preg_replace('/[^0-9]/', '', $cpf);

            $existe = Profissional::cpfJaExiste( $cpf, Auth::user()->toArray()['id'] );

            $data['cpf'] = $cpf;
        }

        // apenas se o CPF não existir
        if( !$existe )
        {
            Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )
                             ->update($data);
        }

        // redirecionando para o perfil do usuário logado
            return redirect()->action('PerfilController@meu');
        else:
            return redirect()->action('IndexController@index');
        endif;
    }

    /*
    * [Método]: verificarMoip
    * [Descrição]:  Verifica se o profissional possui autorização do Moip
    * 
    * @author gustavoaguiar 
    */

    public static function  verificarMoip() {
        if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
        {
            $userId         = Auth::user()->toArray()['id'];
            $profissional   = Profissional::where('usuario_id', $userId )
                           ->get()
                           ->first();

            if($profissional->moip_auth != null && $profissional->moip_auth != '')
                return true;
            else
                return false;

        }
        else
            return false;
    }
    /**
     * Esta função recebe a requisição de nova especialidade do profissional
     */
    public function novaEspecialidade( Request $request )
    {
        // apenas se o usuário logado for profissional
        if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
        {
            $dados = $request->all();

            // verificando nova especialidade
            $rules = [
                'nomeEspecialidade' => 'required|min:3|max:220',

                // documentos
                'arquivoEspecialidade' => 'required|mimes:jpg,jpeg,png,gif,svg,tiff,targa,bmp,pdf,doc,docx,rtf|max:5000'
            ];

            $messages = [
                'required' => 'O campo :attribute é obrigatório.',
                'mimes' => 'Só são permitidos arquivos do tipo: :values',

                // personalização de campo, por causa de tradução
                'nomeEspecialidade.required' => 'O nome da especialidade é obrigatório.',
                'nomeEspecialidade.min' => 'O nome da especialidade deve conter no mínimo :min caracteres.',
                'nomeEspecialidade.max' => 'O nome da especialidade deve conter no máximo :max caracteres.',

                'arquivoEspecialidade.required' => 'É obrigatório anexar um comprovante da especialidade.'
            ];

            $validation = Validator::make(
                $dados,
                $rules,
                $messages
            );

            // no caso de falha de validação
            if( $validation->fails() )
            {
                $errors = $validation->messages();

                // redirecionando com os erros
                // para exibir ao usuário
                // mantendo os campos
                $request->flash();

                // retornando view
                return $this->padraoViewProfissional( Auth::user()->toArray()['id'] )
                            ->withInput($request->all())
                            ->withErrors($errors)
                            ->with('abrirModal', 'modalEspecialidades');
            }
            // caso esteja válido
            // verificar a duplicidade da especialidade
            else if( Especialidade::possuiEspecialidade( Auth::user()->toArray()['id'], trim($dados['nomeEspecialidade']) ) )
            {
                // adicioando o erro
                $errors = new MessageBag(['nomeEspecialidade' => ['Esta especialidade já está vinculada ao seu perfil, caso ainda não esteja aparecendo, aguarde o processo de análise para a liberação.']]);

                // redirecionando com os erros
                // para exibir ao usuário
                // mantendo os campos
                $request->flash();

                // retornando view
                return $this->padraoViewProfissional( Auth::user()->toArray()['id'] )
                            ->withInput($request->all())
                            ->withErrors($errors)
                            ->with('abrirModal', 'modalEspecialidades');
            }
            // no caso de documento inválido
            else if( !Input::file('arquivoEspecialidade')->isValid() )
            {
                // adicioando o erro
                $errors = new MessageBag(['arquivoEspecialidade' => ['Este arquivo não é válido, por favor, envie outro arquivo.']]);

                // redirecionando com os erros
                // para exibir ao usuário
                // mantendo os campos
                $request->flash();

                // retornando view
                return $this->padraoViewProfissional( Auth::user()->toArray()['id'] )
                            ->withInput($request->all())
                            ->withErrors($errors)
                            ->with('abrirModal', 'modalEspecialidades');
            }
            // caso esteja realmente tudo ok
            // inserindo no banco de dados
            else
            {
                $uploadPath = "upload/user/" . Auth::user()->toArray()['id'];

                // salvando o documento
                // armazenando na pasta
                $file = "especialidade_" . microtime() . "." . pathinfo(Input::file('arquivoEspecialidade')->getClientOriginalName(), PATHINFO_EXTENSION);

                $file = str_replace(' ', '-', $file);

                Input::file('arquivoEspecialidade')->move( $uploadPath, $file );

                // pegando dados do profissional
                $profissional = Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )->first()->toArray();

                $especialidade = new Especialidade;
                $especialidade->profissional_id = $profissional['id'];
                $especialidade->especialidade = trim( $dados['nomeEspecialidade'] );
                $especialidade->documento = $file;
                $especialidade->save();

                // abrindo a tela com o aviso de que deve aguardar
                return $this->padraoViewProfissional( Auth::user()->toArray()['id'] )
                            ->with('avisoSistema', [
                                "titulo" => "Nova especialidade salva com sucesso!",
                                "mensagem" => "Sua solicitação de nova especialidade ({$dados['nomeEspecialidade']}), foi registrada com sucesso! Aguarde o processo de validação, quando sua solicitação for aceita, a especialidade solicitada será exibida em seu perfil."
                            ])
                            ->with('abrirModal', 'avisoSistema');
            }
        }
        else
        {
            // retornar um erro
            return response('Checagem negada!', 400);
        }
    }

    /**
     * Esta função verifica se a nova especialidade pode ser cadastrada ao profissional logado
     */
    public function duplicidadeEspecialidade( Request $request )
    {
        // apenas se o usuário logado for profissional
        if( Auth::check() )
        {
            if( $request->has('dados'))
            {
                $dados = $request->input('dados');

                for ($i=0; $i < Count($dados); $i++) {
                    $campo  = explode('_', $dados[$i]["name"]);
                    if($campo != null && $campo[0] != '' && $campo[0] == 'visibilidade'){
                        if(($campo[1] != null)){
                            if($dados[$i]["value"] == 'PUBLICO' || $dados[$i]["value"] == 'PRIVADO')
                                Especialidade::where('id', $campo[1])->update(['visibilidade' => $dados[$i]["value"]] );
                        }
                    }
                }
            }
            if( $request->has('especialidade')){
                // recuperando a listagem de especialidades
                // para verificar se a informada já existe ou não
                $existe = Especialidade::possuiEspecialidade( Auth::user()->toArray()['id'], trim($request->input('especialidade')) );

                if( !$existe )
                    return response('Esta especialidade ainda não existe para este profissional!', 200);
                else
                    return response('Especialidade já existe para este profissional!', 400);
            }
        }
        else
        {
            // retornar um erro
            return response('Checagem negada!', 400);
        }
    }

    /**
     * Função para salvar a edição do curriculo do profissional logado
     */
    public function atualizarCurriculo( Request $request )
    {
        // apenas se o usuário logado for profissional
        if( Auth::check())
        {
            $usuario_id = Auth::user()->toArray()['id'];
            // Verifica se o usuário é profissiona;
            if(Usuario::getUsuarioTipo($usuario_id, 2)):
                // atualizando
                Profissional::whereUsuarioId($usuario_id)
                    ->update([
                        'experiencia_profissional' => $request->input('experiencia'),
                        'afiliacoes' => $request->input('afiliacao'),
                        'formacao_academica' => $request->input('formacao')
                    ]);
            endif;
            // redirecionando para o perfil do usuário logado
           return redirect()->action('PerfilController@meu');
        }
        else{
            // redirecionando para o perfil do usuário logado
           return redirect()->action('IndexController@index');
        }
    }

    /**
     * Função para salvar a edição dos valores do profissional
     */
    public function atualizarValores( Request $request )
    {
        // apenas se o usuário logado for profissional
        if( Auth::check()  )
        {
            // pegando dados do profisisonal
            $profissional = Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )->first()->toArray();

            $dados = $request->all();

            // percorrendo com base nos ids informados
            foreach($dados['id']  as $k => $id )
            {
                // apenas se o serviço existir e for deste profissional
                $item = ProfissionalServico::whereProfissionalId( $profissional['id'] )
                            ->where('id', $id)
                            ->first();

                // atualizando
                // caso o preço relacionado tenha sido informado
                if( $item && $request->has("price_{$id}") )
                {
                    ProfissionalServico::whereId( $item->toArray()['id'] )
                        ->whereProfissionalId( $profissional['id'] )
                        ->update([
                            "valor" => str_replace([".",","], ["", "."], $dados["price_{$id}"]),
                            "data_modificacao" => Carbon::now()
                        ]);
                }
                if( $item && $request->has("visibilidade_{$id}") )
                {
                    ProfissionalServico::whereId( $item->toArray()['id'] )
                        ->whereProfissionalId( $profissional['id'] )
                        ->update([
                            "visibilidade" => $dados["visibilidade_{$id}"]
                        ]);
                }
            }
            if($request->has('primeiraConsultaGratis')){
                // caso ele tenha definido que aceita a primeira consulta gratis
                    Profissional::whereId( $profissional['id'] )
                            ->update([
                                'primeiro_contato_gratis' => $request->input('primeiraConsultaGratis')
                            ]);
            }

        }

            return redirect()->action('PerfilController@meu');
    }

    /**
     * Esta função salva as alterações nos horários de atendimento do profissional
     */
    public function atualiarHorarioAtendimento( Request $request )
    {
        // apenas se o usuário logado for profissional
        if( Auth::check() && Auth::user()->grupoSistema() == 'profissional' )
        {
            // recuperando a listagem de horários 'DE' do sistema
            $de = HorarioAtendimento::lista('de');

            // recuperando a listagem de horários 'ATE' do sistema
            $ate = HorarioAtendimento::lista('ate');

            // recuperando id do profissional logado
            $profissionalId = Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )->first()->toArray()['id'];

            // percorrendo todos os dias do sistema
            foreach( HorarioAtendimento::diasSemana() as $k => $dia )
            {
                // verificando se o dia atual não possui atendimento
                if( $request->has("sem_atendimento_{$k}") )
                {
                    // inserir o registro e caso exista apenas atualiza
                    ProfissionalDiaAtendimento::createOrUpdate(
                        // dados
                        [
                            "dia_semana" => $k,
                            "profissional_id" => $profissionalId,
                            "sem_atendimento" => 1
                        ],

                        // colunas para chave, verificar se existem ou não
                        [
                            "dia_semana" => $k,
                            "profissional_id" => $profissionalId
                        ]
                    );
                }
                // caso não haja
                // verificar se ele existe no request para salvar os horários
                else if( $request->has("horario_de_{$k}") && $request->has("horario_ate_{$k}") )
                {
                    // apenas se forem o DE menor do que o ATE
                    if(
                        // ordem de grandeza
                        strtotime($request->input("horario_de_{$k}")) < strtotime($request->input("horario_ate_{$k}")) &&

                        // existem na listagem de horários válidos
                        in_array($request->input("horario_de_{$k}"), $de) &&
                        in_array($request->input("horario_ate_{$k}"), $ate)
                    )
                    {
                        // inserir o registro e caso exista apenas atualiza
                        ProfissionalDiaAtendimento::createOrUpdate(
                            // dados
                            [
                                "dia_semana" => $k,
                                "profissional_id" => $profissionalId,
                                "horario_inicio" => $request->input("horario_de_{$k}"),
                                "horario_fim" => $request->input("horario_ate_{$k}"),
                                "sem_atendimento" => 0
                            ],

                            // colunas para chave, verificar se existem ou não
                            [
                                "dia_semana" => $k,
                                "profissional_id" => $profissionalId
                            ]
                        );
                    }
                }
            }
        }

        // redirecionando para o perfil do usuário logado
        return redirect()->action('PerfilController@meu');
    }

    /**
     * Esta função recebe a requisição para bloqueio de horário na agenda do profissional
     */
    public function salvarBloqueioHorario( Request $request )
    {
        $dados = $request->all();

        if( isset($dados['diaBloqueio']) && isset($dados['horarioBloqueio']) && Auth::check() )
        {
            // Converter data
            $data = substr($dados['diaBloqueio'],6,4)."-".substr($dados['diaBloqueio'],3,2)."-".substr($dados['diaBloqueio'],0,2);

            // verificar se é uma data e hora válida
            $dateTime = \DateTime::createFromFormat('Y-m-d H:i', "{$data} {$dados['horarioBloqueio']}");

            if( $dateTime )
            {
                // recuperando id do profissional logado
                $profissionalId = Profissional::whereUsuarioId( Auth::user()->toArray()['id'] )->first()->toArray()['id'];

                // inserir o registro e caso exista apenas atualiza
                ProfissionalHorarioBloqueado::createOrUpdate(
                    // dados
                    [
                        "dia_horario" => $dateTime->format('Y-m-d H:i:s'),
                        "profissional_id" => $profissionalId
                    ],

                    // colunas para chave, verificar se existem ou não
                    [
                        "dia_horario" => $dateTime->format('Y-m-d H:i:s'),
                        "profissional_id" => $profissionalId
                    ]
                );
            }
        }

        // redirecionando para o perfil do usuário logado
        return redirect()->action('PerfilController@meu');
    }
}
