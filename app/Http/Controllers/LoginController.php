<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;
use Validator;
use View;
use Hash;
use Session;
use \Illuminate\Support\MessageBag as MessageBag;

// utilitário para disparo de e-mail
use Mail;

// carregando models
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\TokenResetSenha as TokenResetSenha;
use LogDialog\Model\Slider as Slider;

class LoginController extends Controller
{
    /**
     * Autenticação de login
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function index( Request $request )
    {
    	$dados = $request->all();


    	// caso seja uma requisição post
    	if( $request->isMethod('post') )
    	{
    		$rules = [
	            'login' => 'required',
	            'senha' => 'required|min:5|max:110'
	        ];

	        $messages = [
	            'required' => 'O campo :attribute é obrigatório.',
	            'min' => 'O campo :attribute deve conter no mínimo :min caracteres.',
	            'max' => 'O campo :attribute deve conter no máximo :max caracteres.'
	        ];

	    	$validation = Validator::make(
			    array(
			        'login' => $dados['login'],
			        'senha' => $dados['senha']
			    ),
			    $rules,
			    $messages
			);

			// no caso de falha de validação
			if( $validation->fails() )
			{
			    $errors = $validation->messages();

			    // redirecionando com os erros
			    // para exibir ao usuário
			   	$request->flash();

				return View::make('index.home')
							->withInput($request->all())
							->withErrors($errors)
							->with('abrirModal', 'modal-log')
                            ->with('sliders', Slider::get())
							->with('profissionais', Profissional::carregarLista(18));
			}
			else
			{
				// verificando se o perfil está ativado
				$ativado = Usuario::whereUsername($request->get('login'))
				                       ->where('data_ativado', '!=', null)
				                       ->first();

				// se estiver ativado

				if( $ativado )
				{
					// verificando se é profissional
					if( Profissional::whereUsuarioId( $ativado->toArray()['id'] )->first() )
					{
						// se for, a documentação precisa estar aprovada
            $aprovado = Profissional::whereUsuarioId( $ativado->toArray()['id'] )
						                             ->where('data_aprovada_documentacao', '!=', null)
						                             ->first();

						// caso não esteja aprovada
						if( !$aprovado )
						{
							$errors = new MessageBag(['login' => ['Documentação em análise.']]);

						   	return View::make('index.home')
									->withInput($request->all())
									->withErrors($errors)
                                    ->with('sliders', Slider::get())
									->with('abrirModal', 'modal-log')
									->with('profissionais', Profissional::carregarLista(18));
						}
						else
						{
							// autenticando
					    	$credenciais = [
					    		'username' => $request->get('login'),
					    		'password' => $request->get('senha'),
					    	];

					    	if( Auth::attempt($credenciais) )
					    	{
					    		// echo '<pre>';
					    		// print_r( Auth::user() );
					    		// var_dump( Auth::login(Auth::user()) );
					    		// exit;
					    		//if(Auth::check())
							    	return redirect(action('PerfilController@meu'));
							}
							else
							{
								$errors = new MessageBag(['login' => ['Dados inválidos.']]);

							   	return View::make('index.home')
										->withInput($request->all())
										->withErrors($errors)
                                        ->with('sliders', Slider::get())
										->with('abrirModal', 'modal-log')
										->with('profissionais', Profissional::carregarLista(18));
							}
						}
					}
					// caso seja paciente
					else
					{
            
						// autenticando
				    	$credenciais = [
				    		'username' => $request->get('login'),
				    		'password' => $request->get('senha'),
				    	];

				    	if( Auth::attempt($credenciais) )
				    	{
						    return redirect(action('PerfilController@meu'));
						}
						else
						{
							$errors = new MessageBag(['login' => ['Dados inválidos.']]);

						   	return View::make('index.home')
									->withInput($request->all())
									->withErrors($errors)
									->with('abrirModal', 'modal-log')
                                    ->with('sliders', Slider::get())
									->with('profissionais', Profissional::carregarLista(18));
						}
					}
				}
				// caso ainda não esteva ativado
				else
				{
					$errors = new MessageBag(['login' => ['Perfil pendente de ativação.']]);

				   	return View::make('index.home')
							->withInput($request->all())
							->withErrors($errors)
							->with('abrirModal', 'modal-log')
                            ->with('sliders', Slider::get())
							->with('profissionais', Profissional::carregarLista(18));
				}
			}
    	}
    	else
    	{
    		return view('index.home')
                   ->with('sliders', Slider::get())
    		       ->with('profissionais', Profissional::carregarLista(18));
    	}
    }

    /**
     * Executar o logout
     */
    public function logout()
    {
    	Auth::logout();

    	return redirect()->action("IndexController@index");
    }

    /**
     * Esta função
     * Recebe a solicitação de nova senha
     * Registra no banco de dados e envia o e-mail
     */
    public function solicitarNovaSenha( Request $request )
    {
    	// verificando se o e-mail foi informado
    	if( $request->has('emailNovaSenha') )
    	{
    		// verificando se o e-mail existe no sistema e está ativo
    		if( Usuario::emailJaCadastrado($request->input('emailNovaSenha')) )
    		{
    			// pegando dados do usuário
    			$user = Usuario::whereEmail($request->input('emailNovaSenha'))
    						->first()
    						->toArray();

    			// pegando token válido
    			$token = TokenResetSenha::token( $user['id'] );

    			Mail::send('email-mkt.esqueci-senha', ["userId" => $user['id'], "token" => $token], function ($mail) use ($request) {
		            $mail->from('logdialog@criaturo.com', 'LogDialog');

		            $mail->to($request->input('emailNovaSenha'), "")->subject('LogDialog, redefinição de senha.');
		        });

		        // renderizando a tela com a mensagem de e-mail enviado
		        return View::make('index.redefinicao-senha-enviado')
								->with('email', $request->input('emailNovaSenha'));
    		}
    		else
    		{
    			// renderizando com erro de inexistente
    			$errors = new MessageBag(['email' => ['O e-mail informado não foi encontrado, verifique e tente novamente.']]);

	    		return View::make('index.home')
								->withInput($request->all())
								->withErrors($errors)
                       			->with('sliders', Slider::get())
								->with('abrirModal', 'esqueciSenha')
								->with('profissionais', Profissional::carregarLista(18));
    		}
    	}
    	else
    	{
    		// renderizando layout com aviso de erro
    		$errors = new MessageBag(['email' => ['É obrigatório informar o e-mail.']]);

    		return View::make('index.home')
							->withInput($request->all())
							->withErrors($errors)
                       		->with('sliders', Slider::get())
							->with('abrirModal', 'esqueciSenha')
							->with('profissionais', Profissional::carregarLista(18));
    	}
    }

    /**
     * Esta função exibe a tela para redefinição de senha
     */
    public function telaNovaSenha( Request $request, $userId, $token )
    {
    	// verificando se os dados são válidos
    	// apenas se não for POST
    	// pois as requisições POST são para salvar a senha
    	if( !$request->isMethod('post') )
    	{
    		// verificando validade
    		// se for nulo, não é mais válido
    		$valido = TokenResetSenha::whereDeUsuarioId( $userId )
    		               ->whereToken( $token )
    		               ->where('data_expiracao', '>=', strtotime('now'))
    		               ->whereUsado(0)
    		               ->first();

    		// renderizando tela de troca de senha
    		return View::make('index.formulario-nova-senha')
    				   ->with('userId', $userId)
    				   ->with('token', $token)
					   ->with('valido', !is_null($valido));
    	}
    	else
    	{
    		$dados = $request->all();

    		// validando nova senha
    		$rules = [
	            'senha' => 'required|min:6|max:150',
	            'conf_senha' => 'required|min:6|max:150|same:senha'
	        ];

	        $messages = [
	            // personalização de campo, por causa de tradução
	            'senha.required' => 'O campo senha é obrigatório.',
	            'senha.min' => 'O campo senha deve conter no mínimo :min caracteres.',
	            'senha.max' => 'O campo senha deve conter no máximo :max caracteres.',

	            'conf_senha.required' => 'O campo confirmação de senha é obrigatório.',
	            'conf_senha.min' => 'O campo confirmação de senha deve conter no mínimo :min caracteres.',
	            'conf_senha.max' => 'O campo confirmação de senha deve conter no máximo :max caracteres.',

	            // comparação
	            'conf_senha.same' => 'O campo confirmação de senha não coincide com a senha.'
	        ];

	    	$validation = Validator::make(
			    $dados,
			    $rules,
			    $messages
			);

			// no caso de falha de validação
			if( $validation->fails() )
			{
			    $errors = $validation->messages();

			    // redirecionando com os erros
			    // para exibir ao usuário
			   	$request->flash();

				return View::make('index.formulario-nova-senha')
							->withInput($request->all())
							->withErrors($errors)

							->with('userId', $userId)
	    				    ->with('token', $token)
						    ->with('valido', true);
			}
			else
			{
				// executando alteração da senha
				// no perfil do usuário
				Usuario::whereId( $userId )
				            ->update(["senha" => Hash::make( $request->input("senha") )]);

				// setando token como usado
				TokenResetSenha::whereDeUsuarioId( $userId )
				            ->whereToken( $token )
				            ->update(["usado" => 1]);

				// saída de template
				return View::make('index.formulario-nova-senha')
							->with('userId', $userId)
	    				    ->with('token', $token)
						    ->with('valido', false)
						    ->with('alterado', true);
			}
    	}
    }
}
