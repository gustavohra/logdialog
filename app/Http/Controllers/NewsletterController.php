<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando model
use LogDialog\Model\Newsletter as Newsletter;

// carregando utilitários
use Auth;
use Validator;

class NewsletterController extends Controller
{
    /**
     * Esta função recebe o cadastro de newsletter
     */
    public function cadastro(Request $request)
    {
    	$dados = $request->all();

    	$rules = [
            'email' => 'required|email'
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.',
            'email' => 'O campo :attribute deve conter um email válido.'
        ];

    	$validation = Validator::make(
		    $dados,
		    $rules,
		    $messages
		);

    	// no caso de falha de validação
		if( $validation->fails() )
		{
		    // saída json
		    return response()->json(['errors' => $validation->messages()]);
		}
		// agora no caso de estar tudo válido
		else
		{
			$dados['email'] = trim($dados['email']);

			// validando se o domínio existe
			$teste = explode("@",$dados['email']);

			if( !checkdnsrr(array_pop($teste),"ANY") )
			{
				$validation->getMessageBag()->add('email', 'O e-mail informado não possui um domínio válido');

				// saída json
		    	return response()->json(['errors' => $validation->messages()]);
			}

			// verifica se o email informado já está cadastrado
			if( Newsletter::emailJaCadastrado($dados['email']) )
			{
				$validation->getMessageBag()->add('email', 'O e-mail informado já está cadastrado');

				// saída json
		    	return response()->json(['errors' => $validation->messages()]);
			}

			// executando cadastro
			Newsletter::insert([
						'email' => $dados['email']
	    			]);

			// retornando o ok
			return response()->json(['sucesso' => 1]);
		}
    }
}
