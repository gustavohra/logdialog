<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;

class ComoFuncionaController extends Controller
{
    /**
     * Tela com o texto principal de como funciona
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function index()
    {
    	echo 'Tela com o "Como funciona?"';
    }
}
