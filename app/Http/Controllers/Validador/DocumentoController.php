<?php

namespace LogDialog\Http\Controllers\Validador;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use LogDialog\Http\Controllers\Controller;

use Auth;

// carregando models
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;

class DocumentoController extends Controller
{
    /**
     * Função para validar documentos remotamente
     */
    public function validar( Request $request )
    { 

        	// se tiver o cpf de paciente ou de profissional
            if( $request->has('cpf_paciente') ||
                $request->has('cpf_profissional') ||
                $request->has('cpf') )
            {
                $cpf = $request->input('cpf') ?? $request->input('cpf_profissional') ?? $request->input('cpf_paciente') ?? '000';

                $cpf = preg_replace('/[^0-9]/', '', $cpf);

                // retornando de acordo com a validação
                if( self::valida_cpf($cpf) )
                    return response('CPF Válido!', 200);
                else
                    return response('CPF Inválido!', 400);
            }
            else
                return response()->json(false);
    }


    /**
     * Validação de CPF
     * 
     * @param string $cpf O CPF com ou sem pontos e traço
     * @return bool True para CPF correto - False para CPF incorreto
     *
     */
    public static function valida_cpf( $cpf = false )
    {
        // Exemplo de CPF: 025.462.884-23
        
        $cpf = preg_replace('/[^0-9]/', '', $cpf);

        if(
            $cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999'
        )
        {
            $cpf = false;
        }

        // Verifica se o CPF foi enviado
        if( ! $cpf )
        {
            return false;
        }
     
        // Remove tudo que não é número do CPF
        // Ex.: 025.462.884-23 = 02546288423
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );
     
        // Verifica se o CPF tem 11 caracteres
        // Ex.: 02546288423 = 11 números
        if( strlen( $cpf ) != 11 )
        {
            return false;
        }   
     
        // Captura os 9 primeiros dígitos do CPF
        // Ex.: 02546288423 = 025462884
        $digitos = substr($cpf, 0, 9);
            
        function calc_digitos_posicoes( $digitos, $posicoes = 10, $soma_digitos = 0 )
        {
            // Faz a soma dos dígitos com a posição
            // Ex. para 10 posições: 
            //   0    2    5    4    6    2    8    8   4
            // x10   x9   x8   x7   x6   x5   x4   x3  x2
            //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
            for( $i = 0; $i < strlen( $digitos ); $i++  )
            {
                $soma_digitos = $soma_digitos + ( $digitos[$i] * $posicoes );
                $posicoes--;
            }
     
            // Captura o resto da divisão entre $soma_digitos dividido por 11
            // Ex.: 196 % 11 = 9
            $soma_digitos = $soma_digitos % 11;
     
            // Verifica se $soma_digitos é menor que 2
            if( $soma_digitos < 2 )
            {
                // $soma_digitos agora será zero
                $soma_digitos = 0;
            }
            else
            {
                // Se for maior que 2, o resultado é 11 menos $soma_digitos
                // Ex.: 11 - 9 = 2
                // Nosso dígito procurado é 2
                $soma_digitos = 11 - $soma_digitos;
            }
     
            // Concatena mais um dígito aos primeiro nove dígitos
            // Ex.: 025462884 + 2 = 0254628842
            $cpf = $digitos . $soma_digitos;
            
            // Retorna
            return $cpf;
        } 

        // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
        $novo_cpf = calc_digitos_posicoes( $digitos );
        
        // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
        $novo_cpf = calc_digitos_posicoes( $novo_cpf, 11 );
        
        // Verifica se o novo CPF gerado é idêntico ao CPF enviado
        if( $novo_cpf === $cpf )
        {
            // CPF válido
            return true;
        }
        else
        {
            // CPF inválido
            return false;
        }
    }

    /**
     * Esta validação
     * É para verificar se o CPF informado já está sendo usado por outra pessoa
     */
    public function duplicidadeCpfPaciente( Request $request )
    {
        // se não estiver logado
        if(Auth::check()): 

        $cpf = preg_replace('/[^0-9]/', '', $request->input('cpf'));

        // verificando se o CPF já está em uso por outro paciente
        // que não seja o que já está logado
        $existe = Paciente::cpfJaExiste( $cpf, Auth::user()->id );

            // caso exista
            if( $existe )
                return response('CPF Duplicado!', 400);
            else
                return response('CPF Liberado!', 200);
        else:
             return response('Acesso negado!', 400);
         endif;
    }

    /**
     * Esta validação
     * É para verificar se o CPF informado já está sendo usado por outra pessoa
     */
    public function duplicidadeCpfProfissional( Request $request )
    {  


        // se não estiver logado
        if(Auth::check()):
            $cpf = preg_replace('/[^0-9]/', '', $request->input('cpf_profissional'));

            // verificando se o CPF já está em uso por outro paciente
            // que não seja o que já está logado
            $existe = Profissional::cpfJaExiste( $cpf, Auth::user()->id );
            // caso exista
            if( $existe )
                return response('CPF Duplicado!', 400);
            else
                return response('CPF Liberado!', 200);
        else:
             return response('Acesso negado!', 400);
         endif;
    }
}
