<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;
use Carbon\Carbon;
use Hash;
use Avatar;
use Mail;

// carregando models
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\TipoNotificacao as TipoNotificacao;
use LogDialog\Model\Notificacao as Notificacao;
use LogDialog\Model\Anotacao as Anotacao;

class AgendaController extends Controller
{
    /**
     * Função para listar as consultas do usuário logado
     */
    public function minhasConsultas( Request $request )
    {
    	if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // dependendo do perfil
        // carregar o layout correspondente
        // verificando o perfil
        $usuarioId = Auth::user()->toArray()['id'];

        if( Auth::user()->grupoSistema() == 'paciente' )
        {
            // pegando dados do perfil de paciente
            $usuario = Paciente::whereUsuarioId( $usuarioId )
                            ->first()
                            ->toArray();

            // tratamento para o paciente
            $usuario['tratamento'] = '';
        }
        // caso seja profissional
        else if( Auth::user()->grupoSistema() == 'profissional' )
        {
            // pegando dados do perfil de paciente
            $usuario = Profissional::whereUsuarioId( $usuarioId )
                            ->first()
                            ->toArray();

            // tratamento para o profissional
            $usuario['tratamento'] = $usuario['sexo'] == 'feminino' ? 'Dra.': 'Dr.';
        }
        else
        {
            echo "Perfil não encontrado";
            exit;
        }

        // pegando dados das duas próximas consultas
        $consultas = Agenda::listaConsultas( $usuarioId, 10, 0, 'DESC' );
     
        // carregando view
        return view('agenda.minha')
                ->with('consultas', $consultas)
                ->with('usuario', $usuario);
    }

    /**
     * Confirmar consulta
     */
    public function confirmarConsulta( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('agendaId') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // recuperando dados da agenda
        $agenda = Agenda::find( $request->input('agendaId') )->toArray();

        $paciente = Paciente::verDetalhes( Paciente::find($agenda['paciente_id'])->toArray()['usuario_id'] );
        $profissional = Profissional::verDetalhes( Profissional::find($agenda['profissional_id'])->toArray()['usuario_id'] );

        // atualizando o registro de consulta
        Agenda::whereId( $request->input('agendaId') )
        ->update([
            "confirmado_usuario_convidado" => 1,
            "data_confirmacao" => date("Y-m-d H:i:s")
        ]);

        // desativando a notificação relacionada a esta consulta
        // para o usuário destino
        Notificacao::whereRelacionadoAgendaId( $request->input('agendaId') )
        ->whereParaUsuarioId( Auth::user()->toArray()['id'] )
        ->update([
            "exibir" => 0
        ]);

        // avisando o paciente
        Notificacao::insert([
            "para_usuario_id" => $paciente['usuario_id'],
            "tipo_notificacao_id" => TipoNotificacao::whereTipo('agendamento_confirmado')->first()->toArray()['id'],
            "relacionado_usuario_id" => $profissional['usuario_id'],
            "relacionado_agenda_id" => $request->input('agendaId'),
            "texto" => "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}, confirmou seu agendamento"
        ]);

        // notificando por e-mail
        Mail::send('email-mkt.consulta-confirmada', ["texto" => "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}, confirmou seu agendamento"], function ($mail) use ($paciente) {
            $mail->from('logdialog@criaturo.com', 'LogDialog');

            $mail->to($paciente['email'], "")->subject('LogDialog, confirmação de consulta.');
        });
    }

    /**
     * Recusar consulta
     */
    public function recusarConsulta( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('agendaId') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // recuperando dados da agenda
        $agenda = Agenda::find( $request->input('agendaId') )->toArray();

        $paciente = Paciente::verDetalhes( Paciente::find($agenda['paciente_id'])->toArray()['usuario_id'] );
        $profissional = Profissional::verDetalhes( Profissional::find($agenda['profissional_id'])->toArray()['usuario_id'] );

        // atualizando o registro de consulta
        Agenda::whereId( $request->input('agendaId') )
        ->update([
            "data_cancelamento" => date("Y-m-d H:i:s")
        ]);

        // desativando a notificação relacionada a esta consulta
        // para o usuário destino
        Notificacao::whereRelacionadoAgendaId( $request->input('agendaId') )
        ->whereParaUsuarioId( Auth::user()->toArray()['id'] )
        ->update([
            "exibir" => 0
        ]);

        // avisando o paciente
        Notificacao::insert([
            "para_usuario_id" => $paciente['usuario_id'],
            "tipo_notificacao_id" => TipoNotificacao::whereTipo('agendamento_cancelado')->first()->toArray()['id'],
            "relacionado_usuario_id" => $profissional['usuario_id'],
            "relacionado_agenda_id" => $request->input('agendaId'),
            "texto" => "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}, cancelou seu agendamento"
        ]);

        // notificando por e-mail
        Mail::send('email-mkt.consulta-recusada', ["texto" => "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}, recusou seu agendamento"], function ($mail) use ($paciente) {
            $mail->from('logdialog@criaturo.com', 'LogDialog');

            $mail->to($paciente['email'], "")->subject('LogDialog, consulta recusada.');
        });
    }

    /**
     * Recusar consulta
     */
    public function cancelarConsulta( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('agendaId') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // recuperando dados da agenda
        $agenda = Agenda::find( $request->input('agendaId') )->toArray();
        $motivo = urldecode($request->input('motivo'));

        $paciente = Paciente::verDetalhes( Paciente::find($agenda['paciente_id'])->toArray()['usuario_id'] );
        $profissional = Profissional::verDetalhes( Profissional::find($agenda['profissional_id'])->toArray()['usuario_id'] );

        // atualizando o registro de consulta
        Agenda::whereId( $request->input('agendaId') )
        ->update([
            "data_cancelamento" => date("Y-m-d H:i:s")
        ]);

        // desativando a notificação relacionada a esta consulta
        // para o usuário destino
        Notificacao::whereRelacionadoAgendaId( $request->input('agendaId') )
        ->whereParaUsuarioId( Auth::user()->toArray()['id'] )
        ->update([
            "exibir" => 0
        ]);

        // avisando o paciente
        Notificacao::insert([
            "para_usuario_id" => $paciente['usuario_id'],
            "tipo_notificacao_id" => TipoNotificacao::whereTipo('agendamento_cancelado')->first()->toArray()['id'],
            "relacionado_usuario_id" => $profissional['usuario_id'],
            "relacionado_agenda_id" => $request->input('agendaId'),
            "texto" => "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}, cancelou seu agendamento"
        ]);

        // notificando por e-mail
        Mail::send('email-mkt.consulta-cancelada', ["texto" => "{$profissional['tratamento']} {$profissional['nome']} {$profissional['sobrenome']}, cancelou sua consulta. <br /><br /><b>Motivo:</b><br />".$motivo], function ($mail) use ($paciente) {
            $mail->from('logdialog@criaturo.com', 'LogDialog');

            $mail->to($paciente['email'], "")->subject('LogDialog, consulta cancelada.');
        });
            return redirect()->action('PerfilController@meu');
    }
    /**
     * Esta função retorna um json com os detalhes da consulta informada
     * Para mostrar as anotações da consulta na visão dia
     */
    public function carregarAnotacoesConsulta( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('agendaId') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // buscando anotações relacionadas ao agendamento informado
        // na verdade a única anotação, pois por enquanto os usuários fazem uma única nota
        // sobre o agendamento, mas podem editar
        $nota = Anotacao::whereRelacionadoAgendaId( $request->input('agendaId') )
                ->whereUsuarioId( Auth::user()->toArray()['id'] )
                ->first();

        $resposta = $nota ? $nota->toArray() : ['id' => 0];

        // caso não tenha anotação para este agendamento, com este usuário
        // adicionar item
        if( !isset($resposta['usuario_id']) )
        {
            Anotacao::insert([
                "usuario_id" => Auth::user()->toArray()['id'],
                "relacionado_agenda_id" => $request->input('agendaId')
            ]);
        }

        $nota = Anotacao::whereRelacionadoAgendaId( $request->input('agendaId') )
                ->whereUsuarioId( Auth::user()->toArray()['id'] )
                ->first();

        $resposta = $nota ? $nota->toArray() : ['id' => 0];

        // formatação da data e hora, de acordo com o esperado pelo layout
        // referente ao agendamento
        // com base na hora de início e fim da consulta
        if( isset( $resposta['usuario_id'] ) )
        {
            $consulta = Agenda::detalharItem( $request->input('agendaId') );

            // nome da outra pessoa, de acordo com o perfil logado
            if( Auth::user()->grupoSistema() == 'paciente' )
            {
                $resposta['nome'] = "{$consulta->tratamento_profissional} {$consulta->nome_profissional} {$consulta->sobrenome_profissional}";
            }
            else
            {
                $resposta['nome'] = "{$consulta->nome_paciente} {$consulta->sobrenome_paciente}";
            }
            
            $resposta['data'] = Carbon::parse( $consulta->data_compromisso )->formatLocalized("%A, %d/%m");

            // hora inicio
            $resposta['hora'] = Carbon::parse( $consulta->data_compromisso )->format("H\hi");

            // id do profissional
            $profissional       = Profissional::find($consulta->profissional_id)->toArray();
            $resposta['profissionalId'] = $profissional['usuario_id'];

            $dadosProfissional['profissionalFoto'] = Avatar::urlImagem( $resposta["usuario_id"] );
            $dadosProfissional['profissionalCrp'] = $profissional['numero_crp'];
            $dadosProfissional['profissionalEndereco'] = $profissional['rua'] . ', '. $profissional['numero']. ' - '.$profissional['cidade']. '/'. $profissional['estado'];
            $dadosProfissional['profissionalNome'] = $profissional['nome'] . ' ' . $profissional['sobrenome'];
            $dadosProfissional['consulta'] = $consulta->nome_compromisso;
            $resposta["profissional"]   = urlencode(json_encode($dadosProfissional, JSON_UNESCAPED_UNICODE));
            // link de reagendamento
            $resposta['url_reagendar'] = action("ProfissionaisController@agendaCompleta", ["usuarioId" => $resposta['profissionalId']]);

            // link de reagendamento
            $resposta['url_cancelar'] = action("AgendaController@cancelarConsulta", ["agendaId" => $request->input('agendaId')]);

            // caso tenha também um horário de encerramento
            if( !empty($consulta->data_encerramento) )
                $resposta['hora'] .= " às " . Carbon::parse( $consulta->data_encerramento )->format("H\hi");

            // conteúdo
            if( strlen($resposta['conteudo']) == 0 )
                $resposta['conteudo'] = "Sem anotações";
        }

        // a permissão de reagendamento e edição é de acordo com o perfil
        // apenas pacientes podem fazer isso
        $resposta['pode_reagendar'] = $resposta['pode_editar'] = Auth::user()->grupoSistema() == 'paciente';

        return response()->json( $resposta );
    }

    /**
     * Excluir uma nota
     */
    public function excluirNota( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('notaId') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // verificando se a nota pertence ao usuário logado
        $nota = Anotacao::whereUsuarioId( Auth::user()->toArray()['id'] )
                ->whereId( $request->input('notaId') )
                ->first()
                ->get();

        // caso a nota exista
        // e o usuário logado seja um paciente
        if( $nota &&
            Auth::user()->grupoSistema() == 'paciente' )
        {
            Anotacao::find( $request->input('notaId') )
                ->delete();
        }
    }

    /**
     * Atualizar uma nova
     */
    public function atualizarNota( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('notaId') || !$request->has('conteudo') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // verificando se a nota pertence ao usuário logado
        $nota = Anotacao::whereUsuarioId( Auth::user()->toArray()['id'] )
                ->whereId( $request->input('notaId') )
                ->first()
                ->get();

        // caso a nota exista
        // e o usuário logado seja um paciente
        if( $nota &&
            Auth::user()->grupoSistema() == 'paciente' )
        {
            Anotacao::find( $request->input('notaId') )
                ->update([
                    'conteudo' => strip_tags($request->input('conteudo'))
                ]);
        }
    }

    /**
     * Função para salvar ou atualizar nota durante a consulta
     */
    public function notaConsulta( Request $request )
    {
        if( !Auth::check() )
        {
            return response('Autenticação necessária', 401);
        }

        if( !$request->has('agendaId') || !$request->has('conteudo') || !$request->has('token') )
        {
            return response('Parâmetros incorretos', 401);
        }

        // o token é composto pelo id da consulta e o id do usuário logado
        // mas é em formato de hash do laravel
        // apenas para garantir que o usuário não vai alterar a nota
        // sem permissão
        
        $userId = Auth::user()->toArray()['id'];

        // verificando se o token está correto
        if( Hash::check( "{$request->input('agendaId')}_{$userId}", $request->input('token') ) )
        {
            // verificando se já existe uma anotação para a consulta atual
            // criado pelo usuário logado
            
            // inserir o registro e caso exista apenas atualiza
            Anotacao::createOrUpdate(
                // dados
                [
                    "usuario_id" => $userId,
                    "relacionado_agenda_id" => $request->input('agendaId'),
                    "conteudo" => trim($request->input('conteudo'))
                ],

                // colunas para chave, verificar se existem ou não
                [
                    "usuario_id" => $userId,
                    "relacionado_agenda_id" => $request->input('agendaId')
                ]
            );

            return response('Nota atualizada', 200);
        }
        else
        {
            return response('Acesso negado', 403);
        }
    }
}
