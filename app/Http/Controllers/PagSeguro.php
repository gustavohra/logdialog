<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

class PagSeguro extends Controller
{
    /**
     * Tela para onde o usuário será direcionado no checkout
     */
    public function redirect()
    {
    	echo 'Redirect PagSeguro';
    }

    /**
     * Recebe as notificações da API do PagSeguro
     */
    public function notification()
    {
    	echo 'Notification PagSeguro';
    }
}
