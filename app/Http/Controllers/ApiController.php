<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use LogDialog\Http\Requests;
use Illuminate\Support\Facades\DB;

// carregando models
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\Vantagem as Vantagem;
use LogDialog\Model\Especialidade as Especialidade;
use LogDialog\Model\Slider as Slider;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\ProfissionalServico as ProfissionalServico;
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Pagina as Pagina;

class ApiController extends Controller
{
    public function index(Request $request)
    {
        //if(Auth::check()):
            $metodo     = $request->input('metodo');
            $dados      = $request->input('dados');

            if((method_exists($this, $metodo))) {
                call_user_func(array($this, $metodo), $request);
            }
            else
              return response()->json(array('resposta' => false, 'mensagem' => 'Solicitação inválida'));
        // else:
        //     return response()->json(array('resposta' => false, 'mensagem' => 'Credenciais inválidas'));
        // endif;
    }
    function uploadImagem64($arquivo, $diretorio) {
        if (strpos($arquivo, 'base64,') !== false) {
            if($arquivo != null && $diretorio != null):
                $upload_path      = str_replace( '/', DIRECTORY_SEPARATOR, $diretorio ) . DIRECTORY_SEPARATOR;

                list($type, $arquivo) = explode(';', $arquivo);
                list(, $arquivo)      = explode(',', $arquivo);

                $decoded          = base64_decode($arquivo) ;

                $hashed_filename  = md5( microtime() ) . '_.png';
                if (!file_exists($upload_path)) {
                    mkdir($upload_path, 0755, true);
                }
                $image_upload     = file_put_contents( $upload_path . $hashed_filename, $decoded );

                return url('/') . '/' . $diretorio . '/' . $hashed_filename;
            else:
                return '';
            endif;
        }
        else
            return $arquivo;
    }
    /* Login Administrativo */

    public function realizarLogin(Request $request) {

    }
    /*
    *    Vantagens
    */
    public function getVantagens(Request $request) {
        $dados          = Vantagem::get();
        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirVantagem(Request $request) {
        if(Vantagem::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getVantagem(Request $request) {
        $dados = Vantagem::where('id', $request->input('id'))->first();
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function setVantagem(Request $request) {
        if($request->has('imagem'))
            $imagem = $this->uploadImagem64($request->input("imagem"), 'storage/app/public/site/');
        else
            $imagem = '';

        $dados = Vantagem::where('id', $request->input('id'))->update(['conteudo' => $request->input('conteudo'), 'titulo' => $request->input('titulo'), 'imagem' => $imagem]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function inserirVantagem(Request $request) {
        if($request->has('imagem'))
            $imagem = $this->uploadImagem64($request->input("imagem"), 'storage/app/public/site/');
        else
            $imagem = '';

        $usuarioId  =  Auth::user()->toArray()['id'];
        $dados      = Vantagem::insert(['conteudo' => $request->input('conteudo'), 'titulo' => $request->input('titulo'), 'imagem' => $imagem, 'criado_por_usuario_id' => $usuarioId]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    /*
    *    Sliders
    */
    public function getSliders(Request $request) {
        $dados          = Slider::get();
        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirSlider(Request $request) {
        if(Slider::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getSlider(Request $request) {
        $dados = Slider::where('id', $request->input('id'))->first();
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function setSlider(Request $request) {
        if($request->has('imagem'))
            $imagem = $this->uploadImagem64($request->input("imagem"), 'storage/app/public/site/sliders/');
        else
            $imagem = '';

        $dados = Slider::where('id', $request->input('id'))->update(['url' => $request->input('url'), 'botao_texto' => $request->input('botao_texto'), 'titulo' => $request->input('titulo'), 'imagem' => $imagem]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function inserirSlider(Request $request) {
        if($request->has('imagem'))
            $imagem = $this->uploadImagem64($request->input("imagem"), 'storage/app/public/site/');
        else
            $imagem = '';

        $usuarioId  =  Auth::user()->toArray()['id'];
        $dados      = Slider::insert(['url' => $request->input('url'), 'botao_texto' => $request->input('botao_texto'), 'titulo' => $request->input('titulo'), 'imagem' => $imagem, 'criado_por_usuario_id' => $usuarioId]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    /*
    *    Páginas
    */
    public function getPaginas(Request $request) {
        $dados          = Pagina::get();
        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirPagina(Request $request) {
        if(Pagina::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getPagina(Request $request) {
        $dados = Pagina::where('id', $request->input('id'))->first();
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function setPagina(Request $request) {
        if($request->has('imagem'))
            $imagem = $this->uploadImagem64($request->input("imagem"), 'storage/app/public/site/paginas/');
        else
            $imagem = '';

        // $url    = str_slug($request->input('titulo'), '-');
        $dados = Pagina::where('id', $request->input('id'))->update(['conteudo' => $request->input('conteudo'), 'titulo' => $request->input('titulo'), 'imagem' => $imagem,
         // 'url' => $url
         ]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function setPaginaUrl(Request $request) {
         $url    = str_slug($request->input('url'), '-');
        $dados = Pagina::where('id', $request->input('id'))->update(['url' => $url]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function inserirPagina(Request $request) {
        if($request->has('imagem'))
            $imagem = $this->uploadImagem64($request->input("imagem"), 'storage/app/public/site/');
        else
            $imagem = '';

        $usuarioId  =  Auth::user()->toArray()['id'];
        $url        = str_slug($request->input('titulo'), '-');
        $dados      = Pagina::insert(['conteudo' => $request->input('conteudo'), 'titulo' => $request->input('titulo'), 'imagem' => $imagem, 'criado_por_usuario_id' => $usuarioId, 'url' => $url]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    /*
    *    Profissionais
    */
    public function getProfissionais(Request $request) {
        $dados          = Profissional::get();
        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirProfissional(Request $request) {
        if(DB::table('tb_profissional')->delete($request->input('id')))
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getProfissional(Request $request) {
        $dados = Profissional::where('id', $request->input('id'))->first();
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function bloquearProfissional(Request $request) {
        $dados = Profissional::where('id', $request->input('id'))->update(['data_aprovada_documentacao' => null]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function desbloquearProfissional(Request $request) {
        $dataAprovacao =  Date("Y-m-d H:m:s");
        $dados = Profissional::where('id', $request->input('id'))->update(['data_aprovada_documentacao' => $dataAprovacao]);
            die(json_encode(array('resposta' => true, 'data_aprovada_documentacao' => $dataAprovacao)));
    }
    public function inserirProfissional(Request $request) {
        $usuarioId  =  Auth::user()->toArray()['id'];
        $dados      = Profissional::insert(['conteudo' => $request->input('conteudo'), 'titulo' => $request->input('titulo'), 'criado_por_usuario_id' => $usuarioId]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    /*
    *    Pacientes
    */
    public function getPacientes(Request $request) {
        $dados          = Paciente::get();
        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirPaciente(Request $request) {
        if(Paciente::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getPaciente(Request $request) {
        $dados = Paciente::where('id', $request->input('id'))->first();
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }

    public function inserirPaciente(Request $request) {
        $usuarioId  =  Auth::user()->toArray()['id'];
        $dados      = Paciente::insert(['conteudo' => $request->input('conteudo'), 'titulo' => $request->input('titulo'), 'criado_por_usuario_id' => $usuarioId]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    /*
    *    Serviços
    */
    public function getServicos(Request $request) {
        $dados          = ProfissionalServico::get();
        for ($i=0; $i < Count($dados); $i++) {
            $dado = $dados[$i];
            $profissional = Usuario::where('id', $dado->profissional_id)->first();
            $dados[$i]["profissional"] = $profissional->email;
        }

        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirServico(Request $request) {
        if(ProfissionalServico::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getServico(Request $request) {
        $dados              = ProfissionalServico::where('id', $request->input('id'))->first();
        $profissional       = Usuario::where('id', $dados->profissional_id)->first();
        $dados["profissional"] = $profissional;
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function bloquearServico(Request $request) {
        $dados = ProfissionalServico::where('id', $request->input('id'))->update(['data_modificacao' => null]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function desbloquearServico(Request $request) {
        $dataAprovacao =  Date("Y-m-d H:m:s");
        $dados = ProfissionalServico::where('id', $request->input('id'))->update(['data_modificacao' => $dataAprovacao]);
            die(json_encode(array('resposta' => true, 'data_modificacao' => $dataAprovacao)));
    }
    /*
    *    Serviços Solicitados
    */
    public function getServicosSolicitados(Request $request) {
        $dados          = ProfissionalServico::get();
        for ($i=0; $i < Count($dados); $i++) {
            $dado = $dados[$i];
            $profissional = Usuario::where('id', $dado->profissional_id)->first();
            $dados[$i]["profissional"] = $profissional->email;
        }

        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirServicoSolicitado(Request $request) {
        if(ProfissionalServico::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getServicoSolicitado(Request $request) {
        $dados              = ProfissionalServico::where('id', $request->input('id'))->first();
        $profissional       = Profissional::where('id', $dados->profissional_id)->first();
        $dados["profissional"] = $profissional;
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function bloquearServicoSolicitado(Request $request) {
        $dados = ProfissionalServico::where('id', $request->input('id'))->update(['data_modificacao' => null]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function desbloquearServicoSolicitado(Request $request) {
        $dataAprovacao =  Date("Y-m-d H:m:s");
        $dados = ProfissionalServico::where('id', $request->input('id'))->update(['data_modificacao' => $dataAprovacao]);
            die(json_encode(array('resposta' => true, 'data_modificacao' => $dataAprovacao)));
    }
    /*
    *    Serviços Especialidades
    */

      public function getEspecialidades(Request $request) {
        $dados          = Especialidade::get();
        for ($i=0; $i < Count($dados); $i++) {
            $dado = $dados[$i];
            $profissional = Usuario::where('id', $dado->profissional_id)->first();
            $dados[$i]["profissional"] = $profissional->email;
        }

        die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function excluirEspecialidade(Request $request) {
        if(Especialidade::where('id', $request->input('id'))->delete())
            die(json_encode(array('resposta' => true)));
        else
            die(json_encode(array('resposta' => false)));
    }
    public function getEspecialidade(Request $request) {
        $dados              = Especialidade::where('id', $request->input('id'))->first();
        $profissional       = Profissional::where('id', $dados->profissional_id)->first();
        $dados["profissional"] = $profissional;
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function bloquearEspecialidade(Request $request) {
        $dados = Especialidade::where('id', $request->input('id'))->update(['data_aprovado' => null]);
            die(json_encode(array('resposta' => true, 'dados' => $dados)));
    }
    public function desbloquearEspecialidade(Request $request) {
        $dataAprovacao =  Date("Y-m-d H:m:s");
        $dados = Especialidade::where('id', $request->input('id'))->update(['data_aprovado' => $dataAprovacao]);
            die(json_encode(array('resposta' => true, 'data_aprovado' => $dataAprovacao)));
    }

}
