<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;
use Hash;
use Session;
use AutorizacaoMenorIdade;
use Carbon\Carbon;

// carregando PagSeguro
use PagSeguro;

// carregando models
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\Slider as Slider;
use LogDialog\Model\Vantagem as Vantagem;

class IndexController extends Controller
{
    /**
     * Carregando a tela inicial (Home) da aplicação
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function index( Request $request )
    {
        // recuperando as informações dos profissionais cadastrados
        $view = view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                ->with('profissionais', Profissional::carregarLista(18));

        return $view;



    	/**
    	 * Dados do comprador de testes:
    	 *
    	 * Email: c86287020812747184575@sandbox.pagseguro.com.br
    	 * Senha: F9232gc0624UCyXY
    	 *
    	 * Cartão de crédito de testes:
    	 * Número: 4111111111111111
    	 * Bandeira: VISA Válido até: 12/2030 CVV: 123
    	 */

    	// array de exemplo de uma requisição ao PagSeguro
        $data = [
            'items' => [
                [
                    'id' => '18',
                    'description' => 'Item Um',
                    'quantity' => '1',
                    'amount' => '1.15',
                    'weight' => '45',
                    'shippingCost' => '3.5',
                    'width' => '50',
                    'height' => '45',
                    'length' => '60',
                ],
                [
                    'id' => '19',
                    'description' => 'Item Dois',
                    'quantity' => '1',
                    'amount' => '3.15',
                    'weight' => '50',
                    'shippingCost' => '8.5',
                    'width' => '40',
                    'height' => '50',
                    'length' => '80',
                ],
            ],
            'shipping' => [
                'address' => [
                    'postalCode' => '06410030',
                    'street' => 'Rua Leonardo Arruda',
                    'number' => '12',
                    'district' => 'Jardim dos Camargos',
                    'city' => 'Barueri',
                    'state' => 'SP',
                    'country' => 'BRA',
                ],
                'type' => 2,
                'cost' => 30.4,
            ],
            'sender' => [
                'email' => 'c86287020812747184575@sandbox.pagseguro.com.br',
                'name' => 'Isaque de Souza Barbosa',
                'documents' => [
                    [
                        'number' => '01234567890',
                        'type' => 'CPF'
                    ]
                ],
                'phone' => '11985445522',
                'bornDate' => '1988-03-21',
            ]
        ];


        // descomentar o bloco abaixo
        // para executar a requisição
        /*
        echo '<pre>';

        $checkout = PagSeguro::checkout()->createFromArray($data);

        $credentials = PagSeguro::credentials()->get();

        $information = $checkout->send($credentials); // Retorna um objeto de laravel\pagseguro\Checkout\Information\Information
        
        if($information)
        {
            print_r($information->getCode());
            print_r($information->getDate());
            print_r($information->getLink());
        }

        exit;*/

        /**
         * Dados de uma transação de testes:
         *
         * 24572E4C75754D7334922FA3E4A342EFDateTime Object
			(
			    [date] => 2016-05-12 17:00:13
			    [timezone_type] => 1
			    [timezone] => -03:00
			)
			// o link abaixo, é do checkout, direcionar o usuário para esta página para finalizar o processo
			https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=24572E4C75754D7334922FA3E4A342EF
         */
        
        /**
         * Abaixo é para consultar uma transação
         */
        
        /*
        $code = 'A4483FCC3FEA4390AD68B0F6306E2855';
        $credentials = PagSeguro::credentials()->get();
		$transaction = PagSeguro::transaction()->get($code, $credentials);
		$information = $transaction->getInformation();

		echo '<pre>';
		print_r($information);
		exit;
		*/
    }

    /**
     * Esta função, serve para fazer a ativação do perfil quando for cadastrado
     * Tanto para paciente quanto profissional
     */
    public function ativarPerfil( Request $request, $userId )
    {
        // verificando se o perfil existe
        // e ainda não está ativado
        $verificar = Usuario::whereId( $userId )
                                 ->where('data_ativado', null)
                                 ->first();

        if( $verificar )
        {
            // ativando
            Usuario::whereId( $userId )
                        ->update([ 'data_ativado' => Carbon::now()->toDateTimeString() ]);

            // verificando se é paciente ou profissional
            $aviso = Profissional::whereUsuarioId( $userId )->first();

            // se for profissional, terá uma mensagem para aguardar aprovação dos documentos
            $aviso = $aviso ? 'aguardar-aprovar-documentacao' : 'perfil-ativado';

            // redirecionando para aviso
            return redirect(action("IndexController@aviso", ["aviso" => $aviso]));
        }
        else
        {
            // redirecionando simplesmente para a home
            return redirect(action("IndexController@index"));
        }
    }

    /**
     * Esta função processa quando há aviso ao usuário
     */
    public function aviso( Request $request, $aviso = null )
    {
        // avisos do sistema
        switch( $aviso ){
            case 'ativar-email':
                $aviso = [
                        "titulo" => "Ativar perfil",
                        "mensagem" => "Seu cadastro foi feito com sucesso, agora você precisa ativar o seu perfil, através do link que enviamos para o seu e-mail."
                    ];
                
                break;

            case 'perfil-ativado':
                $aviso = [
                        "titulo" => "Perfil ativado",
                        "mensagem" => "Seu perfil foi ativado com sucesso, efetue o login normalmente."
                    ];
                
                break;

            case 'aguardar-aprovar-documentacao':
                $aviso = [
                        "titulo" => "Quase lá",
                        "mensagem" => "Seu perfil foi ativado com sucesso, agora é preciso que você aguarde para que sua documentação seja aprovada, somente depois desta aprovação é que seu acesso ao sistema será liberado."
                    ];
                
                break;
            
            default:
                $aviso = [];

                break;
        }

        // recuperando as informações dos profissionais cadastrados
        $view = view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                ->with('profissionais', Profissional::carregarLista(18));

        if( sizeof($aviso) > 0 )
        {
            $view->with('avisoSistema', $aviso)
                 ->with('abrirModal', 'avisoSistema');
        }

        return $view;
    }
}
