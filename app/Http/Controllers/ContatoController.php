<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando model
use LogDialog\Model\Contato as Contato;

// carregando utilitários
use Validator;

class ContatoController extends Controller
{
    /**
     * Recebe os dados do contato
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function enviar(Request $request)
    {
    	$dados = $request->all();

    	$rules = [
            'nome' => 'required',
            'email' => 'required|email',
            'mensagem' => 'required|min:20|max:2000',
        ];

        $messages = [
            'required' => 'O campo :attribute é obrigatório.',
            'email' => 'O campo :attribute deve conter um email válido.',
            'mensagem.min' => 'O campo mensagem deve conter no mínimo :min caracteres.',
            'mensagem.max' => 'O campo mensagem deve conter no máximo :max caracteres.',
        ];

    	$validation = Validator::make(
		    $dados,
		    $rules,
		    $messages
		);

    	// no caso de falha de validação
		if( $validation->fails() )
		{
		    // saída json
		    return response()->json(['errors' => $validation->messages()]);
		}
		// agora no caso de estar tudo válido
		else
		{
			$dados['email'] = trim($dados['email']);

			// validando se o domínio existe
			$teste = explode("@",$dados['email']);

			if( !checkdnsrr(array_pop($teste),"ANY") )
			{
				$validation->getMessageBag()->add('email', 'O e-mail de contato informado, não possui um domínio válido');

				// saída json
		    	return response()->json(['errors' => $validation->messages()]);
			}

			// inserindo contato no sistema
			Contato::insert([
						'nome' => $dados['nome'],
						'email' => $dados['email'],
						'telefone' => isset($dados['telefone']) ? $dados['telefone'] : null,
						'mensagem' => $dados['mensagem']
	    			]);

			// retornando o ok
			return response()->json(['sucesso' => 1]);
		}
    }
}
