<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use Blade;

// carregando utilitários
use Auth;
use DB;

// carregando models
use LogDialog\Model\Notificacao as Notificacao;

class NotificacoesController extends Controller
{
    /**
     * Listagem de notificações do usuário logado
     */
    public function index()
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // buscando todas as notificações do usuário
        // as mais recentes primeiro
        $notificacoes = Notificacao::listarNotificacoes( Auth::user()->toArray()['id'], 6, false );

        foreach( $notificacoes as $item )
        {
            Notificacao::whereId( $item->id )
                ->update([
                    "data_visualizada" => date("Y-m-d H:i:s")
                ]);
        }

        // saída view
        return view('notificacoes.index')
               ->with('notificacoes', $notificacoes);
    }
    /**
     * Listagem individual
     */
    public function visualizar(Request $request, $notificacao = null)
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index'); 
        $notificacoes = [Notificacao::getNotificacao(Auth::user()->toArray()['id'], $notificacao)->first()];
        
        $tipo = $request->input('tipo');
        switch ($tipo) {
            case 'novo_inbox':
                    return redirect('meu/perfil#mensagens');            
            default: 
                // saída view
                return view('notificacoes.index')
                       ->with('notificacoes', $notificacoes);
                break;
        }
    }
}
