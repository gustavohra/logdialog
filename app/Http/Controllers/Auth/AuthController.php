<?php

namespace LogDialog\Http\Controllers\Auth;

use LogDialog\Model\Usuario as Usuario;

use Validator;
use LogDialog\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
class AuthController extends Controller
{
    use ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Config::set('auth.model', 'LogDialog\User');
        // Config::set('auth.table', 'tb_usuario');

        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        //$this->middleware('guest', ['except' => 'logout']);

    }
}
