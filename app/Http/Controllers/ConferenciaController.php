<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;

// carregando utilitários
use Auth;
use View;
use Twilio;
use DB;
use Input;

// carregando models
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\Anotacao as Anotacao;
use LogDialog\Model\Slider as Slider;
use LogDialog\Model\Vantagem as Vantagem;


class ConferenciaController extends Controller
{
    /**
     * Esta action não renderiza nenhuma tela
     * Apenas recebe o aviso de encerramento de chamada
     */
    public function encerrar( Request $request, $agendaId )
    {
        // o acesso desta tela somente é possível com o usuário logado
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // verificando os dados do usuário na tabela de paciente
        // para ver se ele possui acesso à esta consulta
        $user = Paciente::verDetalhes( Auth::user()->toArray()['id'] );

        if( sizeof( $user ) == 0 )
        {
            $user = Profissional::verDetalhes( Auth::user()->toArray()['id'] );

            // encerrando a consulta informada
            Agenda::where("id", "=", $agendaId)
                ->where("profissional_id", "=", $user['id'])
                ->update([
                    "data_encerramento" => date("Y-m-d H:i:s"),
                    "encerrada_por_usuario_id" => Auth::user()->toArray()['id']
                ]);
        }
        else
        {
            // encerrando a consulta informada
            Agenda::where("id", "=", $agendaId)
                ->where("paciente_id", "=", $user['id'])
                ->update([
                    "data_encerramento" => date("Y-m-d H:i:s"),
                    "encerrada_por_usuario_id" => Auth::user()->toArray()['id']
                ]);
        }
    }

    /**
     * Esta função verifica o encerramento da consulta informada
     * 
     */

    /**
     * Esta tela é a sala de espera
     */
    public function espera( Request $request, int $agendaId )
    {
        // o acesso desta tela somente é possível com o usuário logado
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // recuperando dados da agenda
        $agenda = (array)Agenda::detalharItem($agendaId);

        // caso a agenda não seja do usuário informado
        if( sizeof($agenda) < 1 )
        {
            $aviso = [
                "titulo" => "Opps!",
                "mensagem" => "Não foi possível iniciar a conferência, esta consulta não está atribuída ao seu login. Verifique o agendamento e tente novamente."
            ];

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        // verificando se a consulta foi confirmada
        // pois sem confirmação, não poderá ter acesso
        if( strlen($agenda['data_confirmacao']) < 1 )
        {
            if( Auth::user()->GrupoSistema() == 'profissional' )
            {
                $aviso = [
                    "titulo" => "Confirmação pendente.",
                    "mensagem" => "Ainda não é possível acessar a área da consulta, o paciente ainda precisa confirmar o agendamento."
                ];
            }
            else
            {
                $aviso = [
                    "titulo" => "Confirmação pendente.",
                    "mensagem" => "Ainda não é possível acessar a área da consulta, o profissional ainda precisa confirmar o agendamento."
                ];
            }

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        /**
         * Se estiver no horário da consulta
         * Carrega a página
         */
        $estaNaHora = \Carbon\Carbon::now()->timestamp >= \Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp;

        if( $estaNaHora )
        {
            return redirect(action('ConferenciaController@consulta', ["consulta" => $agenda["agenda_id"]]));
        }

        /*echo '<pre>';
        print_r( $agenda );
        exit;*/

        return View::make('conferencia.espera')
                ->with('agenda', $agenda);
    }

    /**
     * Função para verificações e realização da conferência real
     * Para as consultas dos pacientes.
     *
     * é obrigatório que na requisição seja informado o ID e consulta
     * pois é essencial para a checagem com a outra extremidade da conferência
     */
    public function consulta( Request $request, int $agendaId )
    {
        // o acesso desta tela somente é possível com o usuário logado
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // recuperando dados da agenda
        $agenda = (array)Agenda::detalharItem($agendaId);

        // caso a agenda não seja do usuário informado
        if( sizeof($agenda) < 1 )
        {
            $aviso = [
                "titulo" => "Opps!",
                "mensagem" => "Não foi possível iniciar a conferência, esta consulta não está atribuída ao seu login. Verifique o agendamento e tente novamente."
            ];

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        // verificando se a consulta foi encerrada
        if( strlen($agenda['data_encerramento']) != null )
        {
            // caso tenha o id de quem encerrou a consulta
            if( $agenda['encerrada_por_usuario_id'] != null )
            {
                // pegando dados do usuário
                $user = Paciente::verDetalhes( $agenda['encerrada_por_usuario_id'] );

                if( sizeof( $user ) == 0 )
                {
                    $user = Profissional::verDetalhes( $agenda['encerrada_por_usuario_id'] );
                    $user['nome'] = "{$user['tratamento']} {$user['nome']}";
                }

                $data = \Carbon\Carbon::parse( $agenda['data_encerramento'] );

                $aviso = [
                    "titulo" => "Consulta encerrada.",
                    "mensagem" => "Esta consulta foi encerrada por {$user['nome']} {$user['sobrenome']}, em {$data->format('d/m')} às {$data->format('H:i')}."
                ];
            }
            else
            {
                $aviso = [
                    "titulo" => "Consulta encerrada.",
                    "mensagem" => "Esta consulta já foi encerrada."
                ];
            }

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        // verificando se a consulta foi cancelada
        if( strlen($agenda['data_cancelamento']) != null )
        {
            // caso tenha o id de quem encerrou a consulta
            if( $agenda['cancelada_por_usuario_id'] != null )
            {
                // pegando dados do usuário
                $user = Paciente::verDetalhes( $agenda['cancelada_por_usuario_id'] );

                if( sizeof( $user ) == 0 )
                {
                    $user = Profissional::verDetalhes( $agenda['cancelada_por_usuario_id'] );
                    $user['nome'] = "{$user['tratamento']} {$user['nome']}";
                }

                $data = \Carbon\Carbon::parse( $agenda['data_cancelamento'] );

                $aviso = [
                    "titulo" => "Consulta cancelada.",
                    "mensagem" => "Esta consulta foi cancelada por {$user['nome']} {$user['sobrenome']}, em {$data->format('d/m')} às {$data->format('H:i')}."
                ];
            }
            else
            {
                $aviso = [
                    "titulo" => "Consulta cancelada.",
                    "mensagem" => "Esta consulta foi cancelada."
                ];
            }

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        // se passou mais de 1 hora do horário de agendamento
        if( time() - strtotime($agenda['data_compromisso']) - strtotime(date("Y-m-d")) > 100 )
        {
            $aviso = [
                "titulo" => "Consulta encerrada.",
                "mensagem" => "Desculpe, mas já se passou mais de uma hora do horário desta consulta. Será necessário um novo agendamento."
            ];

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        // verificando se a consulta foi confirmada
        // pois sem confirmação, não poderá ter acesso
        if( strlen($agenda['data_confirmacao']) < 1 )
        {
            if( Auth::user()->GrupoSistema() == 'profissional' )
            {
                $aviso = [
                    "titulo" => "Confirmação pendente.",
                    "mensagem" => "Ainda não é possível acessar a área da consulta, o paciente ainda precisa confirmar o agendamento."
                ];
            }
            else
            {
                $aviso = [
                    "titulo" => "Confirmação pendente.",
                    "mensagem" => "Ainda não é possível acessar a área da consulta, o profissional ainda precisa confirmar o agendamento."
                ];
            }

            // recuperando as informações dos profissionais cadastrados
            return view('index.home')
                ->with('sliders', Slider::get())
                ->with('vantagens', Vantagem::get())
                   ->with('profissionais', Profissional::carregarLista(18))
                   ->with('avisoSistema', $aviso)
                   ->with('abrirModal', 'avisoSistema');
        }

        /**
         * Se ainda não estiver na hora
         * Redirecionar para a sala de espera
         */
        $estaNaHora = \Carbon\Carbon::now()->timestamp >= \Carbon\Carbon::parse($agenda['data_compromisso'])->timestamp;

        if( !$estaNaHora )
        {
            return redirect(action('ConferenciaController@espera', ["consulta" => $agenda["agenda_id"]]));
        }

        // caso esteja ok, prosseguir
        $usuarioId = Auth::user()->toArray()['id'];

        // Carregando todos os tokens necessários para comunicação com o Twilio
        $accountSid = env('TWILIO_ACCOUNT_ID');
        $apiKeySid = env('TWILIO_API_KEY');
        $apiKeySecret = env('TWILIO_ACCOUNT_SECRET');
        $videoProfile = env('TWILIO_RTC_PROFILE');

        // Criando token deste acesso
        $token = new \Services_Twilio_AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            $ttl = 3600,
            $identity = "usuario_{$usuarioId}_agenda_{$agendaId}" // o id do usuário será formado pelo id dele de usuário e o id da agenda
        );

        // verificando o usuário altual
        // se o ID bate com o de profisisonal ou paciente
        // e pegar o ID do outro usuário para iniciar a conversa
        if( $agenda['paciente_usuario_id'] == $usuarioId )
            $convidado = "usuario_{$agenda['profissional_usuario_id']}_agenda_{$agendaId}";
        else
            $convidado = "usuario_{$agenda['paciente_usuario_id']}_agenda_{$agendaId}";

        // echo '<pre>';
        // print_r( $agenda );
        // exit;

        // Grant access to Conversations
        $grant = new \Services_Twilio_Auth_ConversationsGrant();
        $grant->setConfigurationProfileSid($videoProfile);
        $token->addGrant($grant);

        $token = $token->toJWT();

        // carregando mensagens de chat dos usuários
        $profisisonal = Profissional::find( $agenda['profissional_id'])->toArray();
        $paciente = Paciente::find( $agenda['paciente_id'])->toArray();
        $nomeParceiro = "";

        if( $profisisonal['usuario_id'] == Auth::user()->toArray()['id'] )
        {
            $comUsuarioId = $paciente['usuario_id'];
            $eu = $profisisonal;
            $nomeParceiro = "{$agenda['nome_paciente']} {$agenda['sobrenome_paciente']}";
        }
        else
        {
            $comUsuarioId = $profisisonal['usuario_id'];
            $eu = $paciente;
            $nomeParceiro = "{$agenda['tratamento_profissional']} {$agenda['nome_profissional']} {$agenda['sobrenome_profissional']}";
        }

        Input::merge(["com_usuario_id" => $comUsuarioId]);

        $mensagens = new MensagemController;
        $mensagens = json_decode( $mensagens->APICarregarMensagens( $request )->content(), true )['mensagem'];

        // inserindo ou atualizando a consulta de agora
        // pois ela deve existir no sistema e é a consulta que será salva
        // as demais anotações não podem ser editadas
        Anotacao::createOrUpdate(
            // dados
            [
                "usuario_id" => Auth::user()->toArray()['id'],
                "relacionado_agenda_id" => $agenda['agenda_id'],
                "titulo" => "{$nomeParceiro}"
            ],

            // colunas para chave, verificar se existem ou não
            [
                "usuario_id" => Auth::user()->toArray()['id'],
                "relacionado_agenda_id" => $agenda['agenda_id']
            ]
        );

        // carregando anotações que este usuário possui, relacionados ao usuário companheiro
        $notas = Anotacao::minhaComUsuarioId( Auth::user()->toArray()['id'], $comUsuarioId );

        return View::make('conferencia.consultorio')
                    ->with('tokenTwilio', $token)
                    ->with('convidado', $convidado)
                    ->with('agenda', $agenda)
                    ->with('profissional', $profisisonal )
                    ->with('paciente', $paciente )
                    ->with('mensagens', array_reverse($mensagens))
                    ->with('com_usuario_id', $comUsuarioId)
                    ->with('eu', $eu)
                    ->with('notas', $notas);
    }

    /**
     * Esta função verifica se para o usuário logado alguma consulta já irá ocorrer
     * ou se está com alguns minutos iniciada
     */
    public function verificarProxima( Request $request )
    { 
        if( Auth::check() ):

            $usuarioId = Auth::user()->toArray()['id'];

            // recuperando apenas a próxima consulta em aberto
            $consulta = Agenda::whereInativo(0)
                        ->where('data_confirmacao', '!=', null)
                        ->where('data_encerramento', '=', null)
                        ->where('data_cancelamento', '=', null);
 

            if(Usuario::getUsuarioTipo($usuarioId, 2))
            {
                $profissional = Profissional::whereUsuarioId( $usuarioId )->first()->toArray();

                $consulta->whereProfissionalId( $profissional['id'] );
            }
            else if((Usuario::getUsuarioTipo($usuarioId, 3)))
            {
                $paciente = Paciente::whereUsuarioId( $usuarioId )->first()->toArray();

                $consulta->wherePacienteId( $paciente['id'] );
            }

            // pegando 40 minutos atrás
            // até 120 minutos à frente
            $agora  = date("Y-m-d H:i:s", strtotime("-40 minutes"));
            $futuro = date("Y-m-d H:i:s", strtotime("+120 minutes"));

            $consulta = $consulta
                            ->whereRaw( DB::raw("( data_compromisso BETWEEN '{$agora}' AND '{$futuro}' )") )
                            ->first();

            // caso tenha consulta
            // verificar os horários
            if( $consulta )
            {
                $consulta = $consulta->toArray();

                // saída com o id da consulta
                // e o nome da outra pessoa
                $retorno = [
                    'id' => $consulta['id'],
                    'link' => action("ConferenciaController@consulta", ["agendaId" => $consulta['id']])
                ];

                if( Auth::user()->GrupoSistema() == 'profissional' )
                {
                    $paciente = Paciente::find( $consulta['paciente_id'] )->toArray();

                    $retorno['nome'] = "{$paciente['nome']} {$paciente['sobrenome']}";
                }
                else
                {
                    $profissional = Profissional::find( $consulta['profissional_id'] )->toArray();

                    $tratamento = $profissional['sexo'] == 'feminino' ? 'Dra.' : 'Dr.';

                    $retorno['nome'] = "{$tratamento} {$profissional['nome']} {$profissional['sobrenome']}";
                }

                return response()->json( $retorno );
            }
            else
            {
                return response()->json( [] );
            }
        else:
            return response('Autenticação necessária', 401);
        endif;
    }

    /**
     * Função incial
     */
    public function index(Request $request)
    {
    	$accountId = env('TWILIO_ACCOUNT_ID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $from = env('TWILIO_NUMBER');

        $secret = env('TWILIO_ACCOUNT_SECRET');

        $appSid = env('TWILIO_APP_SID');

        // put your default Twilio Client name here
		$clientName = 'AdrianoMaciel';

		// get the Twilio Client name from the page request parameters, if given
		if( $request->has('client') ){
		    $clientName = $request->get('client');
		}

        /**
         * O teste abaixo, extá executando perfeitamente
         * uma ligação é feita para o celular informado,
         * mas para testes, o mesmo precisa ser verificado na twilio
         *
         * Mas os dados de autenticação devem ser válidos
         */
        /*$teste = new \Services_Twilio( $accountId, $authToken, '2010-04-01' );

        try{
            $call = $teste->account->calls->create(
                $from,
                '+5511987408369',
                'http://demo.twilio.com/welcome/voice'
            );

            echo "Ligação iniciada: {$call->sid}";
        }
        catch( Exception $e )
        {
            echo "Error: " . $e->getMessage();
        }*/

        $capability = new \Services_Twilio_Capability($accountId, $authToken);
        $capability->allowClientOutgoing($appSid);
        $capability->allowClientIncoming($clientName);
        $token = $capability->generateToken();

        return View::make('conferencia.index')
					->with('tokenTwilio', $token)
					->with('clientName', $clientName);
    }

    /**
     * Transmissão dos dados de voz
     */
    public function voz()
    {
        header('Content-type: text/xml');

        echo '<Response>
            <Dial callerId="5511981624615">
                <Client>android</Client>
            </Dial>
        </Response>';

        exit;
    }

    /**
     * Transmissão dos dados de mensagens (SMS)
     */
    public function mensagem()
    {
        //
    }

    /**
     * Transmissão dos dados de vídeos
     */
    public function video()
    {
        // token para testes
        // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2Q2NjE0NTYyZGZkNDczMTRiY2JkZWYwNDViMjlkY2FiLTE0Njc1MTAyMzIiLCJpc3MiOiJTS2Q2NjE0NTYyZGZkNDczMTRiY2JkZWYwNDViMjlkY2FiIiwic3ViIjoiQUNiZGU3OTA1NjM4NmIxNmJkYjFjNDYwZWUyYTVhMTI4NSIsImV4cCI6MTQ2NzUxMzgzMiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiVGVzdGUgV2ViIiwicnRjIjp7ImNvbmZpZ3VyYXRpb25fcHJvZmlsZV9zaWQiOiJWUzZkMWNiMGQ4ZTAwNTVhMDcxZmQ3YjRmYzU2MGQyYTI3In19fQ.C5Fa2jlaEYC4YJsIZ6TwLXzstLiP_3F1G7XqTCIA2aY
    }

    /**
     * Teste para video
     */
    public function testeVideo()
    {
        // Substitute your Twilio AccountSid and ApiKey details
        $accountSid = env('TWILIO_ACCOUNT_ID');
        $apiKeySid = env('TWILIO_API_KEY');
        $apiKeySecret = env('TWILIO_ACCOUNT_SECRET');
        $videoProfile = env('TWILIO_RTC_PROFILE');

        // Create an Access Token
        $token = new \Services_Twilio_AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            $ttl=3600,
            $identity='teste usuário 2'
        );

        // Grant access to Conversations
        $grant = new \Services_Twilio_Auth_ConversationsGrant();
        $grant->setConfigurationProfileSid($videoProfile);
        $token->addGrant($grant);

        $token = $token->toJWT();

        //$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2Q2NjE0NTYyZGZkNDczMTRiY2JkZWYwNDViMjlkY2FiLTE0Njc1MTAyMzIiLCJpc3MiOiJTS2Q2NjE0NTYyZGZkNDczMTRiY2JkZWYwNDViMjlkY2FiIiwic3ViIjoiQUNiZGU3OTA1NjM4NmIxNmJkYjFjNDYwZWUyYTVhMTI4NSIsImV4cCI6MTQ2NzUxMzgzMiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiVGVzdGUgV2ViIiwicnRjIjp7ImNvbmZpZ3VyYXRpb25fcHJvZmlsZV9zaWQiOiJWUzZkMWNiMGQ4ZTAwNTVhMDcxZmQ3YjRmYzU2MGQyYTI3In19fQ.C5Fa2jlaEYC4YJsIZ6TwLXzstLiP_3F1G7XqTCIA2aY";

        return View::make('conferencia.video')
                    ->with('tokenTwilio', $token);
    }
}
