<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use LogDialog\Model\Slider as Slider; 

// carregando utilitários
use Auth;

class InstitucionalController extends Controller
{
    /**
     * Tela inicial, com a lsitagem de links
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function index()
    {
        echo "Tela inicial, com a listagem de links";
    }

    /**
     * Tela Sobre
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function sobre()
    {
        return view('institucional.sobre')->with('sliders', Slider::get());
    }


    /**
     * Termos de Uso
     *
     * @author Criaturo Design <criaturo@criaturo.com>
     */
    public function termosdeuso()
    {
        return view('institucional.termosdeuso')->with('sliders', Slider::get());
    }

    /**
     * Termos Profissionais
     *
     * @author Criaturo Design <criaturo@criaturo.com>
     */
    public function termosprofissionais()
    {
        return view('institucional.termosprofissionais')->with('sliders', Slider::get());
    }

    /**
     * Política de Pagamento e Reembolso
     *
     * @author Criaturo Design <criaturo@criaturo.com>
     */
    public function politicaPagamentoReembolso()
    {
        return view('institucional.politica-pagamento-reembolso')->with('sliders', Slider::get());
    }

    /**
     * Contrato com o psicólogo
     *
     * @author Criaturo Design <criaturo@criaturo.com>
     */
    public function contratoPsicologo()
    {
        return view('institucional.contrato-psicologo')->with('sliders', Slider::get());
    }

    /**
     * Perguntas Frequentes
     *
     * @author Criaturo Design <criaturo@criaturo.com>
     */
    public function perguntasFrequentes()
    {
        return view('institucional.perguntas-frequentes')->with('sliders', Slider::get());
    }

    /**
     * Tela Seja Nosso Parceiro
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function sejaNossoParceiro()
    {
        echo "Tela Seja Nosso Parceiro";
    }

    /**
     * Tela Política e Privacidade
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function politicaPrivacidade()
    {
        return view('institucional.politica-privacidade')->with('sliders', Slider::get());
    }

    /**
     * Tela Segurança
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function seguranca()
    {
        echo "Tela Segurança";
    }

    /**
     * Tela Trabalhe Conosco
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function trabalheConosco()
    {
        echo "Tela Trabalhe Conosco";
    }

    /**
     * Tela Vendas Corporativas
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function vendasCorporativas()
    {
        echo "Tela Vendas Corporativas";
    }
}
