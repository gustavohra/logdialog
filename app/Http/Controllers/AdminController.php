<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use Blade;
// carregando utilitários
use Auth;
use Carbon\Carbon;
use Hash;
use Input;
use File;
use Image;
use Validator;
use View;
use \Illuminate\Support\MessageBag as MessageBag;

use Calendario;
use HorarioAtendimento;
use Avatar;

// carregando models
use LogDialog\Model\Usuario as Usuario; 
use LogDialog\Model\Profissional as Profissional; 
use LogDialog\Model\UsuarioAdmin as UsuarioAdmin; 

class AdminController extends Controller
{
    public function index() {
        if(!Auth::Check() || Auth::Check() && !$this->verificarAdmin()){
            $view = view('layouts.logdialog.painel-admin-login');

            return $view;
        }
        else {
                Blade::setContentTags('<%', '%>');        
                Blade::setEscapedContentTags('<%%', '%%>');
                // saída da view
                return view('painel.admin')
                       ->with('usuario', [
                            'user' => Auth::user()
                        ]);

        }
    }
    function verificarAdmin() {
        if(Auth::Check())
            return UsuarioAdmin::where('tb_usuario_id', Auth::User()->toArray()['id'] )->first();
        else
            return false;
    }
    /**
     * Autenticação de login
     *
     * @author Adriano Maciel <adriano_mail@hotmail.com>
     */
    public function Login( Request $request )
    {
        $dados = $request->all();

        // caso seja uma requisição post
        if( $request->isMethod('post') )
        {
            $rules = [
                'login' => 'required',
                'senha' => 'required|min:5|max:110'
            ];

            $messages = [
                'required' => 'O campo :attribute é obrigatório.',
                'min' => 'O campo :attribute deve conter no mínimo :min caracteres.',
                'max' => 'O campo :attribute deve conter no máximo :max caracteres.'
            ];

            $validation = Validator::make(
                array(
                    'login' => $dados['login'],
                    'senha' => $dados['senha']
                ),
                $rules,
                $messages
            );

            // no caso de falha de validação
            if( $validation->fails() )
            {
                $errors = $validation->messages();

                // redirecionando com os erros
                // para exibir ao usuário
                $request->flash();

                return View::make('layouts.logdialog.painel-admin-login')
                            ->withInput($request->all())
                            ->withErrors($errors);
            }
            else
            {
                // verificando se o perfil está ativado
                $ativado = Usuario::whereUsername($request->get('login'))
                                       ->where('data_ativado', '!=', null)
                                       ->first();

                // se estiver ativado
                if( $ativado )
                {
                    // verificando se é profissional
                    if( UsuarioAdmin::where('tb_usuario_id', $ativado->toArray()['id'] )->first() )
                    { 
                            // autenticando
                            $credenciais = [
                                'username' => $request->get('login'),
                                'password' => $request->get('senha'),
                            ];

                            if( Auth::attempt($credenciais) )
                            { 
                                if(Auth::check()){
                                    Blade::setContentTags('<%', '%>');        
                                    Blade::setEscapedContentTags('<%%', '%%>');
                                    // saída da view
                                    return view('painel.admin')
                                           ->with('usuario', [
                                                'user' => Auth::user()
                                            ]);
                                }
                            }
                            else
                            {
                                $errors = new MessageBag(['login' => ['Dados inválidos.']]);

                                return View::make('layouts.logdialog.painel-admin-login')
                                        ->withInput($request->all())
                                        ->withErrors($errors);
                            }
                    }
                    else {
                        $errors = new MessageBag(['login' => ['Perfil não autorizado.']]);

                        return View::make('index.home')
                                ->withInput($request->all())
                                ->withErrors($errors)
                                ->with('abrirModal', 'modal-log')
                                ->with('profissionais', Profissional::carregarLista(18));
                    }

                } 
                else
                {
                    $errors = new MessageBag(['login' => ['Perfil pendente de ativação.']]);

                    return View::make('index.home')
                            ->withInput($request->all())
                            ->withErrors($errors)
                            ->with('abrirModal', 'modal-log')
                            ->with('profissionais', Profissional::carregarLista(18));
                }
            }
        } 
    }
 
}