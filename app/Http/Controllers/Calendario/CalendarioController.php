<?php

namespace LogDialog\Http\Controllers\Calendario;

use Illuminate\Http\Request;

use LogDialog\Http\Requests;
use LogDialog\Http\Controllers\Controller;

// carregando componentes
use Auth;
use Utilitario;
use Calendario;
use Carbon\Carbon;
use HorarioAtendimento; 
// carregando models
use LogDialog\Model\Paciente as Paciente;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Usuario as Usuario;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\Anotacao as Anotacao;

class CalendarioController extends Controller
{
    /**
     * Esta função
     * Retorna o detalhamento do dia informado por timestamp
     * Apenas para o usuário logado
     */
    public static function dia( Request $request, $timestamp = null )
    {
        $timestamp = $timestamp * 1 ?? time();

        if( Auth::check() )
        {
            // carregando agenda de hoje para o usuário logado
            $usuarioId = Auth::user()->toArray()['id'];

            // buscando agenda
            $consultas = Agenda::listarConsultasDia( $usuarioId, (int)$timestamp, 'DESC' );

            // independente de qualquer coisa
            // montar a listagem de horários do dia

            for( $y = (int)HorarioAtendimento::lista('primeiro')[0]; $y <= (int)HorarioAtendimento::lista('ultimo')[0]; $y++ )
            {
                $tmp = str_pad($y, 2, "0", STR_PAD_LEFT);

                $horarios["{$tmp}:00"] = [
                    "hora" => "{$tmp}:00",
                    "compromisso" => "",
                    "status" => "",
                    "classe" => "",
                    "agendaId" => 0
                ];
            }

            // verificando as consultas que coincidem com os horários
            foreach( $consultas as $k => $item )
            {
                // formatando horário
                $tmp = date("H:00", strtotime($item->data_compromisso));
                $tmp = str_pad($tmp, 5, "0", STR_PAD_LEFT);

                $horarios[$tmp] = [
                    "hora" => $tmp,
                    "status" => "",
                    "classe" => "daily-event event-approved",
                    "agendaId" => $item->agenda_id
                ];

                // compromisso de acordo com o perfil
                if( Auth::user()->grupoSistema() == 'paciente' )
                {
                    $horarios[$tmp]['compromisso'] = "{$item->tratamento_profissional} {$item->nome_profissional} {$item->sobrenome_profissional}";
                }
                else if( Auth::user()->grupoSistema() == 'profissional' )
                {
                    $horarios[$tmp]['compromisso'] = "{$item->nome_paciente} {$item->sobrenome_paciente}";
                }
            }

            return response()->json( Utilitario::array_utf8_encode(["horarios" => array_values($horarios), "data" => Calendario::date($timestamp)->formatLocalized('%d de %B de %Y')]) );
        }
        else
        {
            // retornar erro, pois não está logado
            return response('Login obrigatório', 401);
        }
    }

    /**
     * Esta função
     * Retorna o detalhamento do intervalo de dias informado por timestamp
     * Apenas para o usuário logado
     */
    public static function intervalo( Request $request, $timestampInicio = null, $timestampFim = null )
    { 
        $timestampInicio = $timestampInicio * 1 ?? time();
        $timestampFim    = $timestampFim * 1 ?? time();
        $usuarioId       = $request["id"];

        if( $usuarioId != null )
        {
            // carregando agenda de hoje para o usuário logado
          
            $timestampInicio = (int)(is_float($timestampInicio) ? $timestampInicio / 1000 : $timestampInicio);
            $timestampFim    = (int)(is_float($timestampFim) ? $timestampFim / 1000 : $timestampFim);

            // buscando agenda
            $consultas = Agenda::listarConsultasIntervaloDias( $usuarioId, (int)$timestampInicio, (int)$timestampFim, 'DESC' );

            // sempre montar a listagem com os dias da semana
            // para efeito de comparação no client
            // e evitar problemas com tradução
            $lista = Calendario::semanaCompleta( Calendario::date($timestampInicio) );

            $diasSemana = [];

            foreach( $lista as $k => $v )
                $diasSemana[] = mb_convert_encoding(strtoupper( $v->formatLocalized('%a') ), 'UTF-8');

            $letraDiasSemana = [];

            foreach( $lista as $k => $v )
                $letraDiasSemana[] = mb_convert_encoding(strtoupper( $v->formatLocalized('%a') )[0], 'UTF-8');

            // listando nomes de todos os meses do ano
            $listaNomeMeses = Calendario::anoCompleto( Calendario::date($timestampInicio) );
 
            $nomeMeses = [];

            foreach( $listaNomeMeses as $k => $v ){
                    $mes = substr($v->formatLocalized('%B'), 0, 3);
                if( !in_array($mes, $nomeMeses) ){
                    $nomeMeses[] = mb_convert_encoding($mes, 'UTF-8');
                }
            } 
 
            // independente de qualquer coisa
            // montar a listagem de horários do dia
            for( $y = (int)HorarioAtendimento::lista('primeiro')[0]; $y <= (int)HorarioAtendimento::lista('ultimo')[0]; $y++ )
            {
                $tmp = str_pad($y, 2, "0", STR_PAD_LEFT);

                $horarios["{$tmp}:00"] = [
                    "hora" => "{$tmp}:00",
                    "compromisso" => "",
                    "status" => "",
                    "classe" => ""
                ];
            }

            // listagem final com os dias e horários
            $listagemConsultas = [];

            $tmp = Calendario::intervaloDias( Calendario::date($timestampInicio), Calendario::date($timestampFim) );

            // criando listagem de dias
            foreach( $tmp as $dia )
            {
                $tmp = strtoupper( $dia->formatLocalized('%a %d/%m') );

                // adicionando o dia
                // na listagem de horários
                // para efeito de comparação no client
                foreach( $horarios as $k => $v )
                {
                    $horarios[$k]['dia'] = mb_convert_encoding($tmp, 'UTF-8');

                    // dia da semana, útil para controle da visão de mês
                    $horarios[$k]['dia_semana']             = mb_convert_encoding(strtoupper($dia->formatLocalized('%a')), 'UTF-8');
                    $horarios[$k]['letra_dia_semana']       = mb_convert_encoding(strtoupper($dia->formatLocalized('%a'))[0], 'UTF-8');
                    $horarios[$k]['dia_mes']                = mb_convert_encoding($dia->format('j'), 'UTF-8');
                    $horarios[$k]['mes']                    = mb_convert_encoding($dia->formatLocalized('%B'), 'UTF-8');
                    $horarios[$k]['numero_mes']             = mb_convert_encoding($dia->month, 'UTF-8');
                    $horarios[$k]['dia_mes_completo']       = mb_convert_encoding($dia->toAtomString(), 'UTF-8');
                    $horarios[$k]['tem_consulta']           = 0;
                }

                $listagemConsultas[$tmp]['horarios']    = $horarios;
                $listagemConsultas[$tmp]['dia']         = mb_convert_encoding($tmp, 'UTF-8');

                // dia da semana, útil para controle da visão de mês
                $listagemConsultas[$tmp]['dia_semana']          = mb_convert_encoding(strtoupper($dia->formatLocalized('%a')), 'UTF-8');
                $listagemConsultas[$tmp]['letra_dia_semana']    = mb_convert_encoding(strtoupper($dia->formatLocalized('%a'))[0], 'UTF-8');
                $listagemConsultas[$tmp]['dia_mes']             = mb_convert_encoding($dia->format('j'), 'UTF-8');
                $listagemConsultas[$tmp]['mes']                 = mb_convert_encoding($dia->formatLocalized('%B'), 'UTF-8');
                $listagemConsultas[$tmp]['numero_mes']          = mb_convert_encoding($dia->month, 'UTF-8');
                $listagemConsultas[$tmp]['dia_mes_completo']    = mb_convert_encoding($dia->toAtomString(), 'UTF-8');
                $listagemConsultas[$tmp]['tem_consulta']        = 0;
            }

            if($consultas != null && Count($consultas) > 0):
            // verificando as consultas que coincidem com os horários
                foreach( $consultas as $k => $item )
                {

                    // formatando horário
                    $tmp = date("H:00", strtotime($item->data_compromisso));
                    $tmp = str_pad($tmp, 5, "0", STR_PAD_LEFT);

                    // formatando dia
                    $tmpDia = strtoupper( Carbon::parse($item->data_compromisso)->formatLocalized('%a %d/%m') );

                    // dia da semana
                    $tmpDiaSemana = strtoupper( Carbon::parse($item->data_compromisso)->formatLocalized('%a') );

                    // apenas primeira letra dia da semana
                    $tmpLetraDiaSemana = strtoupper( Carbon::parse($item->data_compromisso)->formatLocalized('%a') )[0];

                    // informação completa do dia e hora
                    // exibido no pop up
                    $tmpSemanaCompleta = Carbon::parse($item->data_compromisso)->formatLocalized('%A, %d/%m | %Hh%M');

                    // // caso o usuário logado tenha adicionado uma anotação
                    // // carregar
                    $nota = Anotacao::whereRelacionadoAgendaId( $item->agenda_id )
                            ->whereUsuarioId( $usuarioId )
                            ->first();

                    if( $nota != null )
                    {
                        $nota = [
                            "conteudo" => mb_convert_encoding($nota->toArray()['conteudo'], 'UTF-8')
                        ];
                    }
                    else
                    {
                        $nota = ["conteudo" => null];
                    }

                    // adicionar item
                    $novoItem = [
                        "hora"                  => mb_convert_encoding($tmp, 'UTF-8'),
                        "dia"                   => mb_convert_encoding($tmpDia, 'UTF-8'),
                        "semana_completa"       => mb_convert_encoding($tmpSemanaCompleta, 'UTF-8'),
                        "dia_semana"            => mb_convert_encoding($tmpDiaSemana, 'UTF-8'),
                        "letra_dia_semana"      => mb_convert_encoding($tmpLetraDiaSemana, 'UTF-8'),
                        "dia_mes"               => mb_convert_encoding(Carbon::parse($item->data_compromisso)->format('j'), 'UTF-8'),
                        "dia_mes_completo"      => mb_convert_encoding(Carbon::parse($item->data_compromisso)->toAtomString(), 'UTF-8'),
                        "status"                => "",
                        //"nota"                  => mb_convert_encoding($nota['conteudo'], 'UTF-8'),
                        "classe"                => "event-request"
                    ];

                    // verificando o status da consulta
                    // para definir a classe
                    if( strlen( $item->data_confirmacao ) > 0 && strlen( $item->data_encerramento ) == 0 )
                    {
                        $novoItem["classe"] = "event-approved";
                    }
                    else if( strlen( $item->data_encerramento ) > 0 && strlen( $item->data_confirmacao ) > 0 )
                    {
                        $novoItem["classe"] = "event-ended";
                    }

                    // @TODO: Futuramente adicionar verificação da classe para pagamento

                    $listagemConsultas[$tmpDia]['horarios'][$tmp] = $novoItem;
                    $listagemConsultas[$tmpDia]['tem_consulta'] = 1;

                    // compromisso de acordo com o perfil
                    if( Usuario::getUsuarioTipo($usuarioId, 3) )
                    {
                        $listagemConsultas[$tmpDia]['horarios'][$tmp]['compromisso'] = "{$item->tratamento_profissional} {$item->nome_profissional} {$item->sobrenome_profissional}";
                    }
                    else if( Usuario::getUsuarioTipo($usuarioId, 2) )
                    {
                        $listagemConsultas[$tmpDia]['horarios'][$tmp]['compromisso'] = "{$item->nome_paciente} {$item->sobrenome_paciente}";
                    }
                }
            endif;
            // // resetando índice de horários
            // foreach( $listagemConsultas as $k => $v )
            //     $listagemConsultas[$k]['horarios'] = array_values($v['horarios']);

            $resultado = [];
            $resultado = (["ano" => Calendario::date($timestampInicio)->year,  "dias" => array_values($listagemConsultas), "data" => Calendario::date($timestampInicio)->formatLocalized('%B %Y'), "dia_semana" => $diasSemana, "letra_dia_semana" => $letraDiasSemana, "nome_meses" => $nomeMeses]);

            return response()->json( $resultado );
        }
        else
        {
            // retornar erro, pois não está logado
            return response()->json(false);
        }
    }
}
