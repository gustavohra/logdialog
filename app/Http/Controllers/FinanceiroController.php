<?php

namespace LogDialog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use \Illuminate\Support\MessageBag as MessageBag;

use LogDialog\Http\Requests;
use LogDialog\Model\Profissional as Profissional;
use LogDialog\Model\Agenda as Agenda;
use LogDialog\Model\Paciente as Paciente; 

use Auth; 
use DateTime;
use DateInterval;
use Log;

use Mail;
use Moip\Moip;
use Moip\Auth\BasicAuth;
use Moip\Auth\OAuth;
use Moip\Auth\Connect;


class FinanceiroController extends Controller
{
    public function __construct()
    {
        // Token SandBox
        $this->access_token = 'OFOCLLQW7OVYXQQE2UG6UMZXJ78JOCHW';
        $this->key          = 'FCAKEARO0HUO2MLNFUPYNOYBW8VCWQ82K0XITDQ3';
        $this->client_id    = "APP-52112YZZTAL2";
        $this->secret       = "ff019132c9e140b4ad9e1006631ade2c";
        $this->redirect_uri = url("/profissionais/financeiro/callback");
        $this->scope        = true;
        // Endereço SandBox
        $this->moip = new Moip(new BasicAuth($this->access_token, $this->key), Moip::ENDPOINT_SANDBOX);
        $this->moipMerchant = null;
    }


    /**
     * Solicita permissão ao usuário
     */
    public function gerar_token()
    {
        // Verifica a sessão do usuário
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // Verifica o usuário
        $usuarioId = Auth::user()->toArray()['id'];

        // Caso o usuário seja profissional, solicita uma chave de autenticação junto ao MoIP
        if( Auth::user()->grupoSistema() == 'profissional' ){
           

            $connect = new Connect($this->redirect_uri, $this->client_id, $this->scope, Connect::ENDPOINT_SANDBOX);
            $connect->setScope(Connect::RECEIVE_FUNDS)
                ->setScope(Connect::REFUND)
                ->setScope(Connect::MANAGE_ACCOUNT_INFO)
                ->setScope(Connect::RETRIEVE_FINANCIAL_INFO);
 
            return Redirect::to($connect->getAuthUrl());
        }
        else {
            return redirect()->action('IndexController@index');            
        }
    }

    /**
     * Callback após gerar token
     */
    public function token_callback(Request $request)
    {
        // Verifica a sessão do usuário
        if( !Auth::check() )
            return redirect()->action('IndexController@index');

        // Verifica o usuário
        $usuarioId = Auth::user()->toArray()['id'];
        $errors = [];

        // Caso o usuário seja profissional, solicita uma chave de autenticação junto ao MoIP
        if( Auth::user()->grupoSistema() == 'profissional' ){ 
            $redirect = redirect()->action('PerfilController@meu');
            if(!$request->has("code")) {
                $errors = new MessageBag(['erro' => ['Ocorreu um problema, por favor, tente novamente.']]);
                $redirect = $redirect->withErrors($errors);    
            }
            else {
                $connect        = new Connect($this->redirect_uri, $this->client_id, $this->scope, Connect::ENDPOINT_SANDBOX);

                // Solicita autorização para o profissional
                $connect->setClientSecret($this->secret);
                $connect->setCode($request->input("code"));

                $authorize = $connect->authorize(); 

                // Atualiza o token do profissional
                Profissional::where( 'usuario_id', $usuarioId )
                           ->update([
                                'moip_auth' => $authorize->access_token,
                                'moip_id'   => $authorize->moipAccount->id
                            ]);

                  $redirect = $redirect->with('retornoMoip', 'success');
                
            }

            return $redirect;
        }
        else {
            return redirect()->action('IndexController@index');            
        }
    }
        /*
    * [Método]: realizarPagamento
    * [Descrição]:  
    * 
    * @author gustavoaguiar 
    */
    public static function realizarPagamento(Request $request)
    {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');
 
        $usuarioId = Auth::user()->toArray()['id'];
        $agendaId = $request->input("agendaId");

        if( Auth::user()->grupoSistema() == 'paciente' )
        {
            // pegando dados do perfil de paciente
            $usuario = Paciente::whereUsuarioId( $usuarioId )
                            ->join('tb_usuario', 'tb_paciente.usuario_id', '=', 'tb_usuario.id')
                            ->first()
                            ->toArray();

            // tratamento para o paciente
            $usuario['tratamento'] = '';
        }
        // caso seja profissional
        else
        {
            return redirect()->action('IndexController@index');
        } 
 
        $consulta = Agenda::detalharItem( $agendaId);
     
        // carregando view
        return view('agenda.pagamento')
                ->with('consulta', $consulta)
                ->with('usuario', $usuario);
    }

    private function tratarCep($cep) {
        return str_replace($cep, "-", "");
    }
    private function getCobrancaDados($paciente) {

        return $this->moip->customers()->setOwnId(uniqid())
                        ->setFullname("{$paciente['nome']} {$paciente['sobrenome']}")
                        ->setEmail($paciente['email'])
                        ->setBirthDate($paciente['nascimento'])
                        ->setTaxDocument($paciente['cpf'])
                        ->setPhone(11, 66778899)
                        ->addAddress('BILLING',
                            $paciente['endereco'], $paciente['end_numero'],
                            $paciente['bairro'], $paciente['cidade'], $paciente['estado'],
                            $this->tratarCep($paciente['cep']), 8)
                        ->create();
    }
    private function getPedidoDados($consulta, $dadosCobranca) {  
        return  $this->moip->orders()->setOwnId(uniqid())
                ->addItem($consulta->nome_compromisso, 1, $consulta->agenda_id, (intval($consulta->valor,10)*100) ) 
                ->setCustomer($dadosCobranca) 
                // ->addReceiver($consulta->moip_id, 'SECONDARY', null, 85, true)
                ->create(); 
    }
    private function enviarEmailBoleto($payment, $paciente) {
        $payment = json_decode(json_encode($payment));
        Mail::send('email-financeiro.boleto', ["texto" => "<b>{$paciente['nome']}</b>, para confirmar a sua consulta, por favor, realize o pagamento do boleto <a href='{$payment->_links->payBoleto->printHref}'>clicando aqui</a> ou no botão abaixo:", "urlBoleto" => $payment->_links->payBoleto->printHref], function ($mail) use ($paciente) {
            $mail->from('logdialog@criaturo.com', 'LogDialog');

            $mail->to($paciente['email'], "")->subject('LogDialog, boleto para pagamento.');
        });
    }
    public   function gerarBoleto(Request $request) {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');
 
        $usuarioId = Auth::user()->toArray()['id'];
        $agendaId = $request->input("agendaId");

        $consulta = Agenda::detalharItem( $agendaId);

        if( Auth::user()->grupoSistema() == 'paciente' )
        {
            // pegando dados do perfil de paciente
            $usuario = Paciente::where('usuario_id', $usuarioId )
                            ->join('tb_usuario', 'tb_paciente.usuario_id', '=', 'tb_usuario.id')
                            ->first()
                            ->toArray();

            // tratamento para o paciente
            $usuario['tratamento'] = '';


            try {
                // Iniciando a transação 

                // Gera o OAuth do profissional
                // $this->moipMerchant = new Moip(new OAuth($consulta->moip_auth), Moip::ENDPOINT_SANDBOX);
 
                // Gera informações de pagamento do paciente
                $dadosCobranca = $this->getCobrancaDados($usuario);

                // Gera informações sobre a compra/serviço
                $dadosPedido   = $this->getPedidoDados($consulta, $dadosCobranca);

                // Informações do boleto
                $logo_uri           = 'https://logdialog.com.br/images/logdialog-logo.png';
                $expiration_date    = (new DateTime())->add(new DateInterval('P3D'));
                $instruction_lines  = ['INSTRUÇÃO 1', 'INSTRUÇÃO 2', 'INSTRUÇÃO 3'];

                // Realiza a geração do boleto
                $payment = $dadosPedido->payments()
                    ->setBoleto($expiration_date, $logo_uri, $instruction_lines)
                    ->execute();

                // Envia e-mail com o boleto
                $this->enviarEmailBoleto($payment, $usuario);

 
                // Atualiza o ID de pagamento
                Agenda::where( 'id', $consulta->agenda_id )
                           ->update([
                                'id_pagamento' => $payment->getId(),
                                'status_pagamento' => $payment->getStatus()
                            ]);

                echo json_encode(array("erro" => false, "dados" => $payment)); exit();

            } catch (\Moip\Exceptions\UnautorizedException $e) {
                echo $e->getMessage();
            } catch (\Moip\Exceptions\ValidationException $e) {
                echo json_encode(array("erro" => true, "msg" => "[".$e->getStatusCode()."]: ".$e->__toString())); exit();
            } catch (\Moip\Exceptions\UnexpectedException $e) {
                echo $e->getMessage();
            }
             catch (Exception $e) {
                echo json_encode(array("erro" => true, "msg" => $e->__toString())); exit();
            }
        }
        // caso seja profissional
        else
        {
            return redirect()->action('IndexController@index');
        } 
 
        // carregando view
        // return view('agenda.boleto')
        //         ->with('consulta', $consulta)
        //         ->with('usuario', $usuario);

    }
    private function tratarAnoValidade($ano) { 
        return substr($ano, 2, 2);
    }
    private function tratarNumeroCartao($numero) {
        return preg_replace('/[^0-9]/', '', $numero);
    }
    public   function pagamentoCartao(Request $request) {
        if( !Auth::check() )
            return redirect()->action('IndexController@index');
 
        $usuarioId = Auth::user()->toArray()['id'];
        $agendaId = $request->input("agendaId");

        $consulta = Agenda::detalharItem( $agendaId);

        if( Auth::user()->grupoSistema() == 'paciente' )
        {
            $dados = json_decode(file_get_contents('php://input'), true);

            // pegando dados do perfil de paciente
            $usuario = Paciente::where('usuario_id', $usuarioId )
                            ->join('tb_usuario', 'tb_paciente.usuario_id', '=', 'tb_usuario.id')
                            ->first()
                            ->toArray();

            // tratamento para o paciente
            $usuario['tratamento'] = '';


            try {
                // Iniciando a transação 

                // Gera o OAuth do profissional
                // $this->moipMerchant = new Moip(new OAuth($consulta->moip_auth), Moip::ENDPOINT_SANDBOX);
 
                // Gera informações de pagamento do paciente
                $dadosCobranca = $this->getCobrancaDados($usuario);

                // Gera informações sobre a compra/serviço
                $dadosPedido   = $this->getPedidoDados($consulta, $dadosCobranca);
 
                $payment = $dadosPedido->payments()
                    ->setCreditCard($dados['expiration']['month'], $this->tratarAnoValidade($dados['expiration']['year']), $this->tratarNumeroCartao($dados['number']), $dados['cvc'], $dadosCobranca)
                    ->setInstallmentCount(1)
                    ->setStatementDescriptor("Parc. Única")
                    ->execute();

                // Atualiza o ID de pagamento
                Agenda::where( 'id', $consulta->agenda_id )
                           ->update([
                                'id_pagamento' => $payment->getId(),
                                'status_pagamento' => $payment->getStatus()
                            ]);

                echo json_encode(array("erro" => false, "dados" => $payment)); exit();

            } catch (\Moip\Exceptions\UnautorizedException $e) {
                echo $e->getMessage();
            } catch (\Moip\Exceptions\ValidationException $e) { 
                $mensagemErro   = "";
                $aux            =   $e->__toString();
                switch ($e->getStatusCode()) {
                    case 400:
                            $mensagemErro = explode("The following errors ocurred:", $aux)[1];
                        break;
                    default:
                            $mensagemErro = $aux;
                        break;
                }
                echo json_encode(array("erro" => true, "msg" => "[".$e->getStatusCode()."]: ".$mensagemErro)); exit();
            } catch (\Moip\Exceptions\UnexpectedException $e) {
                echo $e->getMessage();
            }
             catch (Exception $e) {
                echo json_encode(array("erro" => true, "msg" => $e->__toString())); exit();
            }
        }
        // caso seja profissional
        else
        {
            return redirect()->action('IndexController@index');
        } 
 
        // carregando view
        // return view('agenda.boleto')
        //         ->with('consulta', $consulta)
        //         ->with('usuario', $usuario);

    }
    public function visualizarBoleto(Request $request) {

    }
    public function webook(Request $request) {
        Log::info(['Moip' => $request]);
    }   
}
