// O comando abaixo, ajusta as permisões na hierarquia de diretórios e arquivos
// setando os diretórios para 755 e arquivos para 644
// funciona apenas a partir do diretório atual
chmod -R u+rwX,go+rX,go-w .