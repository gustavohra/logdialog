# gravando grupo admin, caso ainda não exista
INSERT INTO logdialog.tb_grupo_sistema (grupo)
	SELECT 'admin'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_grupo_sistema WHERE grupo = 'admin' LIMIT 1 );

# gravando grupo profissional, caso ainda não exista
INSERT INTO logdialog.tb_grupo_sistema (grupo)
	SELECT 'profissional'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_grupo_sistema WHERE grupo = 'profissional' LIMIT 1 );

# gravando grupo paciente, caso ainda não exista
INSERT INTO logdialog.tb_grupo_sistema (grupo)
	SELECT 'paciente'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_grupo_sistema WHERE grupo = 'paciente' LIMIT 1 );