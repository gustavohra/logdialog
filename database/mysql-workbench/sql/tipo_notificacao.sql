# gravando tipo de notificação, caso não exista
INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'novo_inbox'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'novo_inbox' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'solicitacao_agenda'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'solicitacao_agenda' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'agendamento_confirmado'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'agendamento_confirmado' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'agendamento_cancelado'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'agendamento_cancelado' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'agendamento_lembrete'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'agendamento_lembrete' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'pagamento_aguardando'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'pagamento_aguardando' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'pagamento_confirmado'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'pagamento_confirmado' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'pagamento_erro'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'pagamento_erro' LIMIT 1 );

INSERT INTO logdialog.tb_tipo_notificacao (tipo)
	SELECT 'sistema_aviso'
WHERE NOT EXISTS ( SELECT * FROM logdialog.tb_tipo_notificacao WHERE tipo = 'sistema_aviso' LIMIT 1 );