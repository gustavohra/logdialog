# inserindo dados na tabela de resposta
INSERT IGNORE INTO logdialog.tb_orientacao_resposta
	( id, resposta )
VALUES
	( 1, "Se você se irrita ou se frustra com facilidade e seus níveis de tolerância estão muito baixos você provavelmente se beneficiaria em falar com um psicólogo." ),
	( 2, "Sintomas físicos ou somáticos inexplicáveis, tais como dores no corpo, dores de cabeça frequentes ou sintomas gastrointestinais podem ser sinais de sofrimento que merecem cuidado psicológico." ),
	( 3, "Se você sentir que está mais isolado ou com dificuldades de se relacionar com pessoas importantes (amigos, pais, parceiro) ou até mesmo com dificuldades para se  socializar, provavelmente é hora de procurar ajuda." ),
	( 4, "Ambos são sintomas muitas vezes ignorados entretanto muitos conflitos ou sofrimentos psicológicos como ansiedade, tristeza, preocupações excessivas, uso abusivo de álcool e outras substâncias estão associados a distúrbios do sono ou alteração no apetite." ),
	( 5, "A maioria dos problemas psicológicos também provocam dificuldades no funcionamento cognitivo, incluindo falta de atenção e concentração, esquecimento e dificuldade na tomada de decisões. Se estes sintomas se tornarem frequentes, eles merecem ser investigados." ),
	( 6, "Este é um sinal de alarme que não deve ser ignorado. Você deve agir imediatamente, procurando ajuda. Se às vezes você sente o desejo de se ferir fisicamente, com certeza é motivo de preocupação e neste caso, o seu lugar para procurar ajuda não é neste serviço de atendimento psicológico online. Você deve procurar uma ajuda psiquiátrica o quanto antes." );

# inserindo na tabela de perguntas
INSERT IGNORE INTO logdialog.tb_orientacao_pergunta
	( id, titulo, pergunta, resposta_sim )
VALUES
	( 1, "Oscilação de humor", "Seu humor oscila?", 1 ),
	( 2, "Dores inexplicáveis", "Você sente uma dor inexplicável?", 2 ),
	( 3, "Dificuldades para se relacionar", "Você está com dificuldades para se relacionar?", 3 ),
	( 4, "Problemas para dormir ou alteração no apetite", "Você tem problemas para dormir ou alteração no apetite?", 4 ),
	( 5, "Esquecimento, falta de atenção ou sem concentração", "Você anda muito esquecido, desatento ou sem concentração?", 5 ),
	( 6, "Vontade de se prejudicar ou se ferir", "Você quer se prejudicar ou se ferir?", 6 );