# inserindo dados na tabela de usuario
INSERT IGNORE INTO logdialog.tb_usuario
	( 
		id,
		username,
		email,
		senha,
		grupo_sistema_id
	)
VALUES
	( 
		1,
		"adriano.wead",
		"adriano_mail@hotmail.com",
		"$2y$10$C0rpLrVtfU6caYa1X2RO6uqMhkXdbh8KnLqp13VnhW35isXjuTN3K",
		1
	);