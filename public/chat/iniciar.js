"use strict";
var express 		= require('express');
var app     		= express();
 var fs = require('fs');
var privateKey  = fs.readFileSync('/home/logdialog/ssl/keys/d4741_3b9cb_ad9ccb6a829f9228676fa93739abc8c4.key', 'utf8');
var certificate = fs.readFileSync('/home/logdialog/ssl/certs/logdialog_com_br_d4741_3b9cb_1515946080_91fb720465e645b651607ffb34455942.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};
var server  		= require('https').createServer(credentials, app);
var io 				= require('socket.io')(server);
var usuariosOnline 	= [];
var timeout 		= setTimeout(function(){ usuariosOnline = []; timeout; }, 3600000);
var chokidar 		= require('chokidar');
 
 
console.log("Iniciado");
io.on('connection', function (socket) {
  socket.emit('startup', { 'message': 'LogDialog / Iniciado.', 'usuariosOnline': usuariosOnline });
		socket.on('chatEntrar', function (id) {
				var flag = false;
				// Caso o usuário não esteja registrado, realiza o registro
				usuariosOnline.forEach( function(element, index) {
					if(element == id)
						flag = true;
				});
				if(flag == false)
					usuariosOnline.push(id);

			  	io.sockets.emit('chatEntrar', {usuariosOnline: usuariosOnline});
			});


		socket.on('chatDigitando', function (data) {
			  	io.sockets.emit('chatDigitando', {'hash': data.hash, 'digitando': data.flag});
			});	
		socket.on('enviarMensagem', function (data) {
			  	io.sockets.emit('receberMensagem', {'hash': data.hash, 'avatar': data.de_avatar, 'nome': data.de_nome, 'mensagem': data.mensagem, 'data_mensagem': data.data_mensagem});
			});		 
}); 


server.listen(49999, function() {
  console.log('Servidor iniciado na porta 49999.');
});