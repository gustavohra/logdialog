/**
 * Este documento contém pequenas funções
 * utilitárias do sistema
 *
 * @author Adriano Maciel
 */
var _logDialog_Util = {
	/**
	 * Esta função valida o formato do email
	 */
	emailValido : function(email)
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
	},

	/**
	 * Função para exibir um modal no estilo Alert
	 */
	modalAlert : function( titulo, mensagem, botao )
	{
		var janela = $("#javascriptAlert");

		janela.find('.modal-title').eq(0).html( titulo );
		janela.find('button').eq(1).html( botao );
		janela.find('.modal-body').eq(0).html( mensagem );

		// exibindo
		janela.modal('show');
	}
}