/**
 * Este documento contém o controle de envio do formulário de contato
 *
 * @author Adriano Maciel
 */
var contato = {
	// flag setada como true
	// se o sistema ainda estiver executando
	executando : false,

	/**
	 * Esta função executa o envio do contato
	 */
	enviar : function(id)
	{
		// prosseguir apenas se não estiver executando
		if( !contato.executando )
		{
			// flag de execução
			contato.executando = true;

			// prosseguir com o cadastro
			$.ajax({
				url  : '/contato/enviar',
				type : 'post',
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
				data : $("#" + id).serialize(),
				success : function(data)
				{
					// liberando o cadastro
					contato.executando = false;

					// verificando se foi com sucesso
					if( data.sucesso )
					{
						// abrindo modal com aviso
						_logDialog_Util.modalAlert(
							"Contato", // titulo
							"<p>Sua mensagem foi enviada com sucesso.</p>", // mensagem
							"Ok" // texto do botão
						);

						// limpando formulário
						$(':input', '#' + id)
							.not(':button, :submit, :reset, :hidden').val('')
							.removeAttr('checked')
							.removeAttr('selected');
					}
					// no caso de erro
					else if( data.errors )
					{
						var msg = '';

						if( data.errors.nome )
							msg += "- " + data.errors.nome[0] + "<br />";

						if( data.errors.email )
							msg += "- " + data.errors.email[0] + "<br />";

						if( data.errors.mensagem )
							msg += "- " + data.errors.mensagem[0] + "<br />";

						// abrindo modal com aviso
						_logDialog_Util.modalAlert(
							"Contato", // titulo
							"<p>" + msg + "</p>", // mensagem
							"Ok" // texto do botão
						);
					}
				},
				error : function()
				{
					// liberando o cadastro
					contato.executando = false;

					// abrindo modal com aviso
					_logDialog_Util.modalAlert(
						"Contato", // titulo
						"<p>Houve um erro ao enviar sua mensagem, tente novamente em breve.</p>", // mensagem
						"Ok" // texto do botão
					);
				}
			});
		}

		return true;
	}
}


/**
 * Listener ao enviar o formulário
 */
$(document).ready(function(){
	$("#formContato").bind("submit", function(){
		contato.enviar('formContato');
		return false;
	});
});