$("#btn-chat").click(function() {
	if($('#conference-chat').css('display') == 'block'){
		$("#conference-chat").css('display', 'none');
		$("#conference-controllers").css('width', '100%');
		$("#btn-chat").removeClass( "active");
		$("#imgconference").removeClass( "active");
	} else {
		$("#conference-notes").css('display', 'none');
		$("#conference-notes-open").css('display', 'none');
		$("#conference-chat").css('display', 'block');
		$("#conference-controllers").css('width', '75%');
		$("#btn-chat").addClass( "active");
		$("#btn-notes").removeClass( "active");
		$("#imgconference").addClass( "active");
	}
});

$("#btn-notes").click(function() {
	if($('#conference-notes').css('display') == 'block'){
		$("#conference-notes").css('display', 'none');
		$("#conference-notes-open").css('display', 'none');
		$("#conference-controllers").css('width', '100%');
		$("#btn-notes").removeClass( "active");
		$("#imgconference").removeClass( "active");
	} else {
		$("#conference-chat").css('display', 'none');
		$("#conference-notes").css('display', 'block');
		$("#conference-controllers").css('width', '75%');
		$("#btn-notes").addClass( "active");
		$("#btn-chat").removeClass( "active");
		$("#imgconference").addClass( "active");
	}
});

$(".cfclosebtn").click(function() {
	$("#conference-notes").css('display', 'none');
	$("#conference-chat").css('display', 'none');
	$("#conference-controllers").css('width', '100%');
	$("#btn-chat").removeClass( "active");
	$("#btn-notes").removeClass( "active")
	$("#imgconference").removeClass( "active");;
});


$("#note01").click(function() {
	$("#conference-notes").css('display', 'none');
	$("#conference-notes-open").css('display', 'block');
	$("#conference-controllers").css('width', '75%');
	$("#imgconference").addClass( "active");
});

$(".cfcancelbtn").click(function() {
	$("#conference-notes").css('display', 'block');
	$("#conference-notes-open").css('display', 'none');
	$("#imgconference").addClass( "active");
});
