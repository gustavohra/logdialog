
        function changeCalendarView(data, dia) {
            var calendarview = data;
            if (calendarview == 1) {
                if(dia != undefined) {
                    // alterando os botões de navegação
                    $("#calendar-day .daily-left a.avancar").attr("data-timestamp", moment(dia.dia_mes_completo).unix());

                    // executando o click
                    $("#calendar-day .daily-left a.avancar").trigger('click');
                }
                $("#calendarview-1").addClass('active');
                $("#calendarview-2").removeClass('active');
                $("#calendarview-3").removeClass('active');
                $("#calendarview-4").removeClass('active');
                document.getElementById('calendar-day').style.display = "block";
                document.getElementById('calendar-week').style.display = "none";
                document.getElementById('calendar-month').style.display = "none";
                document.getElementById('calendar-year').style.display = "none";
            } else if (calendarview == 2) {
                $("#calendarview-1").removeClass('active');
                $("#calendarview-2").addClass('active');
                $("#calendarview-3").removeClass('active');
                $("#calendarview-4").removeClass('active');
                document.getElementById('calendar-day').style.display = "none";
                document.getElementById('calendar-week').style.display = "block";
                document.getElementById('calendar-month').style.display = "none";
                document.getElementById('calendar-year').style.display = "none";
            } else if (calendarview == 3) {
                $("#calendarview-1").removeClass('active');
                $("#calendarview-2").removeClass('active');
                $("#calendarview-3").addClass('active');
                $("#calendarview-4").removeClass('active');
                document.getElementById('calendar-day').style.display = "none";
                document.getElementById('calendar-week').style.display = "none";
                document.getElementById('calendar-month').style.display = "block";
                document.getElementById('calendar-year').style.display = "none";

    
            } else if (calendarview == 4) {
                $("#calendarview-1").removeClass('active');
                $("#calendarview-2").removeClass('active');
                $("#calendarview-3").removeClass('active');
                $("#calendarview-4").addClass('active');
                document.getElementById('calendar-day').style.display = "none";
                document.getElementById('calendar-week').style.display = "none";
                document.getElementById('calendar-month').style.display = "none";
                document.getElementById('calendar-year').style.display = "block";
                            // ao iniciar o documento
                // processar o ano atual
                calendario.detalharAno( $("#iniciarAno"), true );

            }
        }
