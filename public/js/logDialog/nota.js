/**
 * Processamento referente à notas
 */
var notas = {
	targetId: 0,

	/**
	 * Edição de notas no bloco de notas
	 */
	editarNota: function($obj, notaId )
	{
		// recuperando o objeto da anotação
		$obj = $($obj);

		// removendo a classe de seleção
		$(".anotacoes-list .selecionado").removeClass('selecionado');

		// adicionando classe ao item selecionado
		$obj.addClass('selecionado');

		// setando os valores no bloco de visualização
		$bloco = $(".anotacoesblock-right").eq(0);

		$bloco.find('.nota-titulo').html( $.base64('decode', $obj.attr('data-titulo')) );
		$bloco.find('.nota-data').html( "Criado em " + moment.unix($obj.attr('data-nota-timestamp')).format("DD/MM/YYYY") );
		$bloco.find('.nota-body').html( $.base64('decode', $obj.attr('data-conteudo')) );

		// bind para editar ou excluir nota
		$bloco.find('.nota-editar').unbind('click');
		$bloco.find('.nota-excluir').unbind('click');

		$bloco.find('.nota-editar').parent().show();

		$bloco.find('.nota-editar').bind('click', function(){
			notas.habilitarEdicaoNota( notaId );
		});

		$bloco.find('.nota-excluir').bind('click', function(){
			notas.excluirNota( notaId );
		});
	},

	/**
	 * Editar nota
	 */
	habilitarEdicaoNota: function( notaId )
	{
		// armazenar id atual
		notas.targetId = notaId;

		$bloco = $(".anotacoesblock-right").eq(0);

		$obj = $("#nota_id_" + notas.targetId);

		// pegando o conteúdo
		// setando no modal
		$("#nota-editar-conteudo").html( $.base64('decode', $obj.attr('data-conteudo')).replace(/(<([^>]+)>)/ig,"") );

		// mostrar modal
		$("#editarNotaModel").modal('show');
	},

	/**
	 * Excluir nota
	 */
	excluirNota: function( notaId )
	{
		// armazenar id atual
		notas.targetId = notaId;

		if( notas.targetId > 0 )
		{
			// alerta
			$("#excluirNotaModel").modal('show');
		}
	},

	/**
	 * Executar a exclusão da nota
	 */
	processarExcluirNota: function()
	{
		if( notas.targetId > 0 )
		{
			// removendo do html
			$bloco = $(".anotacoesblock-right").eq(0);

			$bloco.find('.nota-data').html( '&nbsp;' );
			$bloco.find('.nota-body').html( 'Nota removida' );

			$bloco.find('.nota-editar').parent().hide();

			$("#nota_id_" + notas.targetId).remove();

			// submetendo via ajax
			$.ajax({
				url: '/anotacao/excluir',
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
				data: {
					_token: $('meta[name=_token]').attr('content'),
					notaId : notas.targetId
				},
				success: function(){
					notas.targetId = 0;
				}
			});
		}
	},

	/**
	 * Processar a solicitação de salvar a nota
	 */
	processarSalvarNota: function()
	{
		if( notas.targetId > 0 )
		{
			// recuperando o conteúdo editado
			var conteudo = $("#nota-editar-conteudo").val().replace(/(<([^>]+)>)/ig,"");

			// atualizando o html
			$bloco = $(".anotacoesblock-right").eq(0);

			$obj = $("#nota_id_" + notas.targetId);

			//
			$bloco.find('.nota-body').html( conteudo.replace(/\n/g,  '<br>') );

			// setando base64
			$obj.attr( 'data-conteudo', $.base64('encode', conteudo.replace(/\n/g,  '<br>')) );

			// enviando ao servidor
			$.ajax({
				url: '/anotacao/atualizar',
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
				data: {
					_token: $('meta[name=_token]').attr('content'),
					notaId : notas.targetId,
					conteudo : conteudo
				},
				success: function(){
					notas.targetId = 0;
				}
			});
		}
	},

	/**
	 * Quando o perfil de um profissional selecionar uma nota na listagem
	 */
	selecionarNotaProfissional: function( $obj, index )
	{
		// recuperando o objeto da anotação
		$obj = $($obj);

		// removendo a classe de seleção
		$(".anotacoes-list .selecionado").removeClass('selecionado');

		// adicionando classe ao item selecionado
		$obj.addClass('selecionado');

		// setando os valores no bloco de visualização
		$bloco = $(".anotacoesblock-right").eq(0);

		$bloco.find('.nota-titulo').html( $.base64('decode', $obj.attr('data-titulo')) );

		$conteudo = JSON.parse( $.base64('decode', $obj.attr('data-conteudo')) );

		// zerar conteúdo atual
		$bloco.find('.anotacao-content').html('');

		// armazenando o tmp
		$tmp = $('<div>');

		for( i = 0; i < $conteudo.length; i++ )
		{
			var data = $('<p>').append( $('<span>').addClass('align-left').html( $conteudo[i].data ) );
			var left = $('<div>').addClass('align-left').append( data );
			var header = $('<div>').addClass('nota-header').append( left );

			$bloco.find('.anotacao-content').append( header );

			// processar conteúdo
			if($conteudo[i].conteudo != null)
				$tmpConteudo = $conteudo[i].conteudo.replace(/\n/g, '<br>');
			else
				$tmpConteudo = '';

			var conteudo = $('<div>').addClass('nota-body');

			conteudo.append( $tmpConteudo );

			if( $conteudo[ ( i + 1 ) ] )
			{
				conteudo.append('<br><br><br>');
				conteudo.append('===== fim da nota de ' + $conteudo[i].data + ' =====');
				conteudo.append('<br><br><br>');
			}

			$bloco.find('.anotacao-content').append( conteudo );
		}
	}
}