/**
 * Este arquivo, é responsável por se comunicar com a API
 * E processar as informações relacionadas aos calendários e agendas do sistema
 */
function urldecode(str) {
   return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}

 function exibirPopoup(v) { 
 	console.log(v);
 		fecharPopup();
	 	$("#popover-consulta_"+v+"_content").fadeToggle('fast', function() {
	 		
	 	}); 	 
 }
function fecharPopup() {
	$(".popover").fadeOut('fast', function() {
		
	});
}
var calendario = {
	/**
	 * Esta função carrega os compromissos com a visão de dia, para a data informada
	 * Ordenados pelo horário
	 * Apenas do usuário logado
	 */
	detalharDia: function( $obj, data )
	{ 
		// pegando timestamp do item clicado
		if(data == null || data == undefined){
			var timestamp = $($obj).attr('data-timestamp').split("."),
				url = "/calendario/dia/" + timestamp[0];
		}
		else{
			var timestamp =  data.unix(),
			url = "/calendario/dia/" + timestamp;
		}

		var tomorrow = new Date( timestamp * 1000 );
		    tomorrow.setDate( tomorrow.getDate() + 1 );

		var yesterday = new Date( timestamp * 1000 );
		    yesterday.setDate( yesterday.getDate() - 1 );

		// caso as datas sejam idênticas, isso acontece no voltar
		// então basta voltar mais um dia de cada
		if( ( tomorrow.getTime() / 1000 ) == $($obj).parent().find('.avancar').eq(0).attr('data-timestamp') )
		{
			tomorrow.setDate( tomorrow.getDate() - 1 );
			yesterday.setDate( tomorrow.getDate() - 1 );
		}

		$($obj).parent().find('.avancar').eq(0).attr('data-timestamp', ( tomorrow.getTime() / 1000 ) );
		$($obj).parent().find('.voltar').eq(0).attr('data-timestamp', ( yesterday.getTime() / 1000 ) );

		// atualizando links

		// carregando dados do servidor
		$.ajax({
			url: url,
			type: "POST",
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			dataType: "json",
			data: {
				_token: $('meta[name=_token]').attr('content')
			},
			success: function(data){

				// adicionando data no html
				$("#calendar-day h3.data-calendario").eq(0).html( data.data );

				// não armazenar cache
				EJS.config({cache: false});

				// carregando teste
				var html = new EJS({
					url : '/ejs/template/agenda/minha-agenda/dia.ejs'
				}).render({consultas : data.horarios});

				// adicionando na agenda
				$("#detalheAgendaDia").html( html );
			},
			error: function(){
				//
			}
		});
	},

	/**
	 * Esta função carrega os compromissos com a visão de semana, para a data informada
	 * Apenas do usuário logado
	 */
	detalharSemana: function( $obj, manterAtual )
	{
		// pegando timestamp
		var timestampInicio = moment.unix($($obj).parent().find('.voltar').eq(0).attr('data-timestamp'));
		var timestampFim = moment.unix($($obj).parent().find('.avancar').eq(0).attr('data-timestamp'));

		// calcular a atualização do html
		// apenas se não for o carregamento inicial
		// o que significa que seve ser atualizar a data informada
		if( !manterAtual )
		{
			var classe = $($obj).attr('class');

			// no caso se voltar a semana
			if( classe == 'voltar' )
			{
				timestampInicio.subtract(7, 'days');
				timestampFim.subtract(7, 'days');
			}
			// ou avançar
			else if( classe == 'avancar' )
			{
				timestampInicio.add(7, 'days');
				timestampFim.add(7, 'days');
			}

			// setando nos objetos
			$($obj).parent().find('.voltar').eq(0).attr('data-timestamp', timestampInicio.unix() );
			$($obj).parent().find('.avancar').eq(0).attr('data-timestamp', timestampFim.unix() );
		}

		// carregando dados do servidor
		$.ajax({
			url: "/calendario/intervalo/" + timestampInicio.unix() + "/" + timestampFim.unix(),
			type: "POST",
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			dataType: "json",
			data: {
				_token: $('meta[name=_token]').attr('content'),
				id: usuarioId
			},
			success: function(data){

				// adicionando data no html
				$("#calendar-week h3.data-calendario").eq(0).html( data.data );

				// não armazenar cache
				EJS.config({cache: false}); 
				data.dias = Array.prototype.slice.call(data.dias);
				var qtdHorarios =  Object.keys(data.dias[0].horarios).length;
				var horarios = [];

				for(var x in data.dias[0].horarios){
				  horarios.push(data.dias[0].horarios[x]);
				} 
				// carregando teste
				var html = new EJS({
					url : '/ejs/template/agenda/minha-agenda/semana.ejs'
				}).render({dias : data.dias, horarios: horarios,  qtdHorarios:qtdHorarios});

				// adicionando na agenda
				$("#templateVisaoSemana").html( html );
			},
			error: function(){
				//
			}
		});
	},

	/**
	 * Esta função carrega os compromissos com a visão de mês, para a data informada
	 * Apenas do usuário logado
	 */
	detalharMes: function( $obj, manterAtual )
	{
		// pegando timestamp
		var timestampInicio = moment.unix($($obj).parent().find('.voltar').eq(0).attr('data-timestamp'));
		var timestampFim = moment.unix($($obj).parent().find('.avancar').eq(0).attr('data-timestamp'));

		// trabalhar com o inicio e final do mês
		timestampInicio.startOf('month');
		timestampFim.endOf('month');

		// calcular a atualização do html
		// apenas se não for o carregamento inicial
		// o que significa que seve ser atualizar a data informada
		if( !manterAtual )
		{
			var classe = $($obj).attr('class');

			// no caso se voltar o mês
			if( classe == 'voltar' )
			{
				timestampInicio.subtract(1, 'month');
				timestampFim.subtract(1, 'month');
			}
			// ou avançar
			else if( classe == 'avancar' )
			{
				timestampInicio.add(1, 'month');
				timestampFim.add(1, 'month');
			}

			// setando nos objetos
			$($obj).parent().find('.voltar').eq(0).attr('data-timestamp', timestampInicio.startOf('month').unix() );
			$($obj).parent().find('.avancar').eq(0).attr('data-timestamp', timestampFim.endOf('month').unix() );
		}

		// carregando dados do servidor
		$.ajax({
			url: "/calendario/intervalo/" + timestampInicio.startOf('month').unix() + "/" + timestampFim.endOf('month').unix(),
			type: "POST",
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			dataType: "json",
			data:{
				_token: $('meta[name=_token]').attr('content'),
				id: usuarioId
			},
			success: function(data){
				console.log("Mês", data);
				// executando um préprocessamento
				// para completar os dias anteriores e posteriores ao mês atual
				// caso necessário

				// verificando quantos dias anteriores ficaram em branco
				var contagemAnteriores = 0;

				for( var i = 0; i < data.dia_semana.length; i++ )
				{
					// comparando apenas com o primeiro resultado
					if( data.dia_semana[i] != data.dias[0].dia_semana )
						contagemAnteriores++;
					else
						break;
				}

				// verificando quantos dias após ficaram em branco
				var contagemApos = 0;

				for( var i = data.dia_semana.length; i > 0; i-- )
				{
					// comparando apenas com o primeiro resultado
					if( data.dia_semana[i] != data.dias[ data.dias.length - 1 ].dia_semana )
						contagemApos++;
					else
						break;
				}

				// processando os dias anteriores
				var diasAnteriores = [];

				for( var i = contagemAnteriores; i > 0; i-- )
				{
					// regredindo dias a partir da data inicial do mês
					diasAnteriores.push( moment( data.dias[0].dia_mes_completo ).subtract( i, 'days' ).format('D') );
				}

				// processando os dias posteriores
				var diasPosteriores = [];

				for( var i = 1; i < contagemApos; i++ )
				{
					// regredindo dias a partir da data inicial do mês
					diasPosteriores.push( moment( data.dias[ data.dias.length - 1 ].dia_mes_completo ).add( i, 'days' ).format('D') );
				}

				// adicionando data no html
				$("#calendar-month h3.data-calendario").eq(0).html( data.data );

				// não armazenar cache
				EJS.config({cache: false});

				// carregando mês small
				// caso haja o atributo
				if( $($obj).is("[data-small-calendar]") )
				{
					$("#small-calendar .month-title").html( " " + data.data + " " );
					var horarios = ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00'];

					var html = new EJS({
						url : '/ejs/template/agenda/minha-agenda/mes-small.ejs'
					}).render({
						mes : data,
						horarios : horarios,
						anteriores : diasAnteriores,
						posteriores : diasPosteriores
					});

					// adicionando na agenda
					$("#dias-small-calendar").html( html );

					// adicionando bordas, na parte inferior do calendário,
					// que são os sete últimos blocos
					$("#dias-small-calendar li").slice(-7).addClass("tdborderb").addClass("ultimos-dias");
				}
				else
				{
					var horarios = ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00'];
					var html = new EJS({
						url : '/ejs/template/agenda/minha-agenda/mes.ejs'
					}).render({
						mes : data,
						horarios : horarios,
						anteriores : diasAnteriores,
						posteriores : diasPosteriores
					});
					console.log(data);
					// adicionando na agenda
					$("#templateVisaoMes").html( html );

					// adicionando bordas, na parte inferior do calendário,
					// que são os sete últimos blocos
					$("#templateVisaoMes .days li").slice(-7).addClass("tdborderb");
				}
			},
			error: function(){
				//
			}
		});
	},

	/**
	 * Esta função carrega os compromissos com a visão de ano, para a data informada
	 * Apenas do usuário logado
	 */
	detalharAno: function( $obj, manterAtual )
	{
		// // pegando timestamp
		// var timestampInicio = moment.unix($($obj).parent().find('.voltar').eq(0).attr('data-timestamp'));
		// var timestampFim = moment.unix($($obj).parent().find('.avancar').eq(0).attr('data-timestamp'));

		// // trabalhar com o inicio e final do ano
		// timestampInicio.startOf('year');
		// timestampFim.endOf('year');

		// // calcular a atualização do html
		// // apenas se não for o carregamento inicial
		// // o que significa que seve ser atualizar a data informada
		// if( !manterAtual )
		// {
		// 	var classe = $($obj).attr('class');

		// 	// no caso se voltar o ano
		// 	if( classe == 'voltar' )
		// 	{
		// 		timestampInicio.subtract(1, 'year');
		// 		timestampFim.subtract(1, 'year');
		// 	}
		// 	// ou avançar
		// 	else if( classe == 'avancar' )
		// 	{
		// 		timestampInicio.add(1, 'year');
		// 		timestampFim.add(1, 'year');
		// 	}

		// 	// setando nos objetos
		// 	$($obj).parent().find('.voltar').eq(0).attr('data-timestamp', timestampInicio.startOf('year').unix() );
		// 	$($obj).parent().find('.avancar').eq(0).attr('data-timestamp', timestampFim.endOf('year').unix() );
		// }

		// // // carregando dados do servidor
		// // $.ajax({
		// // 	url: "/calendario/intervalo/" + timestampInicio.startOf('year').unix() + "/" + timestampFim.endOf('year').unix(),
		// // 	type: "POST",
		// // 	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
		// // 	dataType: "json",
		// // 	data:{
		// // 		_token: $('meta[name=_token]').attr('content'),
		// // 		id: usuarioId
		// // 	},
		// // 	success: function(data){

		// // 		$("#calendarioAnoHeader").html( "Ano " + data.ano );

		// // 		// // não armazenar cache
		// // 		// EJS.config({cache: false});

		// // 		// // carregando teste
		// // 		// var html = new EJS({
		// // 		// 	url : '/ejs/template/agenda/minha-agenda/ano.ejs'
		// // 		// }).render({
		// // 		// 	ano : data
		// // 		// });

		// // 		// // adicionando na agenda
		// // 		// $("#templateVisaoAno").html( html );
		// // 	},
		// // 	error: function(){
		// // 		//
		// // 	}
		// // });
	},

	/**
	 * Confirmar consulta
	 */
	confirmarConsulta: function( $obj, agendaId )
	{
		// removendo bloco de botões
		if( $($obj).parent().hasClass('notification-actions') )
			$($obj).parent().remove();

		// submetendo ao servidor
		$.ajax({
			url: '/calendario/confirmar-consulta',
			type: 'post',
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			data: {
				_token: $('meta[name=_token]').attr('content'),
				agendaId : agendaId
			},
			success: function(data){
				window.location.href=site_url+"/meu/perfil";
		    },
		    error: function(xhr,desc,err){
		    }
		});
	},

	/**
	 * Recusar consulta
	 */
	recusarConsulta: function( $obj, agendaId )
	{
		// removendo bloco de botões
		if( $($obj).parent().hasClass('notification-actions') )
			$($obj).parent().remove();

		// submetendo ao servidor
		$.ajax({
			url: '/calendario/recusar-consulta',
			type: 'post',
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			data: {
				_token: $('meta[name=_token]').attr('content'),
				agendaId : agendaId
			},
			success: function(data){
				window.location.href=site_url+"/meu/perfil";
		    },
		    error: function(xhr,desc,err){
		    }
		});
	},


	/**
	 * Esta função, deve ser executada no 'small calendar'
	 * Ela serve para carregar o dia selecionado, na visão 'dia'
	 * Que no layout atual fica bem à esquerda do small calendar
	 */
	_carregarDetalheDia: function( $obj, timestamp )
	{
		// pegar o timestamp do dia anterior
		// para executar o mesmo trigger na navegação da visão dia,
		// e aproveitar a estrutura já programada

		var data = moment(timestamp);

		// alterando os botões de navegação
		$("#calendar-day .daily-left a.avancar").attr("data-timestamp", data.unix());

		// executando o click
		$("#calendar-day .daily-left a.avancar").trigger('click');
	},

	/**
	 * Esta função serve para carregar as anotações
	 * E as opções de ceitar ou cancelar a consulta
	 * Além de permitir a edição das anotações
	 *
	 * Apenas para a visão dia do calendário
	 */
	_carregarAnotacoesConsultaDia: function( $obj, agendaId )
	{
		// carregando dados de anotações
		$.ajax({
			url: '/anotacao/consulta',
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			data: {
				_token: $('meta[name=_token]').attr('content'),
				agendaId : agendaId
			},
			dataType: 'json',
			success: function( data ){
				// não armazenar cache
				EJS.config({cache: false});

				// carregando teste
				var html = new EJS({
					url : '/ejs/template/anotacao/small.ejs'
				}).render({
					nota : data
				});

				// adicionando na agenda
				$("#anotacoes-consulta-visao-dia").html( html );
			}
		});
	},

	/**
	 * Função executada quando o paciente clicar para reagendar a consulta
	 */
	reagendarConsulta: function( $obj, agendaId, profissionalId, url )
	{
		// define url no caso de clicar em sim
		$('#reagendarConsultaAviso button').unbind('click');

		$('#reagendarConsultaAviso .btn-success').bind('click', function(){
			window.top.location = url
		});

		// exibe um alerta para reagendamento
		$('#reagendarConsultaAviso').modal('show');
	},
	/**
	 * Função executada quando o paciente clicar para cancelar a consulta
	 */
	cancelarConsulta: function( $obj, agendaId, profissional, url )
	{ 

		$('#cancelarConsulta .btn-cancel button').bind('click', function(){
			var motivo = $("#cancelar-conulta-motivo").val();

			window.top.location = url +"&motivo="+encodeURIComponent(motivo);
		});

		// exibe um alerta para reagendamento
		$('#cancelarConsulta').modal('show');
		profissional = JSON.parse(urldecode(profissional));
		var conteudoprofissional = '<div class="img-top" ><img src="'+profissional.profissionalFoto+'"></div><div class="txt-top"><h4>'+profissional.profissionalNome+'</h4><p>CRP '+profissional.profissionalCrp+'<br>'+profissional.profissionalEndereco+'<br>'+profissional.consulta+'</p></div>';
		$('#cancelarConsulta #cancelar-consulta-profissional').empty().append(conteudoprofissional);
	},
	/**
	 * Função executada quando o paciente clicar para cancelar a consulta
	 */
	agendarDia: function( data, horario )
	{  
		$('#agendarConsulta-adm').modal('show');
		$('input[name="diaConsulta"]').val(data);
		$('input[name="diaConsulta"]').trigger("change");
		setTimeout(function(){
		  $('#formAgendarConsulta [name="horarioConsulta"]').val(horario);
		}, 1000)
		
		// profissional = JSON.parse(urldecode(profissional));
		// var conteudoprofissional = '<div class="img-top" ><img src="'+profissional.profissionalFoto+'"></div><div class="txt-top"><h4>'+profissional.profissionalNome+'</h4><p>CRP '+profissional.profissionalCrp+'<br>'+profissional.profissionalEndereco+'<br>'+profissional.consulta+'</p></div>';
		// $('#cancelarConsulta #cancelar-consulta-profissional').empty().append(conteudoprofissional);
	}
}