/**
 * Controle de funcionalidades da home
 */
var home = {
	/**
	 * Abrir modal de agendamento de consulta
	 */
	agendarConsulta: function( perfilAlvo, userAlvoId, estouLogado )
	{
		// quando o alvo for um profissional
		if( perfilAlvo == 'profissional' )
		{
			// caso não esteja logado
			if( !estouLogado )
			{
				// fechando o atual
				$("#profissional_" + userAlvoId).modal('hide');

				// apenas abrir o modal
				$("#agendarConsulta").modal('show');
			}
			else
			{
				// caso o modal, exista
				if( $("#agendarProfissional_" + userAlvoId).length > 0 )
				{
					// limpando qualquer conteúdo antigo
					// e exibindo mensagem de carregando
					$("#agendarProfissional_" + userAlvoId).find(".opcoesDeAgendamento p").html('Carregando opções de agendamento...');

					// fechando o atual
					$("#profissional_" + userAlvoId).modal('hide');

					// abrir o modal
					$("#agendarProfissional_" + userAlvoId).modal('show');

					// buscar os dados disponíveis de agendamento no servidor
					// @TODO: Implementar quando tiver a definição das dúvidas enviadas em 18/09/2016
				}
				else
				{
					// se não exisitir
					// provavelmente existe um aviso, exibir
					
					// fechando o atual
					$("#profissional_" + userAlvoId).modal('hide');

					// apenas abrir o modal
					$("#agendarConsulta").modal('show');
				}
			}
		}
	},

	/**
	 * Direcionar para a tela logada na aba de mensagens
	 */
	enviarMensagem: function( perfilAlvo, userAlvoId, estouLogado )
	{
		// quando o alvo for um profissional
		if( perfilAlvo == 'profissional' )
		{
			// caso não esteja logado
			if( !estouLogado )
			{
				// fechando o atual
				$("#profissional_" + userAlvoId).modal('hide');

				// apenas abrir o modal
				$("#agendarConsulta").modal('show');
			}
			else
			{
				window.top.location = '/meu/perfil?novaMensagem=' + userAlvoId + '#mensagens'
			}
		}
	},

	/**
	 * Exibir o modal de esqueci a senha
	 */
	modalEsqueciSenha: function()
	{
		// ocultando qualquer modal aberto
		$(".modal").modal("hide");

		// exibindo o de esqueci a senha
		$("#esqueciSenha").modal("show");
	}
}