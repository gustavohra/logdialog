/**
 * Página para controle do perfil de pacientes
 */
var perfilPaciente = {
	/**
	 * Esta função executa todos os incializáveis necessários
	 */
	_init: function()
	{
		// carregando o Bootstrap Input File
		this._initBootstrapFile();
	},

	/**
	 * Esta função, controla a exibição do ícone de carregamento
	 */
	_atualizarCarregando: function( $obj, $tipo )
	{
		// apenas se for do tipo link
		if( $($obj).prop("tagName") == "A" && $tipo == 'carregando' )
		{
			// atualizando classe do item com o Font Awesome
			if( $($obj).find('i.fa-check-circle-o').length > 0 )
			{
				var $item = $($obj).find('i.fa-check-circle-o').eq(0);

				$item.removeClass('fa-check-circle-o');
				$item.addClass('fa-refresh fa-spin fa-fw');
			}
		}
		else if( $($obj).prop("tagName") == "A" && $tipo == 'completo' )
		{
			// atualizando classe do item com o Font Awesome
			if( $($obj).find('i.fa-refresh').length > 0 )
			{
				var $item = $($obj).find('i.fa-refresh').eq(0);

				$item.removeClass('fa-refresh fa-spin fa-fw');
				$item.addClass('fa-check-circle-o');
			}
		}
	},

	/**
	 * Função para processar a atualização dos dados cadastrais do perfil
	 */
	atualizarPerfil: function( $obj )
	{
		// validar form
		$("#formPerfilPaciente").validator('validate');

		// verificando se está válido
		if( $("#formPerfilPaciente").has('.has-error').length == 0 )
		{
			// mostrando o ícone de carregamento
			perfilPaciente._atualizarCarregando( $obj, 'carregando' );

			// verificando se o CPF informado já está sendo usado por outra pessoa
			$.ajax({
				url : '/validar/documento/paciente/cpf',
                cache: false, 
				type: 'get',
				headers: { 'X-CSRF-Token' : $('#_token-paciente').val() },
				data: {
					_token:   $('meta[name=_token]').attr('content'),
					cpf : $.trim($('#cpf_paciente').val())
				},
				success: function( data )
				{
					// executar atualização
					$("#formPerfilPaciente").submit();
				},
				error: function()
				{
					alert('O CPF informado não pode ser utilizado.');

					// mostrando o ícone de carregamento
					perfilPaciente._atualizarCarregando( $obj, 'completo' );
				}
			});
		}
		else
		{
			// aqui caso esteja inválido
		}
	},

	/**
	 * Esta função executa os procedimentos de validação de senha (caso necessário)
	 * E submete o formulário ao servidor
	 */
	trocarSenha: function( $obj )
	{
		// validar form
		$("#formAlterarSenhaPaciente").validator('validate');

		// verificando se está válido
		if( $("#formAlterarSenhaPaciente").has('.has-error').length == 0 )
		{
			// mostrando o ícone de carregamento
			perfilPaciente._atualizarCarregando( $obj, 'carregando' );

			$("#formAlterarSenhaPaciente").submit();
		}
		else
		{
			// caso seja inválido
		}
	},

	/**
	 * Esta função ativa o bootstrap file input
	 */
	_initBootstrapFile: function()
	{
		$("#inputAvatarPaciente").fileinput({
			showCaption: false,
			showUpload: false,
			showRemove: false,
			showPreview: false,

			language: 'pt-BR',

			browseLabel: "Alterar",
			browseClass: "btn btn-modal btn-modaldefault",

			removeClass: "btn btn-modal btn-modalalert",
			removeLabel: "Remover",

			allowedFileExtensions: ["jpg", "jpeg", "png", "bmp"],

			maxFileSize: 2000, // 2MB = 2000 kb

			elErrorContainer: "#errorBlockAvatar"
		});

		// ao selecionar um arquivo
		$("#inputAvatarPaciente").on('change', function(event){
			// caso tenha algum conteúdo
			if( $.trim($(this).val()) != "" )
			{
				perfilPaciente._cortarAvatar( "inputAvatarPaciente" );

				// limpar flag de remover no formulário
				$("#inputRemoverAvatar").val(0);
			}
			else
			{
				perfilPaciente.removerAvatar();
			}
		});

		// ao clicar para remover o avatar
		$(".fileinput-remove-button").eq(0).on('click touchend', function(){
			perfilPaciente.removerAvatar();
		});

		// input de autorização para menor de idade
		if( $("#inputAutorizacao") )
		{
			$("#inputAutorizacao").fileinput({
				showCaption: false,
				showUpload: false,
				showRemove: false,
				showPreview: false,

				language: 'pt-BR',

				browseLabel: "Anexar",
				browseClass: "btn btn-modal btn-modaldefault",

				allowedFileExtensions: ["jpg", "jpeg", "png", "bmp", "pdf", "doc", "docx", "rtf"],

				maxFileSize: 3000, // 2MB = 2000 kb

				elErrorContainer: "#errorBlockAutorizacao"
			});

			// ao selecionar um arquivo
			$("#inputAutorizacao").on('change', function(event){
				// caso tenha algum conteúdo
				if( $.trim($(this).val()) != "" )
				{
					// submeter o formulário
					$("#formAutorizacaoMenor").submit();
				}
			});
		}
	},

	/**
	 * Esta função é executada sempre que o usuário clica no botão para remover o avatar
	 */
	removerAvatar: function()
	{
		// executando o reset do fileinput
		$("#inputAvatarPaciente").fileinput('clear');

		// definindo avatar padrão na imagem
		$('.editarperfil-avatar').find('img').eq(0).attr('src', '/images/avatar-padrao.jpg');

		// setando flag de remover no formulário
		$("#inputRemoverAvatar").val(1);

		// limpando string de upload
		$("#uploadAvatarPaciente").val('');
	},

	/**
	 * Inicializando cropper
	 */
	_initCropper: function()
	{
		perfilPaciente.crop = $('#CropImgTarget img').cropper({
		  	aspectRatio: 1,

		  	viewMode: 2,

		  	minCropBoxWidth: 100,
		  	minCropBoxHeight: 100,

		  	minContainerWidth: 550,
		  	minContainerHeight: 550,
		  	maxContainerWidth: 550,
		  	maxContainerHeight: 550,

		  	zoomable: false,
		  	scalable: false,
		  	rotatable: false,
		  	movable: false
		});
	},

	/**
	 * Esta função inicializa a função para crop da imagem em formato redondo
	 */
	_cortarAvatar: function( id )
	{
		// criando img DOM
		var img = $('<img/>', {
            id: 'dynamicImgCrop'
        });

        // selecionando objeto no DOM
        var file = document.getElementById(id);
            file = file.files[0];

        // iniciando leitor de arquivo
        var reader = new FileReader();

		// Mostrando imagem
        reader.onload = function (e){
        	// convertendo a o código da imagem em um objeto BLOB
        	// para facilitar o debug no browser
        	var blob = URL.createObjectURL( perfilPaciente._conterterParaBlob( e.target.result, e.target.result.split('data:')[1].split(';base64')[0] ) );

            img.attr('src', blob);
            $("#CropImgTarget").html( $(img)[0].outerHTML );

            // abrindo modal
            $('#CropModal').modal('show');

            // cropper
            perfilPaciente._initCropper();
		}

		// lendo arquivo como data url (base64)
        reader.readAsDataURL(file);
	},

	/**
	 * Esta função converte o base64 da imagem em blob
	 */
	_conterterParaBlob( dataURI, type )
	{
		// convert base64 to raw binary data held in a string
	    var byteString = atob(dataURI.split(',')[1]);

	    // separate out the mime component
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

	    // write the bytes of the string to an ArrayBuffer
	    var ab = new ArrayBuffer(byteString.length);
	    var ia = new Uint8Array(ab);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }

	    // write the ArrayBuffer to a blob, and you're done
	    var bb = new Blob([ab], { type: type });
	    return bb;
	},

	/**
	 * Esta função pega o BLOB do corte do avatar
	 * E define na imagem do usuário
	 * Para futuro upload
	 */
	_pegarCorteAvatar: function()
	{
		var dataURL = perfilPaciente.crop.cropper('getCroppedCanvas', {
				width: 700,
				height: 700
			}).getContext('2d').canvas.toDataURL();

		// convertendo a o código da imagem em um objeto BLOB
        // para facilitar o debug no browser
        var blob = URL.createObjectURL( perfilPaciente._conterterParaBlob( dataURL, dataURL.split('data:')[1].split(';base64')[0] ) );

		$("#avatarPacienteImg").attr('src', blob);

		$("#inputAvatarPaciente").val('');

		$("#uploadAvatarPaciente").val( dataURL );

			$.ajax({
				url : '/meu/perfil/trocar/avatar/paciente/'+id_fix,
				type: 'post',
				headers: { 'X-CSRF-Token' : token_fix},
				data: {
					_token: token_fix,
					uploadAvatarPaciente : dataURL,
					inputRemoverAvatar: 0 
				},
				success: function( data )
				{ 
				},
				error: function()
				{

				}
			});
	},

	/**
	 * Função para fazer o upload do formulário
	 */
	uploadAvatar: function( $obj )
	{
		// mostrando o ícone de carregamento
		perfilPaciente._atualizarCarregando( $obj, 'carregando' );

		$("#formImagemPaciente").submit();
	}
}

/**
 * Start na inicialização
 */
$(document).ready(function(){
	perfilPaciente._init();
});