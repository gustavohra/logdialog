/**
 * Página para controle do perfil de profissionais
 */
var perfilProfissional = {
	/**
	 * Esta função executa todos os incializáveis necessários
	 */
	_init: function()
	{
		// carregando o Bootstrap Input File
		this._initBootstrapFile();

		// bind modal de especialidade
		this._bindModalEspecialidade();
	},

	/**
	 * Esta função, controla a exibição do ícone de carregamento
	 */
	_atualizarCarregando: function( $obj, $tipo )
	{
		// apenas se for do tipo link
		if( $($obj).prop("tagName") == "A" && $tipo == 'carregando' )
		{
			// atualizando classe do item com o Font Awesome
			if( $($obj).find('i.fa-check-circle-o').length > 0 )
			{
				var $item = $($obj).find('i.fa-check-circle-o').eq(0);

				$item.removeClass('fa-check-circle-o');
				$item.addClass('fa-refresh fa-spin fa-fw');
			}
		}
		else if( $($obj).prop("tagName") == "A" && $tipo == 'completo' )
		{
			// atualizando classe do item com o Font Awesome
			if( $($obj).find('i.fa-refresh').length > 0 )
			{
				var $item = $($obj).find('i.fa-refresh').eq(0);

				$item.removeClass('fa-refresh fa-spin fa-fw');
				$item.addClass('fa-check-circle-o');
			}
		}
	},

	/**
	 * Função para processar a atualização dos dados cadastrais do perfil
	 */
	atualizarPerfil: function( $obj )
	{
		// validar form
		$("#formPerfilProfissional").validator('validate');

		// verificando se está válido
		if( $("#formPerfilProfissional").has('.has-error').length == 0 )
		{
			// mostrando o ícone de carregamento
			perfilProfissional._atualizarCarregando( $obj, 'carregando' );
			var novasenha 			= $("#novasenha").val();
			var conf_novasenha 		= $("#conf_novasenha").val();
			
			// verificando se o CPF informado já está sendo usado por outra pessoa
			$.ajax({
				url : '/validar/documento/profissional/cpf',
                cache: false,				
				type: 'get',
				headers: { 'X-CSRF-Token' : $('#_token').val()},
				data: {
					_token: $('meta[name=_token]').attr('content'),
					cpf : $.trim($('#cpf_profissional').val())
				},
				success: function( data )
				{
					// executar atualização
					$("#formPerfilProfissional").submit();
				},
				error: function()
				{
					alert('O CPF informado não pode ser utilizado.');

					// mostrando o ícone de carregamento
					perfilProfissional._atualizarCarregando( $obj, 'completo' );
				}
			});
		}
		else
		{
			// aqui caso esteja inválido
		}
	},
	atualizarCurriculo: function( $obj )
	{
		// validar form
		$("#formMeuCurriculo").validator('validate');

		// verificando se está válido
		if( $("#formMeuCurriculo").has('.has-error').length == 0 )
		{
			// mostrando o ícone de carregamento
			perfilProfissional._atualizarCarregando( $obj, 'carregando' );
 
			$.ajax({
				url : '/meu/perfil/atualizar/curriculo/'+$("#_id-curriculo").val(),
                cache: false,				
				type: 'post',
				headers: { 'X-CSRF-Token' : $('#_token').val()},
				data: {
					_token: $('meta[name=_token]').attr('content'),
					id: $("#_id-curriculo").val(),
					experiencia : $('#input_experiencia').val(),
					formacao : $('#input_formacao').val(),
					afiliacao : $('#input_afiliacao').val()
				},
				success: function( data )
				{ 
					$("#modalMeuCurriculo").modal('hide');
					window.location.reload();
				},
				error: function()
				{ 
					// mostrando o ícone de carregamento
					perfilProfissional._atualizarCarregando( $obj, 'completo' );
				}
			});
		}
		else
		{
			// aqui caso esteja inválido
		}
	},
	atualizarValores: function( $obj )
	{
		// validar form
		$("#formAtualizarValores").validator('validate');

		// verificando se está válido
		if( $("#formAtualizarValores").has('.has-error').length == 0 )
		{ 
			// mostrando o ícone de carregamento
			perfilProfissional._atualizarCarregando( $obj, 'carregando' );
 			var dados 		= $( "#formAtualizarValores" ).serializeArray();   
			    dados 		= dados.concat(
			            jQuery('#formAtualizarValores input[type=checkbox]:not(:checked)').map(
			                    function() {
			                        return {"name": this.name, "value": "PRIVADO"}
			                    }).get()
		    );
			$.ajax({
				url : '/meu/perfil/atualizar/valores/'+$("#_id-valores").val(),
                cache: false,				
				type: 'post',
				headers: { 'X-CSRF-Token' : $('#_token-valores').val()},
				data: dados, 
				success: function( data )
				{ 
					$("#modalValores").modal('hide');
					window.location.reload();
				},
				error: function()
				{ 
					// mostrando o ícone de carregamento
					perfilProfissional._atualizarCarregando( $obj, 'completo' );
				}
			});
		}
		else
		{
			// aqui caso esteja inválido
		}
	},

	/**
	 * Esta função executa os procedimentos de validação de senha (caso necessário)
	 * E submete o formulário ao servidor
	 */
	trocarSenha: function( $obj )
	{
		// validar form
		$("#formAlterarSenhaProfissional").validator('validate');

		// verificando se está válido
		if( $("#formAlterarSenhaProfissional").has('.has-error').length == 0 )
		{
			// mostrando o ícone de carregamento
			perfilProfissional._atualizarCarregando( $obj, 'carregando' );

			$("#formAlterarSenhaProfissional").submit();
		}
		else
		{
			// caso seja inválido
		}
	},

	/**
	 * Esta função ativa o bootstrap file input
	 */
	_initBootstrapFile: function()
	{
		$("#inputAvatarProfissional").fileinput({
			showCaption: false,
			showUpload: false,
			showRemove: false,
			showPreview: false,

			language: 'pt-BR',

			browseLabel: "Alterar",
			browseClass: "btn btn-modal btn-modaldefault",

			removeClass: "btn btn-modal btn-modalalert",
			removeLabel: "Remover",

			allowedFileExtensions: ["jpg", "jpeg", "png", "bmp"],

			maxFileSize: 2000, // 2MB = 2000 kb

			elErrorContainer: "#errorBlockAvatar"
		});

		// ao selecionar um arquivo
		$("#inputAvatarProfissional").on('change', function(event){
			// caso tenha algum conteúdo
			if( $.trim($(this).val()) != "" )
			{
				perfilProfissional._cortarAvatar( "inputAvatarProfissional" );

				// limpar flag de remover no formulário
				$("#inputRemoverAvatar").val(0);
			}
			else
			{
				perfilProfissional.removerAvatar();
			}
		});

		// ao clicar para remover o avatar
		$(".fileinput-remove-button").eq(0).on('click touchend', function(){
			perfilProfissional.removerAvatar();
		});
	},

	/**
	 * Esta função é executada sempre que o usuário clica no botão para remover o avatar
	 */
	removerAvatar: function()
	{
		// executando o reset do fileinput
		$("#inputAvatarProfissional").fileinput('clear');

		// definindo avatar padrão na imagem
		$('.editarperfil-avatar').find('img').eq(0).attr('src', '/images/avatar-padrao.jpg');

		// setando flag de remover no formulário
		$("#inputRemoverAvatar").val(1);

		// limpando string de upload
		$("#uploadAvatarProfissional").val('');
		$.ajax({
			url : '/meu/perfil/trocar/avatar/profissional/'+id_fix,
			type: 'post',
			headers: { 'X-CSRF-Token' : token_fix},
			data: {
				_token: token_fix,
				uploadAvatarProfissional : null,
				inputRemoverAvatar:1
			},
			success: function( data )
			{ 
			},
			error: function()
			{

			}
		});
	},

	/**
	 * Inicializando cropper
	 */
	_initCropper: function()
	{
		perfilProfissional.crop = $('#CropImgTargetProfissional img').cropper({
		  	aspectRatio: 1,

		  	viewMode: 2,

		  	minCropBoxWidth: 100,
		  	minCropBoxHeight: 100,

		  	minContainerWidth: 550,
		  	minContainerHeight: 550,
		  	maxContainerWidth: 550,
		  	maxContainerHeight: 550,

		  	zoomable: false,
		  	scalable: false,
		  	rotatable: false,
		  	movable: false
		});
	},

	/**
	 * Esta função inicializa a função para crop da imagem em formato redondo
	 */
	_cortarAvatar: function( id )
	{
		// criando img DOM
		var img = $('<img/>', {
            id: 'dynamicImgCrop'
        });

        // selecionando objeto no DOM
        var file = document.getElementById(id);
            file = file.files[0];

        // iniciando leitor de arquivo
        var reader = new FileReader();

		// Mostrando imagem
        reader.onload = function (e){
        	// convertendo a o código da imagem em um objeto BLOB
        	// para facilitar o debug no browser
        	var blob = URL.createObjectURL( perfilProfissional._conterterParaBlob( e.target.result, e.target.result.split('data:')[1].split(';base64')[0] ) );

            img.attr('src', blob);
            $("#CropImgTargetProfissional").html( $(img)[0].outerHTML );

            // abrindo modal
            $('#CropModalProfissional').modal('show');

            // cropper
            perfilProfissional._initCropper();
		}

		// lendo arquivo como data url (base64)
        reader.readAsDataURL(file);
	},

	/**
	 * Esta função converte o base64 da imagem em blob
	 */
	_conterterParaBlob( dataURI, type )
	{
		// convert base64 to raw binary data held in a string
	    var byteString = atob(dataURI.split(',')[1]);

	    // separate out the mime component
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

	    // write the bytes of the string to an ArrayBuffer
	    var ab = new ArrayBuffer(byteString.length);
	    var ia = new Uint8Array(ab);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }

	    // write the ArrayBuffer to a blob, and you're done
	    var bb = new Blob([ab], { type: type });
	    return bb;
	},

	/**
	 * Esta função pega o BLOB do corte do avatar
	 * E define na imagem do usuário
	 * Para futuro upload
	 */
	_pegarCorteAvatar: function()
	{
		var dataURL = perfilProfissional.crop.cropper('getCroppedCanvas', {
				width: 700,
				height: 700
			}).getContext('2d').canvas.toDataURL();

		// convertendo a o código da imagem em um objeto BLOB
        // para facilitar o debug no browser
        var blob = URL.createObjectURL( perfilProfissional._conterterParaBlob( dataURL, dataURL.split('data:')[1].split(';base64')[0] ) );

		$("#avatarPacienteImg").attr('src', blob);

		$("#inputAvatarProfissional").val('');

		$("#uploadAvatarProfissional").val( dataURL );


			$.ajax({
				url : '/meu/perfil/trocar/avatar/profissional/'+id_fix,
				type: 'post',
				headers: { 'X-CSRF-Token' : token_fix},
				data: {
					_token: token_fix,
					uploadAvatarProfissional : dataURL,
					inputRemoverAvatar: 0 
				},
				success: function( data )
				{ 
				},
				error: function()
				{

				}
			});
	},

	/**
	 * Função para fazer o upload do formulário
	 */
	uploadAvatar: function( $obj )
	{
		// mostrando o ícone de carregamento
		perfilProfissional._atualizarCarregando( $obj, 'carregando' );

		$("#formImagemProfissional").submit();
	},

	/**
	 * Ao exibir o modal de especialidades
	 */
	_bindModalEspecialidade: function()
	{
		$("#modalEspecialidades").on("show.bs.modal", function(evt){
			$("#arquivoEspecialidade").fileinput({
				showCaption: false,
				showUpload: false,
				showRemove: false,
				showPreview: false,

				language: 'pt-BR',

				browseLabel: "Procurar",
				browseClass: "btn btn-modal btn-modaldefault",

				allowedFileExtensions: ["jpg", "jpeg", "png", "gif", "svg", "tiff", "targa", "bmp", "pdf", "doc", "docx", "rtf"],

				maxFileSize: 5000, // 2MB = 2000 kb

				elErrorContainer: "#errorBlockEspecialidade"
			});

			// ao selecionar um arquivo
			$("#arquivoEspecialidade").on('change', function(event){
				var caminho = $.trim($(this).val());
				var nome = caminho.replace(/^.*[\\\/]/, '');

				$("#nomeSelecionado").val(nome);
			});
		});
	},

	/**
	 * Esta função valida o formulário e envia a solicitação de nova especialidade
	 */
	_novaEspecialidade: function( $obj )
	{
		// validar formulário
		$("#formNovaEspecialidade").validator('validate');

		// verificando se está válido
		if( $("#formNovaEspecialidade").has('.has-error').length == 0 )
		{
			// mostrando o ícone de carregamento
			perfilProfissional._atualizarCarregando( $obj, 'carregando' );
			var dadosForm = $( "#formNovaEspecialidade" ).serializeArray();
			    dadosForm = dadosForm.concat(
			            jQuery('#formNovaEspecialidade input[type=checkbox]:not(:checked)').map(
			                    function() {
			                        return {"name": this.name, "value": "PRIVADO"}
			                    }).get()
		    );
			// verificando se a especialidade já está em análise ou aprovada para este profissional
			$.ajax({
				url : '/validar/especialidade/profissional',
				type: 'get',
				headers: { 'X-CSRF-Token' : $('#_token-especialidade').val()},
				data: {
					_token: $('meta[name=_token]').attr('content'),
					especialidade : $.trim($('#nomeEspecialidade').val()),
					dados : dadosForm
				},
				success: function( data )
				{
					// executar atualização
					$("#formNovaEspecialidade").submit();
				},
				error: function()
				{
					alert('A especialidade informada não pode ser submetida, se você já tiver enviado anteriormente aguarde o processo de aprovação.');

					// mostrando o ícone de carregamento
					perfilProfissional._atualizarCarregando( $obj, 'completo' );
				}
			});
		}
		else
		{
			// aqui caso esteja inválido
		}
	}
}

/**
 * Start na inicialização
 */
$(document).ready(function(){
	perfilProfissional._init();
});



