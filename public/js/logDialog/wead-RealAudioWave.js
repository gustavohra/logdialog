$(document).ready(function(){
	var canvas = document.getElementById('audioWave');
	var ctx = canvas.getContext('2d');
	
	//canvas.width = document.body.clientWidth / 1.4;

	const CANVAS_HEIGHT = canvas.height;
	const CANVAS_WIDTH = canvas.width;

	window.audio = new Audio();
	audio.src = $("#audioPath").val();
	audio.volume = $("#audioVolume").val();
	audio.controls = true;
	//audio.autoplay = true;
	audio.loop = true;

	document.querySelector('#myaudio').appendChild(audio);

	// Check for non Web Audio API browsers.
	// or volume zero
	if (!window.AudioContext || $("#audioVolume").val() == 0)
	{
		// ocultando o canvas e edibindo a imagem padrão
		$("#audioWave").parent().css({"display":"none"});
		$("#imagemPadrao").parent().css({"display":"block"});

	  	document.querySelector('#myaudio').classList.toggle('show');
	  	return;
	}

	var context = new AudioContext();
	var analyser = context.createAnalyser();

	function rafCallback(time) {
	  	window.requestAnimationFrame(rafCallback, canvas);

	  	var freqByteData = new Uint8Array(analyser.frequencyBinCount);
	  	analyser.getByteFrequencyData(freqByteData); //analyser.getByteTimeDomainData(freqByteData);

	  	var SPACER_WIDTH = 5;
	  	var BAR_WIDTH = 3;
	  	var OFFSET = 100;
	  	var CUTOFF = 23;
	  	var numBars = Math.round(CANVAS_WIDTH / SPACER_WIDTH);

	  	ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
	  	ctx.fillStyle = '#00c690';
	  	ctx.lineCap = 'round';

	  	// Draw rectangle for each frequency bin.
	  	/*for (var i = 0; i < numBars / 2 - CUTOFF; ++i) {
	    	var magnitude = freqByteData[i + OFFSET];
	    	ctx.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
	  	}
	  	for (var i = numBars / 2 + CUTOFF; i < numBars; ++i) {
	    	var magnitude = freqByteData[i + OFFSET];
	    	ctx2.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
	  	}*/
	  	for (var i = 0; i < numBars; ++i) {
	    	var magnitude = freqByteData[i + OFFSET] * 0.25; // o float é para ajustar a altura das barras, mude de acordo com o tamanho do seu canvas
	    	ctx.fillRect(i * SPACER_WIDTH, CANVAS_HEIGHT, BAR_WIDTH, -magnitude);
	  	}
	}

	function onLoad(e) {
	  	var source = context.createMediaElementSource(audio);
	  	source.connect(analyser);
	  	analyser.connect(context.destination);

	  	audio.play();

	  	rafCallback();
	}

// Need window.onload to fire first. See crbug.com/112368.
window.addEventListener('load', onLoad, false);
});