/**
 * Este arquivo contém todas as funções para integração com o Facebook
 *
 * @author Adriano Maciel
 */
var appFB = {
	/**
	 * Solicitar login
	 * Fazer com que o usuário se autentique usando o facebook
	 *
	 * @author Adriano Maciel
	 */
	doLogin: function()
	{
		// chamando a API para verificar o status do login
		FB.getLoginStatus(function(response){
			// chamando uma função local, para tomar as devidas ações
			// dependendo o status do login
      		appFB.statusChangeCallback(response);
    	});
	},

	/**
	 * Processar a resposta do status de login
	 * @author Adriano Maciel
	 */
	statusChangeCallback: function(response)
	{
		console.log('statusChangeCallback');
	    console.log(response);
	    
		// The response object is returned with a status field that lets the
	    // app know the current login status of the person.
	    // Full docs on the response object can be found in the documentation
	    // for FB.getLoginStatus().
	    
	    if (response.status === 'connected')
	    {
	      	// Logged into your app and Facebook.
	      	// chamando função para testes das informações retornadas
	      	appFB.testAPI();
	    }
	    else if (response.status === 'not_authorized')
	    {
	      	// The person is logged into Facebook, but not your app.
	      	document.getElementById('status').innerHTML = 'Please log ' +
	        'into this app.';

	        // solicitando o login, para pedir permissão
	        appFB.callLogin();
	    }
	    else
	    {
	      	// The person is not logged into Facebook, so we're not sure if
	      	// they are logged into this app or not.
	      	document.getElementById('status').innerHTML = 'Please log ' +
	        'into Facebook.';

	        // solicitando o login, já que ainda não está logado
	        appFB.callLogin();
	    }
	},

	/**
	 * Função para testar as informações retornadas
	 * Após o login com sucesso
	 *
	 * @author Adriano Maciel
	 */
	testAPI: function()
	{
		console.log('Welcome!  Fetching your information.... ');
	    
	    FB.api('/me', function(response){
	      	console.log('Successful login for: ' + response.name);
	      	
	      	document.getElementById('status').innerHTML =
	        'Thanks for logging in, ' + response.name + '!';
	    });
	},

	/**
	 * Função para executar a autenticação com a API
	 *
	 * @author Adriano Maciel
	 */
	callLogin: function()
	{
		FB.login(function(response){
		   		console.log(response);
			},
			{
				scope: 'user_friends,email'
			}
		);
	}
}