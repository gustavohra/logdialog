/**
 * Este documento contém o controle de cadastro do usuário
 * na newsletter do sistema
 *
 * @author Adriano Maciel
 */
var newsletter = {
	// flag setada como true
	// se o sistema ainda estiver executando o cadastro
	executando : false,

	/**
	 * Esta função executa o cadastro do email na newsletter
	 */
	cadastrar : function(id)
	{
		// prosseguir apenas se não estiver executando o cadastro
		if( !newsletter.executando )
		{
			// recuperando o email
			var email = $.trim($('#' + id).val());

			// caso esteja vazio
			if( email.length == 0 )
			{
				// abrindo modal com aviso
				_logDialog_Util.modalAlert(
					"Newsletter", // titulo
					"<p>É obrigatório informar seu e-mail para cadastro na newsletter.</p>", // mensagem
					"Ok" // texto do botão
				);

				return false;
			}

			// caso seja inválido
			if( !_logDialog_Util.emailValido(email) )
			{
				// abrindo modal com aviso
				_logDialog_Util.modalAlert(
					"Newsletter", // titulo
					"<p>O e-mail informado é inválido.</p>", // mensagem
					"Ok" // texto do botão
				);

				return false;
			}

			// limpando o campo
			$("#" + id).val('');

			// flag de execução
			newsletter.executando = true;

			// prosseguir com o cadastro
			$.ajax({
				url  : '/newsletter/cadastro',
				type : 'post',
				headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
				data : {
					email  : email,
					_token : javascriptToken
				},
				success : function(data)
				{
					// liberando o cadastro
					newsletter.executando = false;

					// verificando se foi com sucesso
					if( data.sucesso )
					{
						// abrindo modal com aviso
						_logDialog_Util.modalAlert(
							"Newsletter", // titulo
							"<p>Seu e-mail foi cadastrado com sucesso na nossa newsletter.</p>", // mensagem
							"Ok" // texto do botão
						);
					}
					// no caso de erro
					else if( data.errors.email )
					{
						var msg = '';

						for( var i = 0; i < data.errors.email.length; i++ )
							msg += data.errors.email[i] + "<br />";

						// abrindo modal com aviso
						_logDialog_Util.modalAlert(
							"Newsletter", // titulo
							"<p>" + msg + "</p>", // mensagem
							"Ok" // texto do botão
						);
					}
				},
				error : function()
				{
					// liberando o cadastro
					newsletter.executando = false;

					// abrindo modal com aviso
					_logDialog_Util.modalAlert(
						"Newsletter", // titulo
						"<p>Houve um erro ao cadastrar seu e-mail, tente novamente em breve.</p>", // mensagem
						"Ok" // texto do botão
					);
				}
			});
		}

		return true;
	}
}


/**
 * Listener ao enviar o formulário
 */
$(document).ready(function(){
	$("#cadastroNewsletter").bind("submit", function(){
		newsletter.cadastrar('emailNewsletter');
		return false;
	});
});