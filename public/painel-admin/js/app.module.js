(function () {
  'use strict';

  angular.module('logDialogAdmin', [
    'ui.bootstrap',
    'ngAnimate',
    'ui.router',  // Controlar URL 
    'angular-loading-bar',
    'cub3.iziToast',
    'ngDialog',
    'ngFileUpload',
    'angularUtils.directives.dirPagination',
    'oitozero.ngSweetAlert',
    'anim-in-out', 
    'formly',
    'formlyBootstrap',
    'ui.tinymce',
    'cub3Filtros'
  ])
    .filter('unsafe', function($filter, $sce) {
      return function(text) {
        return  $sce.trustAsHtml(text);
      };
    }
  )

.config(function($sceProvider) {
    $sceProvider.enabled(false);
 })
  .config(function(formlyConfigProvider) {
    formlyConfigProvider.setType({
      name: 'tinymce',
      templateUrl: 'textarea-tinymce.html', 
      wrapper: ['bootstrapLabel']
    });
  })

  .run(['$rootScope', function ($rootScope) {
    $rootScope.voltar = function() {
        window.history.back();
    }
  }]);
})(); 
