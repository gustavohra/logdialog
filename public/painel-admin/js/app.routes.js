(function () {
  'use strict';

  angular.module('logDialogAdmin')
    // Configuração de rotas/states
    .config(function($stateProvider, $locationProvider, $urlRouterProvider) {
        $stateProvider

            /* Vantagens */
              .state('vantagens', {
                url: '/vantagens',
                cache: false,  
                templateUrl: 'vantagens.html',
                controller: 'VantagensCtrl as vm'
              })   
              .state('vantagem', {
                url: '/vantagem?id',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'VantagemCtrl as vm'                
              })
              .state('vantagemAdicionar', {
                url: '/vantagemAdicionar',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'VantagemAdicionarCtrl as vm'                
              })
              /* Sliders */
              .state('sliders', {
                url: '/sliders',
                cache: false,  
                templateUrl: 'sliders.html',
                controller: 'SlidersCtrl as vm'
              })   
              .state('slider', {
                url: '/slider?id',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'SliderCtrl as vm'                
              })
              .state('sliderAdicionar', {
                url: '/sliderAdicionar',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'SliderAdicionarCtrl as vm'                
              })
              /* Páginas */
              .state('paginas', {
                url: '/paginas',
                cache: false,  
                templateUrl: 'paginas.html',
                controller: 'PaginasCtrl as vm'
              })   
              .state('pagina', {
                url: '/pagina?id',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'PaginaCtrl as vm'                
              })
              .state('paginaAdicionar', {
                url: '/paginaAdicionar',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'PaginaAdicionarCtrl as vm'                
              })

              /* Profissionais */
              .state('profissionais', {
                url: '/profissionais',
                cache: false,  
                templateUrl: 'profissionais.html',
                controller: 'ProfissionaisCtrl as vm'
              })   
              .state('profissional', {
                url: '/profissional?id',
                cache:false, 
                templateUrl: 'profissional.html',
                controller: 'ProfissionalCtrl as vm'                
              }) 

              /* Pacientes */
              .state('pacientes', {
                url: '/pacientes',
                cache: false,  
                templateUrl: 'pacientes.html',
                controller: 'PacientesCtrl as vm'
              })   
              .state('paciente', {
                url: '/paciente?id',
                cache:false, 
                templateUrl: 'paciente.html',
                controller: 'PacienteCtrl as vm'                
              })
              .state('pacienteAdicionar', {
                url: '/pacienteAdicionar',
                cache:false, 
                templateUrl: 'formulario.html',
                controller: 'PacienteAdicionarCtrl as vm'                
              })

              /* Serviços Solicitados */
              .state('servicosSolicitados', {
                url: '/servicosSolicitados',
                cache: false,  
                templateUrl: 'servicos-solicitados.html',
                controller: 'ServicosSolicitadosCtrl as vm'
              })   
              .state('servicoSolicitado', {
                url: '/servicoSolicitado?id',
                cache:false, 
                templateUrl: 'servico-especializado.html',
                controller: 'ServicoSolicitadoCtrl as vm'                
              }) 

              /* Especialidades */
              .state('especialidades', {
                url: '/especialidades',
                cache: false,  
                templateUrl: 'especialidades.html',
                controller: 'EspecialidadesCtrl as vm'
              })   
              .state('especialidade', {
                url: '/especialidade?id',
                cache:false, 
                templateUrl: 'especialidade.html',
                controller: 'EspecialidadeCtrl as vm'                
              }) 

              .state('login', {
                url: '/login',
                cache: false,  
                templateUrl: 'vantagens.html',
                controller: 'VantagensCtrl as vm'
              })  
              /* Denúncias */
              .state('denuncias', {
                url: '/denuncias',
                cache: false,  
                templateUrl: 'denuncias.html',
                controller: 'DenunciasCtrl as vm'
              })    
            // Fallback para demais requisições
          $urlRouterProvider.otherwise('vantagens');
        $locationProvider.html5Mode(false);
    })

})();