(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('EspecialidadesCtrl', EspecialidadesCtrl);

  EspecialidadesCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert'];
  function EspecialidadesCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert) {  
    $scope.dados = [];
    $scope.acao = "";
    $scope.selecao = [];

    logDialogSvc.postRequest("getEspecialidades", null).then(function(resposta) {
      $scope.dados = resposta.dados;
    }); 
    $scope.realizarAcao = function() {
      switch($scope.acao) {
        case 'excluir':
          logDialogSvc.postRequest("")
          break; 
      }
    };

    $scope.bloquear = function(item, indice) {
        SweetAlert.swal({
        title: 'Bloquear Especialidade',
        text: 'Confirma o bloqueio do especialidade "'+item.especialidade+'"?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sim',
        closeOnConfirm: true,
      }, function(isConfirm){     

       if(isConfirm){ 
          logDialogSvc.postRequest("bloquearEspecialidade", {id: item.id}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Bloquear Especialização", "Bloqueio realizado com sucesso!");
              $scope.dados[indice].data_aprovado = null;
            }
            else
              cub3Toast.erro("Bloquear Especialização", "Ocorreu um problema, por favor, tente novamente.");
          })
        }
     });

    }
    $scope.excluir = function(item, indice) {
        SweetAlert.swal({
        title: 'Excluir Especialidade',
        text: 'Confirma a remoção do item "'+item.titulo+'"?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sim',
        closeOnConfirm: true,
      }, function(isConfirm){     

       if(isConfirm){ 
          logDialogSvc.postRequest("excluirEspecialidade", {id: item.id}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Excluir item", "Exclusão realizada com sucesso!");
              $scope.dados.splice(indice, 1);
            }
            else
              cub3Toast.erro("Excluir item", "Ocorreu um problema, por favor, tente novamente.");
          })
        }
     });

    }
  }

})();
(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('EspecialidadeCtrl', EspecialidadeCtrl);

  EspecialidadeCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert', 'Upload'];
  function EspecialidadeCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert, Upload) {  
    if($state.params.id == null){
      $state.go("Especializacoes");
      cub3Toast.err("Ocorreu um problema", "O id não é válido.");
    }
     var vm = this; 
     vm.dados = {};
     vm.model = {}; 
     vm.envioDeImagem = true;

    logDialogSvc.postRequest("getEspecialidade", {id: $state.params.id}).then(function(resposta) {
      if(resposta.dados != null)
        vm.model = resposta.dados;
      else {
        cub3Toast.erro("Ocorreu um problema", "O id não é válido.");
        $state.go("especialidades");
      }
    }); 

 
    $scope.desbloquear = function(item, indice) {
        SweetAlert.swal({
        title: 'Desbloquear Especialização',
        text: 'Confirma o desbloqueio da especialidade "'+item.especialidade+'"?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sim',
        closeOnConfirm: true,
      }, function(isConfirm){     

       if(isConfirm){ 
          logDialogSvc.postRequest("desbloquearEspecialidade", {id: item.id}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Desbloquear Especialização", "Desbloqueio realizado com sucesso!");
              $state.go("especialidades");
            }
            else
              cub3Toast.erro("Desbloquear Especialização", "Ocorreu um problema, por favor, tente novamente.");
          })
        }
     });

    }
  }

})();

 