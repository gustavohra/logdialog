(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('ServicosSolicitadosCtrl', ServicosSolicitadosCtrl);

  ServicosSolicitadosCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert'];
  function ServicosSolicitadosCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert) {  
    $scope.dados = [];
    $scope.acao = "";
    $scope.selecao = [];

    logDialogSvc.postRequest("getServicosSolicitados", null).then(function(resposta) {
      $scope.dados = resposta.dados;
    }); 
    $scope.realizarAcao = function() {
      switch($scope.acao) {
        case 'excluir':
          logDialogSvc.postRequest("")
          break; 
      }
    };

    $scope.bloquear = function(item, indice) {
        SweetAlert.swal({
        title: 'Bloquear Serviço',
        text: 'Confirma o bloqueio do serviço "'+servico+'"?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sim',
        closeOnConfirm: true,
      }, function(isConfirm){     

       if(isConfirm){ 
          logDialogSvc.postRequest("bloquearServicoSolicitado", {id: item.id}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Bloquear serviço", "Bloqueio realizado com sucesso!");
              $scope.dados[indice].data_modificacao = '';
            }
            else
              cub3Toast.erro("Bloquear serviço", "Ocorreu um problema, por favor, tente novamente.");
          })
        }
     });

    }
    $scope.excluir = function(item, indice) {
        SweetAlert.swal({
        title: 'Excluir ServicoSolicitado',
        text: 'Confirma a remoção do item "'+item.titulo+'"?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sim',
        closeOnConfirm: true,
      }, function(isConfirm){     

       if(isConfirm){ 
          logDialogSvc.postRequest("excluirServicoSolicitado", {id: item.id}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Excluir item", "Exclusão realizada com sucesso!");
              $scope.dados.splice(indice, 1);
            }
            else
              cub3Toast.erro("Excluir item", "Ocorreu um problema, por favor, tente novamente.");
          })
        }
     });

    }
  }

})();
(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('ServicoSolicitadoCtrl', ServicoSolicitadoCtrl);

  ServicoSolicitadoCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert', 'Upload'];
  function ServicoSolicitadoCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert, Upload) {  
    if($state.params.id == null){
      $state.go("servicosSolicitados");
      cub3Toast.err("Ocorreu um problema", "O id não é válido.");
    }
     var vm = this; 
     vm.dados = {};
     vm.model = {}; 
     vm.envioDeImagem = true;

    logDialogSvc.postRequest("getServicoSolicitado", {id: $state.params.id}).then(function(resposta) {
      if(resposta.dados != null)
        vm.model = resposta.dados;
      else {
        cub3Toast.erro("Ocorreu um problema", "O id não é válido.");
        $state.go("Servicos");
      }
    }); 

 
    $scope.desbloquear = function(item, indice) {
        SweetAlert.swal({
        title: 'Desbloquear Serviço',
        text: 'Confirma o desbloqueio do serviço "'+item.servico+'"?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Não',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Sim',
        closeOnConfirm: true,
      }, function(isConfirm){     

       if(isConfirm){ 
          logDialogSvc.postRequest("desbloquearServicoSolicitado", {id: item.id}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Desbloquear serviço", "Desbloqueio realizado com sucesso!");
              $state.go("servicosSolicitados");
            }
            else
              cub3Toast.erro("Desbloquear serviço", "Ocorreu um problema, por favor, tente novamente.");
          })
        }
     });

    }
  }

})();

 