(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('SlidersCtrl', SlidersCtrl);

  SlidersCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert'];
  function SlidersCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert) {  
  	$scope.dados = [];
  	$scope.acao = "";
  	$scope.selecao = [];

  	logDialogSvc.postRequest("getSliders", null).then(function(resposta) {
  		$scope.dados = resposta.dados;
  	}); 
  	$scope.realizarAcao = function() {
  		switch($scope.acao) {
  			case 'excluir':
  				logDialogSvc.postRequest("")
  				break; 
  		}
  	};
  	$scope.excluir = function(item, indice) {
  			SweetAlert.swal({
				title: 'Excluir Slider',
				text: 'Confirma a remoção do item "'+item.titulo+'"?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Sim',
				closeOnConfirm: true,
			}, function(isConfirm){     

			 if(isConfirm){	
		 			logDialogSvc.postRequest("excluirSlider", {id: item.id}).then(function(resposta) {
		 				if(resposta.resposta){
		 					cub3Toast.sucesso("Excluir item", "Exclusão realizada com sucesso!");
		 					$scope.dados.splice(indice, 1);
		 				}
		 				else
		 					cub3Toast.erro("Excluir item", "Ocorreu um problema, por favor, tente novamente.");
		 			})
		  	}
		 });

  	}
  }

})();

(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('SliderCtrl', SliderCtrl);

  SliderCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert', 'Upload'];
  function SliderCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert, Upload) {  
  	if($state.params.id == null){
  		$state.go("sliders");
  		cub3Toast.err("Ocorreu um problema", "O id não é válido.");
  	}
     var vm = this; 
     vm.dados = {};
     vm.model = {}; 
     vm.envioDeImagem = true;

  	logDialogSvc.postRequest("getSlider", {id: $state.params.id}).then(function(resposta) {
  		if(resposta.dados != null)
  			vm.model = resposta.dados;
  		else {
	  		cub3Toast.erro("Ocorreu um problema", "O id não é válido.");
	  		$state.go("sliders");
  		}
  	});
        vm.campos = [
        {
            key: 'titulo',
            type: 'input',
            className: 'col-xs-12',
            templateOptions: {
              type: 'text',
              label: 'Título ou breve descrição',
              placeholder: 'Título',
              addonLeft: {
                class: "ion-ios-chatbubble-outline"
              }
            }
          },  
          {
            key: 'url',
            type: 'input',
            className: 'col-xs-6',
            templateOptions: { 
              label: 'URL',
              placeholder: 'Link do botão ao clicar' 
            }
          },
          {
            key: 'botao_texto',
            type: 'input',
            className: 'col-xs-6',
            templateOptions: { 
              label: 'Texto do botão',
              placeholder: 'Texto do botão' 
            }

          }];

		$scope.uploadImagem = function(file){  
			if(file != undefined){
				Upload.base64DataUrl(file).then(function(resposta){ 
					vm.model.imagem = resposta; 
				})
				.catch(function(resposta) {
					console.log(resposta);
				}); 
			}
			else
				cub3Toast.erro("Ocorreu um problema", "Por favor, verifique o formato do arquivo enviado.");
		};
       $scope.enviar = function(frm) {
       	if(frm.$valid){
          if(vm.model.imagem == '' || vm.model.imagem == null)
            cub3Toast.erro("Ocorreu um problema", "Por favor, envie uma imagem.");
          else {
    		  	logDialogSvc.postRequest("setSlider", vm.model).then(function(resposta) {
    		  		if(resposta.resposta){
    		  			cub3Toast.sucesso("Alteração realizada", "A alteração foi realizada com sucesso.");
    		  			$state.go("sliders");
    		  		}
    		  		else
    					cub3Toast.erro("Ocorreu um problema", "Por favor, tente novamente.");
		  	     }); 
            } 
	  	}
	  	else
	  		cub3Toast.erro("Formulário inválido", "Por favor, preencha os campos obrigatórios.");
       };

  }

})();

(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('SliderAdicionarCtrl', SliderAdicionarCtrl);

  SliderAdicionarCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert', 'Upload'];
  function SliderAdicionarCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert, Upload) {  
 
     var vm = this; 
     vm.dados = {};
     vm.envioDeImagem = true;
     vm.model = {}; 
     vm.model.imagem = ""; 
        vm.campos = [
        {
            key: 'titulo',
            type: 'input',
            className: 'col-xs-12',
            templateOptions: {
              type: 'text',
              label: 'Título ou breve descrição',
              placeholder: 'Título',
              addonLeft: {
                class: "ion-ios-chatbubble-outline"
              }
            }
          },  
          {
            key: 'url',
            type: 'input',
            className: 'col-xs-6',
            templateOptions: { 
              label: 'URL',
              placeholder: 'Link do botão ao clicar' 
            }
          },
          {
            key: 'botao_texto',
            type: 'input',
            className: 'col-xs-6',
            templateOptions: { 
              label: 'Texto do botão',
              placeholder: 'Texto do botão' 
            }
          } ];

		$scope.uploadImagem = function(file){  
			if(file != undefined){
				Upload.base64DataUrl(file).then(function(resposta){ 
					vm.model.imagem = resposta; 
				})
				.catch(function(resposta) {
					console.log(resposta);
				}); 
			}
			else
				cub3Toast.erro("Ocorreu um problema", "Por favor, verifique o formato do arquivo enviado.");
		};
       $scope.enviar = function(frm) {
       	if(frm.$valid){
          if(vm.model.imagem == '' || vm.model.imagem == null)
            cub3Toast.erro("Ocorreu um problema", "Por favor, envie uma imagem.");
          else {
      		  	logDialogSvc.postRequest("inserirSlider", vm.model).then(function(resposta) {
      		  		if(resposta.resposta){
      		  			cub3Toast.sucesso("Inserção realizada", "A inserão foi realizada com sucesso.");
      		  			$state.go("sliders");
      		  		}
      		  		else
      					cub3Toast.erro("Ocorreu um problema", "Por favor, tente novamente.");
      		  	});
            } 
	  	}
	  	else
	  		cub3Toast.erro("Formulário inválido", "Por favor, preencha os campos obrigatórios.");
       };

  }

})();