(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .factory(
                    "transformRequestAsFormPost",
                    function() {
                        function transformRequest( data, getHeaders ) {
                            var headers = getHeaders();
                                headers[ "Content-type" ] = "application/x-www-form-urlencoded; charset=utf-8";
                            return( serializeData( data ) );
                        }
                        return( transformRequest );
                        function serializeData( data ) {
                            if ( ! angular.isObject( data ) ) {
                                return( ( data == null ) ? "" : data.toString() );
                            }
                            var buffer = [];
                            for ( var name in data ) {
                                if ( ! data.hasOwnProperty( name ) ) {
                                    continue;
                                }
                                var value = data[ name ];
                                buffer.push(
                                    encodeURIComponent( name ) +
                                    "=" +
                                    encodeURIComponent( ( value == null ) ? "" : value )
                                );
                            }
                            var source = buffer
                                .join( "&" )
                                .replace( /%20/g, "+" )
                            ;
                            return( source );
                        }
                    }
                )

        .filter("validarInformacao", function($sce) {
            return function(texto){
                return texto != null && texto != 'null' ? texto : "Sem informação";
            };
        })
        .factory('getJsonExterno', function($http) {
                    return {
                        get: function(urlApi, loadingBar) {
                            if(loadingBar == undefined)
                                loadingBar = {};
                            
                            return $http.get(urlApi, loadingBar).then(function(response) {
                                return response.data;
                            });
                        }
                    };
                })    
})();