(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('PaginasCtrl', PaginasCtrl);

  PaginasCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert'];
  function PaginasCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert) {  
  	$scope.dados = [];
  	$scope.acao = "";
  	$scope.selecao = [];

  	logDialogSvc.postRequest("getPaginas", null).then(function(resposta) {
  		$scope.dados = resposta.dados;
  	}); 
  	$scope.realizarAcao = function() {
  		switch($scope.acao) {
  			case 'excluir':
  				logDialogSvc.postRequest("")
  				break; 
  		}
  	}; 
    $scope.alterarUrl = function(item) {
          logDialogSvc.postRequest("setPaginaUrl", {id: item.id, url: item.url}).then(function(resposta) {
            if(resposta.resposta){
              cub3Toast.sucesso("Alteração da URL", "Alteração realizada com sucesso!");
              $scope.dados.splice(indice, 1);
            }
            else
              cub3Toast.erro("Alteração da URL", "Ocorreu um problema, por favor, tente novamente.");
          });
    };
  	$scope.excluir = function(item, indice) {
  			SweetAlert.swal({
				title: 'Excluir Pagina',
				text: 'Confirma a remoção do item "'+item.titulo+'"?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Sim',
				closeOnConfirm: true,
			}, function(isConfirm){     

			 if(isConfirm){	
		 			logDialogSvc.postRequest("excluirPagina", {id: item.id}).then(function(resposta) {
		 				if(resposta.resposta){
		 					cub3Toast.sucesso("Excluir item", "Exclusão realizada com sucesso!");
		 					$scope.dados.splice(indice, 1);
		 				}
		 				else
		 					cub3Toast.erro("Excluir item", "Ocorreu um problema, por favor, tente novamente.");
		 			});
		  	}
		 });

  	}
  }

})();

(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('PaginaCtrl', PaginaCtrl);

  PaginaCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert', 'Upload'];
  function PaginaCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert, Upload) {  
  	if($state.params.id == null){
  		$state.go("paginas");
  		cub3Toast.err("Ocorreu um problema", "O id não é válido.");
  	}
     var vm = this; 
     vm.dados = {};
     vm.model = {}; 
     vm.envioDeImagem = false;

  	logDialogSvc.postRequest("getPagina", {id: $state.params.id}).then(function(resposta) {
  		if(resposta.dados != null)
  			vm.model = resposta.dados;
  		else {
	  		cub3Toast.erro("Ocorreu um problema", "O id não é válido.");
	  		$state.go("paginas");
  		}
  	});
        vm.campos = [
        {
            key: 'titulo',
            type: 'input',
            className: 'col-xs-12',
            templateOptions: {
              type: 'text',
              label: 'Título ou breve descrição',
              placeholder: 'Título',
              addonLeft: {
                class: "ion-ios-chatbubble-outline"
              }
            }
          },  
          {
            key: 'conteudo',
            type: 'tinymce',
            className: 'col-xs-12',
            data:{                 tinymceOption: {   "height" : "200",  "font_formats": 'Tahoma=tahoma, arial, verdana;Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n', "media_live_embeds": true, "cleanup_on_startup": false, "trim_span_elements": false, "verify_html": false, "cleanup": false, "convert_urls": false, inline: false, skin: 'lightgray', theme: 'modern', paste_data_images: true, language: 'pt_BR', plugins : 'advlist autolink link image lists charmap print preview imagetools  media code paste ', menubar: false, image_advtab: false, toolbar1: 'bold italic fontselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | print preview media code paste'}},
            templateOptions: { 
              label: 'Conteudo',
              placeholder: 'Conteudo' 
            }
          }];

		$scope.uploadImagem = function(file){  
			if(file != undefined){
				Upload.base64DataUrl(file).then(function(resposta){ 
					vm.model.imagem = resposta; 
				})
				.catch(function(resposta) {
					console.log(resposta);
				}); 
			}
			else
				cub3Toast.erro("Ocorreu um problema", "Por favor, verifique o formato do arquivo enviado.");
		};
       $scope.enviar = function(frm) {
       	if(frm.$valid){
		  	logDialogSvc.postRequest("setPagina", vm.model).then(function(resposta) {
		  		if(resposta.resposta){
		  			cub3Toast.sucesso("Alteração realizada", "A alteração foi realizada com sucesso.");
		  			$state.go("paginas");
		  		}
		  		else
					cub3Toast.erro("Ocorreu um problema", "Por favor, tente novamente.");
		  	}); 
	  	}
	  	else
	  		cub3Toast.erro("Formulário inválido", "Por favor, preencha os campos obrigatórios.");
       };

  }

})();

(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('PaginaAdicionarCtrl', PaginaAdicionarCtrl);

  PaginaAdicionarCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert', 'Upload'];
  function PaginaAdicionarCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert, Upload) {  
 
     var vm = this; 
     vm.dados = {};
     vm.envioDeImagem = false;
     vm.model = {}; 
     vm.model.imagem = ""; 
        vm.campos = [
        {
            key: 'titulo',
            type: 'input',
            className: 'col-xs-12',
            templateOptions: {
              type: 'text',
              label: 'Título ou breve descrição',
              placeholder: 'Título',
              addonLeft: {
                class: "ion-ios-chatbubble-outline"
              }
            }
          },  
          {
            key: 'conteudo',
            type: 'tinymce',
            className: 'col-xs-12',
            data:{                 tinymceOption: {   "height" : "200",  "font_formats": 'Tahoma=tahoma, arial, verdana;Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n', "media_live_embeds": true, "cleanup_on_startup": false, "trim_span_elements": false, "verify_html": false, "cleanup": false, "convert_urls": false, inline: false, skin: 'lightgray', theme: 'modern', paste_data_images: true, language: 'pt_BR', plugins : 'advlist autolink link image lists charmap print preview imagetools  media code paste ', menubar: false, image_advtab: false, toolbar1: 'bold italic fontselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages | print preview media code paste'}},
            templateOptions: { 
              label: 'Conteudo',
              placeholder: 'Conteudo' 
            }
          } ];

		$scope.uploadImagem = function(file){  
			if(file != undefined){
				Upload.base64DataUrl(file).then(function(resposta){ 
					vm.model.imagem = resposta; 
				})
				.catch(function(resposta) {
					console.log(resposta);
				}); 
			}
			else
				cub3Toast.erro("Ocorreu um problema", "Por favor, verifique o formato do arquivo enviado.");
		};
       $scope.enviar = function(frm) {
       	if(frm.$valid){
		  	logDialogSvc.postRequest("inserirPagina", vm.model).then(function(resposta) {
		  		if(resposta.resposta){
		  			cub3Toast.sucesso("Inserção realizada", "A inserão foi realizada com sucesso.");
		  			$state.go("paginas");
		  		}
		  		else
					cub3Toast.erro("Ocorreu um problema", "Por favor, tente novamente.");
		  	}); 
	  	}
	  	else
	  		cub3Toast.erro("Formulário inválido", "Por favor, preencha os campos obrigatórios.");
       };

  }

})();