/**
 * Requisição padrão
 */
(function(angular, undefined) {
angular
	.module('logDialogAdmin')

	.service('logDialogSvc', [
		'$http',
		'$q', 
		'transformRequestAsFormPost',  
		function ($http, $q, transformRequestAsFormPost) {
			'use strict';   
 			var urlWebrequest 	= URL_API+"api";

		    function post(metodo, helper, dados) {
		      var call;
		      var deferred = $q.defer();
		      var dados = {
		        'dados'  : angular.toJson(dados),
		        'metodo' : metodo,
		        'helper' : helper, 
		        'authKey' : AUTH_KEY
		      };
		      call = montarRequisicao(dados);

		      call.then(function (data) {
		        deferred.resolve(data.data);
		      }).catch(function () {
		        deferred.reject(arguments);
		      });

		       return deferred.promise;
		    }
		    function postRequest(metodo, dados) {
		      var call;
		      var deferred = $q.defer();
		      var dadosPost = { 
		        'metodo' : metodo, 
		        'authKey' : AUTH_KEY
		      };
		      angular.forEach(dados, function(value, key) {
		      	dadosPost[key] = value;
		      });
		      call = montarRequisicao(dadosPost);

		      call.then(function (data) {
		        deferred.resolve(data.data);
		      }).catch(function () {
		        deferred.reject(arguments);
		      });

		       return deferred.promise;
		    }
			function montarRequisicao(dados) {
		        var requisicao = $http({
		                        method: "POST",
		                        url: urlWebrequest,
		                        transformRequest: transformRequestAsFormPost,
		                        headers: {
		                            'Content-Type': 'application/x-www-form-urlencoded'
		                        },
		                        data: dados
		        });
		        return requisicao;
			}

			return {
				post: post,
				postRequest, postRequest
			};
		}
	]) 

})(window.angular);


