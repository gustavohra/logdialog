(function () {
  'use strict';

  angular
    .module('logDialogAdmin')
    .controller('DenunciasCtrl', DenunciasCtrl);

  DenunciasCtrl.$inject = ['$scope', '$state', 'logDialogSvc', 'cub3Toast', '$rootScope', 'getJsonExterno',  '$http', 'ngDialog', '$q', 'SweetAlert'];
  function DenunciasCtrl($scope, $state, logDialogSvc, cub3Toast, $rootScope, getJsonExterno,  $http, ngDialog, $q, SweetAlert) {  
  	$scope.dados = [];
  	$scope.acao = "";
  	$scope.selecao = [];

  	logDialogSvc.postRequest("getDenuncias", null).then(function(resposta) {
  		$scope.dados = resposta.dados;
  	}); 
  	$scope.realizarAcao = function() {
  		switch($scope.acao) {
  			case 'excluir':
  				logDialogSvc.postRequest("")
  				break; 
  		}
  	};
  	$scope.excluir = function(item, indice) {
  			SweetAlert.swal({
				title: 'Excluir denúncia',
				text: 'Confirma a remoção do item "'+item.titulo+'"?',
				type: 'warning',
				showCancelButton: true,
				cancelButtonText: 'Não',
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Sim',
				closeOnConfirm: true,
			}, function(isConfirm){     

			 if(isConfirm){	
		 			logDialogSvc.postRequest("excluirDenuncia", {id: item.id}).then(function(resposta) {
		 				if(resposta.resposta){
		 					cub3Toast.sucesso("Excluir item", "Exclusão realizada com sucesso!");
		 					$scope.dados.splice(indice, 1);
		 				}
		 				else
		 					cub3Toast.erro("Excluir item", "Ocorreu um problema, por favor, tente novamente.");
		 			})
		  	}
		 });

  	}
  }

})();
