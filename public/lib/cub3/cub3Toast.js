/**
@fileOverview

@toc

*/

(function (root, factory) {
	"use strict";

	/*global define*/
	if (typeof define === 'function' && define.amd) {
		define(['angular', 'izitoast'], factory);  // AMD
	} else if (typeof module === 'object' && module.exports) {
		module.exports = factory(require('angular'), require('izitoast')); // Node
	} else {
		factory(root.angular, root.iziToast);					// Browser
	}

}(this, function (angular, iziToast) {
	"use strict";

	angular.module('cub3.iziToast', [])
		.factory('cub3Toast', [ '$rootScope', function ( $rootScope ) {
			//public methods
			var self = {

				iziToast: function ( arg1, arg2, arg3 ) {
					$rootScope.$evalAsync(function(){
						if( typeof(arg2) === 'function' ) {
							iziToast( arg1, function(isConfirm){
								$rootScope.$evalAsync( function(){
									arg2(isConfirm);
								});
							}, arg3 );
						} else {
							iziToast( arg1, arg2, arg3 );
						}
					});
				},
				simples: function(titulo, mensagem) {
					$rootScope.$evalAsync(function(){
						iziToast.show({
						    title: titulo, 
						    position: 'bottomCenter',
						    message: mensagem
						});
					});
				},
				sucesso: function(titulo, mensagem) {
					$rootScope.$evalAsync(function(){
						iziToast.show({
						    title: titulo,
						    icon: 'ion-ios-checkmark-outline',
						    iconColor: 'green',
						    color: 'green',
						    position: 'bottomCenter',
						    message: mensagem
						});
					});
				},
				alerta: function(titulo, mensagem) {
					$rootScope.$evalAsync(function(){
						iziToast.show({
						    title: titulo,
						    icon: 'ion-alert',
						    iconColor: 'orange',
						    color: 'orange',
						    position: 'bottomCenter',
						    message: mensagem
						});
					});
				},
				erro: function(titulo, mensagem) {
					$rootScope.$evalAsync(function(){
						iziToast.show({
						    title: titulo,
						    icon: 'ion-ios-close-outline',
						    iconColor: 'red',
						    color: 'red',
						    position: 'bottomCenter',
						    message: mensagem
						});
					});
				}			
			};

			return self;
		}]);
}));


