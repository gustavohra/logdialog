    /*
    * Plugin cub3Filtros
    * [Descrição]: 
    * [Comentários]: 
    * 
    * @author gustavoaguiar
    * @package cub3/cub3-filtros/cub3Filtros.js
    * @param 
    * @return 
    */


(function() {
    "use strict";

    var mod = angular.module("cub3Filtros", []);
      
  mod.constant('$base64', (function() {

        var PADCHAR = '=';

        var ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

        function getbyte64(s,i) {
            var idx = ALPHA.indexOf(s.charAt(i));
            if (idx == -1) {
                throw "Cannot decode base64";
            }
            return idx;
        }

        function decode(s) {
            // convert to string
            s = "" + s;
            var pads, i, b10;
            var imax = s.length;
            if (imax == 0) {
                return s;
            }

            if (imax % 4 != 0) {
                throw "Cannot decode base64";
            }

            pads = 0;
            if (s.charAt(imax -1) == PADCHAR) {
                pads = 1;
                if (s.charAt(imax -2) == PADCHAR) {
                    pads = 2;
                }
                // either way, we want to ignore this last block
                imax -= 4;
            }

            var x = [];
            for (i = 0; i < imax; i += 4) {
                b10 = (getbyte64(s,i) << 18) | (getbyte64(s,i+1) << 12) |
                    (getbyte64(s,i+2) << 6) | getbyte64(s,i+3);
                x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff, b10 & 0xff));
            }

            switch (pads) {
                case 1:
                    b10 = (getbyte64(s,i) << 18) | (getbyte64(s,i+1) << 12) | (getbyte64(s,i+2) << 6);
                    x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff));
                    break;
                case 2:
                    b10 = (getbyte64(s,i) << 18) | (getbyte64(s,i+1) << 12);
                    x.push(String.fromCharCode(b10 >> 16));
                    break;
            }
            return x.join('');
        }

        function getbyte(s,i) {
            var x = s.charCodeAt(i);
            if (x > 255) {
                throw "INVALID_CHARACTER_ERR: DOM Exception 5";
            }
            return x;
        }

        function encode(s) {
            if (arguments.length != 1) {
                throw "SyntaxError: Not enough arguments";
            }

            var i, b10;
            var x = [];

            // convert to string
            s = "" + s;

            var imax = s.length - s.length % 3;

            if (s.length == 0) {
                return s;
            }
            for (i = 0; i < imax; i += 3) {
                b10 = (getbyte(s,i) << 16) | (getbyte(s,i+1) << 8) | getbyte(s,i+2);
                x.push(ALPHA.charAt(b10 >> 18));
                x.push(ALPHA.charAt((b10 >> 12) & 0x3F));
                x.push(ALPHA.charAt((b10 >> 6) & 0x3f));
                x.push(ALPHA.charAt(b10 & 0x3f));
            }
            switch (s.length - imax) {
                case 1:
                    b10 = getbyte(s,i) << 16;
                    x.push(ALPHA.charAt(b10 >> 18) + ALPHA.charAt((b10 >> 12) & 0x3F) +
                        PADCHAR + PADCHAR);
                    break;
                case 2:
                    b10 = (getbyte(s,i) << 16) | (getbyte(s,i+1) << 8);
                    x.push(ALPHA.charAt(b10 >> 18) + ALPHA.charAt((b10 >> 12) & 0x3F) +
                        ALPHA.charAt((b10 >> 6) & 0x3f) + PADCHAR);
                    break;
            }
            return x.join('');
        }

        return {
            encode: encode,
            decode: decode
        };
    })());


    mod.filter("getLabel", function($sce) {
        return function(status){
            var retorno = "";
                switch(status)
                {
                    case "ATIVO":
                        retorno = '<span class="label label-success">'+status+'</span>';
                        break;
                    case "INATIVO":
                        retorno = '<span class="label label-danger">'+status+'</span>';
                        break;
                    case "EXCLUÍDO":
                        retorno = '<span class="label label-danger">'+status+'</span>';
                        break;
                    default:
                        retorno = '<span class="label label-primary">Sem informação</span>';
                        break;
                }
                return $sce.trustAsHtml(retorno);
        };
    });

    mod.filter("validarInformacao", function($sce) {
        return function(texto){
            return texto != null ? texto : "Sem informação";
        };
    });

    mod.filter('verificarTipoMidia', function ($sce) {
        return function (prmTipoArquivo) {
            if(prmTipoArquivo != null){
            var retorno = ""; 
            var tipoArquivo = prmTipoArquivo.split("/");
            var tipo        = tipoArquivo[0] != 'undefined' ? tipoArquivo[0] : tipoArquivo; 
            switch(tipo) {
                case "image":
                    retorno = "<i class='fa fa-lg fa-file-image-o color-swatch brand-primary'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'nenhum')+"</small>";
                    break;
                case "video":
                    retorno = "<i class='fa fa-lg fa-file-video-o color-swatch brand-success'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'nenhum')+"</small>";
                    break;
                case "application":
                    retorno = "<i class='fa fa-lg fa-file-archive-o color-swatch brand-info'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'nenhum')+"</small>";
                    break;
                case "audio":
                    retorno = "<i class='fa fa-lg fa-file-audio-o color-swatch brand-warning'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'nenhum')+"</small>";
                    break;
                case "text":
                    retorno = "<i class='fa fa-lg fa-file-code-o color-swatch brand-danger'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'nenhum')+"</small>";
                    break;
                case "imagem":
                    retorno = "<i class='fa fa-lg fa-file-image-o color-swatch brand-primary'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Imagem')+"</small>";
                    break;
                case "Video":
                    retorno = "<i class='fa fa-lg fa-file-video-o color-swatch brand-primary'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Vídeo')+"</small>";
                    break;
                case "Texto":
                    retorno = "<i class='fa fa-lg fa-newspaper-o color-swatch brand-primary'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Texto')+"</small>";
                    break;
                case "YOUTUBE":
                    retorno = "<i class='fa fa-lg fa-youtube color-swatch brand-danger'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Youtube')+"</small>";
                    break;
                default:
                    retorno = "<i class='fa fa-lg fa-file-archive-o color-swatch brand-danger'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'nenhum')+"</small>";
                    break;
            }
                return $sce.trustAsHtml(retorno);
        }
        else{
                    return $sce.trustAsHtml("<i class='fa fa-lg fa-file-archive-o color-swatch brand-danger'></i>"); 
        }
        };
    });

    mod.filter('verificarTipoMidiaImagem', function ($sce) {
        return function (prmTipoArquivo) {
            if(prmTipoArquivo != null){
            var retorno = ""; 
            var tipoArquivo = prmTipoArquivo.split("/");
            var tipo        = tipoArquivo[0] != 'undefined' ? tipoArquivo[0] : tipoArquivo; 
            switch(tipo) {
                case "image":
                    retorno = "<img src='assets/img/icones/midia/imagem.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Imagem')+"</small>";
                    break;
                case "video":
                    retorno = "<img src='assets/img/icones/midia/video.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Vídeo')+"</small>";
                    break;
                case "application":
                    retorno = "<img src='assets/img/icones/midia/programa.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Programa')+"</small>";
                    break;
                case "audio":
                    retorno = "<img src='assets/img/icones/midia/audio.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Aúdio')+"</small>";
                    break;
                case "text":
                    retorno = "<img src='assets/img/icones/midia/doc.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Texto')+"</small>";
                    break;
                case "imagem":
                    retorno = "<img src='assets/img/icones/midia/imagem.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Imagem')+"</small>";
                    break;
                case "Video":
                    retorno = "<img src='assets/img/icones/midia/video.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Vídeo')+"</small>";
                    break;
                case "Texto":
                    retorno = "<img src='assets/img/icones/midia/doc.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Texto')+"</small>";
                    break;
                case "YOUTUBE":
                    retorno = "<i class='fa fa-lg fa-youtube color-swatch brand-danger'></i><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Youtube')+"</small>";
                    break;
                default:
                    retorno = "<img src='assets/img/icones/midia/outros.png' style='height:30px;max-heigth:30px' /><br><small class='tipoArquivo'>"+(tipoArquivo[1] != null ? tipoArquivo[1] : 'Outros')+"</small>";
                    break;
            }
                return $sce.trustAsHtml(retorno);
        }
        else{
                    return $sce.trustAsHtml("<i class='fa fa-lg fa-file-archive-o color-swatch brand-danger'></i>"); 
        }
        };
    });
    mod.filter('verificarArray', function() {

    return function(array, auxKey, valor, keyRetorno)
    {
        var retorno = {};
        
        if(!angular.isUndefined(array) || !angular.isUndefined(auxKey) || !angular.isUndefined(valor)){
            if(!angular.isUndefined(keyRetorno)){
                angular.forEach(array, function(value, key) {
                  if(value[auxKey] == valor)  
                      retorno[keyRetorno] = value[keyRetorno];
                }, retorno);
            }else{
                angular.forEach(array, function(value, key) {
                  if(value[auxKey] == valor)  
                      retorno = array[key];
                }, retorno);
            }
            


            if(!angular.isUndefined(retorno))
                return retorno;
            else
                return "";
        }
        else
            return "";
        };
    });

    mod.filter('getBool', function() {

        return function (valor) {
            if(valor == 1 || valor == '1' || valor == 'true')
                return true;
            else if(valor == 0 || valor == '0' || valor == 'false')
                return false;
        };
    });

    mod.filter('timestampToDate', function () {
        return function (timestamp) {
            var date = new Date(timestamp * 1000);
            var dateObject = ('0' + date.getDate()).slice(-2) +'/'+ ('0' + (date.getMonth() + 1)).slice(-2) +'/'+ date.getFullYear();
            return dateObject;
        };
    });

    mod.filter('_uriseg', function($location) {
      return function(segment) {
        // Get URI and remove the domain base url global var
        var query = $location.absUrl().replace("http://localhost/previna","");
        // To obj
        var data = query.split("/");    
        // Return segment *segments are 1,2,3 keys are 0,1,2
        if(data[segment-1]) {
          return data[segment-1];
        }
        return false;
      }
    });

    mod.filter('formatarData', function($filter)
    {
     return function(input)
     {
      if(input == null){ return ""; } 
      var date = $filter('date')(new Date(input.replace(/-/g, '/')),'dd/MM/yyyy');
      return date;
     };
    });
    
    mod.filter('formatarDataPorExtenso', function($filter)
    {
     return function(input)
     {
      if(input == null){ return ""; } 
      var date = $filter('date')(new Date(input.replace(/-/g, '/')),'dd/MM/yyyy');
      return date;
     };
    });


    mod.filter('formatarHorario', function($filter)
    {
     return function(input)
     {
      if(input == null){ return ""; } 
      var date = $filter('date')(new Date(input.replace(/-/g, '/')),'HH:mm');
      return date;
     };
    });

    mod.filter('cifracub3', function($base64) {
        return function(valor)
        {
            return $base64.encode(valor);
        }
    });

    mod.filter('decifracub3', function($base64) {
        return function(valor)
        {
                var valorCodificado = decodeURIComponent(escape(valor));
                return $base64.decode(valorCodificado);
        }
    });
    mod.filter('cifracub3Url', function($base64) {
        return function(valor)
        {
            return encodeURIComponent($base64.encode(valor));
        }
    });
    mod.filter('unsafe', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]); 
    mod.filter('decodeHtml', ['$sce', function ($sce) {
        var pre = document.createElement('pre');
        var decodeHtmlEntity = function(str) {
          return str.replace(/&#(\d+);/g, function(match, dec) {
            return String.fromCharCode(dec);
          });
        }; 

        return function (content) {
            content = angular.isString(content) ?
                decodeHtmlEntity(content) :
                content; 

            return $sce.trustAsHtml(content);
        };
    }]);

    mod.filter('inverterNegativo', ["$filter", function ($filter) {       
      return function(valor){
        valor = String(valor);
        
            return valor.replace("-", ""); 
      };

        }]);
     mod.filter('prettyJson', [function () {
            return function (obj, pretty) {
                if (!obj) {
                    return '';
                }
                var json = angular.toJson(obj, pretty);
                if (!pretty) {
                    return json;
                }
                return json
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                        var cls;
                        if (/^"/.test(match)) {
                            cls = /:$/.test(match) ? 'key' : 'string';
                        } else if (/true|false/.test(match)) {
                            cls = 'boolean';
                        } else if (/null/.test(match)) {
                            cls = 'null';
                        } else {
                            cls = 'number';
                        }
                        return '<span class="json-' + cls + '">' + match + '</span>';
                    });
            };
        }]);

mod.directive('compileTemplate', function($compile, $parse){
    return {
        link: function(scope, element, attr){
            var parsed = $parse(attr.ngBindHtml);
            function getStringValue() { return (parsed(scope) || '').toString(); }

            scope.$watch(getStringValue, function() {
                $compile(element, null, -9999)(scope);
            });
        }         
    }
});


mod.filter('temperatura', function($filter) {
    return function(input, precision) {
        if (!precision) {
            precision = 1;
        }
        var numberFilter = $filter('number');
        return numberFilter(input, precision) + '\u00B0C';
    };
});
mod.filter('traduzirClima', function($base64) {
    return function(valor)
    {
        switch (valor) {
            case 'Clouds':
                 return 'Nublado';
            case 'Rainy':
                return 'Chuvoso';
            case 'Sunny':
                return 'Ensolarado';
            default:
                 
                break;
        }
    }
});

mod.filter('removerAspas', ["$filter", function ($filter) {       
  return function(valor){
    var aux = valor.replace('"', ''); 
    return aux.replace('"', '');
};
}]);

// Filtros de padrões Brasil 
mod.filter('cep', function () {
  return function (input) {
    var str = input + '';
    str = str.replace(/\D/g, '');
    str = str.replace(/^(\d{2})(\d{3})(\d)/, '$1.$2-$3');
    return str;
  };
}); 
mod.filter('cnpj', function () {
  return function (input) { 
    var str = input + '';
        str = str.replace(/\D/g, '');
        str = str.replace(/^(\d{2})(\d)/, '$1.$2');
        str = str.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
        str = str.replace(/\.(\d{3})(\d)/, '.$1/$2');
        str = str.replace(/(\d{4})(\d)/, '$1-$2');
    return str;
  };
}); 
mod.filter('cpf', function () {
  return function (input) {
    var str = input + '';
        str = str.replace(/\D/g, '');
        str = str.replace(/(\d{3})(\d)/, '$1.$2');
        str = str.replace(/(\d{3})(\d)/, '$1.$2');
        str = str.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
    return str;
  };
}); 
mod.filter('cpfcnpj', function () {
  return function (input) {

    if(input.length <= 11){
        var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/(\d{3})(\d)/, '$1.$2');
            str = str.replace(/(\d{3})(\d)/, '$1.$2');
            str = str.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        return str;        
    }
    else {
    var str = input + '';
        str = str.replace(/\D/g, '');
        str = str.replace(/^(\d{2})(\d)/, '$1.$2');
        str = str.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
        str = str.replace(/\.(\d{3})(\d)/, '.$1/$2');
        str = str.replace(/(\d{4})(\d)/, '$1-$2');
    return str;          
    }
  };
}); 
function formatReal(int) {
  var tmp = int.toString().indexOf('.') !== -1 ? int + '' : int + '.00';
  var res = tmp.replace('.', '');
  tmp = res.replace(',', '');
  var neg = false;
  if (tmp.indexOf('-') === 0) {
    neg = true;
    tmp = tmp.replace('-', '');
  }
  if (tmp.length === 1) {
    tmp = '0' + tmp;
  }
  tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
  if (tmp.length > 6) {
    tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');
  }
  if (tmp.length > 9) {
    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, '.$1.$2,$3');
  }
  if (tmp.length > 12) {
    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, '.$1.$2.$3,$4');
  }
  if (tmp.indexOf('.') === 0) {
    tmp = tmp.replace('.', '');
  }
  if (tmp.indexOf(',') === 0) {
    tmp = tmp.replace(',', '0,');
  }
  return neg ? '-' + tmp : tmp;
}
mod.filter('realbrasileiro', function () {
  return function (input) {
    return 'R$ ' + formatReal(input);
  };
}); 
mod.filter('tel', function () {
  return function (input) {
    var str = input + '';
    str = str.replace(/\D/g, '');
    if (str.length === 11) {
      str = str.replace(/^(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
    } else {
      str = str.replace(/^(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
    }
    return str;
  };
});
mod.filter('calcularBarraTarefa', function () {
    return function (tarefa) {
        var status = tarefa.ptaStatus;
        var now = 1000*60*60*24;
        var dataFinal = new Date(tarefa.praDataTermino);
        var dataInicio = new Date();
        var dataDiferenca = dataFinal.getTime() - dataInicio.getTime();

        //var diferenca = Math.round(100 - ((dataInicio - dataFinal) * 100 ) / now) + '%' ;
        var diferenca = Math.round( ((dataDiferenca/now) ));

        if(status == "EM ABERTO" || status == "CANCELADA")
        {
            if(diferenca == 100)
                return "width:"+diferenca+"%;background-color:#af1624";
            else if(diferenca == 0)
                return  "width:100%;background-color:#af1624";
            else
                return "width:"+diferenca+"%";
        }
        else if(status == "ENTREGUE")
        {
            return "width:100%;background-color:#84c124";
        }
    };
});
mod.filter('getUrlParametros', function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
          query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
          var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
          query_string[pair[0]] = arr;
        } else {
          query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
      } 
    return query_string;
});



mod.filter('numberFixedLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });

mod.filter('tratarEnderecoGoogle', function (endereco) {
    return endereco.adress_compoents.long_name;
});



})();